﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// NOTE: Used for having an editor list of lists for Unity
[System.Serializable]
public struct EditorBuildingAdvancedTypeLister
{
	public List<BuildingAdvancedType> list;
}

// NOTE: Used for having an editor list of lists for Unity
[System.Serializable]
public struct EditorMythTypeLister
{
	public List<MythType> list;
}

// NOTE: Used for revealing or unlocking a building tier
[System.Serializable]
public struct Prequisites
{
	public bool noPrerequisites; // NOTE: This used to make the editor stats clearer, instead of having to check all the arrays. This flag overrides the other behavior.
	public int trainedWorkersAtSelf; // NOTE: If this preqrequisite applies to a building, this means the workers only at that building.
	public ResourceSetComponent minimumResources; // NOTE: All of the resources minimums must be met. Each non-zero minimum presented as a separate row. "- 10000 gold". The intent of this is to require players a certain unrelated resource that's not spent
	public ResourceSetComponent minimumResourceGains; // NOTE: Same as above
													  // TODO: The design of this doesn't make sense. "Or later upgrade" feels unintuitive when there's intertwined upgrade trees, at least.
	public List<EditorBuildingAdvancedTypeLister> editorRequiredBuildings; // NOTE: For populating the below hash set
	public List<HashSet<BuildingAdvancedType>> requiredBuildings; // NOTE: One from each set in the list is required. Each list item presented as "- BuildingXYZ or later upgrade"
	public List<EditorMythTypeLister> editorRequiredMyths; // NOTE: For populating the below hash set
	public List<HashSet<MythType>> requiredMyths; // Note: One from each set in the list is required. Each list item presented as "- MythABC, MythGHI, or MythXYZ"
	public HashSet<BuildingAdvancedType> bannedBuildings; // NOTE: If any of the buildings is present, prerequisite failed "- No BuildingXYZ", don't use plural
	public HashSet<MythType> bannedMyths; // NOTE: If any of the buildings is present, prerequisite failed "- No BuildingXYZ", don't use plural
										  // TODO: There should be a quest prerequisite

	public ResourceSetComponent overridePayment; // NOTE: Conceptually, the user can reveal or unlock something by paying a one time sum (per... ascend, or something...)... The game needs to remember these unlocks, of course. This may just be paying "Spirit" to reveal things early.
}

[CreateAssetMenu(fileName = "BuildingStatsScriptableObject", menuName = "ScriptableObjects/BuildingStatsScriptableObject", order = 2)]
public class BuildingStatsScriptableObject : ScriptableObject
{
	public enum BuffAreaShapeType
	{
		Self = 0,
		Global = 10,
		DiamondWithRadius = 110,
		SquareWithRadius = 120,
		PlusWithRadius = 130,
		XWithRadius = 140,
		CustomSomething = 310 // TODO: Replace when you start defining custom ones
			// TODO: For shapes that change over time, I think it's preferred to define them later.
	}

	public enum BuildingCategoryType
	{
		Basic = 20,
		Advanced = 30, 
	}

	public static bool BuffAreaShapeTypeUsesCoords(BuffAreaShapeType buffAreaShapeType)
	{
		switch (buffAreaShapeType)
		{
			case BuffAreaShapeType.Self:
			case BuffAreaShapeType.Global:
				return false;
			case BuffAreaShapeType.DiamondWithRadius:
			case BuffAreaShapeType.SquareWithRadius:
			case BuffAreaShapeType.PlusWithRadius:
			case BuffAreaShapeType.XWithRadius:
			case BuffAreaShapeType.CustomSomething:
				return true;
			default:
				throw new System.Exception("Unexpected BuffAreaShapeType when determining if the shape uses coords.");
		}
	}

	public BuildingAdvancedType buildingAdvancedType;
	public BuildingBaseType impliedBuildingBaseType;

	// TODO: consider renaming this to Construction
	public ResourceSetComponent baseBuildOrTierUpgradeCost;
	public float baseBuildOrTierUpgradeTime;

	public ResourceSetComponent baseTrainingCost;
	public float baseTrainingTime;
	public double customTrainingCost0; // TODO: These are unused
	public double customTrainingCost1;
	public double customTrainingCost2;
	public double customTrainingCost3;
	public int baseFogSightRadius;

	// TODO: Consider refactoring these into a separate scriptable object for base types. No need for now.
	// TODO: Do these flags get rid of the need for buildingAdvancedType?
	public bool isFarm;
	public bool isLumberMill;
	public bool isTownHall;
	public bool isQuarry;
	public bool isMine;
	public bool isVariousFoodWoodStoneOreProduction;
	public bool isShrine;
	public bool isGuild;
	public BuildingCategoryType buildingCategoryType;

	public ResourceSetComponent baseResourceProductionCoefficients;

	// TODO: Assess if this note makes sense
	// NOTE: The game might break if there are two buff giver components of the same buff type. However, it's more conventient to use a list.
	// This is because systems to add/remove buffs may work on the assumption that it's ok to stop after they encounter a buff from a building for the first time. Or maybe this won't be a problem.
	public List<PrototypeBuffGiverAreaComponent> prototypeBuffGiverAreaComponents;

	public PrototypeRitualPickupComponent prototypeRitualPickupComponent;

	public Prequisites buildingTierRevealPrerequisites;
	public Prequisites buildingTierUnlockPreqrequisites;
}
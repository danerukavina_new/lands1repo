﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MythAsset", menuName = "ScriptableObjects/MythAssetsDataScriptableObject", order = 1)]
public class MythAssetsDataScriptableObject : ScriptableObject
{
	public MythType mythType;

	public Color mainColor;
	public Color borderColor;
	public Sprite mythIconSprite;

	public ResourceAmountComponent mythCost;
	public string mythName;
	public string mythDescription;
}

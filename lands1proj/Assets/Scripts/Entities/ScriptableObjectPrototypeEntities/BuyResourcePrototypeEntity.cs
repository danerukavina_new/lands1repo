﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BuyResourceType
{
	BuyFood = 20,
	BuyWood = 30,
	BuyFoodViaTrade = 120,
	BuyWoodViaTrade = 130,
	BuyStoneViaTrade = 140,
	BuyMetalViaTrade = 150
}

[CreateAssetMenu(fileName = "BuyResourcePrototypeEntity", menuName = "ScriptableObjects/BuyResourcePrototypeEntity", order = 3)]
public class BuyResourcePrototypeEntity : ScriptableObject
{
	// TODO: Consider adding the pieces of the button text here

	public BuyResourceType buyResourceType;
	public ResourceScalingAmountComponent baseResourceCost;
	public ResourceScalingAmountComponent baseResourceGain;
}

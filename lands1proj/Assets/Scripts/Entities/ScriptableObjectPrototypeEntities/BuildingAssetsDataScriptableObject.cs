﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BuildingAsset", menuName = "ScriptableObjects/BuildingAssetsDataScriptableObject", order = 1)]
public class BuildingAssetsDataScriptableObject : ScriptableObject
{
	public BuildingAdvancedType buildingAdvancedType;

	public Color mainColor;
	public Color borderColor;
	public Sprite buildingIconSprite;

	public string buildingName;
	public string buildingDescription;
	public string workerName;
	public string workerNamePlural;
	public bool hasTrainingButton;
	public string trainingSentence; // TODO: Not used since redesign
	public string productionDescription;
	public string additionalProductionDescription0;

	public string[] additionalTrainingSentences;

	public Sprite additionalBackgroundArtSprite;
}

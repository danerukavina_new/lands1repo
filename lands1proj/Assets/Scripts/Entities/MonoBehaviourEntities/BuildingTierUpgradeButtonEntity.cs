﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// TODO: Can this be unified further with Myth Research? Can this be unified with training?
public class BuildingTierUpgradeButtonEntity : MonoBehaviour
{
	// TODO: Should this have a building ScriptableObject, or just the type? Does it make sense to ha
	public BuildingAssetsDataScriptableObject buildingAssetsDataScriptableObject;
	public int correspondingTierUpgradePathIndex;

	// UI State
	public ResearchableUIComponent researchableUIComponent;
	public TextMeshProUGUI buildingNameText;

	// State
	public ResearchableStateComponent researchableStateComponent;
	public ResourceSetComponent baseResearchCost;

	public void ResearchButtonClick() { researchableUIComponent.researchButton.ButtonClick(); }
	public void CancelResearchButtonClick() { researchableUIComponent.cancelResearchButton.ButtonClick(); }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum MythType
{
	None = -1,
	FarmMyth0 = 1000,
	LumberMillMyth0 = 1010,
	LumberMillAndShrineMyth0 = 1020,
	QuarryMyth0 = 1030,
	TownHallMyth0 = 1040,
	RitualsAndWellsMyth0 = 1050,
	RitualsMyth0 = 1060,
	MineMyth0 = 1070,
	GuildMyth0 = 1080,
	FarmMyth1 = 1090,
	HeroMyth0 = 1100,
	FarmAndMineMyth0 = 1110,
	SpiritMyth0 = 1120,
	GodsMyth0 = 1130,
	Myth14 = 1140,
	Myth15 = 1150,
	Myth16 = 1160,
	Myth17 = 1170,
	Myth18 = 1180,
}

public enum MythSlotForResearchOrCantResearch
{
	CantResearch = -1,
	First = 1,
	Second = 2
}

public enum MythSlot
{
	None = -1,
	FirstMyth = 1, 
	SecondMyth = 2,
	FirstTraditionMyth = 11,
	SecondTraditionMyth = 12,
}

// TODO: This is an experiment to implement Myths just through the button. Compare to how Buildings are implemented
public class MythButtonEntity : MonoBehaviour
{
	// TODO: Should this have a myth ScriptableObject, or just the type?
	public MythAssetsDataScriptableObject mythAssetsDataScriptableObject; // TODO: Perhaps there's no reason for the MythButtonEntity to hold onto its entire prototypical scriptable object? Considering it copies the data?

	// UI State
	public ResearchableUIComponent researchableUIComponent;
	public Image mythCheckmark;

	// State
	public ResearchableStateComponent researchableStateComponent;
	public double baseResearchCost;
	public MythSlotForResearchOrCantResearch mythSlotForResearchOrCantResearch;

	public void ResearchButtonClick() { researchableUIComponent.researchButton.ButtonClick(); }
	public void CancelResearchButtonClick() { researchableUIComponent.cancelResearchButton.ButtonClick(); }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RitualPickupMenuPickupRowEntity : MonoBehaviour
{
	public bool isRitualPickupTypeRelated;
	public RitualPickupType ritualPickupType;
	public GameObject ritualPickupAnimationPanelsGridView;
	public TextMeshProUGUI alternateToGridViewText;
	public ResourceDisplayPanelEntity resourceDisplayPanelEntity;
	public TextMeshProUGUI resourceAmountDisplayText;
	public TextMeshProUGUI resourceGainsOverTimeDisplayText;
	public TextMeshProUGUI alternateToResourceText;
	public TextMeshProUGUI additionalTextOnTheRight;
}

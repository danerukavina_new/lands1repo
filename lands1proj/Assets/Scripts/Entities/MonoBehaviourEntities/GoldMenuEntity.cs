﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//public enum GoldMenuState
//{
//	InitialMenu = 20,
//	MenuWithTrade = 30,
//}

public enum GoldMenuButtonClickAction
{
	BuyFood = 20,
	BuyWood = 30,
	BuyTile = 40,
	OpenTradeScreen = 120,
	// Tooltip buttons
}

public class GoldMenuEntity : MonoBehaviour
{
	// public GameObject initialMenuPanel;
	// public GameObject menuWithTrade;
	
	public Button buyFoodButton;
	public Button buyWoodButton;
	public Button buyTileButton;
	public Button openTradeScreenButton;

	// State
	// public GoldMenuState goldMenuState;

	public bool buttonClicked;
	public GoldMenuButtonClickAction buttonClickAction;

	public void BuyFoodButtonClick()
	{
		buttonClickAction = GoldMenuButtonClickAction.BuyFood;
		buttonClicked = true;
	}

	public void BuyWoodButtonClick()
	{
		buttonClickAction = GoldMenuButtonClickAction.BuyWood;
		buttonClicked = true;
	}

	public void BuyTileButtonClick()
	{
		buttonClickAction = GoldMenuButtonClickAction.BuyTile;
		buttonClicked = true;
	}

	public void OpenTradeScreenButtonClick()
	{
		buttonClickAction = GoldMenuButtonClickAction.OpenTradeScreen;
		buttonClicked = true;
	}
}

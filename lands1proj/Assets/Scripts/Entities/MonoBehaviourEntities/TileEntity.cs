﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TileMenuState
{
	InitialMenu = 20,
	Build = 30,
	BuildAdvanced = 40,
	BuildHeroic = 50,
	BuildBorder = 60,
	BuiltOnTopOfNoMenu = 120
}

public class TileEntity : MonoBehaviour
{
	public BuildingEntity buildingEntity;

	public Canvas canvas;

	public GameObject initialMenuPanel;
	public GameObject buildMenuPanel;
	public GameObject buildAdvancedMenuPanel;
	public GameObject buildHeroicMenuPanel;
	public GameObject buildBorderMenuPanel;

	public GameObject generalCancelButton;
	
	public GameObject buildAdvancedButton;
	public GameObject buildHeroicButton;
	public GameObject buildBorderButton;

	// State
	public Vector2Int coord;
	public TileMenuState tileMenuState;

	public bool isDirtyClearOnUpdate;

	public bool buffEntitiesDirtyClearOnUpdate;
	public HashSet<BuffEntity> buffEntities;

	// TODO: IMMEDIATELY FIX - Make a fog clearing component

	// TODO: All the below bools should just be a candidateState enum
	public bool buildButtonClicked;
	public bool buildAdvancedButtonClicked;
	public bool buildHeroicButtonClicked;
	public bool buildBorderButtonClicked;
	public bool generalCancelButtonClicked;
	public bool gameEventDemandsReturnToInitialMenu;

	public bool buildBuildingButtonClicked;
	public BuildingAdvancedType buildBuildingButtonClickBuildingAdvancedType; // TODO: It appears this should be refactored to the base type - look at the BuildClickSystem. However, the more general case of building upgrades to a building, if implemented, should use advanced type... so who knows.

	public bool buildingBuiltOnTopOfThisTile; // TODO: Get rid of this, we can just check the buildingEntity?

	public void BuildButtonClick()
	{
		buildButtonClicked = true;
	}

	public void BuildAdvancedButtonClick()
	{
		buildAdvancedButtonClicked = true;
	}

	public void BuildHeroicButtonClick()
	{
		buildHeroicButtonClicked = true;
	}

	public void BuildBorderButtonClick()
	{
		buildBorderButtonClicked = true;
	}

	public void GeneralCancelButtonClick()
	{
		generalCancelButtonClicked = true;
	}

	// TODO: Refactor this terrible pattern for the buttons. The buttons should know what they are, determining their text, what they send on click, everything.

	// TODO: The build buttons should use the base type

	public void BuildFarmButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.Farm);
		buildBuildingButtonClicked = true;
	}

	public void BuildLumberMillButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.LumberMill);
		buildBuildingButtonClicked = true;
	}

	public void BuildTownHallButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.TownHall);
		buildBuildingButtonClicked = true;
	}

	public void BuildQuarryButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.Quarry);
		buildBuildingButtonClicked = true;
	}

	public void BuildMineButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.Mine);
		buildBuildingButtonClicked = true;
	}

	public void BuildWellButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.Well);
		buildBuildingButtonClicked = true;
	}

	public void BuildHouseButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.House);
		buildBuildingButtonClicked = true;
	}

	public void BuildShrineButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.Shrine);
		buildBuildingButtonClicked = true;
	}

	public void BuildWindmillButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.Windmill);
		buildBuildingButtonClicked = true;
	}

	public void BuildGuildButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.Guild);
		buildBuildingButtonClicked = true;
	}

	public void BuildTavernButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.Tavern);
		buildBuildingButtonClicked = true;
	}

	public void BuildTempleButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.Temple);
		buildBuildingButtonClicked = true;
	}

	public void BuildLaboratoryButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.Laboratory);
		buildBuildingButtonClicked = true;
	}

	public void BuildBlacksmithButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.Blacksmith);
		buildBuildingButtonClicked = true;
	}

	public void BuildInnButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.Inn);
		buildBuildingButtonClicked = true;
	}

	public void BuildTrainingHallButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.TrainingGrounds);
		buildBuildingButtonClicked = true;
	}

	public void BuildDungeonButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.Dungeon);
		buildBuildingButtonClicked = true;
	}

	public void BuildTradeRouteButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.TradeRoute);
		buildBuildingButtonClicked = true;
	}

	public void BuildShipyardButtonClick()
	{
		BuildBuildingButtonClick(BuildingAdvancedType.Shipyard);
		buildBuildingButtonClicked = true;
	}

	// The Unity OnClick delegate can't take some arbitrary enum... so no point in making this public and using it in editor
	void BuildBuildingButtonClick(BuildingAdvancedType buildingAdvancedType)
	{
		buildBuildingButtonClicked = true;
		buildBuildingButtonClickBuildingAdvancedType = buildingAdvancedType;
	}

	public override string ToString()
	{
		return "Tile: " + coord.x + ", " + coord.y;
	}
}

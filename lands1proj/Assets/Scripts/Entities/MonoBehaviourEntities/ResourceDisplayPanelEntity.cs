﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResourceDisplayPanelEntity : MonoBehaviour
{
    public Image resourceIcon; // TODO: Current intent is that this should just be set in the editor. So perhaps remove the variable?
	public TextMeshProUGUI resourceAmount;
	public TextMeshProUGUI resourceGainsOverTime;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpiritualityMenuEntity_Old : MonoBehaviour
{
	public enum SpiritualityMenuButtonClickAction
	{
		OpenRitualScreen = 20,
		OpenMythScreen = 30,
		OpenGodScreen = 40,
		OpenIdolScreen = 50,
		RepeatPrevoiusRitual = 120
		// Tooltip buttons
	}

	public Button openRitualScreenButton;
	public Button openMythScreenButton;
	public Button openGodScreenButton;
	public Button openIdolScreenButton;
	public Button repeatPrevoiusRitualButton;

	// State

	public bool buttonClicked;
	public SpiritualityMenuButtonClickAction buttonClickAction;

	public void OpenRitualScreenButtonClick()
	{
		buttonClickAction = SpiritualityMenuButtonClickAction.OpenRitualScreen;
		buttonClicked = true;
	}

	public void OpenMythScreenButtonClick()
	{
		buttonClickAction = SpiritualityMenuButtonClickAction.OpenMythScreen;
		buttonClicked = true;
	}

	public void OpenGodScreenButtonClick()
	{
		buttonClickAction = SpiritualityMenuButtonClickAction.OpenGodScreen;
		buttonClicked = true;
	}

	public void OpenIdolScreenButtonClick()
	{
		buttonClickAction = SpiritualityMenuButtonClickAction.OpenIdolScreen;
		buttonClicked = true;
	}

	public void RepeatPrevoiusRitualButtonClick()
	{
		buttonClickAction = SpiritualityMenuButtonClickAction.RepeatPrevoiusRitual;
		buttonClicked = true;
	}
}

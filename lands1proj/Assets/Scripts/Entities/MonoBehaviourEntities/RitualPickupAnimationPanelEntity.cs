﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// NOTE: This is highly similar to the RitualPickupAnimationEntity
// TODO: This is awfully named. When there's a more general approach for displaying... "things" like resources, bonuses, whatever... in panels, then rename this
public class RitualPickupAnimationPanelEntity : MonoBehaviour
{
	// public Canvas canvas;
	// NOTE: The current plan is for this to use the same animation controllers as the RitualPickupAnimationEntity
	public Animator ritualPickupPanelAnimationController;
	public TextMeshProUGUI valueText; // TODO: Everywhere else weuse TextMeshProUGUI and a canvas when displaying text. But for the BuffGainOrRemoveAnimation(s) we went canvasless. Let's try canvasless here and see how it goes.
}

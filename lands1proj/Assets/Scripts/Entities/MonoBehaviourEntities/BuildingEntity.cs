﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BuildingBaseType
{
	Farm = 120,
	LumberMill = 130,
	TownHall = 140,
	Quarry = 150,
	Mine = 160,
	Well = 170,
	House = 180,

	Shrine = 220,
	Windmill = 230,
	Guild = 240,
	Tavern = 250,
	Temple = 260,
	Laboratory = 270,

	Blacksmith = 320,
	Inn = 330,
	TrainingGrounds = 340,
	Dungeon = 350,

	TradeRoute = 420,
	Shipyard = 430,

	SpecialCaptureInProgress = 10120,

	SpecialSeaBorder = 10220,
	SpecialMountainPassBorder = 10230,
	SpecialMountainBorder = 10240
}

// TODO: Expand on this, right now it's needed for assets
public enum BuildingAdvancedType
{
	Farm = 120,
	LumberMill = 130,
	TownHall = 140,
	Quarry = 150,
	Mine = 160,
	Well = 170,
	House = 180,

	Shrine = 220,
	Windmill = 230,
	Guild = 240,
	Tavern = 250,
	Temple = 260,
	Laboratory = 270,

	Blacksmith = 320,
	Inn = 330,
	TrainingGrounds = 340,
	Dungeon = 350,

	TradeRoute = 420,
	Shipyard = 430,

	// TODO: Improve how these are stored in the system manager. I think it should be a ~triple list - each BaseType has a BaseBuilding and a list of paths which are tiers. This should ALSO replace the value holder implementation of the tier lists.
	// To further improve organization, don't store this in the SystemManager. Instead, store it in scriptable objects, one for each base type
	FarmA2 = 12020,
	FarmA3 = 12030,
	FarmA4 = 12040,

	FarmB2 = 12120,
	FarmB3 = 12130,
	FarmB4 = 12140,

	FarmC2 = 12220,
	FarmC3 = 12230,
	FarmC4 = 12240,

	LumberMillA2 = 13020,
	LumberMillA3 = 13030,
	LumberMillA4 = 13040,

	LumberMillB2 = 13120,
	LumberMillB3 = 13130,
	LumberMillB4 = 13140,

	HouseA2 = 18020,
	HouseA3 = 18030,
	HouseA4 = 18040,

	HouseB2 = 18120, // TODO: Design - Concern, making the alternate upgrade path switch from adding limit for basic buildings to for advanced buildings may be bad gameplay... suddenly the adjancent basic buildings lose the benefit...
	HouseB3 = 18130,
	HouseB4 = 18140,

	ShineA2 = 22020,
	ShineA3 = 22030,
	ShineA4 = 22040,
	ShineB2 = 22120,
	ShineB3 = 22130,
	ShineB4 = 22140,

	SpecialCaptureInProgress = 100120,

	SpecialSeaBorder = 100220,
	SpecialMountainPassBorder = 100230,
	SpecialMountainBorder = 100240,
}

public enum BuildingMenuState
{
	BuildInProgress = 10,
	InitialMenu = 20,
	Build = 30,
	BuildAdvanced = 40,
	BuildHeroic = 50,
	BuildBorder = 60
}

public enum BuildingButtonClickAction
{
	Train = 20,
	OpenTierUpgradeScreen = 25,
	BuildingDetailsTopRightLensClick = 30,
	BuildingLockingTopLeftLockClick = 40,
	TrainSpecial0 = 100,
	TrainSpecial1 = 110,
	TrainSpecial2 = 120,
	LockTraining = 130,
	DemolishBuilding = 140,
	HeroChooseGear = 150,
	HeroCaptureTile = 160,
	HeroLevelUp = 170,
	HeroAutoCombat = 180
}

// NOTE: If you ever get the idea to make a set of helper functions for this, ask yourself, can it be a property on the BuildingStatsScriptableObject? See the refactor from early May 2021 for getting rid of case statements

public class BuildingEntity : MonoBehaviour
{
	// Unity components
	public Canvas canvas;
	public BuildingAssetsObjectConnector buildingAssetsObjectConnector; // TODO: WTF is the point of this? Shouldn't the members of this class just be members of BuildingEntity? I guess it organizes the the Unity components in one clean place

	// State
	// TODO: Why are the assetdata and stats separate? Can they ever be separate? They're using the same enum now.
	public BuildingAssetsDataScriptableObject buildingAssetsDataScriptableObject;
	public BuildingStatsScriptableObject buildingStatsScriptableObject;
	public bool isDirtyFromBeingBuilt; // TODO: Currently this can't be in ClearOnUpdate mode since the SetInitialBuildingAssetsSystem runs before many other systems in the next update
	public TileEntity tileEntity;
	public BuildingMenuState buildingMenuState;

	public bool buildingButtonClicked;
	public BuildingButtonClickAction buildingButtonClickAction;

	// TODO: trained workers, flat production increase, percentage production increase... WorkersComponent, ProductionComponent... if we want to make it complicated
	public int trainedWorkers;
	public bool isAtMaxTrainedWorkersForTierAndThusMayTierUpgrade; // TODO: Should this be MaxTrainedWorkersForTier? Yes. Find where it's used
	public double extraWorkers; // NOTE: A double because buildings can have fractions of extra workers added by other buildings
	public int maxExtraWorkers; // TODO: This isn't getting updated when the building's buffs are updated or when a building's tier upgrade happens... but it doesn't cause gameplay issues at the time of writing
	public bool isAtMaxExtraWorkers; // TODO: This isn't getting updated when the building's buffs are updated or when a building's tier upgrade happens... but it doesn't cause gameplay issues at the time of writing
	public int finalWorkers; // TODO: Not all buildings have trained workers

	public HashSet<BuffEntity> applicableBuffEntities; // TODO: This is just nuts... using a BuffEntity -> (source) BuildingEntity dictionary, to match it to the Buff -> (source) BuildingEntity -> BuffEntity dictionary on the tile seems soooo contrived
	public List<BuffEntity> addedApplicableBuffEntitiesQueue;
	public List<BuffEntity> removedApplicableBuffEntitiesQueue;
	public bool isInBuffAnimationCreationDelayFromLastDequeue;
	public float timeOfLastDequeueFromBuffAddRemoveQueue;
	// NOTE: This is a direct approach for housing these stats. Consider them the peers of trainedWorkers
	public double flatProductionIncrease;
	public double percentageProductionIncrease;
	public double extraWorkersOverTime;
	public double extraWorkersThisUpdate;
	public double additionalExtraWorkerLimit;
	public float trainingSpeedMultiplier;
	public double trainingCheapnessMultiplier; // TODO: Just like ResourceBuffSetComponent, how do we accumulate these? Addition? That may be incorrect, since different types will multiply... Does a 0d here mean no multiplier?

	public int trainedWorkersClearOnUpdate; // NOTE: This is only used in conjunction with "globalBuffDataEntity.playerGlobalBuffSetComponent.bonusFarmWorkersPerMineWorkerTrained"
	public bool isTraining;
	public float trainingStartTime;
	public bool isBuildingOrTierUpgrading;
	public float buildingOrUpradingStartTime;
	public int tierIndex;

	public ResourceSetComponent finalTrainingCost;
	public float finalTrainingTime;
	public double customTrainingCost0;
	public double customTrainingCost1;
	public double customTrainingCost2;
	public double customTrainingCost3;
	public bool canTrain;
	public bool startTraining;
	public bool openBuildingTierUpgradeMenu;
	public int chosenBuildingTierUpgradePathIndex;
	public bool startUpgradingBuildingTier;
	public int startBuildingTierUpgradePathIndex;
	public int finalTierUpgradePathIndex;
	public bool tierUpgradeRequestIsFromPlayerInteraction; // NOTE: If a building is being upgraded by the automatic process, instead of a player interaction, then don't close the currently open upgrade menu. This assumes strongly that the currently open upgrade menu is the only upgrade interaction.
	public List<BuildingAdvancedType?> nextBuildingTiers; // NOTE: Stores what the next tier upgrades will be, and the lists that follow it have matching indexing. This should not be allocated, it always points to ValueHolder arrays
	public bool anyTierUpgradeIsAvailable; // NOTE: Effectively can mean "should nextBuildingTiers be used or is it invalid, along with most of the other arrays"
	public List<float> finalBuildOrTierUpgradeTimes;
	public List<ResourceSetComponent> finalBuildOrTierUpgradeCosts;
	public List<bool> canStartNextBuildingTierUpgrades;
	public bool canAlsoAutoTierUpgrade;

	public ResourceSetComponent buildingResourceSetGainsOverTimeComponent;
	public ResourceBuffSetComponent buildingResourceBuffSetComponent;
	public int finalFogSightRadius; // TODO: IMMEDIATELY FIX - Make this a component

	// TODO: Concern - if building A has buffs L, M, and bit's upgrade B has buffs M, N, O. Do we need to remove L specifically? And track diffs between A and B? Or is it better to just clear all that building's buffs and then add them. (definitely the latter)

	public bool buffGiverComponentsAddedClearOnUpdate; // Something like this... should be tracked on first train and on building upgrade. Not important if add/update buff gets merged
	public List<BuffGiverComponent> buffGiverComponents; // TODO: Generally the copying being done to the Buildings... to have them receive "BuffEntities" seems wrong. They should just point to these!
	public List<BuffGiverComponent> removedBuffGiverComponentsClearOnUpdate;
	public RitualPickupEntity ritualPickupEntity;

	// UI State
	public bool workersOrTierIsDirtyClearOnUpdate; // TODO: Should the tier variable be separate? Seems like more extra work than it's worth

	public bool showOrRefreshProductionDisplayPanel;
	public float startedShowingProductionDisplayPanelTime;
	public float lastRefreshOfProductionDisplayPanelTime;

	public List<BuffGainOrRemoveAnimationEntity> buffGainOrRemoveAnimationEntities; // NOTE: It's helpful to have this per building rather than in a global list, for now, because it's easier to "stagger" them appearing on the building. Certainly there are advantages to a global list.

	// NOTE: RitualPickupAnimationEntities are created when the building changes.
	public RitualPickupAnimationEntity ritualPickupAnimationEntity;
	// NOTE: RitualPickupAnimationPanelEntities are created when the ritual changes.
	public RitualPickupAnimationPanelEntity ritualPickupAnimationPanelEntity;

	public void TrainClick()
	{
		buildingButtonClickAction = BuildingButtonClickAction.Train;
		buildingButtonClicked = true;
	}

	public void OpenTierUpgradeScreenClick()
	{
		buildingButtonClickAction = BuildingButtonClickAction.OpenTierUpgradeScreen;
		Debug.Log("Open Tier Upgrade Screen Click");
		buildingButtonClicked = true;
	}

	public void BuildingDetailsTopRightLensClick()
	{
		buildingButtonClickAction = BuildingButtonClickAction.BuildingDetailsTopRightLensClick;
		Debug.Log("Details");
		buildingButtonClicked = true;
	}

	public void BuildingLockingTopLeftLockClick()
	{
		buildingButtonClickAction = BuildingButtonClickAction.BuildingLockingTopLeftLockClick;
		Debug.Log("Lock");
		buildingButtonClicked = true;
	}

	public void TrainSpecial0Click()
	{
		buildingButtonClickAction = BuildingButtonClickAction.TrainSpecial0;
		buildingButtonClicked = true;
	}

	public void TrainSpecial1Click()
	{
		buildingButtonClickAction = BuildingButtonClickAction.TrainSpecial1;
		buildingButtonClicked = true;
	}

	public void TrainSpecial2Click()
	{
		buildingButtonClickAction = BuildingButtonClickAction.TrainSpecial2;
		buildingButtonClicked = true;
	}

	public void LockTrainingClick()
	{
		buildingButtonClickAction = BuildingButtonClickAction.LockTraining;
		buildingButtonClicked = true;
	}

	// TODO: A lot of these may be operated from a modal building menu. Does it make sense to expose them this way?
	public void DemolishBuildingClick()
	{
		buildingButtonClickAction = BuildingButtonClickAction.DemolishBuilding;
		buildingButtonClicked = true;
	}

	public void HeroChooseGearClick()
	{
		buildingButtonClickAction = BuildingButtonClickAction.HeroChooseGear;
		buildingButtonClicked = true;
	}

	public void HeroCaptureTileClick()
	{
		buildingButtonClickAction = BuildingButtonClickAction.HeroCaptureTile;
		buildingButtonClicked = true;
	}

	public void HeroLevelUpClick()
	{
		buildingButtonClickAction = BuildingButtonClickAction.HeroLevelUp;
		buildingButtonClicked = true;
	}

	public void HeroAutoCombatClick()
	{
		buildingButtonClickAction = BuildingButtonClickAction.HeroAutoCombat;
		buildingButtonClicked = true;
	}
}

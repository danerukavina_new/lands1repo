﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScreenEntity : MonoBehaviour
{
	// TODO: This is super hacky, but only matters for like 2 screens
	public MenuEntity menuInScreenEntity0; // NOTE: If the screen has multiple menus, then this is typically the initial menu, where the player chooses something
	public MenuEntity menuInScreenEntity1;

	// State
	public bool closeScreenClicked;

	public void CloseScreenClick()
	{
		closeScreenClicked = true;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BuffGainOrRemoveAnimationType
{
	ResourceOnly = 20,
	SymbolAndResource = 30,
	SymbolAndTwoResources = 40,
	SymbolAndThreeOreMoreResources = 50
}

public enum BuffGainOrRemoveAnimationAssetType
{
	None = -1, // TODO: Only used when there's no symbol needed, which only happens for removal... consider getting rid of it

	GeneralRemoveIcon = 100,

	DoublePlusIcon = 110,
	PercentIcon = 120,
	DoubleChevronDownIcon = 130,
	DoubleChevronRightIcon = 140,

	FoodIcon = 210,
	WoodIcon = 220,
	StoneIcon = 230,
	OreIcon = 240,
	SpiritIcon = 250,
	GoldIcon = 260,

	BasicTrainedPersonIcon = 310,
	BasicExtraPersonIcon = 320,
	BasicExtraHouseIcon = 330,
	AdvancedTrainedPersonIcon = 340,
	AdvancedExtraPersonIcon = 350,
	AdvancedExtraHouseIcon = 360,
}

[System.Serializable]
public class BuffGainOrRemoveAnimationControllerData
{
	public BuffGainOrRemoveAnimationAssetType buffGainOrRemoveAnimationAssetType;
	public RuntimeAnimatorController runtimeAnimatorController;
}


public class BuffGainOrRemoveAnimationEntity : MonoBehaviour
{
	// public Canvas canvas;

	public BuffGainOrRemoveAnimationType buffGainOrRemoveAnimationType;
	public bool isGainAnimation;

	public FloatUpInWorldComponent floatUpInWorldComponent;
	public Animator symbolAnimationController;
	public Animator[] resourceAnimationControllers;
}

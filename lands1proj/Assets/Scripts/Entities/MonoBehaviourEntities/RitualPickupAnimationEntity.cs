﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// NOTE: There is only one kind of RitualPickupAnimation, so no enum defined for that

// NOTE: The ritual pickup assets are entirely dictated by the RitualPickupType, and just their text is dynamically altered. So there's no separate enum for the assets themselves which we then combine to build the animation, like for BuffGainOrRemoveAnimationEntity.

[System.Serializable]
public class RitualPickupAnimationControllerData
{
	public RitualPickupType ritualPickupType;
	public RuntimeAnimatorController runtimeAnimatorController;
}

// TODO: Should this be canvas based, like everything else?
public class RitualPickupAnimationEntity : MonoBehaviour
{
	// public Canvas canvas;
	public HoverPickupInWorldComponent hoverPickupInWorldComponent;
	public Animator ritualPickupAnimationController;
	public TextMeshPro valueText; // TODO: Everywhere else weuse TextMeshProUGUI and a canvas when displaying text. But for the BuffGainOrRemoveAnimation(s) we went canvasless. Let's try canvasless here and see how it goes.
}

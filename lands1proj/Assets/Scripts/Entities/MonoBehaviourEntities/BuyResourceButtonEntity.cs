﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyResourceButtonEntity : MonoBehaviour
{
	public BuyWithCostAndGainButtonComponent buyWithCostAndGainButton;

	public BuyResourcePrototypeEntity buyResourcePrototypeEntity;

	// State
	// [HideInInspector]
	public int level;
	// [HideInInspector]
	public ResourceAmountComponent finalResourceCost;
	// [HideInInspector]
	public ResourceAmountComponent finalResourceGain;

	public void ButtonClick() { buyWithCostAndGainButton.ButtonClick(); }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralButtonEntity : MonoBehaviour
{
	public GeneralButtonComponent generalButton;

	public void ButtonClick() { generalButton.ButtonClick(); }
}

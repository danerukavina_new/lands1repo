﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// TODO: Starting to use this for SpriteAndBackgroundPanel as well, let's see how it goes...
// NOTE: SpriteAndBackgroundPanel never needs a progress bar... these should be split up, re: TODO above
public class SpriteAndBoarderUIDataEntity : MonoBehaviour
{
	public Image backgroundImage;
	public Image borderSpriteImage;
	public Image progressBarImage;
	public Image additionalArtImage; // TODO: Not used currently, especially note it's not used to represent additional background art even, which was the original intent
}

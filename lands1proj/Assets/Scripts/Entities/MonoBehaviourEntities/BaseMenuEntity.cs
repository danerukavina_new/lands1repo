﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMenuEntity<TEnum> : MonoBehaviour
	where TEnum : System.Enum
{
	public bool buttonClicked;
	public TEnum buttonClickAction;

	// NOTE: If you want to add an array of references to the buttons to this base class, check out SpiritualityMenuEntity_DEMOWithButtonRegistry
};

public class BaseEnumButtonClickMenuConnectComponent<TEnum, TMenu> : MonoBehaviour
	where TEnum : System.Enum
	where TMenu : BaseMenuEntity<TEnum> // NOTE: This is required for the button to be able to see the menu in the editor. Why? It's so that a concrete subclass can be made that explicity defines what the Menu is. This seems to be a prereq of it being seen in the editor.
{
	public TEnum buttonEnum;
	public TMenu menu;

	public void ForwardButtonClickToMenu()
	{
		menu.buttonClickAction = buttonEnum;
		menu.buttonClicked = true;
	}
}

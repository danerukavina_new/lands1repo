﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// TODO: Maybe it WOULD make sense to require it to specify it's TConnector in the definition. Otherwise this definition is sort of clueless as to who the connector is...
public class SpiritualityMenuEntity : BaseMenuEntity<SpiritualityMenuEntity.SpiritualityMenuButtonClickAction>
{
	public enum SpiritualityMenuButtonClickAction
	{
		OpenRitualScreen = 20,
		OpenMythScreen = 30,
		OpenGodScreen = 40,
		OpenIdolScreen = 50,
		RepeatPrevoiusRitual = 120
		// Tooltip buttons
	}
}

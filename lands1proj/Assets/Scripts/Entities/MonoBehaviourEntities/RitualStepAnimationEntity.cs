﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum RitualStepAnimationType
{
	Isolated = 20,
	Start = 30,
	End = 40,
	UpToUp = 50,
	UpToRight = 60,
	UpToLeft = 70
}

[System.Serializable]
public class RitualStepAnimationControllerData
{
	public RitualStepAnimationType ritualStepAnimationType;
	public RuntimeAnimatorController runtimeAnimatorController;
}

public class RitualStepAnimationEntity : MonoBehaviour
{
	public Animator ritualStepAnimationController;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// TODO: Compare and contrast this to the public Dictionary<RitualPickupType, RitualPickupMenuPickupRowEntity> ritualPickupMenuPickupRows; in MenuManagerEntity
public class ResourceDisplayEntity : MonoBehaviour
{
	public ResourceDisplayPanelEntity foodResourceDisplayPanelEntity;
	public ResourceDisplayPanelEntity woodResourceDisplayPanelEntity;
	public ResourceDisplayPanelEntity stoneResourceDisplayPanelEntity;
	public ResourceDisplayPanelEntity oreResourceDisplayPanelEntity;
	public ResourceDisplayPanelEntity spiritResourceDisplayPanelEntity;

	// NOTE: These are currently only used for the ritual resource display. This is a little hacky
	public ResourceDisplayPanelEntity goldResourceDisplayPanelEntity;
	// public ResourceDisplayPanelEntity basicTrainedPersonSpeedResourceDisplayPanelEntity;
	public ResourceDisplayPanelEntity basicExtraPersonResourceDisplayPanelEntity;
	// public ResourceDisplayPanelEntity advancedTrainedPersonSpeedResourceDisplayPanelEntity;
	public ResourceDisplayPanelEntity advancedExtraPersonResourceDisplayPanelEntity;
	// Gonna skip extend ritual as well
}

//public enum RitualPickupType
//{
//	FoodPickup = 210,
//	WoodPickup = 220,
//	StonePickup = 230,
//	OrePickup = 240,
//	// SpiritPickup = 250, // Spirit pickup shouldn't exist, it's equivalent is ExtendRitual
//	GoldPickup = 260,

//	BasicTrainedPersonSpeedPickup = 310, // TODO: TBD... does speed make sense? Since it's an instant bonus?
//	BasicExtraPersonPickup = 320,
//	// NOTE: Can't do house space as a ritual, I'd think
//	AdvancedTrainedPersonSpeedPickup = 340, // TODO: TBD... does speed make sense? Since it's an instant bonus?
//	AdvancedExtraPersonPickup = 350,
//	// NOTE: Can't do house space as a ritual, I'd think

//	ExtendRitual = 410,
//}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GlobalBuffDataEntity
{
	public ResourceBuffSetComponent playerGlobalBuffSetComponent;

	// TODO: Perhaps the entities in the chosen myths area should contain this data
	public bool inProgressFirstMyth;
	public bool inProgressSecondMyth;

	// TODO: Consider changing GlobalBuffDataEntity to have a different entity type for displaying the bought myth
	// TODO: It was hard for me to recall, months later, how these are set. Turns out it's in GlobalBuffDataEntity and via Enshrine
	public MythButtonEntity firstBoughtMythDisplayEntity;
	public MythButtonEntity secondBoughtMythDisplayEntity;
	public MythButtonEntity firstEnshrinedBoughtMythDisplayEntity;
	public MythButtonEntity secondEnshrinedBoughtMythDisplayEntity;
	public bool activeMythsChangedClearOnUpdate;

	public void Initialize()
	{
		Reinitialize();
	}

	public void Reinitialize()
	{
		// NOTE: This sets all the values to 0d, 0f, false, etc. which is fine. Not gonna bother with a long initalization
		playerGlobalBuffSetComponent = new ResourceBuffSetComponent();

		inProgressFirstMyth = inProgressSecondMyth = false;

		DestroyBoughtMythDisplayEntity(firstBoughtMythDisplayEntity);
		DestroyBoughtMythDisplayEntity(secondBoughtMythDisplayEntity);
		DestroyBoughtMythDisplayEntity(firstEnshrinedBoughtMythDisplayEntity);
		DestroyBoughtMythDisplayEntity(secondEnshrinedBoughtMythDisplayEntity);
	}

	void DestroyBoughtMythDisplayEntity(MythButtonEntity boughtMythDisplayEntity)
	{
		if (boughtMythDisplayEntity != null)
		{
			Object.Destroy(boughtMythDisplayEntity);
		}
	}

	public bool FirstMythFilled()
	{
		return !(firstBoughtMythDisplayEntity == null && !inProgressFirstMyth);
	}

	public bool SecondMythFilled()
	{
		return !(secondBoughtMythDisplayEntity == null && !inProgressSecondMyth);
	}
}

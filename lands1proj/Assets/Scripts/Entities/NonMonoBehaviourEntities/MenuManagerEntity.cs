﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// NOTE: This is a different approach I'm testing - unlike the buttonManagerEntity, the menuManagerEntity talks about concrete menus
[System.Serializable]
public class MenuManagerEntity
{
	public MenuEntity goldMenuEntity;
	public MenuEntity ascendMenuEntity;
	public MenuEntity spiritualityMenuEntity;
	public MenuEntity superAscendMenuEntity;
	public MenuEntity civilizationMenuEntity;

	public MenuEntity ritualInputMenuEntity;
	public GameObject ritualPickupRowsGridView;
	public RitualPickupMenuPickupRowEntity spiritCostRitualPickupRowEntity; // TODO: Is this pattern a good idea? Making them all the same type, instead of customized? There's no reason for them to have a resource display...
	public RitualPickupMenuPickupRowEntity[] editorRitualPickupMenuPickupRows;
	public Dictionary<RitualPickupType, RitualPickupMenuPickupRowEntity> ritualPickupMenuPickupRows;
	public ResourceDisplayEntity ritualResourceDisplayEntity; // TODO: Compare and contrast this to how the above is implemented. Hardcoding vs using keys, ways to mark type, and editor arrays
	public RitualPickupMenuPickupRowEntity largestSetBonusRitualPickupRowEntity;
	public RitualPickupMenuPickupRowEntity differentBuildingsBonusRitualPickupRowEntity;
}

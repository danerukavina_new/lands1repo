﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// NOTE: This is a different approach I'm testing - unlike the buttonManagerEntity, the menuManagerEntity talks about concrete menus
[System.Serializable]
public class ScreenManagerEntity
{
	public ScreenEntity ritualsScreen;

	public ScreenEntity mythsScreen;
	public GameObject showBoughtMythsMenuGridView;

	public ScreenEntity godsScreen;

	public ScreenEntity idolsScreen;

	public ScreenEntity ritualDetailsScreen;

	public ScreenEntity buildingTierUpgradeScreen;
	public GameObject showBuildingTierUpgradesMenuGridView;
	[HideInInspector]
	public bool buildingTierUpgradeScreenIsDirtyClearOnUpdate;
	public bool buildingTierUpgradeScreenShouldBeVisible;
	public BuildingEntity buildingTierUpgradeBuildingEntity;
	public TextMeshProUGUI currentBuildingNameText;
	public SpriteAndBoarderUIDataEntity buildingTierUpgradeScreenMenuSpriteAndBorderPanel;
	public SpriteAndBoarderUIDataEntity buildingTierUpgradeScreenCloseScreenButtonSpriteAndBorderPanel;
	public TextMeshProUGUI buildingTierUpgradeScreenMenuLabelText;

	public void Initialize()
	{
		buildingTierUpgradeScreenIsDirtyClearOnUpdate = false;
	}
}

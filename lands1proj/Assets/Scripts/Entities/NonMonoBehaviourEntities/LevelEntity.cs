﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelEntity
{
	TileEntity[,] tileEntities;
	int maxTilesX;
	int maxTilesY;
	public List<TileEntity> activeTileEntities;

	BuildingEntity[,] buildingEntities; // TODO: This may not be needed, it's enough to have the tiles in a grid
	public List<BuildingEntity> activeBuildingEntities;
	BuyTileOnLevelEntity[,] buyTileOnLevelEntities;
	public List<BuyTileOnLevelEntity> activeBuyTileOnLevelEntities;
	public bool tileEntitiesAreDirtiedClearOnUpdate;
	public bool buildingEntitiesAreDirtiedClearOnUpdate;
	public bool buildingEntitiesAreDirtiedClearOnFixedUpdate; // TODO: Unused

	public Dictionary<BuildingBaseType, int> buildingCountsByBaseBuilding;

	GameObject[,] fullFogEntities;
	GameObject[,] partialFogEntities;
	GameObject[,] inputIndicatorFogEntities;
	bool[,] fogWasPreviouslyRevealed;

	public void Initialize()
	{
		maxTilesX = ValueHolder.MAX_TILES_X;
		maxTilesY = ValueHolder.MAX_TILES_Y;
		tileEntities = new TileEntity[maxTilesX, maxTilesY];
		activeTileEntities = new List<TileEntity>(ValueHolder.MAX_ANTICIPATED_ACTIVE_TILES);
		buildingEntities = new BuildingEntity[maxTilesX, maxTilesY];
		activeBuildingEntities = new List<BuildingEntity>(maxTilesX * maxTilesY);
		buyTileOnLevelEntities = new BuyTileOnLevelEntity[maxTilesX, maxTilesY];
		activeBuyTileOnLevelEntities = new List<BuyTileOnLevelEntity>(maxTilesX * maxTilesY);
		fogWasPreviouslyRevealed = new bool[maxTilesX, maxTilesY]; // NOTE: This is intended for preserving fogs between ascensions and also in possible game load edge cases... though perhaps it should be a list then?

		buildingCountsByBaseBuilding = new Dictionary<BuildingBaseType, int>(LogicHelpers.baseBuildingTypeKeys.Count);

		foreach (var item in LogicHelpers.baseBuildingTypeKeys)
		{
			buildingCountsByBaseBuilding.Add(item, 0);
		}

		fullFogEntities = new GameObject[maxTilesX, maxTilesY];
		partialFogEntities = new GameObject[maxTilesX, maxTilesY];
		inputIndicatorFogEntities = new GameObject[maxTilesX, maxTilesY];

		SetAllDirtyFlags(); // TODO: TBD what needs to actually be drity here
	}

	// TODO: This reinitilizeFog parameter is pretty rudimentary and probably not used right. Fix it when tightening up ascend and super ascend.
	public void Reinitialize(bool reinitializeFog)
	{
		LogicHelpers.EntityRemover(tileEntities);
		activeTileEntities.Clear();

		LogicHelpers.EntityRemover(buildingEntities);
		activeBuildingEntities.Clear();
		RemoveBuyTileOnLevelEntities();

		ResetBuildingCounts();

		if (reinitializeFog)
		{
			LogicHelpers.EntityRemover(fullFogEntities);
			LogicHelpers.BoolClearer(fogWasPreviouslyRevealed);
		}

		LogicHelpers.EntityRemover(partialFogEntities);
		LogicHelpers.EntityRemover(inputIndicatorFogEntities);

		SetAllDirtyFlags(); // TODO: TBD what needs to actually be drity here
	}

	public bool CoordIsInLevel(Vector2Int coord) { return coord.x >= 0 && coord.x < maxTilesX && coord.y >= 0 && coord.y < maxTilesY;  }

	public TileEntity GetTileEntity(Vector2Int coord) { return tileEntities[coord.x, coord.y]; }
	public BuyTileOnLevelEntity GetBuyTileOnLevelEntity(Vector2Int coord) { return buyTileOnLevelEntities[coord.x, coord.y]; }
	public GameObject GetFullFogEntity(Vector2Int coord) { return fullFogEntities[coord.x, coord.y]; }
	public GameObject GetPartialFogEntity(Vector2Int coord) { return partialFogEntities[coord.x, coord.y]; }
	public GameObject GetInputIndicatorFogEntity(Vector2Int coord) { return inputIndicatorFogEntities[coord.x, coord.y]; }
	public bool GetFogWasPreviouslyRevealed(Vector2Int coord) { return fogWasPreviouslyRevealed[coord.x, coord.y]; }
	public void SetFogWasPreviouslyRevealed(Vector2Int coord, bool value) { fogWasPreviouslyRevealed[coord.x, coord.y] = value; }

	public TileEntity GetTileEntityOrNull(Vector2Int coord)
	{
		if (coord.x >= 0 && coord.x < tileEntities.GetLength(0)
			&& coord.y >= 0 && coord.y < tileEntities.GetLength(1)
			)
			return this.tileEntities[coord.x, coord.y];

		return null;
	}

	public void ResetBuildingCounts()
	{
		foreach (var item in LogicHelpers.baseBuildingTypeKeys)
		{
			buildingCountsByBaseBuilding[item] = 0;
		}
	}

	// TODO: There needs to be a single system that does adding tiles... and then this function should belong to that system. SEe example in UpdateRitualPickupAnimationSystem
	public void AddTileEntity(Vector2Int coord, TileEntity tileEntityPrefab, GameObject tileEntitiesObject, Camera mainCamera)
	{
		Vector3 pos = LogicHelpers.CoordToPosition(coord);

		TileEntity tileEntity = GameObject.Instantiate(tileEntityPrefab, pos, Quaternion.identity, tileEntitiesObject.transform);

		tileEntity.canvas.worldCamera = mainCamera;

		tileEntity.coord = coord;
		tileEntity.tileMenuState = TileMenuState.InitialMenu;

		tileEntity.isDirtyClearOnUpdate = true;

		LogicHelpers.InitializeHashSetWithCapacity(ref tileEntity.buffEntities, System.Enum.GetValues(typeof(BuffType)).Length);

		tileEntities[coord.x, coord.y] = tileEntity;
		activeTileEntities.Add(tileEntity);
	}

	public void RemoveTileEntity(Vector2Int coord)
	{
		TileEntity tileEntity = tileEntities[coord.x, coord.y];

		if (tileEntity != null)
		{
			Object.Destroy(tileEntity);
			tileEntities[coord.x, coord.y] = null;

			if (!activeTileEntities.Remove(tileEntity))
				throw new System.Exception("Attempted to remove a tile entity that was found in the array, but not found in the activeTileEntities list");
		}
	}

	// TODO: See TODO on AddTileEntity... move this to a system
	public void AddBuildingEntity(
		TileEntity tileEntity, 
		BuildingAdvancedType buildingAdvancedType, 
		BuildingEntity buildingEntityPrefab, 
		Dictionary<BuildingAdvancedType, BuildingAssetsDataScriptableObject> buildingAssetsDataScriptableObjects,
		Dictionary<BuildingAdvancedType, BuildingStatsScriptableObject> buildingStatsScriptableObjects,
		GameObject buildingEntitiesObject, 
		Camera mainCamera
		)
	{
		Vector2Int coord = tileEntity.coord;

		Vector3 pos = LogicHelpers.CoordToPosition(coord);

		BuildingEntity buildingEntity = GameObject.Instantiate(buildingEntityPrefab, pos, Quaternion.identity, buildingEntitiesObject.transform);

		buildingEntity.canvas.worldCamera = mainCamera;

		buildingEntity.buildingAssetsDataScriptableObject = buildingAssetsDataScriptableObjects[buildingAdvancedType];
		buildingEntity.buildingStatsScriptableObject = buildingStatsScriptableObjects[buildingAdvancedType];
		buildingEntity.isDirtyFromBeingBuilt = true;
		buildingEntity.tileEntity = tileEntity;
		buildingEntity.buildingMenuState = BuildingMenuState.BuildInProgress;

		LogicHelpers.InitializeHashSetWithCapacity(ref buildingEntity.applicableBuffEntities, ValueHolder.MAX_BUFF_ENTITIES); // TODO: Consider if we need a more conservative constant for the initial size of these hash sets
		buildingEntity.addedApplicableBuffEntitiesQueue = new List<BuffEntity>(ValueHolder.MAX_BUFF_GAIN_OR_REMOVE_ANIMATION_ENTITIES_PER_BUILDING);
		buildingEntity.removedApplicableBuffEntitiesQueue = new List<BuffEntity>(ValueHolder.MAX_BUFF_GAIN_OR_REMOVE_ANIMATION_ENTITIES_PER_BUILDING);
		buildingEntity.isInBuffAnimationCreationDelayFromLastDequeue = false;

		buildingEntity.trainedWorkers = 0;
		buildingEntity.trainedWorkersClearOnUpdate = 0;
		buildingEntity.extraWorkers = 0d;
		// TODO: Consider having extraWorkersClearOnUpdate and using it for displaying the effect of gaining extra workers.
		buildingEntity.tierIndex = 0;
		buildingEntity.isAtMaxTrainedWorkersForTierAndThusMayTierUpgrade = false; // TODO: Setting some flags, but not others? What's the logic? This will matter for object pooling
		buildingEntity.isAtMaxExtraWorkers = false; // TODO: Setting some flags, but not others? What's the logic? This will matter for object pooling
		buildingEntity.chosenBuildingTierUpgradePathIndex = -1; // NOTE: Setting this to an invalid value to uncover possible bugs of it not being set.

		buildingEntity.workersOrTierIsDirtyClearOnUpdate = true;

		buildingEntity.finalTierUpgradePathIndex = 0; // NOTE: Just in case, since otherwise the argument will be undefined
		// buildingEntity.nextBuildingTiers ... // NOTE: This does not need to be initialized, it always points to ValueHolder arrays
		buildingEntity.anyTierUpgradeIsAvailable = false;
		buildingEntity.finalBuildOrTierUpgradeTimes = new List<float>(ValueHolder.MAX_PARALLEL_BUILDING_TIER_UPGRADE_PATHS);
		buildingEntity.finalBuildOrTierUpgradeCosts = new List<ResourceSetComponent>(ValueHolder.MAX_PARALLEL_BUILDING_TIER_UPGRADE_PATHS);
		buildingEntity.canStartNextBuildingTierUpgrades = new List<bool>(ValueHolder.MAX_PARALLEL_BUILDING_TIER_UPGRADE_PATHS);

		// TODO: Fix all this code... awful (Later comment - Presumably the constantly fixed sized lists rather than it being a naturally gorwn array?)
		for (int i = 0; i < ValueHolder.MAX_PARALLEL_BUILDING_TIER_UPGRADE_PATHS; i++)
		{
			buildingEntity.finalBuildOrTierUpgradeTimes.Add(0f);
			buildingEntity.finalBuildOrTierUpgradeCosts.Add(null);
			buildingEntity.canStartNextBuildingTierUpgrades.Add(false);
		}

		buildingEntity.showOrRefreshProductionDisplayPanel = false;

		buildingEntity.buffGiverComponents = new List<BuffGiverComponent>(ValueHolder.MAX_BUFFS_A_BUILDING_GIVES);
		buildingEntity.removedBuffGiverComponentsClearOnUpdate = new List<BuffGiverComponent>(ValueHolder.MAX_BUFFS_A_BUILDING_GIVES);

		buildingEntity.buffGainOrRemoveAnimationEntities = new List<BuffGainOrRemoveAnimationEntity>(ValueHolder.MAX_BUFF_GAIN_OR_REMOVE_ANIMATION_ENTITIES_PER_BUILDING);

		buildingEntities[coord.x, coord.y] = buildingEntity;
		activeBuildingEntities.Add(buildingEntity);

		tileEntity.buildingEntity = buildingEntity;

		this.buildingEntitiesAreDirtiedClearOnFixedUpdate = true;
		this.buildingEntitiesAreDirtiedClearOnUpdate = true;
	}

	public void RemoveBuildingEntity(Vector2Int coord)
	{
		BuildingEntity buildingEntity = buildingEntities[coord.x, coord.y];

		if (buildingEntity != null)
		{
			Object.Destroy(buildingEntity);
			tileEntities[coord.x, coord.y] = null;

			if (!activeBuildingEntities.Remove(buildingEntity))
				throw new System.Exception("Attempted to remove a building entity that was found in the array, but not found in the activeBuildingEntities list");
		}

		this.buildingEntitiesAreDirtiedClearOnFixedUpdate = true;
		this.buildingEntitiesAreDirtiedClearOnUpdate = true;
	}

	public void RemoveBuildingEntities()
	{
		// TODO: Implement a class that describes the coord based array + active elements with coords from array relationship. This class should take care of removing the items with this coord based logic.

		foreach (var buildingEntity in activeBuildingEntities)
		{
			buildingEntities[buildingEntity.tileEntity.coord.x, buildingEntity.tileEntity.coord.y] = null;
		}

		LogicHelpers.EntityRemover(activeBuildingEntities);

		this.buildingEntitiesAreDirtiedClearOnFixedUpdate = true;
		this.buildingEntitiesAreDirtiedClearOnUpdate = true;
	}

	public void MakeDirtyTileEntities()
	{
		// TODO: Implement a class that describes the coord based array + active elements with coords from array relationship. This class should take care of removing the items with this coord based logic.

		foreach (var tileEntity in activeTileEntities)
		{
			tileEntity.isDirtyClearOnUpdate = true;
		}

		this.tileEntitiesAreDirtiedClearOnUpdate = true;
	}

	public void AddBuyTileOnLevelEntity(Vector2Int coord, BuyTileOnLevelEntity buyTileOnLevelEntityPrefab, GameObject tileEntitiesObject, Camera mainCamera)
	{
		Vector3 pos = LogicHelpers.CoordToPosition(coord);

		BuyTileOnLevelEntity buyTileOnLevelEntity = GameObject.Instantiate(buyTileOnLevelEntityPrefab, pos, Quaternion.identity, tileEntitiesObject.transform);

		buyTileOnLevelEntity.canvas.worldCamera = mainCamera;

		buyTileOnLevelEntity.coord = coord;

		buyTileOnLevelEntities[coord.x, coord.y] = buyTileOnLevelEntity;
		activeBuyTileOnLevelEntities.Add(buyTileOnLevelEntity);
	}

	public void RemoveBuyTileOnLevelEntities()
	{
		LogicHelpers.EntityRemover(buyTileOnLevelEntities);
		activeBuyTileOnLevelEntities.Clear();
	}

	public void AddAllFogEntities(
		GameObject fullFogEntityPrefab,
		GameObject partialFogEntityPrefab,
		GameObject inputIndicatorFogEntityPrefab,
		GameObject fullFogEntitiesObject,
		GameObject partialFogEntitiesObject,
		GameObject inputIndicatorFogEntitiesObject
		)
	{
		Vector2Int coord;
		Vector3 pos;

		for (int x = 0; x < maxTilesX; x++)
		{
			for (int y = 0; y < maxTilesY; y++)
			{
				coord = new Vector2Int(x, y);

				pos = LogicHelpers.CoordToPosition(coord);

				GameObject fullFogEntity = GameObject.Instantiate(fullFogEntityPrefab, pos, Quaternion.identity, fullFogEntitiesObject.transform);
				GameObject partialFogEntity = GameObject.Instantiate(partialFogEntityPrefab, pos, Quaternion.identity, partialFogEntitiesObject.transform);
				GameObject inputIndicatorFogEntity = GameObject.Instantiate(inputIndicatorFogEntityPrefab, pos, Quaternion.identity, inputIndicatorFogEntitiesObject.transform);

				fullFogEntities[coord.x, coord.y] = fullFogEntity;
				partialFogEntities[coord.x, coord.y] = partialFogEntity;
				inputIndicatorFogEntities[coord.x, coord.y] = inputIndicatorFogEntity;
			}
		}
	}

	public void ReenableSomeFogEntities()
	{
		Vector2Int coord;

		for (int x = 0; x < maxTilesX; x++)
		{
			for (int y = 0; y < maxTilesY; y++)
			{
				coord = new Vector2Int(x, y);

				// UIHelpers.ShowHideGameObject(GetFullFogEntity(coord).gameObject, true); // NOTE: There's no reason to re-enable full fogs on ascend, they don't come back
				UIHelpers.ShowHideGameObject(GetPartialFogEntity(coord).gameObject, true);
				UIHelpers.ShowHideGameObject(GetInputIndicatorFogEntity(coord).gameObject, true);
			}
		}
	}

	// TODO: Feel like this should only be called by systems, and that the LevelEntity owns too much logic
	public void SetAllDirtyFlags()
	{
		// NOTE: This function isn't creat, and I've decided to not include this.tileEntitiesAreDirtiedClearOnUpdate = true;

		this.buildingEntitiesAreDirtiedClearOnFixedUpdate = true;
		this.buildingEntitiesAreDirtiedClearOnUpdate = true;
		// TODO: make a better determination of this based on what it means for the building... whether it's a TownHall which does update, or a Farm that doesn't
	}

	// NOTE: The functions below are intended to prevent repeats in logic while also not making extra flags which don't hold any additional useful information
	public bool CanAscend()
	{
		if (activeBuildingEntities.Count > activeTileEntities.Count)
			throw new System.Exception("When determining if it's possible to Ascend, found there's more buildings than tiles, which should be impossible.");

		return activeBuildingEntities.Count == activeTileEntities.Count;
	}

	public bool CanSuperAscend()
	{
		return false;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RitualType
{
	BasicRitual = 20,
	WaterRitual = 30,
	AirRitual = 40,
	MetalRitual = 50
}

public class RitualStepEntity
{
	public TileEntity tileEntity;
	// TODO: Add other alternatives to tileEntity, such as coords for metal ritual

	public RitualStepAnimationEntity ritualStepAnimationEntity;

	public override string ToString()
	{
		if (tileEntity != null)
			return tileEntity.ToString();
		return "null tile";
	}
}

// TODO: ritualStepEntities should be integrated with an object pool
public class RitualEntity
{
	// Primary state
	public RitualType ritualType;
	public UndoableList<RitualStepEntity> ritualStepEntities;

	// Derived state // TODO: Come up with a standard nomenclature for this. The point is the only thing that matters for the RitualEntity is its steps and type
	public List<RitualStepEntity> ritualStepEntitiesAddingPickupsClearOnUpdate;
	public List<RitualStepEntity> ritualStepEntitiesUpdatingPickupsClearOnUpdate;
	public List<RitualStepEntity> ritualStepEntitiesRemovingPickupsClearOnUpdate;
	public double ritualSpiritCost;
	public double largestSetBonus;
	public double differentBuildingsBonus;
	public ResourceSetComponent ritualResourceGains;
	public double ritualBasicExtraWorkers; // TODO: This seems a little ugly... mixing resource gains tracking and what's traditioanlly in the realm of buffs. But it's simple.
	public double ritualAdvancedExtraWorkers;
}

[System.Serializable]
public class RitualStateEntity
{
	// NOTE: The functions below are intended to prevent repeats in logic while also not making extra flags which don't hold any additional useful information
	// public bool EnoughGoldToBuyTile() { return playerResourceSetAmountComponent.gold >= buyTileCostAmount; }

	public RitualEntity ritualEntity;

	// TODO: Add a consolidated variable for inputting the next, vetted, ritual step
	public bool attemptToAddRitualStepEntity;
	public TileEntity candidateTileForNextPlannedRitualStepEntity;
	public int undoRitualStepsTriggered;
	public int redoRitualStepsTriggered;

	public HashSet<Vector2Int> validNextRitualStepCoords;
	public Dictionary<Vector2Int, int> backtrackCountsForAllowedRitualStepCoords;
	public bool validNextRitualStepTilesIsDirtyClearOnUpdate;

	public float previouslyPerformedRitualTime;
	public bool ritualIsOnCooldownFromBeingPerformed;
	public float basePerformRitualCooldown;
	public bool playerCanPerformRitual;

	public bool performRitual;

	public bool isDirtyClearOnUpdate;

	public bool initializeRitualPickupRowsGridView;

	public void Initialize()
	{
		ritualEntity = new RitualEntity()
		{
			ritualStepEntities = new UndoableList<RitualStepEntity>(ValueHolder.MAX_RITUAL_STEPS),
			ritualStepEntitiesAddingPickupsClearOnUpdate = new List<RitualStepEntity>(ValueHolder.MAX_RITUAL_STEPS),
			ritualStepEntitiesUpdatingPickupsClearOnUpdate = new List<RitualStepEntity>(ValueHolder.MAX_RITUAL_STEPS),
			ritualStepEntitiesRemovingPickupsClearOnUpdate = new List<RitualStepEntity>(ValueHolder.MAX_RITUAL_STEPS),
			ritualResourceGains = new ResourceSetComponent()
		};

		validNextRitualStepCoords = new HashSet<Vector2Int>();
		LogicHelpers.InitializeHashSetWithCapacity(ref validNextRitualStepCoords, ValueHolder.MAX_ANTICIPATED_ACTIVE_TILES);
		backtrackCountsForAllowedRitualStepCoords = new Dictionary<Vector2Int, int>(ValueHolder.MAX_ANTICIPATED_ACTIVE_TILES);

		initializeRitualPickupRowsGridView = true;

		Reinitialize();
	}

	public void Reinitialize()
	{
		ritualEntity.ritualStepEntities.Clear();

		previouslyPerformedRitualTime = float.MinValue;
		ritualIsOnCooldownFromBeingPerformed = false;
		basePerformRitualCooldown = ValueHolder.BASE_RITUAL_COOLDOWN;

		isDirtyClearOnUpdate = true;
	}
}

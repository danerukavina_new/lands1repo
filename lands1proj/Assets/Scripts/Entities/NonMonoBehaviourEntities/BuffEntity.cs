﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BuffType
{
	TrainingCheapnessMultiplierFlat = 30,
	TrainingSpeedMultiplierFlat = 40,
	FarmFoodProductionPerWorker = 130,
	VariousFoodWoodStoneOreProductionPerWorker = 140,
	ShrineSpiritProductionPerWorker = 150,
	FarmExtraWorkerPerFive = 230,
	VariousFoodWoodStoneOreWorkerPerFive = 240,
	ShrineExtraWorkerPerFive = 250,
	FarmFoodProductionPercentagePerWorker = 330,
	VariousFoodWoodStoneOreProductionPercentagePerWorker = 340,
	ShrineSpiritPercentageProductionPerWorker = 350,
	AdditionalExraWorkerLimitForBasicBuildings = 430,
	AdditionalExraWorkerLimitForAdvancedBuildings = 440, // TODO: Untested
}

// TODO: Determine if you want to handle these as entities.
public class BuffEntity
{
	public BuffType buffType;
	public double buffStrength;
	public BuildingEntity sourceBuildingEntity; // TODO: Use this, stop using dictionaries on tiles and buildings, you nut

	// TODO: Not used, consider deleting
	public static bool AreEqual(BuffEntity a, BuffEntity b)
	{
		if (a.buffType != b.buffType || a.buffStrength != b.buffStrength || a.sourceBuildingEntity != b.sourceBuildingEntity)
			return false;

		return true;

		// TODO: Considering adding some logic here to check... something like previously recorded trained workers or not, using a case based on buff type.
	}
}

public static class BuffTypeExtensions
{
	public static bool BuffTypeDependsOnWorkers(this BuffType buffType)
	{
		switch (buffType)
		{
			case BuffType.TrainingCheapnessMultiplierFlat:
			case BuffType.TrainingSpeedMultiplierFlat:
				return false;
			case BuffType.FarmFoodProductionPerWorker:
			case BuffType.VariousFoodWoodStoneOreProductionPerWorker:
			case BuffType.ShrineSpiritProductionPerWorker:
				return true;
			case BuffType.FarmExtraWorkerPerFive:
			case BuffType.VariousFoodWoodStoneOreWorkerPerFive:
			case BuffType.ShrineExtraWorkerPerFive:
				return true;
			case BuffType.FarmFoodProductionPercentagePerWorker:
			case BuffType.VariousFoodWoodStoneOreProductionPercentagePerWorker:
			case BuffType.ShrineSpiritPercentageProductionPerWorker:
				return true;
			case BuffType.AdditionalExraWorkerLimitForBasicBuildings:
			case BuffType.AdditionalExraWorkerLimitForAdvancedBuildings:
				return true;
			default:
				throw new System.Exception("Unexpected buff type when assessing whether its effects depend on workers");
		}
	}

	// TODO: Originally this was a function for 1 + x * 0.1 scaling... but we got rid of it. More importantly, make this a property of some scriptable object
	public static bool BuffUsesOneTenthScaling(this BuffType buffType)
	{
		switch (buffType)
		{
			case BuffType.TrainingCheapnessMultiplierFlat:
			case BuffType.TrainingSpeedMultiplierFlat:
				return false;
			case BuffType.FarmFoodProductionPerWorker:
			case BuffType.VariousFoodWoodStoneOreProductionPerWorker:
			case BuffType.ShrineSpiritProductionPerWorker:
				return false;
			case BuffType.FarmExtraWorkerPerFive:
			case BuffType.VariousFoodWoodStoneOreWorkerPerFive:
			case BuffType.ShrineExtraWorkerPerFive:
				return true;
			case BuffType.FarmFoodProductionPercentagePerWorker:
			case BuffType.VariousFoodWoodStoneOreProductionPercentagePerWorker:
			case BuffType.ShrineSpiritPercentageProductionPerWorker:
				return false;
			case BuffType.AdditionalExraWorkerLimitForBasicBuildings:
			case BuffType.AdditionalExraWorkerLimitForAdvancedBuildings:
				return false;
			default:
				throw new System.Exception("Unexpected buff type when assessing whether it uses special scaling");
		}
	}
}

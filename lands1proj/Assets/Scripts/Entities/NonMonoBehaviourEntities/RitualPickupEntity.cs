﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RitualPickupType
{
	FoodPickup = 210,
	WoodPickup = 220,
	StonePickup = 230,
	OrePickup = 240,
	// SpiritPickup = 250, // Spirit pickup shouldn't exist, it's equivalent is ExtendRitual
	GoldPickup = 260,

	BasicTrainedPersonSpeedPickup = 310, // TODO: TBD... does speed make sense? Since it's an instant bonus?
	BasicExtraPersonPickup = 320,
	// NOTE: Can't do house space as a ritual, I'd think
	AdvancedTrainedPersonSpeedPickup = 340, // TODO: TBD... does speed make sense? Since it's an instant bonus?
	AdvancedExtraPersonPickup = 350,
	// NOTE: Can't do house space as a ritual, I'd think

	ExtendRitual = 410,
}

// TODO: Compare and contrast this to PrototypeBuffGiverAreaComponents
public class RitualPickupEntity
{
	// Prototype
	public RitualPickupType ritualPickupType;
	public double ritualPickupStrength;
	// TODO: Should this be the owner of the animation? Or leave it to the building?

	// State
	public BuildingEntity sourceBuildingEntity;
}

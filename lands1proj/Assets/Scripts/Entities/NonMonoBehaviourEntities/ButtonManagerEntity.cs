﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ButtonManagerEntity
{
	public List<BuyResourceButtonEntity> buyResourceButtonEntities;

	// TODO: Tracking of the myth button entites used for displaying what was bought is done by the GlobalBuffDataEntity. Seems a little disoragnized.
	// TODO: NOTE: These need to have their appearances initialized, since they are sorta just placed in the scene... other objects in the scene use the simpler "SpriteAndBorderEntitiesColorSetter" approach, but here we want to draw the scripable object assets. Think on maybe finding another way to do this.
	public List<MythButtonEntity> mythBuyingButtonEntities;

	public List<BuildingTierUpgradeButtonEntity> buildingTierUpgradeButtonEntities; // TODO: It sucks they're defined separately from the screen they belong to. In October 2021 I spent 10-15 minutes finding them.

	// Gold Menu
	public GeneralButtonEntity buyTileButtonEntity;

	// Ascend Menu
	public GeneralButtonEntity ascendButtonEntity;
	public GeneralButtonEntity masterButtonEntity;
	public GeneralButtonEntity enshrineButtonEntity;

	// Spirituality Menu
	public GeneralButtonEntity ritualsButtonEntity;
	public GeneralButtonEntity mythsButtonEntity;
	public GeneralButtonEntity godsButtonEntity;
	public GeneralButtonEntity idolsButtonEntity;
	public GeneralButtonEntity repeatRitualButtonEntity;

	// Civilization Menu
	public GeneralButtonEntity manageCivilizationsScreenButton; // TODO: Very debateable if this should be a GeneralButtonEntity, or something more specific. And the naming convention for the "screen" that opens.

	// Super Ascend Menu
	public GeneralButtonEntity superAscendButtonEntity;

	// Choose World Input Type Menu
	public GeneralButtonEntity defaultWorldInputTypeButton;
	public GeneralButtonEntity ritualWorldInputTypeButton;

	// Ritual Input Menu
	public GeneralButtonEntity undoRitualStepButton;
	public GeneralButtonEntity redoRitualStepButton;
	public GeneralButtonEntity performRitualButton;
	public Image performRitualButtonProgressBarImage; // TODO: This is a hack, consider the MythButtonEntity implementation that uses the ResearchableUIComponent researchableUIComponent
	public GeneralButtonEntity showRitualDetailsButtonEntity;

	// Ritual Details Menu
	public GeneralButtonEntity performRitualFromRitualDetailsButton;
	public Image performRitualFromRitualDetailsButtonProgressBarImage; // TODO: This is a hack, consider the MythButtonEntity implementation that uses the ResearchableUIComponent researchableUIComponent

	public void Initialize()
	{
		buildingTierUpgradeButtonEntities = new List<BuildingTierUpgradeButtonEntity>(ValueHolder.MAX_PARALLEL_BUILDING_TIER_UPGRADE_PATHS);

		Reinitialize();
	}

	public void Reinitialize()
	{
		// TODO: We don't do this for the other button lists... hmmm
		LogicHelpers.EntityRemover(buildingTierUpgradeButtonEntities);
	}
}

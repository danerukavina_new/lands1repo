﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameStateEntity
{
	public enum AscensionType
	{
		Ascend = 0,
		Master,
		Enshrine
	}

	public bool levelShouldBeLoaded;

	// TODO: Maybe move this to a different entity
	public float gameTime; // TODO: This seems to be used for both game logic and UI logic. Seems iffy.
	public float updateGameDeltaTime;
	public float previouslyProcessedAddWorkerOverTimeTime;

	public bool gameTimeAdded;
	public float addedGameTime;

	public bool showAdvancedBuildMenus;
	public bool showHeroicBuildMenus;
	public bool showBorderBuildMenus;

	public ResourceSetComponent playerResourceSetAmountComponent;
	public ResourceSetComponent playerResourceGainsThisUpdateComponent; // TODO: Perhaps get rid of the word component on these
	public ResourceSetComponent playerResourceSetGainsOverTimeComponent;
	public ResourceBuffSetComponent playerGlobalBuffSetComponent;

	public int totalBoughtTiles;
	public double buyTileCostAmount;
	public bool buyTileState;
	public bool changedBuyTileStateClearOnUpdate;

	public double ascendGoldGainAmount;
	public bool ascensionTriggered;
	public AscensionType triggeredAscensionType;

	public bool canBuyIdols;

	public bool autoTrain;

	public bool canHaveSecondMyth;
	public bool canHaveSecondTraditionMyth;

	public Dictionary<BuildingBaseType, bool> canBuildTileBuilding;

	// NOTE: The functions below are intended to prevent repeats in logic while also not making extra flags which don't hold any additional useful information
	public bool EnoughGoldToBuyTile() { return playerResourceSetAmountComponent.gold >= buyTileCostAmount; }
	public bool CanMaster() { return false; }
	public bool CanEnshrine() { return false; }
	public bool CanRepeatRitual() { return false; }
	public bool CanManageCvilization() { return false; }
	public bool SpiritualityAndRitualsAndMythsAreAvailable() { return playerResourceSetAmountComponent.spirit > 0d; }
	public bool GodsAreAvailable() { return playerResourceSetAmountComponent.spirit >= ValueHolder.GODS_SPIRIT_COST; }

	public void Initialize()
	{
		canBuildTileBuilding = new Dictionary<BuildingBaseType, bool>(LogicHelpers.baseBuildingTypeKeys.Count);

		foreach (var item in LogicHelpers.baseBuildingTypeKeys)
		{
			canBuildTileBuilding.Add(item, false);
		}

		Reinitialize();
	}

	public void Reinitialize()
	{
		levelShouldBeLoaded = true;

		gameTime = 0f;
		gameTimeAdded = false;
		previouslyProcessedAddWorkerOverTimeTime = gameTime;

		showAdvancedBuildMenus = false;
		showHeroicBuildMenus = false;
		showBorderBuildMenus = false;

		playerResourceSetAmountComponent = new ResourceSetComponent()
		{
			gold = 1999900d, // = 0d,
			ore = 5000d, // = 0d
		};

		PartiallyReinitializeResourcesAfterAscend();

		totalBoughtTiles = 0;

		autoTrain = false;

		canHaveSecondMyth = ValueHolder.BASE_CAN_HAVE_SECOND_MYTH;
		canHaveSecondTraditionMyth = ValueHolder.BASE_CAN_HAVE_SECOND_TRADITION_MYTH;

		foreach (var item in LogicHelpers.baseBuildingTypeKeys)
		{
			canBuildTileBuilding[item] = false;
		}
	}

	public void PartiallyReinitializeResourcesAfterAscend()
	{
		// NOTE: Gold and Ore persist

		// NOTE: Intended values for the game
		playerResourceSetAmountComponent.food = ValueHolder.STARTING_WHEAT;
		playerResourceSetAmountComponent.wood = 0d;
		playerResourceSetAmountComponent.stone = 0d;
		playerResourceSetAmountComponent.ore /= ValueHolder.ASCEND_ORE_REDUCTION_FACTOR;
		playerResourceSetAmountComponent.spirit = 0d;

		// NOTE: Testing values
		playerResourceSetAmountComponent.food = 4000d;
		playerResourceSetAmountComponent.wood = 3000d;
		playerResourceSetAmountComponent.stone = 2990d;
		playerResourceSetAmountComponent.ore = 1990d;
		playerResourceSetAmountComponent.spirit = 27770d;

		// NOTE: Testing values
		playerResourceSetAmountComponent.food = 10000d; // 100d;
		playerResourceSetAmountComponent.wood = 4000d; // 30d;
		playerResourceSetAmountComponent.stone = 2990d;
		playerResourceSetAmountComponent.ore = 1990d;
		playerResourceSetAmountComponent.spirit = 27770d;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InputStateEntity
{
	public enum TapInputState
	{
		None = 0,
		FreeSpaceClickAndDragToMoveCamera = 20,
		RitualInputBasedOnRitualInputType = 50
		// TODO: add others, like swiping over buildings to train
	}

	public enum RitualInputType
	{
		TapNextNode = 20,
		SwipeToSelectWithRecenteringWhenLettingGo = 30,
		SwipeToSelectWithRubberBandingAtEdges = 40 // TODO: Determine if this one is workable, since it's likely to be the best
	}

	public enum CameraControlType
	{
		ClickAndDrag = 1
	}

	public enum WorldInputType
	{
		DefaultWorldInput = 20,
		RitualWorldInput = 50
	}

	// Config
	public CameraControlType cameraControlType;
	public RitualInputType ritualInputType;

	// State
	public bool anyInputEnabled;
	public Dictionary<TapInputState, bool> specificInputEnabled;
	public TapInputState tapInputState;
	public float inputStartTime;
	public bool initialTapThisUpdateClearOnUpdate;
	public Vector3 tapStartWorldPosition;
	public Vector3 tapStartScreenPosition;
	public Vector3 tapCurrentWorldPosition;
	public Vector3 tapCurrentScreenPosition;
	public Vector3 tapStartCameraWorldPosition;
	public bool inputStateMayCauseStartOfCameraMovement; // TODO: Is this the same as initialTapThisUpdateClearOnUpdate?
	public WorldInputType worldInputType;
	public bool worldInputTypeHasChangedClearOnUpdate;
	public WorldInputType prevWorldInputTypeForControllingTransitions; // TODO: Perhaps... the transition system that is the sole user of this should just save this as state? Eh....
	public bool initialWorldInputTypeTransition;

	public List<InputIndicatorAnimationEntity> inputIndicatorAnimationEntities; // NOTE: Currently there's no need to distinguish between the different entities

	public void Initialize()
	{
		anyInputEnabled = true;
		specificInputEnabled = new Dictionary<InputStateEntity.TapInputState, bool>()
		{
			{  InputStateEntity.TapInputState.FreeSpaceClickAndDragToMoveCamera, true },
		};
		tapInputState = InputStateEntity.TapInputState.None;

		cameraControlType = InputStateEntity.CameraControlType.ClickAndDrag;
		ritualInputType = InputStateEntity.RitualInputType.SwipeToSelectWithRubberBandingAtEdges;

		worldInputType = InputStateEntity.WorldInputType.DefaultWorldInput;
		worldInputTypeHasChangedClearOnUpdate = false; // TODO: This and how it interacts with prevWorldInputType is crappy. Setting it to true here causes exceptions
		prevWorldInputTypeForControllingTransitions = InputStateEntity.WorldInputType.DefaultWorldInput;
		initialWorldInputTypeTransition = true;

		inputIndicatorAnimationEntities = new List<InputIndicatorAnimationEntity>(ValueHolder.MAX_ANTICIPATED_INPUT_INDICATOR_ANMATIONS);
	}
}

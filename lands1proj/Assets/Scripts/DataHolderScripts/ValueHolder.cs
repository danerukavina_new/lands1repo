﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ResourceType
{
	Gold,
	Food = 120,
	Wood = 130,
	Stone = 140,
	Metal = 150,
	Spirit = 160
}

// TODO: This might not be specific enough
public enum SpecificBuffType
{
	Myth = 120,
	God = 130,
	GodAbility = 140,
	Disciple = 150,
	DiscipleAbility = 160,
	Artifact = 170,
	Civilization = 180,
}

public static class ValueHolder
{
	public const int MAX_LOOP_CONTROL = 1024;

	public const int MAX_TILES_X = 128;
	public const int MAX_TILES_Y = 128;
	public const int STARTING_TILE_OFFSET_X = 64; // NOTE: As of October 2020 I also set the Camera's position manually to 64, 64. However, the load level system also repositions it to the first tile which is seen in levelData below
	public const int STARTING_TILE_OFFSET_Y = 64;
	public const int MAX_ACTIVE_BUILDING_ENTITIES = 256; // TODO: Not used, but think about it
	public const int MAX_ANTICIPATED_ACTIVE_TILES = MAX_ACTIVE_BUILDING_ENTITIES + 2 * MAX_TILES_X + 2 * MAX_TILES_Y; // NOTE: This is so we don't use MAX_TILES_X * MAX_TILES_Y
	public const int MAX_BUFF_ENTITIES = MAX_ACTIVE_BUILDING_ENTITIES; // Is it true that the only buffs come from buildings? It is also then implied here a building can self-buff or it would be -1.
	public const int MAX_BUFFS_A_BUILDING_GIVES = 8;

	// Gameplay Constants
	public const double TRAINING_COST_INCREASE_FACTOR = 1.2d; // NOTE: Used for both resource cost and time at the moment
	public const double STARTING_WHEAT = 100d;
	public const double ASCEND_ORE_REDUCTION_FACTOR = 2d;
	public const double BASE_TILE_COST = 1d;
	public const double EXPONENTIAL_BASE_TILE_COST = 10d;
	public const double ASECEND_COEFFICIENT = 0.1d;
	public const float MYTH_RESEARCH_TIME = 7f;
	public const double GODS_SPIRIT_COST = 1e6;
	public const bool BASE_CAN_HAVE_SECOND_MYTH = true;
	public const bool BASE_CAN_HAVE_SECOND_TRADITION_MYTH = false;
	public const int TWENTY_WORKERS = 20;
	public const float DELAY_BETWEEN_AUTO_ADD_WORKER_OVER_TIME = 2 * 3f; // NOTE: By design it's 5 minutes
	public const double BASE_RITUAL_COST = 100f;
	public const int MINIMUM_RITUAL_STEPS = 3;
	public const double RITUAL_TO_RESOURCE_GAIN_SECONDS_FACTOR = 60d;
	public const double BASE_RITUAL_BONUS_COEFFICIENT = 0.1d;
	public const int LARGEST_SET_ACTIVATION_THRESHOLD = 3;
	public const int DIFFERENT_BUILDING_ACTIVATION_THRESHOLD = 3;
	public const float BASE_RITUAL_COOLDOWN = 10f;
	public const int BASE_TILE_SIGHT_RADIUS = 1;

	// UI Constants
	public const int MAX_BUFF_GAIN_OR_REMOVE_ANIMATION_ENTITIES_PER_BUILDING = 8;
	public const int MAX_BUFF_GAIN_OR_REMOVE_RESOURCE_ASSETS_PER_BUFF_ANIMATION = 4;
	public const float DELAY_BETWEEN_ADD_REMOVE_BUFF_ANIMATIONS = 0.4f;
	public const int MAX_RITUAL_STEPS = 7; // TODO: 7 is a temporary value, do MAX_ACTIVE_BUILDING_ENTITIES or something like that
	public static readonly Bounds RITUAL_CAMERA_NO_RUBBER_BANDING_BOUNDS = new Bounds(new Vector3(0.5f, (0.1f + 0.7f)/2f), new Vector3(0.8f, 0.7f - 0.1f));
	public const float RITUAL_CAMERA_RUBBER_BANDING_SPEED_COEFFICIENT = 30f;
	public const bool ALLOW_FAR_RITUAL_BACKTRACK_UNDO_ON_INITIAL_TAP = true;
	public const int FIRST_RITUAL_STEP_CANT_BE_BACKTRACK_UNDONE = 1; // NOTE: In other words, you can't swipe away the initial ritual step... // TODO: How do you switch your initial Shrine easily? That should be treated explicitly
	public const int MAX_RITUAL_BACKTRACK_COUNT = 3;
	public const int MAX_RITUAL_BACKTRACK_SQR_MAGNITUDE = 4;
	public const float DURATION_FOR_PRODUCTION_DISPLAY_ANIMATION = 0.15f;
	public const float DURATION_FOR_SHOWING_PRODUCTION_DISPLAY = 3.5f;
	public const float PRODUCTION_DISPLAY_COLLAPSED_TOP_VALUE = 210f / 400f;
	public const float PRODUCTION_DISPLAY_FULLY_OPEN_TOP_VALUE = 32f / 400f;
	public const float PRODUCTION_DISPLAY_FIXED_BOTTOM_VALUE = 122f / 400f;
	public static readonly Vector3 TILE_TOP_LEFT_CORNER_OFFSET_FROM_ORIGIN = new Vector3(-0.5f, -0.5f, 0f);
	public static readonly Vector3 INITIAL_RITUAL_STEP_ANIMATION_DIRECTION = Vector3.up;
	public static readonly Quaternion ROTIATION_OF_180_ABOUT_Z_QUATERNION = Quaternion.AngleAxis(180f, Vector3.back); // 180 degrees about the Z axis
	public const string RITUAL_STEP_ANIMATION_STATE_NAME = "RitualStepAnimation";
	public const int RITUAL_STEP_ANIMATION_LAYER = 0;
	public const float BASE_RITUAL_STEP_ANIMATION_DURATION = 1f;
	public const int MAX_ANTICIPATED_INPUT_INDICATOR_ANMATIONS = 32;
	public const float IMMEDIATE_DISSIPATION_DURATION = -1f;
	public const float QUICK_INPUT_INDICATOR_DISSIPATION_TIME = 0.25f;

	// NOTE: General purpose string constants are in StringHelpers.StringConstants

	public const string TEST_LEVEL_1 = "TestLevel1";
	public const string TEST_LEVEL_2 = "TestLevel2";

	public const string INITIAL_TRAIN_BUTTON_TEXT = "Train";
	public const string TIER_UPGRADE_BUTTON_TEXT = "Upgrade";
	public const string TIER_UPGRADE_MENU_LABEL_TEXT = "Choose an Upgrade"; // NOTE: The intent here is that I expect there to be multiple labels for the menu

	public static readonly Dictionary<string, LevelDataEntity> levelData = new Dictionary<string, LevelDataEntity>()
	{
		{  TEST_LEVEL_1, new LevelDataEntity()
		{
			activeTiles = new List<Vector2Int>() {
				new Vector2Int(STARTING_TILE_OFFSET_X, STARTING_TILE_OFFSET_Y),
				new Vector2Int(STARTING_TILE_OFFSET_X + 1, STARTING_TILE_OFFSET_Y),
				new Vector2Int(STARTING_TILE_OFFSET_X, STARTING_TILE_OFFSET_Y + 1),
				new Vector2Int(STARTING_TILE_OFFSET_X + 1, STARTING_TILE_OFFSET_Y + 1),
			}
		} },
		{  TEST_LEVEL_2, new LevelDataEntity()
		{
			activeTiles = new List<Vector2Int>() {
				new Vector2Int(STARTING_TILE_OFFSET_X + 1, STARTING_TILE_OFFSET_Y + 1), // Center tile at 0 index, used for Camera placement
				new Vector2Int(STARTING_TILE_OFFSET_X, STARTING_TILE_OFFSET_Y),
				new Vector2Int(STARTING_TILE_OFFSET_X + 1, STARTING_TILE_OFFSET_Y),
				new Vector2Int(STARTING_TILE_OFFSET_X + 2, STARTING_TILE_OFFSET_Y),
				new Vector2Int(STARTING_TILE_OFFSET_X, STARTING_TILE_OFFSET_Y + 1),
				new Vector2Int(STARTING_TILE_OFFSET_X + 2, STARTING_TILE_OFFSET_Y + 1),
				new Vector2Int(STARTING_TILE_OFFSET_X, STARTING_TILE_OFFSET_Y + 2),
				new Vector2Int(STARTING_TILE_OFFSET_X + 1, STARTING_TILE_OFFSET_Y + 2),
				new Vector2Int(STARTING_TILE_OFFSET_X + 2, STARTING_TILE_OFFSET_Y + 2),
			}
		} }
	};

	public static readonly List<Vector2Int> fourWayDirections = new List<Vector2Int>()
	{
		Vector2Int.left,
		Vector2Int.up,
		Vector2Int.right,
		Vector2Int.down,
	};

	// NOTE: Not used yet
	public static readonly Vector2Int leftUp = Vector2Int.left + Vector2Int.up;
	public static readonly Vector2Int upRight = Vector2Int.up + Vector2Int.right;
	public static readonly Vector2Int rightDown = Vector2Int.right + Vector2Int.down;
	public static readonly Vector2Int downLeft = Vector2Int.down + Vector2Int.left;

	// NOTE: Not used yet
	public static readonly List<Vector2Int> eightWayMDirections = new List<Vector2Int>()
	{
		Vector2Int.right,
		ValueHolder.upRight,
		Vector2Int.up,
		ValueHolder.leftUp,
		Vector2Int.left,
		ValueHolder.downLeft,
		Vector2Int.down,
		ValueHolder.rightDown
	};

	// NOTE: Not implemented for Self or Global, since that should be handled via code
	public static readonly Dictionary<BuildingStatsScriptableObject.BuffAreaShapeType, List<List<Vector2Int>>> buffAreaShapes = new Dictionary<BuildingStatsScriptableObject.BuffAreaShapeType, List<List<Vector2Int>>>()
	{
		{
			BuildingStatsScriptableObject.BuffAreaShapeType.DiamondWithRadius,
			new List<List<Vector2Int>>()
			{
				new List<Vector2Int>
				{
				},
				new List<Vector2Int>
				{
					new Vector2Int(0, 1),
					new Vector2Int(1, 0),
					new Vector2Int(0, -1),
					new Vector2Int(-1, 0),
				},
				new List<Vector2Int>
				{
					new Vector2Int(-1, -1),
					new Vector2Int(0, -1),
					new Vector2Int(1, -1),
					new Vector2Int(-1, 0),
					// new Vector2Int(0, 0),
					new Vector2Int(1, 0),
					new Vector2Int(-1, 1),
					new Vector2Int(0, 1),
					new Vector2Int(1, 1),
					new Vector2Int(0, 2),
					new Vector2Int(2, 0),
					new Vector2Int(0, -2),
					new Vector2Int(-2, 0),
				},
				new List<Vector2Int>
				{
					// new Vector2Int(-2, -2),
					new Vector2Int(-1, -2),
					new Vector2Int(0, -2),
					new Vector2Int(1, -2),
					// new Vector2Int(2, -2),
					new Vector2Int(-2, -1),
					new Vector2Int(-1, -1),
					new Vector2Int(0, -1),
					new Vector2Int(1, -1),
					new Vector2Int(2, -1),
					new Vector2Int(-2, 0),
					new Vector2Int(-1, 0),
					// new Vector2Int(0, 0),
					new Vector2Int(1, 0),
					new Vector2Int(2, 0),
					new Vector2Int(-2, 1),
					new Vector2Int(-1, 1),
					new Vector2Int(0, 1),
					new Vector2Int(1, 1),
					new Vector2Int(2, 1),
					// new Vector2Int(-2, 2),
					new Vector2Int(-1, 2),
					new Vector2Int(0, 2),
					new Vector2Int(1, 2),
					// new Vector2Int(2, 2),
					new Vector2Int(0, 3),
					new Vector2Int(3, 0),
					new Vector2Int(0, -3),
					new Vector2Int(-3, 0),
				},
				new List<Vector2Int> // TODO: Stop making these manually... radius 4 was easier than expected but come on
				{
					new Vector2Int(-2, -2),
					new Vector2Int(-1, -2),
					new Vector2Int(0, -2),
					new Vector2Int(1, -2),
					new Vector2Int(2, -2),
					new Vector2Int(-2, -1),
					new Vector2Int(-1, -1),
					new Vector2Int(0, -1),
					new Vector2Int(1, -1),
					new Vector2Int(2, -1),
					new Vector2Int(-2, 0),
					new Vector2Int(-1, 0),
					new Vector2Int(0, 0),
					new Vector2Int(1, 0),
					new Vector2Int(2, 0),
					new Vector2Int(-2, 1),
					new Vector2Int(-1, 1),
					new Vector2Int(0, 1),
					new Vector2Int(1, 1),
					new Vector2Int(2, 1),
					new Vector2Int(-2, 2),
					new Vector2Int(-1, 2),
					new Vector2Int(0, 2),
					new Vector2Int(1, 2),
					new Vector2Int(2, 2),
					////////
					new Vector2Int(-1, -3),
					new Vector2Int(0, -3),
					new Vector2Int(1, -3),
					new Vector2Int(-3, -1),
					new Vector2Int(3, -1),
					new Vector2Int(-3, 0),
					new Vector2Int(3, 0),
					new Vector2Int(-3, 1),
					new Vector2Int(3, 1),
					new Vector2Int(-1, 3),
					new Vector2Int(0, 3),
					new Vector2Int(1, 3),
					//////////
					new Vector2Int(0, 4),
					new Vector2Int(4, 0),
					new Vector2Int(0, -4),
					new Vector2Int(-4, 0),
				},
			}
		},
		{
			BuildingStatsScriptableObject.BuffAreaShapeType.SquareWithRadius,
			new List<List<Vector2Int>>()
			{
				new List<Vector2Int>
				{
				},
				new List<Vector2Int>
				{
					new Vector2Int(-1, -1),
					new Vector2Int(0, -1),
					new Vector2Int(1, -1),
					new Vector2Int(-1, 0),
					// new Vector2Int(0, 0),
					new Vector2Int(1, 0),
					new Vector2Int(-1, 1),
					new Vector2Int(0, 1),
					new Vector2Int(1, 1),
				},
				new List<Vector2Int>
				{
					new Vector2Int(-2, -2),
					new Vector2Int(-1, -2),
					new Vector2Int(0, -2),
					new Vector2Int(1, -2),
					new Vector2Int(2, -2),
					new Vector2Int(-2, -1),
					new Vector2Int(-1, -1),
					new Vector2Int(0, -1),
					new Vector2Int(1, -1),
					new Vector2Int(2, -1),
					new Vector2Int(-2, 0),
					new Vector2Int(-1, 0),
					// new Vector2Int(0, 0),
					new Vector2Int(1, 0),
					new Vector2Int(2, 0),
					new Vector2Int(-2, 1),
					new Vector2Int(-1, 1),
					new Vector2Int(0, 1),
					new Vector2Int(1, 1),
					new Vector2Int(2, 1),
					new Vector2Int(-2, 2),
					new Vector2Int(-1, 2),
					new Vector2Int(0, 2),
					new Vector2Int(1, 2),
					new Vector2Int(2, 2),
				},
				new List<Vector2Int>
				{
					new Vector2Int(-3, -3),
					new Vector2Int(-2, -3),
					new Vector2Int(-1, -3),
					new Vector2Int(0, -3),
					new Vector2Int(1, -3),
					new Vector2Int(2, -3),
					new Vector2Int(3, -3),
					new Vector2Int(-3, -2),
					new Vector2Int(-2, -2),
					new Vector2Int(-1, -2),
					new Vector2Int(0, -2),
					new Vector2Int(1, -2),
					new Vector2Int(2, -2),
					new Vector2Int(3, -2),
					new Vector2Int(-3, -1),
					new Vector2Int(-2, -1),
					new Vector2Int(-1, -1),
					new Vector2Int(0, -1),
					new Vector2Int(1, -1),
					new Vector2Int(2, -1),
					new Vector2Int(3, -1),
					new Vector2Int(-3, 0),
					new Vector2Int(-2, 0),
					new Vector2Int(-1, 0),
					// new Vector2Int(0, 0),
					new Vector2Int(1, 0),
					new Vector2Int(2, 0),
					new Vector2Int(3, 0),
					new Vector2Int(-3, 1),
					new Vector2Int(-2, 1),
					new Vector2Int(-1, 1),
					new Vector2Int(0, 1),
					new Vector2Int(1, 1),
					new Vector2Int(2, 1),
					new Vector2Int(3, 1),
					new Vector2Int(-3, 2),
					new Vector2Int(-2, 2),
					new Vector2Int(-1, 2),
					new Vector2Int(0, 2),
					new Vector2Int(1, 2),
					new Vector2Int(2, 2),
					new Vector2Int(3, 2),
					new Vector2Int(-3, 3),
					new Vector2Int(-2, 3),
					new Vector2Int(-1, 3),
					new Vector2Int(0, 3),
					new Vector2Int(1, 3),
					new Vector2Int(2, 3),
					new Vector2Int(3, 3),
				},
			}
		},
		{
			BuildingStatsScriptableObject.BuffAreaShapeType.PlusWithRadius,
			new List<List<Vector2Int>>()
			{
				new List<Vector2Int>
				{
				},
				new List<Vector2Int>
				{
					new Vector2Int(0, 1),
					new Vector2Int(1, 0),
					new Vector2Int(0, -1),
					new Vector2Int(-1, 0),
				},
				new List<Vector2Int>
				{
					new Vector2Int(0, -1),
					new Vector2Int(-1, 0),
					new Vector2Int(1, 0),
					new Vector2Int(0, 1),
					new Vector2Int(0, 2),
					new Vector2Int(2, 0),
					new Vector2Int(0, -2),
					new Vector2Int(-2, 0),
				},
				new List<Vector2Int>
				{
					new Vector2Int(0, -1),
					new Vector2Int(-1, 0),
					new Vector2Int(1, 0),
					new Vector2Int(0, 1),
					new Vector2Int(0, 2),
					new Vector2Int(2, 0),
					new Vector2Int(0, -2),
					new Vector2Int(-2, 0),
					new Vector2Int(0, 3),
					new Vector2Int(3, 0),
					new Vector2Int(0, -3),
					new Vector2Int(-3, 0),
				},
			}
		},
		{
			BuildingStatsScriptableObject.BuffAreaShapeType.XWithRadius,
			new List<List<Vector2Int>>()
			{
				new List<Vector2Int>
				{
				},
				new List<Vector2Int>
				{
					new Vector2Int(1, 1),
					new Vector2Int(1, -1),
					new Vector2Int(-1, -1),
					new Vector2Int(-1, 1),
				},
				new List<Vector2Int>
				{
					new Vector2Int(1, 1),
					new Vector2Int(1, -1),
					new Vector2Int(-1, -1),
					new Vector2Int(-1, 1),
					new Vector2Int(2, 2),
					new Vector2Int(2, -2),
					new Vector2Int(-2, -2),
					new Vector2Int(-2, 2),
				},
				new List<Vector2Int>
				{
					new Vector2Int(1, 1),
					new Vector2Int(1, -1),
					new Vector2Int(-1, -1),
					new Vector2Int(-1, 1),
					new Vector2Int(2, 2),
					new Vector2Int(2, -2),
					new Vector2Int(-2, -2),
					new Vector2Int(-2, 2),
					new Vector2Int(3, 3),
					new Vector2Int(3, -3),
					new Vector2Int(-3, -3),
					new Vector2Int(-3, 3),
				},
			}
		},
		{
			BuildingStatsScriptableObject.BuffAreaShapeType.CustomSomething,
			new List<List<Vector2Int>>()
			{
				new List<Vector2Int>
				{
					new Vector2Int(0, 0)
				}
			}
		},
	};

	// TODO: By design, this implies that buildings must have at least somewhat comparable trained workers progression?
	// TODO: Intended values as of March 26th, 2021 are 25, 100, 200... change back after extensive testing
	public static readonly List<int> buildingTierLevels = new List<int>() { 5, 10, 15 };// new List<int>() { 25, 100, 200 };
	// TODO: Separate out a system that controls and updates max extra workers and knows to check the right flags on a building. see the note below. Also don't use max int below because it overflows when its used.
	public const int BASE_MAX_EXTRA_WORKERS_AT_MAX_TIER = 50; // 500 // NOTE: Design - I considered making max extra workers past the last tier depend on trained workers... but this dependency caused a need for a lot of new updates... so I abandoned it.
	const int BUILDING_TIER_LEVELS_CONST_FOR_INITIALIZATION = 3; // TODO: Why is this 3? It doesn't make sense. What should be "3" is the maximum paths...
	public const int MAX_PARALLEL_BUILDING_TIER_UPGRADE_PATHS = 3; // TODO: These two constants need evaluation... How do we control the value type lists? Should they be arrays with nulls on the sides when the path is missing?
	public const int TIER_WITH_LOCKED_UPGRADE_PATH = 1;
	// public const int HIGHEST_BUILDING_TIER_INDEX = BUILDING_TIER_LEVELS_CONST_FOR_INITIALIZATION;

	// TODO: Make this out of BuildingTierNodes?
	// TODO: The BuildingTierNode should just be defined in the BuildingStats...? But what abou the preqrequisites? Hard to define in that... unless list prototypes are used for the hash sets.

	// NOTE: The [] is probably used in order to control the strict "BUILDING_TIER_LEVELS_CONST_FOR_INITIALIZATION" initialization
	public static readonly Dictionary<BuildingBaseType, List<BuildingAdvancedType?>[]> buildingTierMap = new Dictionary<BuildingBaseType, List<BuildingAdvancedType?>[]>()
	{
		{
			BuildingBaseType.Farm,
			new List<BuildingAdvancedType?>[BUILDING_TIER_LEVELS_CONST_FOR_INITIALIZATION]
			{
				new List<BuildingAdvancedType?> { BuildingAdvancedType.FarmA2, BuildingAdvancedType.FarmB2, BuildingAdvancedType.FarmC2 },
				new List<BuildingAdvancedType?> { BuildingAdvancedType.FarmA3, BuildingAdvancedType.FarmB3, BuildingAdvancedType.FarmC3 },
				new List<BuildingAdvancedType?> { BuildingAdvancedType.FarmA4, BuildingAdvancedType.FarmB4, BuildingAdvancedType.FarmC4 },
			}
		},
		{
			BuildingBaseType.LumberMill,
			new List<BuildingAdvancedType?>[BUILDING_TIER_LEVELS_CONST_FOR_INITIALIZATION]
			{
				new List<BuildingAdvancedType?> { BuildingAdvancedType.LumberMillA2, BuildingAdvancedType.LumberMillB2 },
				new List<BuildingAdvancedType?> { BuildingAdvancedType.LumberMillA3, BuildingAdvancedType.LumberMillB3 },
				new List<BuildingAdvancedType?> { BuildingAdvancedType.LumberMillA4, BuildingAdvancedType.LumberMillB4 },
			}
		},
		{
			BuildingBaseType.House,
			new List<BuildingAdvancedType?>[BUILDING_TIER_LEVELS_CONST_FOR_INITIALIZATION]
			{
				new List<BuildingAdvancedType?> { BuildingAdvancedType.HouseA2, BuildingAdvancedType.HouseB2 },
				new List<BuildingAdvancedType?> { BuildingAdvancedType.HouseA3, BuildingAdvancedType.HouseB3 },
				new List<BuildingAdvancedType?> { BuildingAdvancedType.HouseA4, BuildingAdvancedType.HouseB4 },
			}
		},
	};
}


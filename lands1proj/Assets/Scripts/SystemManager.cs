﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemManager : MonoBehaviour
{
	// Game state Entities
	public GameStateEntity gameStateEntity;
	public InputStateEntity inputStateEntity;
	public LevelEntity levelEntity;
	public GlobalBuffDataEntity globalBuffDataEntity;
	public RitualStateEntity ritualStateEntity;

	// UI Entities
	public ButtonManagerEntity buttonManagerEntity;
	public MenuManagerEntity menuManagerEntity; // NOTE: This is a different approach I'm testing - unlike the buttonManagerEntity, the menuManagerEntity talks about concrete menus
	public ScreenManagerEntity screenManagerEntity;
	public ResourceDisplayEntity resourceDisplayEntity;

	// Game logic Systems
	public LoadLevelSystem loadLevelSystem;
	public BuildClickSystem buildClickSystem;
	public BeforeUIUpdatesSetBuildingTrainingCostsAndActionFlagsSystem beforeUIUpdatesSetBuildingTrainingCostsAndActionFlagsSystem;
	public UpdateBuyButtonEntityStatsSystem updateBuyEntityStatsSystem;
	public ProcessBuyButtonEntityClicksSystem processBuyEntityButtonClicksSystem;
	public ProcessGeneralButtonEntityClicksSystem processGeneralButtonEntityClicksSystem;
	// TODO: Make a calculator for global resource gain bonuses
	public UpdateBuffGiverComponentsForEachBuildingSystem updateBuffGiverComponentsForEachBuildingSystem;
	public UpdateBuffsForEachTileBasedOnLevelEntitySystem updateBuffsForEachTileBasedOnLevelEntitySystem;
	public UpdateApplicableBuffsForEachBuildingBasedOnTileSystem updateApplicableBuffsForEachBuildingBasedOnTileSystem;
	public DefunctUpdateGlobalBuffsSystem updateGlobalBuffsSystem;
	public UpdateResourceGainsOverTimeForEachBuildingBasedOnLevelEntitySystem updateResourceGainsOverTimeForEachBuildingBasedOnLevelEntitySystem; // TODO: This currently also affects the UI, consider breaking out into another system, or keep it simple
	public UpdateResourceGainsOverTimeBasedOnLevelEntitySystem updateResourceGainsOverTimeBasedOnLevelEntitySystem;
	public GainResourcesSystem gainResourcesSystem;
	public PerformBuildingTrainingAndOtherTimedThingsSystem performBuildingTrainingAndOtherTimedThingsSystem;
	public ComputeGameTimeSystem computeGameTimeSystem;
	public BuildingAutoTrainSystem buildingAutoTrainSystem;
	public UpdateGameStateGainsAndCostsSystem updateGameStateGainsAndCostsSystem;
	public PerformAscensionSystem performAscensionSystem;
	public UpdateGameStateAndGlobalBuffsSystem updateGameStateAndGlobalBuffsSystem;
	public UpdateBuildingCountsSystem updateBuildingCountsSystem;
	public ProcessOtherTrainingSideEffectsSystem processOtherTrainingSideEffectsSystem;
	public AddWorkerOverTimeSystem addWorkerOverTimeSystem;
	public SetCanBuildTileBuildingSystem setCanBuildTileBuildingSystem;
	public SetFinalWorkersForBuildingsSystem setFinalWorkersForBuildingsSystem;
	public PerformRitualSystem performRitualSystem;
	public RemoveFogsSystem removeFogsSystem;
	public UpdateValidNextRitualStepsSystem updateValidNextRitualStepsSystem;

	// Game logic and UI systems, when it's straightforward
	public CreateOrUpdateRitualPickupEntityAndAnimationForEachBuildingSystem createOrUpdateRitualPickupEntityAndAnimationForEachBuildingSystem; // TODO: Should this system also set the animation? Probably, so we don't have to set dirty just to do a simple function // This is then also a UI System
	public UpdatePlannedRitualSystem updatePlannedRitualSystem;
	public UpdateRitualStatsAndRitualPickupRowsSystem updateRitualStatsAndRitualPickupRowsSystem; // TODO: This system is responsible for a lot

	// UI Systems
	public SetInitialBuildingAssetsSystem setInitialBuildingAssetsSystem;
	public SetInitialMythAssetsSystem setInitialMythAssetsSystem;
	public TileMenuClickSystem tileMenuClickSystem;
	public BuildingAndOtherTimedThingsClickSystem buildingAndOtherTimedThingsClickSystem;
	public UpdateBuyButtonEntityDisplaySystem updateBuyEntityButtonDisplaySystem;
	public UpdateResourceDisplayBasedOnGameStateSystem updateResourceDisplayBasedOnGameStateSystem;
	public UpdateBuildingTrainedWorkerAndOtherTimedThingsDisplaySystem updateBuildingTrainedWorkerAndOtherTimedThingsDisplaySystem;
	public UpdateBuildingTrainingAndOtherTimedThingsProgressSystem updateBuildingTrainingAndOtherTimedThingsProgressSystem;
	public PlayerInputSystem playerInputSystem;
	public CameraMovementSystem cameraMovementSystem;
	public BuyTileSystem buyTileSystem;
	public UpdateLevelForBuyTileSystem updateLevelForBuyTileSystem;
	public ShowHideMenusAndSomeButtonsSystem showHideMenusAndSomeButtonsSystem;
	public ShowHideBuildingProductionDisplaySystem showHideBuildingProductionDisplaySystem;
	// public UpdateBuildingButtonsBasedOnBuildingStateSystem updateBuildingButtonsBasedOnBuildingStateSystem;
	public ProcessBuildingButtonClicksThatCauseScreensSystem processBuildingButtonClicksThatCauseScreensSystem;
	public CreateApplicableBuffAnimationsForEachBuildingSystem createApplicableBuffAnimationsForEachBuildingSystem;
	public FloatUpInWorldSystem floatUpInWorldSystem;
	public HoverPickupInWorldSystem hoverPickupInWorldSystem;
	public ProcessPlayerWorldTapRitualInputSystem processPlayerWorldTapRitualInputSystem;
	public TransitionUIWhenWorldInputTypeChangesSystem transitionUIWhenWorldInputTypeChangesSystem;
	public UpdateRitualPickupAnimationsSystem updateRitualPickupAnimationsSystem;
	public UpdateBuildingTierUpgradeScreenSystem updateBuildingTierUpgradeScreenSystem;
	public DissipationSystem dissipationSystem;

	// Prefabs
	public TileEntity tileEntityPrefab;
	public BuildingEntity buildingEntityPrefab;
	public BuyTileOnLevelEntity buyTileOnLevelEntityPrefab;
	public MythButtonEntity mythButtonEntityPrefab;
	public BuildingTierUpgradeButtonEntity buildingTierUpgradeButtonEntityPrefab;
	public RitualPickupAnimationEntity ritualPickupAnimationEntityPrefab;
	public RitualPickupAnimationPanelEntity ritualPickupAnimationPanelEntityPrefab;
	public RitualStepAnimationEntity ritualStepAnimationEntityPrefab;
	public InputIndicatorAnimationEntity ritualValidNextStepIndicatorAnimationEntityPrefab;
	public InputIndicatorAnimationEntity invalidInputIndicatorAnimationEntityPrefab;
	public GameObject fullFogEntityPrefab;
	public GameObject partialFogEntityPrefab;
	public GameObject inputIndicatorFogEntityPrefab;

	public BuildingAssetsDataScriptableObject[] editorBuildingAssetsDataScriptableObjects;
	public Dictionary<BuildingAdvancedType, BuildingAssetsDataScriptableObject> buildingAssetsDataScriptableObjects;
	public BuildingStatsScriptableObject[] editorBuildingStatsScriptableObjects;
	public Dictionary<BuildingAdvancedType, BuildingStatsScriptableObject> buildingStatsScriptableObjects;
	public MythAssetsDataScriptableObject[] editorMythAssetsDataScriptableObjects;
	public Dictionary<MythType, MythAssetsDataScriptableObject> mythAssetsDataScriptableObjects;
	public BuffGainOrRemoveAnimationEntity[] editorBuffGainOrRemoveAnimationEntities;
	public Dictionary<BuffGainOrRemoveAnimationType, BuffGainOrRemoveAnimationEntity> buffGainOrRemoveAnimationEntities;
	public BuffGainOrRemoveAnimationControllerData[] editorBuffGainOrRemoveAnAnimationControllers; // TODO: There's a typo here, extra "An", but fixing it would kill the array, so don't do that for now
	public Dictionary<BuffGainOrRemoveAnimationAssetType, RuntimeAnimatorController> buffGainOrRemoveAnimationControllers;
	public RitualPickupAnimationControllerData[] editorRitualPickupAnimationControllers;
	public Dictionary<RitualPickupType, RuntimeAnimatorController> ritualPickupAnimationControllers; // TODO: this seems wrong, should these instead be prefabs? Or are we going to perform synthesis on ritual pickups... or something? Assess...
	public RitualPickupAnimationControllerData[] editorRitualPickupAnimationPanelControllers;
	public Dictionary<RitualPickupType, RuntimeAnimatorController> ritualPickupAnimationPanelControllers; // TODO: making these separate from the above screen space editorRitualPickupAnimationControllers is retarded. there's gotta be a way to unify this, simplify. The root cause of the problem is that the world space ritual pickups don't use a canvas, and thus have a "Sprite", while the screen space ritual pickup panels use a canvas and ahve an "Image"
	public RitualStepAnimationControllerData[] editorRitualStepAnimationControllers;
	public Dictionary<RitualStepAnimationType, RuntimeAnimatorController> ritualStepAnimationControllers;

	// TODO: Consider why these aren't part of levelEntity... in some sense it helps to not pass the whole level entity to some odd systems but... certainly they are the "level"
	// Objects that help organize entities in the scene
	public GameObject tileEntitiesObject;
	public GameObject buildingEntitiesObject;
	public GameObject buffGainOrRemoveAnimationEntitiesObject;
	public GameObject ritualPickupAnimationEntitiesObject;
	public GameObject ritualStepAnimationEntitiesObject;
	public GameObject inputIndicatorAnimationEntitiesObject;
	public GameObject fullFogEntitiesObject;
	public GameObject partialFogEntitiesObject;
	public GameObject inputIndicatorFogEntitiesObject;

	static void HashSetifyEditorBuildingAdvancedTypeLister(List<EditorBuildingAdvancedTypeLister> editorRequiredBuildings, ref List<HashSet<BuildingAdvancedType>> requiredBuildings)
	{
		requiredBuildings = new List<HashSet<BuildingAdvancedType>>();
		HashSet<BuildingAdvancedType> requiredBuildingsItem;
		foreach (var editorRequiredBuilding in editorRequiredBuildings)
		{
			requiredBuildingsItem = new HashSet<BuildingAdvancedType>();
			requiredBuildingsItem.UnionWith(editorRequiredBuilding.list);
			requiredBuildings.Add(requiredBuildingsItem);
		}
	}

	static void HashSetifyEditorMythTypeLister(List<EditorMythTypeLister> editorRequiredMyths, ref List<HashSet<MythType>> requiredMyths)
	{
		requiredMyths = new List<HashSet<MythType>>();
		HashSet<MythType> requiredMythsItem;
		foreach (var editorRequiredBuilding in editorRequiredMyths)
		{
			requiredMythsItem = new HashSet<MythType>();
			requiredMythsItem.UnionWith(editorRequiredBuilding.list);
			requiredMyths.Add(requiredMythsItem);
		}
	}

	public static void DictionerifyEditorArraysForLevelData(
		BuildingAssetsDataScriptableObject[] editorBuildingAssetsDataScriptableObjects,
		ref Dictionary<BuildingAdvancedType, BuildingAssetsDataScriptableObject> buildingAssetsDataScriptableObjects,
		BuildingStatsScriptableObject[] editorBuildingStatsScriptableObjects,
		ref Dictionary<BuildingAdvancedType, BuildingStatsScriptableObject> buildingStatsScriptableObjects,
		MythAssetsDataScriptableObject[] editorMythAssetsDataScriptableObjects,
		ref Dictionary<MythType, MythAssetsDataScriptableObject> mythAssetsDataScriptableObjects,
		BuffGainOrRemoveAnimationEntity[] editorBuffGainOrRemoveAnimationEntities,
		ref Dictionary<BuffGainOrRemoveAnimationType, BuffGainOrRemoveAnimationEntity> buffGainOrRemoveAnimationEntities,
		BuffGainOrRemoveAnimationControllerData[] editorBuffGainOrRemoveAnAnimationControllers,
		ref Dictionary<BuffGainOrRemoveAnimationAssetType, RuntimeAnimatorController> buffGainOrRemoveAnimationControllers,
		RitualPickupAnimationControllerData[] editorRitualPickupAnimationControllers,
		ref Dictionary<RitualPickupType, RuntimeAnimatorController> ritualPickupAnimationControllers,
		RitualPickupAnimationControllerData[] editorRitualPickupAnimationPanelControllers,
		ref Dictionary<RitualPickupType, RuntimeAnimatorController> ritualPickupAnimationPanelControllers,
		RitualPickupMenuPickupRowEntity[] editorRitualPickupMenuPickupRows,
		ref Dictionary<RitualPickupType, RitualPickupMenuPickupRowEntity> ritualPickupMenuPickupRows,
		RitualStepAnimationControllerData[] editorRitualStepAnimationControllers,
		ref Dictionary<RitualStepAnimationType, RuntimeAnimatorController> ritualStepAnimationControllers
	)
	{
		buildingAssetsDataScriptableObjects = new Dictionary<BuildingAdvancedType, BuildingAssetsDataScriptableObject>();
		foreach (var item in editorBuildingAssetsDataScriptableObjects)
		{
			if (item != null)
				buildingAssetsDataScriptableObjects.Add(item.buildingAdvancedType, item);
		}

		buildingStatsScriptableObjects = new Dictionary<BuildingAdvancedType, BuildingStatsScriptableObject>();
		foreach (var item in editorBuildingStatsScriptableObjects)
		{
			if (item != null)
			{
				// TODO: This seems quite wrong, since we should presumably be cloning the asset... not modifying it. Let's see how it works out
				HashSetifyEditorBuildingAdvancedTypeLister(item.buildingTierRevealPrerequisites.editorRequiredBuildings, ref item.buildingTierRevealPrerequisites.requiredBuildings);
				HashSetifyEditorBuildingAdvancedTypeLister(item.buildingTierUnlockPreqrequisites.editorRequiredBuildings, ref item.buildingTierUnlockPreqrequisites.requiredBuildings);
				HashSetifyEditorMythTypeLister(item.buildingTierRevealPrerequisites.editorRequiredMyths, ref item.buildingTierRevealPrerequisites.requiredMyths);
				HashSetifyEditorMythTypeLister(item.buildingTierUnlockPreqrequisites.editorRequiredMyths, ref item.buildingTierUnlockPreqrequisites.requiredMyths);

				buildingStatsScriptableObjects.Add(item.buildingAdvancedType, item);
			}
		}

		mythAssetsDataScriptableObjects = new Dictionary<MythType, MythAssetsDataScriptableObject>();
		foreach (var item in editorMythAssetsDataScriptableObjects)
		{
			if (item != null)
				mythAssetsDataScriptableObjects.Add(item.mythType, item);
		}

		buffGainOrRemoveAnimationEntities = new Dictionary<BuffGainOrRemoveAnimationType, BuffGainOrRemoveAnimationEntity>();
		foreach (var item in editorBuffGainOrRemoveAnimationEntities)
		{
			if (item != null)
				buffGainOrRemoveAnimationEntities.Add(item.buffGainOrRemoveAnimationType, item);
		}

		buffGainOrRemoveAnimationControllers = new Dictionary<BuffGainOrRemoveAnimationAssetType, RuntimeAnimatorController>();
		foreach (var item in editorBuffGainOrRemoveAnAnimationControllers)
		{
			// NOTE: Gaps are left in the arrays, for easier insertion later. Here, the gap check also involves checking if the animation controller is set
			if (item != null && item.runtimeAnimatorController != null)
				buffGainOrRemoveAnimationControllers.Add(item.buffGainOrRemoveAnimationAssetType, item.runtimeAnimatorController);
		}

		ritualPickupAnimationControllers = new Dictionary<RitualPickupType, RuntimeAnimatorController>();
		foreach (var item in editorRitualPickupAnimationControllers)
		{
			// NOTE: Gaps are left in the arrays, for easier insertion later. Here, the gap check also involves checking if the animation controller is set
			if (item != null && item.runtimeAnimatorController != null)
				ritualPickupAnimationControllers.Add(item.ritualPickupType, item.runtimeAnimatorController);
		}

		ritualPickupAnimationPanelControllers = new Dictionary<RitualPickupType, RuntimeAnimatorController>();
		foreach (var item in editorRitualPickupAnimationPanelControllers)
		{
			// NOTE: Gaps are left in the arrays, for easier insertion later. Here, the gap check also involves checking if the animation controller is set
			if (item != null && item.runtimeAnimatorController != null)
				ritualPickupAnimationPanelControllers.Add(item.ritualPickupType, item.runtimeAnimatorController);
		}

		ritualPickupMenuPickupRows = new Dictionary<RitualPickupType, RitualPickupMenuPickupRowEntity>();
		foreach (var item in editorRitualPickupMenuPickupRows)
		{
			// NOTE: Gaps are left in the arrays, for easier insertion later. Here, the gap check also involves checking if the animation controller is set
			if (item != null)
				ritualPickupMenuPickupRows.Add(item.ritualPickupType, item);
		}

		ritualStepAnimationControllers = new Dictionary<RitualStepAnimationType, RuntimeAnimatorController>();
		foreach (var item in editorRitualStepAnimationControllers)
		{
			// NOTE: Gaps are left in the arrays, for easier insertion later. Here, the gap check also involves checking if the animation controller is set
			if (item != null && item.runtimeAnimatorController != null)
				ritualStepAnimationControllers.Add(item.ritualStepAnimationType, item.runtimeAnimatorController);
		}

		//RitualPickupMenuPickupRowEntity[] editorRitualPickupMenuPickupRows,
		//ref Dictionary<RitualPickupType, RitualPickupMenuPickupRowEntity> ritualPickupMenuPickupRows
	}

	void Start()
	{
		DictionerifyEditorArraysForLevelData(
			editorBuildingAssetsDataScriptableObjects,
			ref buildingAssetsDataScriptableObjects,
			editorBuildingStatsScriptableObjects,
			ref buildingStatsScriptableObjects,
			editorMythAssetsDataScriptableObjects,
			ref mythAssetsDataScriptableObjects,
			editorBuffGainOrRemoveAnimationEntities,
			ref buffGainOrRemoveAnimationEntities,
			editorBuffGainOrRemoveAnAnimationControllers,
			ref buffGainOrRemoveAnimationControllers,
			editorRitualPickupAnimationControllers,
			ref ritualPickupAnimationControllers,
			editorRitualPickupAnimationPanelControllers,
			ref ritualPickupAnimationPanelControllers,
			menuManagerEntity.editorRitualPickupMenuPickupRows,
			ref menuManagerEntity.ritualPickupMenuPickupRows,
			editorRitualStepAnimationControllers,
			ref ritualStepAnimationControllers
			);

		gameStateEntity.Initialize();

		inputStateEntity.Initialize();

		levelEntity.Initialize();

		globalBuffDataEntity.Initialize();

		ritualStateEntity.Initialize();

		buttonManagerEntity.Initialize();

		screenManagerEntity.Initialize();

		// TODO: Set the dirtyUI parameter in the MythGridPanel of the BuyMythsMenu entity programatically, instead of in the unity editor. the ResearchableUIComponent of the MythButtonEntity has this DirtyUI parameter

		loadLevelSystem = new LoadLevelSystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			mainCamera = Camera.main,
			tileEntityPrefab = tileEntityPrefab,
			tileEntitiesObject = tileEntitiesObject,
			fullFogEntityPrefab = fullFogEntityPrefab,
			partialFogEntityPrefab = partialFogEntityPrefab,
			inputIndicatorFogEntityPrefab = inputIndicatorFogEntityPrefab,
			fullFogEntitiesObject = fullFogEntitiesObject,
			partialFogEntitiesObject = partialFogEntitiesObject,
			inputIndicatorFogEntitiesObject = inputIndicatorFogEntitiesObject
		};

		buildClickSystem = new BuildClickSystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			mainCamera = Camera.main,
			buildingEntityPrefab = buildingEntityPrefab,
			buildingAssetsDataScriptableObjects = buildingAssetsDataScriptableObjects,
			buildingStatsScriptableObjects = buildingStatsScriptableObjects,
			buildingEntitiesObject = buildingEntitiesObject,
			globalBuffDataEntity = globalBuffDataEntity
		};

		setInitialBuildingAssetsSystem = new SetInitialBuildingAssetsSystem()
		{
			levelEntity = levelEntity
		};

		setInitialMythAssetsSystem = new SetInitialMythAssetsSystem()
		{
			buttonManagerEntity = buttonManagerEntity,
			globalBuffDataEntity = globalBuffDataEntity
		};

		tileMenuClickSystem = new TileMenuClickSystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity
		};

		buildingAndOtherTimedThingsClickSystem = new BuildingAndOtherTimedThingsClickSystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			buttonManagerEntity = buttonManagerEntity,
			screenManagerEntity = screenManagerEntity
		};

		beforeUIUpdatesSetBuildingTrainingCostsAndActionFlagsSystem = new BeforeUIUpdatesSetBuildingTrainingCostsAndActionFlagsSystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			globalBuffDataEntity = globalBuffDataEntity,
			ritualStateEntity = ritualStateEntity,
			buttonManagerEntity = buttonManagerEntity,
			buildingStatsScriptableObjects = buildingStatsScriptableObjects
		};

		updateBuyEntityStatsSystem = new UpdateBuyButtonEntityStatsSystem()
		{
			gameStateEntity = gameStateEntity,
			buttonManagerEntity = buttonManagerEntity
		};

		updateBuyEntityButtonDisplaySystem = new UpdateBuyButtonEntityDisplaySystem()
		{
			gameStateEntity = gameStateEntity,
			buttonManagerEntity = buttonManagerEntity
		};

		processBuyEntityButtonClicksSystem = new ProcessBuyButtonEntityClicksSystem()
		{
			gameStateEntity = gameStateEntity,
			buttonManagerEntity = buttonManagerEntity
		};

		processGeneralButtonEntityClicksSystem = new ProcessGeneralButtonEntityClicksSystem()
		{
			gameStateEntity = gameStateEntity,
			inputStateEntity = inputStateEntity,
			levelEntity = levelEntity,
			ritualStateEntity = ritualStateEntity,
			buttonManagerEntity = buttonManagerEntity,
			screenManagerEntity = screenManagerEntity
		};

		updateResourceDisplayBasedOnGameStateSystem = new UpdateResourceDisplayBasedOnGameStateSystem()
		{
			gameStateEntity = gameStateEntity,
			resourceDisplayEntity = resourceDisplayEntity,
			menuManagerEntity = menuManagerEntity,
			buttonManagerEntity = buttonManagerEntity,
			screenManagerEntity = screenManagerEntity
		};

		updateResourceGainsOverTimeForEachBuildingBasedOnLevelEntitySystem = new UpdateResourceGainsOverTimeForEachBuildingBasedOnLevelEntitySystem()
		{
			gameStateEntity = gameStateEntity,
			globalBuffDataEntity = globalBuffDataEntity,
			levelEntity = levelEntity
		};

		updateResourceGainsOverTimeBasedOnLevelEntitySystem = new UpdateResourceGainsOverTimeBasedOnLevelEntitySystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity
		};

		gainResourcesSystem = new GainResourcesSystem()
		{
			gameStateEntity = gameStateEntity
		};

		updateBuildingTrainedWorkerAndOtherTimedThingsDisplaySystem = new UpdateBuildingTrainedWorkerAndOtherTimedThingsDisplaySystem()
		{
			levelEntity = levelEntity,
			buttonManagerEntity = buttonManagerEntity
		};

		updateGlobalBuffsSystem = new DefunctUpdateGlobalBuffsSystem()
		{
			globalBuffDataEntity = globalBuffDataEntity,
			levelEntity = levelEntity
		};

		updateBuffGiverComponentsForEachBuildingSystem = new UpdateBuffGiverComponentsForEachBuildingSystem()
		{
			levelEntity = levelEntity,
			globalBuffDataEntity = globalBuffDataEntity
		};

		updateBuffsForEachTileBasedOnLevelEntitySystem = new UpdateBuffsForEachTileBasedOnLevelEntitySystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
		};

		updateApplicableBuffsForEachBuildingBasedOnTileSystem = new UpdateApplicableBuffsForEachBuildingBasedOnTileSystem()
		{
			levelEntity = levelEntity
		};

		createApplicableBuffAnimationsForEachBuildingSystem = new CreateApplicableBuffAnimationsForEachBuildingSystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			buffGainOrRemoveAnimationEntities = buffGainOrRemoveAnimationEntities,
			buffGainOrRemoveAnimationControllers = buffGainOrRemoveAnimationControllers,
			buffGainOrRemoveAnimationEntitiesObject = buffGainOrRemoveAnimationEntitiesObject
		};

		performBuildingTrainingAndOtherTimedThingsSystem = new PerformBuildingTrainingAndOtherTimedThingsSystem()
		{
			mainCamera = Camera.main,
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			globalBuffDataEntity = globalBuffDataEntity,
			buttonManagerEntity = buttonManagerEntity,
			screenManagerEntity = screenManagerEntity,
			mythButtonEntityPrefab = mythButtonEntityPrefab,
			buildingAssetsDataScriptableObjects = buildingAssetsDataScriptableObjects,
			buildingStatsScriptableObjects = buildingStatsScriptableObjects,
			mythAssetsDataScriptableObjects = mythAssetsDataScriptableObjects
		};

		updateBuildingTrainingAndOtherTimedThingsProgressSystem = new UpdateBuildingTrainingAndOtherTimedThingsProgressSystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			ritualStateEntity = ritualStateEntity,
			buttonManagerEntity = buttonManagerEntity
		};

		computeGameTimeSystem = new ComputeGameTimeSystem()
		{
			gameStateEntity = gameStateEntity
		};

		buildingAutoTrainSystem = new BuildingAutoTrainSystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			buildingStatsScriptableObjects = buildingStatsScriptableObjects,
			globalBuffDataEntity = globalBuffDataEntity
		};

		playerInputSystem = new PlayerInputSystem()
		{
			mainCamera = Camera.main,
			gameStateEntity = gameStateEntity,
			inputStateEntity = inputStateEntity
		};

		cameraMovementSystem = new CameraMovementSystem()
		{
			mainCamera = Camera.main,
			gameStateEntity = gameStateEntity,
			inputStateEntity = inputStateEntity,
			ritualStateEntity = ritualStateEntity
		};

		buyTileSystem = new BuyTileSystem()
		{
			mainCamera = Camera.main,
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			tileEntityPrefab = tileEntityPrefab,
			tileEntitiesObject = tileEntitiesObject
		};

		updateLevelForBuyTileSystem = new UpdateLevelForBuyTileSystem()
		{
			mainCamera = Camera.main,
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			buyTileOnLevelEntityPrefab = buyTileOnLevelEntityPrefab,
			tileEntitiesObject = tileEntitiesObject
		};

		updateGameStateGainsAndCostsSystem = new UpdateGameStateGainsAndCostsSystem()
		{
			gameStateEntity = gameStateEntity
		};

		performAscensionSystem = new PerformAscensionSystem()
		{
			gameStateEntity = gameStateEntity,
			buttonManagerEntity = buttonManagerEntity,
			levelEntity = levelEntity
		};

		showHideMenusAndSomeButtonsSystem = new ShowHideMenusAndSomeButtonsSystem()
		{
			gameStateEntity = gameStateEntity,
			inputStateEntity = inputStateEntity,
			levelEntity = levelEntity,
			globalBuffDataEntity = globalBuffDataEntity,
			ritualStateEntity = ritualStateEntity,
			buttonManagerEntity = buttonManagerEntity,
			menuManagerEntity = menuManagerEntity,
			screenManagerEntity = screenManagerEntity
		};

		updateGameStateAndGlobalBuffsSystem = new UpdateGameStateAndGlobalBuffsSystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			globalBuffDataEntity = globalBuffDataEntity,
			buttonManagerEntity = buttonManagerEntity
		};

		updateBuildingCountsSystem = new UpdateBuildingCountsSystem()
		{
			levelEntity = levelEntity
		};

		processOtherTrainingSideEffectsSystem = new ProcessOtherTrainingSideEffectsSystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			globalBuffDataEntity = globalBuffDataEntity
		};

		addWorkerOverTimeSystem = new AddWorkerOverTimeSystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
			globalBuffDataEntity = globalBuffDataEntity
		};

		setCanBuildTileBuildingSystem = new SetCanBuildTileBuildingSystem()
		{
			gameStateEntity = gameStateEntity,
			buildingStatsScriptableObjects = buildingStatsScriptableObjects,
			globalBuffDataEntity = globalBuffDataEntity
		};

		showHideBuildingProductionDisplaySystem = new ShowHideBuildingProductionDisplaySystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity
		};

		//updateBuildingButtonsBasedOnBuildingStateSystem = new UpdateBuildingButtonsBasedOnBuildingStateSystem()
		//{
		//	levelEntity = levelEntity
		//};

		processBuildingButtonClicksThatCauseScreensSystem = new ProcessBuildingButtonClicksThatCauseScreensSystem()
		{
			screenManagerEntity = screenManagerEntity,
			levelEntity = levelEntity
		};

		setFinalWorkersForBuildingsSystem = new SetFinalWorkersForBuildingsSystem()
		{
			levelEntity = levelEntity,
		};

		floatUpInWorldSystem = new FloatUpInWorldSystem()
		{
			gameStateEntity = gameStateEntity,
			levelEntity = levelEntity,
		};

		processPlayerWorldTapRitualInputSystem = new ProcessPlayerWorldTapRitualInputSystem()
		{
			gameStateEntity = gameStateEntity,
			inputStateEntity = inputStateEntity,
			levelEntity = levelEntity,
			ritualStateEntity = ritualStateEntity,
			ritualValidNextStepIndicatorAnimationEntityPrefab = ritualValidNextStepIndicatorAnimationEntityPrefab,
			invalidInputIndicatorAnimationEntityPrefab = invalidInputIndicatorAnimationEntityPrefab,
			inputIndicatorAnimationEntitiesObject = inputIndicatorAnimationEntitiesObject
		};

		createOrUpdateRitualPickupEntityAndAnimationForEachBuildingSystem = new CreateOrUpdateRitualPickupEntityAndAnimationForEachBuildingSystem()
		{
			// TODO: Player input state? Or make a separate system for showing and hiding the pickup objects. Yes, do the latter
			levelEntity = levelEntity,
			ritualStateEntity = ritualStateEntity,
			ritualPickupAnimationControllers = ritualPickupAnimationControllers,
			ritualPickupAnimationEntityPrefab = ritualPickupAnimationEntityPrefab,
			ritualPickupAnimationEntitiesObject = ritualPickupAnimationEntitiesObject,
		};

		updatePlannedRitualSystem = new UpdatePlannedRitualSystem()
		{
			gameStateEntity = gameStateEntity,
			inputStateEntity = inputStateEntity,
			ritualStateEntity = ritualStateEntity,
			ritualStepAnimationControllers = ritualStepAnimationControllers,
			ritualStepAnimationEntityPrefab = ritualStepAnimationEntityPrefab,
			ritualStepAnimationEntitiesObject = ritualStepAnimationEntitiesObject
		};

		updateRitualPickupAnimationsSystem = new UpdateRitualPickupAnimationsSystem()
		{
			ritualStateEntity = ritualStateEntity,
			ritualPickupAnimationPanelControllers = ritualPickupAnimationPanelControllers,
			ritualPickupAnimationPanelEntityPrefab = ritualPickupAnimationPanelEntityPrefab,
			menuManagerEntity = menuManagerEntity
		};

		hoverPickupInWorldSystem = new HoverPickupInWorldSystem()
		{
			gameStateEntity = gameStateEntity,
			inputStateEntity = inputStateEntity,
			levelEntity = levelEntity
		};

		transitionUIWhenWorldInputTypeChangesSystem = new TransitionUIWhenWorldInputTypeChangesSystem()
		{
			gameStateEntity = gameStateEntity,
			inputStateEntity = inputStateEntity,
			ritualPickupAnimationEntitiesObject = ritualPickupAnimationEntitiesObject,
			ritualStepAnimationEntitiesObject = ritualStepAnimationEntitiesObject,
			inputIndicatorFogEntitiesObject = inputIndicatorFogEntitiesObject
		};

		updateRitualStatsAndRitualPickupRowsSystem = new UpdateRitualStatsAndRitualPickupRowsSystem()
		{
			gameStateEntity = gameStateEntity,
			ritualStateEntity = ritualStateEntity,
			menuManagerEntity = menuManagerEntity
		};

		performRitualSystem = new PerformRitualSystem()
		{
			gameStateEntity = gameStateEntity,
			ritualStateEntity = ritualStateEntity,
			menuManagerEntity = menuManagerEntity
		};

		updateBuildingTierUpgradeScreenSystem = new UpdateBuildingTierUpgradeScreenSystem()
		{
			screenManagerEntity = screenManagerEntity,
			buildingTierUpgradeButtonEntityPrefab = buildingTierUpgradeButtonEntityPrefab,
			buildingAssetsDataScriptableObjects = buildingAssetsDataScriptableObjects,
			buttonManagerEntity = buttonManagerEntity
		};

		removeFogsSystem = new RemoveFogsSystem()
		{
			levelEntity = levelEntity,
			ritualStateEntity = ritualStateEntity
		};

		updateValidNextRitualStepsSystem = new UpdateValidNextRitualStepsSystem()
		{
			inputStateEntity = inputStateEntity,
			levelEntity = levelEntity,
			ritualStateEntity = ritualStateEntity
		};

		dissipationSystem = new DissipationSystem()
		{
			gameStateEntity = gameStateEntity,
			inputStateEntity = inputStateEntity
		};
	}

	void FixedUpdate()
	{
		loadLevelSystem.SystemUpdate();

		updateBuyEntityStatsSystem.SystemUpdate();

		levelEntity.buildingEntitiesAreDirtiedClearOnFixedUpdate = false;
	}

	void Update()
    {
		if (Input.GetKeyDown(KeyCode.A))
		{
			gameStateEntity.autoTrain = !gameStateEntity.autoTrain;
		}

		if (Input.GetKeyDown(KeyCode.T))
		{
			gameStateEntity.gameTimeAdded = true;
			gameStateEntity.addedGameTime = 10f; // 10 seconds
		}

		computeGameTimeSystem.SystemUpdate();

		updateGameStateGainsAndCostsSystem.SystemUpdate(); // TODO: This system depends on the updateResourceGainsOverTimeBasedOnLevelEntitySystem... but player actions depend on this system...

		// TODO: If this was in FixedUpdate, I can see a bug occuring here, where a cost is increased, the UI hasn't updated the buttons being unclickable, and a buy attempt is processed... maybe just put everything in update... including the updateBuyEntityStatsSystem
		processBuyEntityButtonClicksSystem.SystemUpdate();
		processGeneralButtonEntityClicksSystem.SystemUpdate();
		performAscensionSystem.SystemUpdate(); // NOTE: The position of this is risky, considering it affects so much. That's why it's placed early. Uses resource gains from previous frame to calculate ascension gold, that seems ok.
		buyTileSystem.SystemUpdate();
		updateLevelForBuyTileSystem.SystemUpdate();
		processBuildingButtonClicksThatCauseScreensSystem.SystemUpdate();

		// NOTE: Should go before tileMenuSystem and setInitialBuildingAssetsSystem, since it also triggers changing the underlying tile
		setCanBuildTileBuildingSystem.SystemUpdate();
		buildClickSystem.SystemUpdate();
		setCanBuildTileBuildingSystem.SystemUpdate(); // TODO: buildClickSystem both depends on and can dirty canBuildTileBuilding, so we do it twice... eww
		buildingAutoTrainSystem.SystemUpdate();
		setInitialBuildingAssetsSystem.SystemUpdate();
		setInitialMythAssetsSystem.SystemUpdate();
		tileMenuClickSystem.SystemUpdate();

		beforeUIUpdatesSetBuildingTrainingCostsAndActionFlagsSystem.SystemUpdate();
		buildingAndOtherTimedThingsClickSystem.SystemUpdate();
		performBuildingTrainingAndOtherTimedThingsSystem.SystemUpdate();
		updateBuildingTrainingAndOtherTimedThingsProgressSystem.SystemUpdate();

		// NOTE: At this point most of the changes to game logic should have occurred. It's time to recompute the game state
		updateGameStateAndGlobalBuffsSystem.SystemUpdate();
		processOtherTrainingSideEffectsSystem.SystemUpdate(); // NOTE: This must come after the global buffs update
		addWorkerOverTimeSystem.SystemUpdate(); // NOTE: This must come after the global buffs update
		setFinalWorkersForBuildingsSystem.SystemUpdate();

		updateBuildingCountsSystem.SystemUpdate();
		updateGlobalBuffsSystem.SystemUpdate(); // TODO: Get rid of global buffs code and approach

		updateBuffGiverComponentsForEachBuildingSystem.SystemUpdate();
		updateBuffsForEachTileBasedOnLevelEntitySystem.SystemUpdate();
		updateApplicableBuffsForEachBuildingBasedOnTileSystem.SystemUpdate();

		updateResourceGainsOverTimeForEachBuildingBasedOnLevelEntitySystem.SystemUpdate();

		updateResourceGainsOverTimeBasedOnLevelEntitySystem.SystemUpdate();

		playerInputSystem.SystemUpdate();
		processPlayerWorldTapRitualInputSystem.SystemUpdate();
		cameraMovementSystem.SystemUpdate();

		updateBuyEntityButtonDisplaySystem.SystemUpdate();
		updateResourceDisplayBasedOnGameStateSystem.SystemUpdate();
		createOrUpdateRitualPickupEntityAndAnimationForEachBuildingSystem.SystemUpdate(); // NOTE: Both game logic and UI updates
		updatePlannedRitualSystem.SystemUpdate();
		updateRitualStatsAndRitualPickupRowsSystem.SystemUpdate();
		updateBuildingTierUpgradeScreenSystem.SystemUpdate();

		performRitualSystem.SystemUpdate();
		gainResourcesSystem.SystemUpdate();
		updateValidNextRitualStepsSystem.SystemUpdate();

		removeFogsSystem.SystemUpdate();

		// Late UI Update
		// NOTE: In theory, all systems that just "read" state, and don't write to it, besides to update UI, should be here
		updateRitualPickupAnimationsSystem.SystemUpdate();
		createApplicableBuffAnimationsForEachBuildingSystem.SystemUpdate(); // NOTE: Should go after updateApplicableBuffsForEachBuildingBasedOnTileSystem // TODO: Should this go to Late UI Update? Player can't really click an animation the same frame it's created
		updateBuildingTrainedWorkerAndOtherTimedThingsDisplaySystem.SystemUpdate(); // Update UI based on both building a building and training at a building
		showHideMenusAndSomeButtonsSystem.SystemUpdate();
		showHideBuildingProductionDisplaySystem.SystemUpdate();
		floatUpInWorldSystem.SystemUpdate();
		dissipationSystem.SystemUpdate();
		hoverPickupInWorldSystem.SystemUpdate();
		transitionUIWhenWorldInputTypeChangesSystem.SystemUpdate();

		////

		gameStateEntity.changedBuyTileStateClearOnUpdate = false;

		levelEntity.tileEntitiesAreDirtiedClearOnUpdate = false;
		levelEntity.buildingEntitiesAreDirtiedClearOnUpdate = false;

		foreach (var tileEntity in levelEntity.activeTileEntities)
		{
			tileEntity.isDirtyClearOnUpdate = false;
			tileEntity.buffEntitiesDirtyClearOnUpdate = false;
		}

		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			buildingEntity.workersOrTierIsDirtyClearOnUpdate = false;
			buildingEntity.trainedWorkersClearOnUpdate = 0;
			buildingEntity.removedBuffGiverComponentsClearOnUpdate.Clear();
		}

		foreach (var mythButtonEntity in buttonManagerEntity.mythBuyingButtonEntities)
		{
			mythButtonEntity.researchableStateComponent.researchIsDirtyClearOnUpdate = false;
		}

		inputStateEntity.inputStateMayCauseStartOfCameraMovement = false;
		inputStateEntity.initialTapThisUpdateClearOnUpdate = false;
		inputStateEntity.worldInputTypeHasChangedClearOnUpdate = false;

		globalBuffDataEntity.activeMythsChangedClearOnUpdate = false;

		ritualStateEntity.isDirtyClearOnUpdate = false;
		ritualStateEntity.validNextRitualStepTilesIsDirtyClearOnUpdate = false;
		ritualStateEntity.ritualEntity.ritualStepEntitiesAddingPickupsClearOnUpdate.Clear();
		ritualStateEntity.ritualEntity.ritualStepEntitiesUpdatingPickupsClearOnUpdate.Clear();
		ritualStateEntity.ritualEntity.ritualStepEntitiesRemovingPickupsClearOnUpdate.Clear();

		screenManagerEntity.buildingTierUpgradeScreenIsDirtyClearOnUpdate = false;
	}
}

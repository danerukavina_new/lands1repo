﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteAndBorderEntitiesColorSetter : MonoBehaviour
{
	[System.Serializable]
	public class ColorsAndSpriteAndBorderEntityPairing
	{
		public Color backgroundColor;
		public Color borderColor;
		public Sprite additionalArtSprite;

		public SpriteAndBoarderUIDataEntity[] spriteAndBoarderUIDataEntities;
	}

	public ColorsAndSpriteAndBorderEntityPairing[] colorAndSpriteRendererPairings;

	void Start()
	{
		foreach (var colorAndSpriteRendererPairing in colorAndSpriteRendererPairings)
		{
			foreach (var spriteAndBoarderUIDataEntity in colorAndSpriteRendererPairing.spriteAndBoarderUIDataEntities)
			{
				spriteAndBoarderUIDataEntity.backgroundImage.color = colorAndSpriteRendererPairing.backgroundColor;
				spriteAndBoarderUIDataEntity.borderSpriteImage.color = colorAndSpriteRendererPairing.borderColor;

				if (colorAndSpriteRendererPairing.additionalArtSprite != null)
				{
					spriteAndBoarderUIDataEntity.additionalArtImage.sprite = colorAndSpriteRendererPairing.additionalArtSprite;

					spriteAndBoarderUIDataEntity.additionalArtImage.gameObject.SetActive(true);
				}
				else
					spriteAndBoarderUIDataEntity.additionalArtImage.gameObject.SetActive(false);

				// TODO: This line of code did nothing for giving building training buttons their color. Yet this "Start" function does color menus... Why is it duplicated with SetInitialBuildingAssetsSystem?
				spriteAndBoarderUIDataEntity.progressBarImage.color = colorAndSpriteRendererPairing.borderColor; 
			}
		}
	}
}


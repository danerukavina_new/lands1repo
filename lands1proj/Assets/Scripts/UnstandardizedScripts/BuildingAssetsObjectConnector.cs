﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BuildingAssetsObjectConnector : MonoBehaviour
{
	// NOTE: The Building's icon is set via the "background"'s additional art image

	public SpriteAndBoarderUIDataEntity background;
	public TextMeshProUGUI buildingNameText;
	public SpriteAndBoarderUIDataEntity trainingButtonSpriteAndBoarder; // TODO: Compare to productionDisplayPanelSpriteAndBoarder. Perhaps trainingButton should be renamed to trainingButtonSpriteAndBoarder, etc.
	public TextMeshProUGUI trainingButtonText;
	public Button openTierUpgradeScreenButton;
	public SpriteAndBoarderUIDataEntity openTierUpgradeScreenButtonSpriteAndBoarder;
	public TextMeshProUGUI openTierUpgradeScreenButtonText;
	public SpriteAndBoarderUIDataEntity buildingDetailsTopRightLensButton;
	public SpriteAndBoarderUIDataEntity buildingLockingTopLeftLockButton;

	public SpriteAndBoarderUIDataEntity[] additionalTrainingButtons;
	public TextMeshProUGUI[] additionalTrainingButtonTexts;

	public TextMeshProUGUI workerCountDisplayText; // TODO: Gone
	public RectTransform productionDisplayPanel; // TODO: What controls this being disabled? Is the variable part of the building assets object connector? Or the building entity? Or a characteristic of the display panel itself?
	public SpriteAndBoarderUIDataEntity productionDisplayPanelSpriteAndBoarder; // TODO: Compare to trainingButton
	public TextMeshProUGUI productionAmountText;
	public TextMeshProUGUI productionDescriptionText;

	public SpriteAndBoarderUIDataEntity lockTrainingButton; // TODO: Gone
	public TextMeshProUGUI lockTrainingButtonText; // TODO: Gone
	public SpriteAndBoarderUIDataEntity demolishBuildingButton; // TODO: Gone
	public TextMeshProUGUI demolishBuildingButtonText; // TODO: Gone

	public Image additionalBackroungArtImage;
}
﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class DEMO_SpiritualityMenuButton_X1A : DEMO_MenuButtonEnumMapping_X1<DEMO_SpiritualityMenuButtonClickAction>
{

};

[System.Serializable]
public class DEMO_SpiritualityMenuButton_X2A : DEMO_MenuButtonEnumMapping_X2<DEMO_SpiritualityMenuButtonClickAction, DEMO_EnumMenuButtonData_X2<DEMO_SpiritualityMenuButtonClickAction>>
{

};

[System.Serializable]
public class DEMO_EnumMenuButtonData_X2B_SPECIFIC : DEMO_EnumMenuButtonData_X2<DEMO_SpiritualityMenuButtonClickAction>
{

};

[System.Serializable]
public class DEMO_SpiritualityMenuButton_X2B : DEMO_MenuButtonEnumMapping_X2<DEMO_SpiritualityMenuButtonClickAction, DEMO_EnumMenuButtonData_X2B_SPECIFIC>
{

};

public class GENERICS_DEMONSTRATION_SpiritualityMenuEntity_NeatTest : MonoBehaviour
{
	// State

	public bool buttonClicked;
	public DEMO_SpiritualityMenuButtonClickAction buttonClickAction;

	public DEMO_MenuButtonEnumMapping<DEMO_SpiritualityMenuButtonClickAction> buttons; // Does not work

	public DEMO_MenuButtonEnumMapping_2 buttons2; // Works
	public DEMO_MenuButtonEnumMapping_2B buttons2b; // Works

	public DEMO_MenuButtonEnumMapping_3<DEMO_SpiritualityMenuButtonClickAction> buttons3; // Does not work

	public DEMO_MenuButtonEnumMapping_4<DEMO_SpiritualityMenuButtonClickAction> buttons4; // Does not work

	public DEMO_MenuButtonEnumMapping_X1<DEMO_SpiritualityMenuButtonClickAction> buttonsX1; // Does not work

	public DEMO_SpiritualityMenuButton_X1A buttonsX1A; // Does not work

	public float derp;

	public DEMO_MenuButtonEnumMapping_X2<DEMO_SpiritualityMenuButtonClickAction, DEMO_EnumMenuButtonData_X2<DEMO_SpiritualityMenuButtonClickAction>> buttonsX2; // Does not work

	public DEMO_SpiritualityMenuButton_X2A buttonsX2A; // Does not work

	public DEMO_SpiritualityMenuButton_X2B buttonsX2B; // WORKS - remember to use [System.Serializeable]
}

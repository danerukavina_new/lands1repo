﻿//using System;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;

//[System.Serializable]
//public class MenuEntity_DEMOWithButtonRegister<TEnum, TButtonConnector> : MonoBehaviour // NOTE: TButtonConnector required to create the buttonRegistry
//	where TEnum : System.Enum
//{
//	public bool buttonClicked;
//	public TEnum buttonClickAction;

//	public TButtonConnector[] buttonRegistry; // All this is is an array to which you can hook up buttons that are in the menu. In order for menus to have this feature built in from the base class, the above TButtonConnector acrobatics is requried, maybe
//};

//[System.Serializable]
//public class EnumButtonClickMenuConnectComponent_DEMOCompatibleWithButtonRegister<TEnum, TMenu, TButtonConnector> : MonoBehaviour // NOTE: TButtonConnected required in order to be able to set a TMenu that supports a buttonRegistry
//	where TEnum : System.Enum
//	where TMenu : MenuEntity_DEMOWithButtonRegister<TEnum, TButtonConnector> // NOTE: This is required for the button to be able to see the menu in the editor. Why? It's so that a concrete subclass can be made that explicity defines what the Menu is. This seems to be a prereq of it being seen in the editor.
//{
//	public TEnum buttonEnum;
//	public TMenu menu;

//	public void ForwardButtonClickToMenu()
//	{
//		menu.buttonClickAction = buttonEnum;
//		menu.buttonClicked = true;

//		Debug.Log("It worksss, with a button registry even! " + menu.buttonClickAction);
//	}
//}


//// X3C works! Not sure if it can make a "registry", but the buttons themselves can see the menu and link to it, which is more desireable
//public class SpiritualityMenuEntity_DEMOWithButtonRegistry : MenuEntity_DEMOWithButtonRegister<SpiritualityMenuEntity_DEMOWithButtonRegistry.SpiritualityMenuButtonClickAction, SpiritualityMenuEnumButtonConnectorEntity_DEMOCompatibleWithButtonRegister> // TODO: THIS WORKS! At least it creates an array on the menu that can be filled with buttons
//{
//	public enum SpiritualityMenuButtonClickAction
//	{
//		OpenRitualScreen = 20,
//		OpenMythScreen = 30,
//		OpenGodScreen = 40,
//		OpenIdolScreen = 50,
//		RepeatPrevoiusRitual = 120
//		// Tooltip buttons
//	}

//	// This will already have a "buttonRegistry" from the base class

//	public SpiritualityMenuEnumButtonConnectorEntity_DEMOCompatibleWithButtonRegister[] aMoreManualButtonRegistryNotRequiringTheExtraGenericParam; // NOTE: This is another way to make a button registry in the regular menus
//}

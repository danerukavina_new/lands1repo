﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DEMO_SpiritualityMenuButtonClickAction
{
	OpenRitualScreen = 20,
	OpenMythScreen = 30,
	OpenGodScreen = 40,
	OpenIdolScreen = 50,
	RepeatPrevoiusRitual = 120
	// Tooltip buttons
}

[System.Serializable]
public class DEMO_MenuButtonEnumMapping<T> where T : System.Enum
{
	[System.Serializable]
	public class DEMO_EnumMenuButtonData
	{
		public T type;
		public DEMO_ButtonClickFlagScript button;
	}

	public DEMO_EnumMenuButtonData[] editorEnumToMenuButtons;
	public Dictionary<T, DEMO_ButtonClickFlagScript> enumToMenuButtons;
}

[System.Serializable]
public class DEMO_MenuButtonEnumMapping_2 // This works, but look it uses fixed types
{
	[System.Serializable]
	public class EnumMenuButtonData
	{
		public DEMO_SpiritualityMenuButtonClickAction type;
		public DEMO_ButtonClickFlagScript button;
	}

	public EnumMenuButtonData[] editorEnumToMenuButtons;
	public Dictionary<DEMO_SpiritualityMenuButtonClickAction, DEMO_ButtonClickFlagScript> enumToMenuButtons;
}

[System.Serializable]
public class DEMO_MenuButtonEnumMapping_2B // This works but look, it uses fixed types
{
	[System.Serializable]
	public class EnumMenuButtonData
	{
		public DEMO_SpiritualityMenuButtonClickAction type;
		public DEMO_ButtonClickFlagScript button;
	}

	public List<EnumMenuButtonData> editorEnumToMenuButtons;
	public Dictionary<DEMO_SpiritualityMenuButtonClickAction, DEMO_ButtonClickFlagScript> enumToMenuButtons;
}

[System.Serializable]
public class DEMO_MenuButtonEnumMapping_3<T> where T : System.Enum
{
	[System.Serializable]
	public class DEMO_EnumMenuButtonData
	{
		[SerializeField]
		public T type;
		[SerializeField]
		public DEMO_ButtonClickFlagScript button;
	}

	public List<DEMO_EnumMenuButtonData> editorEnumToMenuButtons;
	public Dictionary<T, DEMO_ButtonClickFlagScript> enumToMenuButtons;
}

[System.Serializable]
public class DEMO_MenuButtonEnumMapping_4<T> where T : System.Enum
{
	[System.Serializable]
	public class DEMO_EnumMenuButtonData<T1> where T1 : System.Enum
	{
		[SerializeField]
		public T type;
		[SerializeField]
		public DEMO_ButtonClickFlagScript button;
	}

	public List<DEMO_EnumMenuButtonData<T>> editorEnumToMenuButtons;
	public Dictionary<T, DEMO_ButtonClickFlagScript> enumToMenuButtons;
}

[System.Serializable]
public class DEMO_EnumMenuButtonData_X1<T> where T : System.Enum
{
	public T type;
	public DEMO_ButtonClickFlagScript button;
}

[System.Serializable]
public class DEMO_MenuButtonEnumMapping_X1<T> where T : System.Enum
{
	public DEMO_EnumMenuButtonData_X1<T>[] editorEnumToMenuButtons;
	public Dictionary<T, DEMO_ButtonClickFlagScript> enumToMenuButtons;
}

[System.Serializable]
public class DEMO_EnumMenuButtonData_X2<T> where T : System.Enum
{
	public T type;
	public DEMO_ButtonClickFlagScript button;
}

[System.Serializable]
public class DEMO_MenuButtonEnumMapping_X2<T, T2> where T : System.Enum
{
	public T2[] editorEnumToMenuButtons;
	public Dictionary<T, DEMO_ButtonClickFlagScript> enumToMenuButtons;
}



public class DEMO_ButtonClickFlagScript : MonoBehaviour
{
	public bool buttonClicked;

	public void ButtonClicked()
	{
		buttonClicked = true;
	}
}

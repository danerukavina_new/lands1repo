﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class GeneralButtonComponent
{
	public bool buttonClicked;
	public Button button;

	public void ButtonClick()
	{
		buttonClicked = true;
	}
}

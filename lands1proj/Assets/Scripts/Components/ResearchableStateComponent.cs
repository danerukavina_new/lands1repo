﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResearchableStateComponent
{
	public float baseResearchTime; // NOTE: Modified training times and costs are NYI?
	public bool startResearch;
	public bool cancelResearch;
	public float researchStartTime;
	public bool isBeingResearched;
	public bool isResearched;
	public bool researchIsDirtyClearOnUpdate;
}

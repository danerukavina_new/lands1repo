﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class BuyWithCostAndGainButtonComponent
{
	public bool buttonClicked;
	public Button button;
	public TextMeshProUGUI costText1;
	public Image costImage1;
	public TextMeshProUGUI gainText1;
	public Image gainImage1;

	public void ButtonClick()
	{
		buttonClicked = true;
	}
}

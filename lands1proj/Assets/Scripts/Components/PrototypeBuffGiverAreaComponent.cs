﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PrototypeBuffGiverAreaComponent
{
	public BuffType buffType;
	public BuildingStatsScriptableObject.BuffAreaShapeType buffAreaShapeType;
	public int buffAreaRadius;
	public double buffStrengthFactor;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Is this overkill, perhaps buildings can just have the buff entities... which are copied from
[System.Serializable]
public class PrototypeRitualPickupComponent
{
	public RitualPickupType ritualPickupType;
	public double ritualPickupStrengthFactor;
	public Color textColor;
}

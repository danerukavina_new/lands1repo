﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResourceScalingAmountComponent
{
	public ResourceType resourceType;
	public double baseValue;
	public double scalingFactor;
}

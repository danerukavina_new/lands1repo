﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Should this be a structure? Considering the cloning constructor for example...
// TODO: Probably get rid of this when refactoring the global buff approach. See UpdateResourceGainsOverTimeForBuilding, which suffers from it the most
[System.Serializable]
public class ResourceBuffSetComponent
{
	public double extraFoodFromFarmWorkers;
	public double extraWoodFromLumberMillWorkers;
	public double extraStoneFromQuarryWorkers;
	public double extraOreFromMineWorkers;
	public double extraSpiritFromShrineWorkers;
	// NOTE: Uses a constant for the 20
	public double extraGoldFromTwentyMineWorkers;  // TODO: This is different than the other "extras" since it starts at "0" resources per 20 workers. Ugh... is there need for a "base resources per worker" definition which is 1d for everything except these few cases.
	public float farmTrainingSpeedMultiplier; // TODO: How do we accumulate these? Addition? That may be incorrect, since different types will multiply... Does a 0d here mean no multiplier?
	public float lumberMillTrainingSpeedMultiplier;
	public float quarryTrainingSpeedMultiplier;
	public float townHallTrainingSpeedMultiplier;
	public float anyTrainingSpeedMultiplier;
	public double spiritGainMultiplier;
	public double discipleCheapnessMultiplier; // TODO: NYI
	public double extraConditionalGodResource; // TODO: Name this resource

	public bool removeLumberMillWoodCost; // TODO: Not used
	public bool removeQuarryGoldCost;
	public int bonusQuarryWorkersPerRitual; // TODO: NYI
	public int bonusFarmWorkersPerMineWorkerTrained;
	public bool doubleTownHallEffect;
	public bool doubleGuildEffect;
	public float extraRitualDuration; // TODO: NYI
	public int extraRitualAutorepeat; // TODO: NYI
	public bool enableAutoTrainControl;  // TODO: NYI
	public bool specialRitualConditionsExtraRitualTime;  // TODO: NYI
	public bool doubleExperienceGain;  // TODO: NYI
	public bool enableIdolPurchases;  // TODO: NYI
	public double farmWorkerCheapnessPerFarmMultiplier;
	public int addFarmWorkerOverFiveMinutes;
	public int addMineWorkersOverFiveMinutes;

	public ResourceBuffSetComponent()
	{
		extraFoodFromFarmWorkers = 0d;
		extraWoodFromLumberMillWorkers = 0d;
		extraStoneFromQuarryWorkers = 0d;
		extraOreFromMineWorkers = 0d;
		extraSpiritFromShrineWorkers = 0d;
		extraGoldFromTwentyMineWorkers = 0d;
		farmTrainingSpeedMultiplier = 0f;
		lumberMillTrainingSpeedMultiplier = 0f;
		quarryTrainingSpeedMultiplier = 0f;
		townHallTrainingSpeedMultiplier = 0f;
		anyTrainingSpeedMultiplier = 0f;
		spiritGainMultiplier = 0d;
		discipleCheapnessMultiplier = 0d;
		extraConditionalGodResource = 0d;

		removeLumberMillWoodCost = false;
		removeQuarryGoldCost = false;
		bonusQuarryWorkersPerRitual = 0;
		bonusFarmWorkersPerMineWorkerTrained = 0;
		doubleTownHallEffect = false;
		doubleGuildEffect = false;
		extraRitualDuration = 0f;
		extraRitualAutorepeat = 0;
		enableAutoTrainControl = false;
		specialRitualConditionsExtraRitualTime = false;
		doubleExperienceGain = false;
		enableIdolPurchases = false;
		farmWorkerCheapnessPerFarmMultiplier = 0d;
		addFarmWorkerOverFiveMinutes = 0;
		addMineWorkersOverFiveMinutes = 0;
	}

	public ResourceBuffSetComponent(ResourceBuffSetComponent a)
	{
		extraFoodFromFarmWorkers = a.extraFoodFromFarmWorkers;
		extraWoodFromLumberMillWorkers = a.extraWoodFromLumberMillWorkers;
		extraStoneFromQuarryWorkers = a.extraStoneFromQuarryWorkers;
		extraOreFromMineWorkers = a.extraOreFromMineWorkers;
		extraSpiritFromShrineWorkers = a.extraSpiritFromShrineWorkers;
		extraGoldFromTwentyMineWorkers = a.extraGoldFromTwentyMineWorkers;
		farmTrainingSpeedMultiplier = a.farmTrainingSpeedMultiplier;
		lumberMillTrainingSpeedMultiplier = a.lumberMillTrainingSpeedMultiplier;
		quarryTrainingSpeedMultiplier = a.quarryTrainingSpeedMultiplier;
		townHallTrainingSpeedMultiplier = a.townHallTrainingSpeedMultiplier;
		anyTrainingSpeedMultiplier = a.anyTrainingSpeedMultiplier;
		spiritGainMultiplier = a.spiritGainMultiplier;
		discipleCheapnessMultiplier = a.discipleCheapnessMultiplier;
		extraConditionalGodResource = a.extraConditionalGodResource;

		removeLumberMillWoodCost = a.removeLumberMillWoodCost;
		removeQuarryGoldCost = a.removeQuarryGoldCost;
		bonusQuarryWorkersPerRitual = a.bonusQuarryWorkersPerRitual;
		bonusFarmWorkersPerMineWorkerTrained = a.bonusFarmWorkersPerMineWorkerTrained;
		doubleTownHallEffect = a.doubleTownHallEffect;
		doubleGuildEffect = a.doubleGuildEffect;
		extraRitualDuration = a.extraRitualDuration;
		extraRitualAutorepeat = a.extraRitualAutorepeat;
		enableAutoTrainControl = a.enableAutoTrainControl;
		specialRitualConditionsExtraRitualTime = a.specialRitualConditionsExtraRitualTime;
		doubleExperienceGain = a.doubleExperienceGain;
		enableIdolPurchases = a.enableIdolPurchases;
		farmWorkerCheapnessPerFarmMultiplier = a.farmWorkerCheapnessPerFarmMultiplier;
		addFarmWorkerOverFiveMinutes = a.addFarmWorkerOverFiveMinutes;
		addMineWorkersOverFiveMinutes = a.addMineWorkersOverFiveMinutes;
	}

	public static ResourceBuffSetComponent operator +(ResourceBuffSetComponent a, ResourceBuffSetComponent b)
	{
		return new ResourceBuffSetComponent()
		{
			extraFoodFromFarmWorkers = a.extraFoodFromFarmWorkers + b.extraFoodFromFarmWorkers,
			extraWoodFromLumberMillWorkers = a.extraWoodFromLumberMillWorkers + b.extraWoodFromLumberMillWorkers,
			extraStoneFromQuarryWorkers = a.extraStoneFromQuarryWorkers + b.extraStoneFromQuarryWorkers,
			extraOreFromMineWorkers = a.extraOreFromMineWorkers + b.extraOreFromMineWorkers,
			extraSpiritFromShrineWorkers = a.extraSpiritFromShrineWorkers + b.extraSpiritFromShrineWorkers,
			extraGoldFromTwentyMineWorkers = a.extraGoldFromTwentyMineWorkers + b.extraGoldFromTwentyMineWorkers,
			farmTrainingSpeedMultiplier = a.farmTrainingSpeedMultiplier + b.farmTrainingSpeedMultiplier,
			lumberMillTrainingSpeedMultiplier = a.lumberMillTrainingSpeedMultiplier + b.lumberMillTrainingSpeedMultiplier,
			quarryTrainingSpeedMultiplier = a.quarryTrainingSpeedMultiplier + b.quarryTrainingSpeedMultiplier,
			townHallTrainingSpeedMultiplier = a.townHallTrainingSpeedMultiplier + b.townHallTrainingSpeedMultiplier,
			anyTrainingSpeedMultiplier = a.anyTrainingSpeedMultiplier + b.anyTrainingSpeedMultiplier,
			spiritGainMultiplier = a.spiritGainMultiplier + b.spiritGainMultiplier,
			discipleCheapnessMultiplier = a.discipleCheapnessMultiplier + b.discipleCheapnessMultiplier,
			extraConditionalGodResource = a.extraConditionalGodResource + b.extraConditionalGodResource,

			removeLumberMillWoodCost = a.removeLumberMillWoodCost || b.removeLumberMillWoodCost,
			removeQuarryGoldCost = a.removeQuarryGoldCost || b.removeQuarryGoldCost,
			bonusQuarryWorkersPerRitual = a.bonusQuarryWorkersPerRitual + b.bonusQuarryWorkersPerRitual,
			bonusFarmWorkersPerMineWorkerTrained = a.bonusFarmWorkersPerMineWorkerTrained + b.bonusFarmWorkersPerMineWorkerTrained,
			doubleTownHallEffect = a.doubleTownHallEffect || b.doubleTownHallEffect,
			doubleGuildEffect = a.doubleGuildEffect || b.doubleGuildEffect,
			extraRitualDuration = a.extraRitualDuration + b.extraRitualDuration,
			extraRitualAutorepeat = a.extraRitualAutorepeat + b.extraRitualAutorepeat,
			enableAutoTrainControl = a.enableAutoTrainControl || b.enableAutoTrainControl,
			specialRitualConditionsExtraRitualTime = a.specialRitualConditionsExtraRitualTime || b.specialRitualConditionsExtraRitualTime,
			doubleExperienceGain = a.doubleExperienceGain || b.doubleExperienceGain,
			enableIdolPurchases = a.enableIdolPurchases || b.enableIdolPurchases,
			farmWorkerCheapnessPerFarmMultiplier = a.farmWorkerCheapnessPerFarmMultiplier + b.farmWorkerCheapnessPerFarmMultiplier,
			addFarmWorkerOverFiveMinutes = a.addFarmWorkerOverFiveMinutes + b.addFarmWorkerOverFiveMinutes,
			addMineWorkersOverFiveMinutes = a.addMineWorkersOverFiveMinutes + b.addMineWorkersOverFiveMinutes,
		};
	}

	public static bool operator ==(ResourceBuffSetComponent a, ResourceBuffSetComponent b)
	{
		return a.extraFoodFromFarmWorkers == b.extraFoodFromFarmWorkers
			&& a.extraWoodFromLumberMillWorkers == b.extraWoodFromLumberMillWorkers
			&& a.extraStoneFromQuarryWorkers == b.extraStoneFromQuarryWorkers
			&& a.extraOreFromMineWorkers == b.extraOreFromMineWorkers
			&& a.extraSpiritFromShrineWorkers == b.extraSpiritFromShrineWorkers
			&& a.extraGoldFromTwentyMineWorkers == b.extraGoldFromTwentyMineWorkers
			&& a.farmTrainingSpeedMultiplier == b.farmTrainingSpeedMultiplier
			&& a.lumberMillTrainingSpeedMultiplier == b.lumberMillTrainingSpeedMultiplier
			&& a.quarryTrainingSpeedMultiplier == b.quarryTrainingSpeedMultiplier
			&& a.townHallTrainingSpeedMultiplier == b.townHallTrainingSpeedMultiplier
			&& a.anyTrainingSpeedMultiplier == b.anyTrainingSpeedMultiplier
			&& a.spiritGainMultiplier == b.spiritGainMultiplier
			&& a.discipleCheapnessMultiplier == b.discipleCheapnessMultiplier
			&& a.extraConditionalGodResource == b.extraConditionalGodResource

			&& a.removeLumberMillWoodCost == b.removeLumberMillWoodCost
			&& a.removeQuarryGoldCost == b.removeQuarryGoldCost
			&& a.bonusQuarryWorkersPerRitual == b.bonusQuarryWorkersPerRitual
			&& a.bonusFarmWorkersPerMineWorkerTrained == b.bonusFarmWorkersPerMineWorkerTrained
			&& a.doubleTownHallEffect == b.doubleTownHallEffect
			&& a.doubleGuildEffect == b.doubleGuildEffect
			&& a.extraRitualDuration == b.extraRitualDuration
			&& a.extraRitualAutorepeat == b.extraRitualAutorepeat
			&& a.enableAutoTrainControl == b.enableAutoTrainControl
			&& a.specialRitualConditionsExtraRitualTime == b.specialRitualConditionsExtraRitualTime
			&& a.doubleExperienceGain == b.doubleExperienceGain
			&& a.enableIdolPurchases == b.enableIdolPurchases
			&& a.farmWorkerCheapnessPerFarmMultiplier == b.farmWorkerCheapnessPerFarmMultiplier
			&& a.addFarmWorkerOverFiveMinutes == b.addFarmWorkerOverFiveMinutes
			&& a.addMineWorkersOverFiveMinutes == b.addMineWorkersOverFiveMinutes;
	}

	public static bool operator !=(ResourceBuffSetComponent a, ResourceBuffSetComponent b)
	{
		return !(a == b);
	}

	public override bool Equals(object obj)
	{
		ResourceBuffSetComponent resourceBuffSetComponent = obj as ResourceBuffSetComponent;

		if (resourceBuffSetComponent == null)
			return false;
		else
			return this == resourceBuffSetComponent;
	}

	// TODO: What's the point of this... why is C# making me implement this?
	public override int GetHashCode()
	{
		throw new System.Exception("Don't ask for a HashCode for ResourceBuffSetComponent... or implement it.");
	}
}

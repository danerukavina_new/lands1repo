﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HoverPickupInWorldComponentState
{
	Hovering = 20,
	PickupDissipating = 30,
	FullyDissipated = 40,
	UndoPickupMaterializing = 50
}

[System.Serializable]
public class HoverPickupInWorldComponent
{
	// Editor values // TODO: What to call this
	// public TextMeshPro textComponent;
	// public float fontSize;
	public float verticalOffsetFromSource;
	public float dissipationOrMaterializationDuration;
	public float dissipationOrMaterializationWorldMovementSpeed;
	public float dissipationOrMaterializationLingerInPlaceDelay;
	public float oscillationPeriod;
	public float oscillationWorldAmplitude;
	public float oscillationPhaseOffsetWorldScale;
	public Transform objectForAnimatingTransform;

	// State
	public HoverPickupInWorldComponentState hoverPickupInWorldComponentState;
	public bool transitionToNextState;
	public HoverPickupInWorldComponentState nextHoverPickupInWorldComponentState;
	public float dissipationOrMaterializationStartTime;
}
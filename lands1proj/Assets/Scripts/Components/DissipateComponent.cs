﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Consider unifying this and float up in world component... meh
[System.Serializable]
public class DissipateComponent
{
	// TODO: In the future, consider if this should know its parent gameObject. Then the system can simply go through components and delete via the parent reference. Or perhaps thats a trait of a "Component" base class?
	public float dissipationDuration;

	// State
	// [HideInInspector]
	public float dissipationStartAnimationTime;

	// [HideInInspector]
	public bool dissipateMoreQuickly;
	// [HideInInspector]
	public float quickerDissipationDuration;
	// [HideInInspector]
	public float quickerDissipationStartAnimationTime;

	public void DissipateImmediately(float animationTime)
	{
		dissipateMoreQuickly = true;
		quickerDissipationStartAnimationTime = animationTime;
		quickerDissipationDuration = ValueHolder.IMMEDIATE_DISSIPATION_DURATION;
	}

	public void DissipateMoreQuickly(float animationTime, float quickerDuration)
	{
		dissipateMoreQuickly = true;
		quickerDissipationStartAnimationTime = animationTime;
		quickerDissipationDuration = quickerDuration;
	}
}
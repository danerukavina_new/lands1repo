﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// TODO: This works! The key to it is passing the concrete menu type
[System.Serializable]
public class SpiritualityMenuEnumButtonConnectorComponent : BaseEnumButtonClickMenuConnectComponent<SpiritualityMenuEntity.SpiritualityMenuButtonClickAction, SpiritualityMenuEntity> { };

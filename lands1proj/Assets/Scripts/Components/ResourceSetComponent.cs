﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Should this be a structure? Considering the cloning constructor for example...
// TODO: Warnings about equals and gethashcode
[System.Serializable]
public class ResourceSetComponent
{
	public double gold;
	public double food;
	public double wood;
	public double stone;
	public double ore;
	public double spirit;

	public ResourceSetComponent()
	{
		SetToZero();
	}

	public ResourceSetComponent(ResourceSetComponent a)
	{
		gold = a.gold;
		food = a.food;
		wood = a.wood;
		stone = a.stone;
		ore = a.ore;
		spirit = a.spirit;
	}

	public void SetToZero()
	{
		gold = 0d;
		food = 0d;
		wood = 0d;
		stone = 0d;
		ore = 0d;
		spirit = 0d;
	}

	public void CopyValues(ResourceSetComponent a)
	{
		gold = a.gold;
		food = a.food;
		wood = a.wood;
		stone = a.stone;
		ore = a.ore;
		spirit = a.spirit;
	}

	// TODO: Consider moving this logic maybe. Also this makes the component act like a dictionary - maybe just do that? Or it doesn't really matter... and make the fields private.
	// TODO: Consider making this a dictionary of ResourceAmountComponents... then it could also handle dirtying per resource
	double ResourceFromType(ResourceType resourceType)
	{
		switch (resourceType)
		{
			case ResourceType.Gold:
				return gold;
			case ResourceType.Food:
				return food;
			case ResourceType.Wood:
				return wood;
			case ResourceType.Stone:
				return stone;
			case ResourceType.Metal:
				return ore;
			case ResourceType.Spirit:
				return spirit;
			default:
				throw new System.Exception("Unexpected resource type when retrieving resource.");
		}
	}

	public double GetCorrespondingResourceAmount(ResourceAmountComponent resourceAmountComponent)
	{
		return ResourceFromType(resourceAmountComponent.resourceType);
	}

	public void AddResourceAmount(ResourceAmountComponent resourceAmountComponent)
	{
		AddResourceWithType(resourceAmountComponent.resourceType, resourceAmountComponent.amount);
	}

	public void SubtractResourceAmount(ResourceAmountComponent resourceAmountComponent)
	{
		AddResourceWithType(resourceAmountComponent.resourceType, -1d * resourceAmountComponent.amount);
	}

	void AddResourceWithType(ResourceType resourceType, double addedAmount)
	{
		switch (resourceType)
		{
			case ResourceType.Gold:
				gold += addedAmount;
				break;
			case ResourceType.Food:
				food += addedAmount;
				break;
			case ResourceType.Wood:
				wood += addedAmount;
				break;
			case ResourceType.Stone:
				stone += addedAmount;
				break;
			case ResourceType.Metal:
				ore += addedAmount;
				break;
			case ResourceType.Spirit:
				spirit += addedAmount;
				break;
			default:
				throw new System.Exception("Unexpected resource type when adding a resource.");
		}
	}

	public static ResourceSetComponent operator +(ResourceSetComponent a, ResourceSetComponent b)
	{
		return new ResourceSetComponent()
		{
			gold = a.gold + b.gold,
			food = a.food + b.food,
			wood = a.wood + b.wood,
			stone = a.stone + b.stone,
			ore = a.ore + b.ore,
			spirit = a.spirit + b.spirit
		};
	}

	public static ResourceSetComponent operator -(ResourceSetComponent a, ResourceSetComponent b)
	{
		return new ResourceSetComponent()
		{
			gold = a.gold - b.gold,
			food = a.food - b.food,
			wood = a.wood - b.wood,
			stone = a.stone - b.stone,
			ore = a.ore - b.ore,
			spirit = a.spirit - b.spirit
		};
	}

	public static ResourceSetComponent operator *(ResourceSetComponent a, double factor)
	{
		return new ResourceSetComponent()
		{
			gold = a.gold * factor,
			food = a.food * factor,
			wood = a.wood * factor,
			stone = a.stone * factor,
			ore = a.ore * factor,
			spirit = a.spirit * factor
		};
	}

	public static bool operator ==(ResourceSetComponent a, ResourceSetComponent b)
	{
		return
			a.gold == b.gold
			&& a.food == b.food
			&& a.wood == b.wood
			&& a.stone == b.stone
			&& a.ore == b.ore
			&& a.spirit == b.spirit;
	}

	public static bool operator !=(ResourceSetComponent a, ResourceSetComponent b)
	{
		return !(a == b);
	}

	public override bool Equals(object obj)
	{
		ResourceSetComponent resourceSetComponent = obj as ResourceSetComponent;

		if (resourceSetComponent == null)
			return false;
		else
			return this == resourceSetComponent;
	}

	// TODO: What's the point of this... why is C# making me implement this?
	public override int GetHashCode()
	{
		throw new System.Exception("Don't ask for a HashCode for ResourceSetComponent... or implement it.");
	}

	public bool EntirelyGreaterThanOrEqualTo(ResourceSetComponent b)
	{
		// NOTE: The <= 0d resource sets should not go negative. Shouldn't happen, hence this check.
		// TODO: Remove this check or make it more official somewhere. Once removed, the below <= 0d checks should perhaps also be removed.
		if (this.gold < 0d
			|| this.food < 0d
			|| this.wood < 0d
			|| this.stone < 0d
			|| this.ore < 0d
			|| this.spirit < 0d)
			Debug.Log("A resource set component has negative of some resource, shouldn't happen.");

		// TODO: As above
		if (b.gold < 0d
			|| b.food < 0d
			|| b.wood < 0d
			|| b.stone < 0d
			|| b.ore < 0d
			|| b.spirit < 0d)
			Debug.Log("A resource set component has negative of some resource, shouldn't happen.");

		return (b.gold <= 0d || this.gold >= b.gold)
			&& (b.food <= 0d || this.food >= b.food)
			&& (b.wood <= 0d || this.wood >= b.wood)
			&& (b.stone <= 0d || this.stone >= b.stone)
			&& (b.ore <= 0d || this.ore >= b.ore)
			&& (b.spirit <= 0d || this.spirit >= b.spirit);
	}
}

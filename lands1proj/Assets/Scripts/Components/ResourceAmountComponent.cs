﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResourceAmountComponent
{
	public ResourceType resourceType;
	public double amount;
}

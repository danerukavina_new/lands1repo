﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FloatUpInWorldComponent
{
	// Editor values // TODO: What to call this
	// public TextMeshPro textComponent;
	// public float fontSize;
	// public float verticalOffsetFromSource; // Not used
	public float dissipationDuration;
	public float worldMovementSpeed;
	public float lingerInPlaceDelay;
	public Transform objectForAnimatingTransform;

	// State
	public float dissipationStartTime;
}
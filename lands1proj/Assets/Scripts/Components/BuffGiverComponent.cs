﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// NOTE: Serializable for editor viewing
[System.Serializable]
public class BuffGiverComponent
{
	public BuffEntity buffEntity;
	public BuildingStatsScriptableObject.BuffAreaShapeType buffAreaShapeType;
	// TODO: Carefully control when this is updated. Should be very rare. Will this maybe have to be readonly?
	public List<Vector2Int> buffOffsetCoords; // NOTE: Although this data may seem redundant with the inheirt BuildingsStat, it allows us to modify where the building gives buffs based on various effects and feels like the right approach. AND it lets us group up all the data for giving the buffs in one place, rather than ridiculously accessing it from ValueHolder all the time.
}

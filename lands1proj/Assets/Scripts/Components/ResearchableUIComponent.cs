﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class ResearchableUIComponent
{
	public bool dirtyUI;
	public GeneralButtonComponent researchButton;
	public GeneralButtonComponent cancelResearchButton;
	public Image entityIcon;
	public SpriteAndBoarderUIDataEntity background;
	public TextMeshProUGUI descriptionText;
	public TextMeshProUGUI costProgressText;
	public SpriteAndBoarderUIDataEntity researchButtonSpriteAndBoarderEntity; // TODO: Calling this Research and the Building equivalnet thing Train? Whatever, but pointless distinction
	public TextMeshProUGUI researchButtonText;
}

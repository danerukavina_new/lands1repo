﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// NOTE: This system also creates the buff gain or remove animation entities.
public class CreateApplicableBuffAnimationsForEachBuildingSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public Dictionary<BuffGainOrRemoveAnimationType, BuffGainOrRemoveAnimationEntity> buffGainOrRemoveAnimationEntities;
	public Dictionary<BuffGainOrRemoveAnimationAssetType, RuntimeAnimatorController> buffGainOrRemoveAnimationControllers;
	public List<BuffGainOrRemoveAnimationAssetType> resourceAnimationAssetTypes; // TODO: Rename this array, to indicate it's just used to prevent memory allocations
	public GameObject buffGainOrRemoveAnimationEntitiesObject;

	bool buildingsHaveAddOrRemoveQueuesLeftFromPreviousUpdate; // TODO: Is this a good idea, seems like it will cause more bugs than optimization... there should be a system that sets this parameter on the levelEntity... Or maybe make that change now

	public CreateApplicableBuffAnimationsForEachBuildingSystem()
	{
		buildingsHaveAddOrRemoveQueuesLeftFromPreviousUpdate = false;
		resourceAnimationAssetTypes = new List<BuffGainOrRemoveAnimationAssetType>(ValueHolder.MAX_BUFF_GAIN_OR_REMOVE_RESOURCE_ASSETS_PER_BUFF_ANIMATION);
	}

	// TODO: Refactor this hacky function and it's function call. Vision: we should know the incoming buff entity and what it actually succeeded at increasing in the building it targeted.
	static void ResourceAnimationAssetTypeFromResourceProduction(
		ResourceSetComponent resourceProduction,
		BuffType buffType,
		ref List<BuffGainOrRemoveAnimationAssetType> resourceAnimationAssetTypes
		)
	{
		// TODO: This is getting ridiculous. Certainly buff entities need a scriptable object with flags.
		double foodCoeff = 0d, woodCoeff = 0d, stoneCoeff = 0d, oreCoeff = 0d, spiritCoeff = 0d, goldCoeff = 0d;

		switch (buffType)
		{
			case BuffType.FarmFoodProductionPerWorker:
				foodCoeff = 1d;
				break;
			case BuffType.VariousFoodWoodStoneOreProductionPerWorker:
				foodCoeff = woodCoeff = stoneCoeff = oreCoeff = 1d;
				break;
			case BuffType.ShrineSpiritProductionPerWorker:
				spiritCoeff = 1d;
				break;
			case BuffType.FarmFoodProductionPercentagePerWorker:
				foodCoeff = 1d;
				break;
			case BuffType.VariousFoodWoodStoneOreProductionPercentagePerWorker:
				foodCoeff = woodCoeff = stoneCoeff = oreCoeff = 1d;
				break;
			case BuffType.ShrineSpiritPercentageProductionPerWorker:
				spiritCoeff = 1d;
				break;
			default:
				break;
		}

		resourceAnimationAssetTypes.Clear();

		if (resourceProduction.food * foodCoeff > 0d) resourceAnimationAssetTypes.Add(BuffGainOrRemoveAnimationAssetType.FoodIcon);
		if (resourceProduction.wood * woodCoeff > 0d) resourceAnimationAssetTypes.Add(BuffGainOrRemoveAnimationAssetType.WoodIcon);
		if (resourceProduction.stone * stoneCoeff > 0d) resourceAnimationAssetTypes.Add(BuffGainOrRemoveAnimationAssetType.StoneIcon);
		if (resourceProduction.ore * oreCoeff > 0d) resourceAnimationAssetTypes.Add(BuffGainOrRemoveAnimationAssetType.OreIcon);
		if (resourceProduction.spirit * spiritCoeff > 0d) resourceAnimationAssetTypes.Add(BuffGainOrRemoveAnimationAssetType.SpiritIcon);
		if (resourceProduction.gold * goldCoeff > 0d) resourceAnimationAssetTypes.Add(BuffGainOrRemoveAnimationAssetType.GoldIcon);

		if (resourceAnimationAssetTypes.Count == 0)
			throw new System.Exception("Attempting to determine resource animation asset when there is no resource being produced (probably from a building).");
	}

	// TODO: Consider having this encode the dynamic spacing between animation assets in the prefab.
	public static void DetermineBuffGainOrRemoveAnimationData(
		bool isBuffRemoval,
		BuffType buffType,
		BuildingEntity buildingEntityReceivingBuff,
		out BuffGainOrRemoveAnimationType buffGainOrRemoveAnimationType,
		out BuffGainOrRemoveAnimationAssetType symbolAnimationAssetType,
		ref List<BuffGainOrRemoveAnimationAssetType> resourceAnimationAssetTypes
		)
	{
		resourceAnimationAssetTypes.Clear();

		if (isBuffRemoval)
		{
			symbolAnimationAssetType = BuffGainOrRemoveAnimationAssetType.None;
			resourceAnimationAssetTypes.Add(BuffGainOrRemoveAnimationAssetType.GeneralRemoveIcon);
			buffGainOrRemoveAnimationType = BuffGainOrRemoveAnimationType.ResourceOnly; // TODO: Move up
			return;
		}

		// TODO: Assuming this basic / advanced - trained / extra division sticks, consider just making a lookup function for the right asset... since this is just ugly. It should take the BuildingCateogryType and trained/extra/extrahouse
		switch (buffType)
		{
			case BuffType.TrainingCheapnessMultiplierFlat:
				symbolAnimationAssetType = BuffGainOrRemoveAnimationAssetType.DoubleChevronDownIcon;

				switch (buildingEntityReceivingBuff.buildingStatsScriptableObject.buildingCategoryType)
				{
					case BuildingStatsScriptableObject.BuildingCategoryType.Basic:
						resourceAnimationAssetTypes.Add(BuffGainOrRemoveAnimationAssetType.BasicTrainedPersonIcon);
						break;
					case BuildingStatsScriptableObject.BuildingCategoryType.Advanced:
						resourceAnimationAssetTypes.Add(BuffGainOrRemoveAnimationAssetType.AdvancedTrainedPersonIcon);
						break;
					default:
						throw new System.Exception("Unexpected building category type when determining buff animation.");
				}
				break;
			case BuffType.TrainingSpeedMultiplierFlat:
				symbolAnimationAssetType = BuffGainOrRemoveAnimationAssetType.DoubleChevronRightIcon;

				switch (buildingEntityReceivingBuff.buildingStatsScriptableObject.buildingCategoryType)
				{
					case BuildingStatsScriptableObject.BuildingCategoryType.Basic:
						resourceAnimationAssetTypes.Add(BuffGainOrRemoveAnimationAssetType.BasicTrainedPersonIcon);
						break;
					case BuildingStatsScriptableObject.BuildingCategoryType.Advanced:
						resourceAnimationAssetTypes.Add(BuffGainOrRemoveAnimationAssetType.AdvancedTrainedPersonIcon);
						break;
					default:
						throw new System.Exception("Unexpected building category type when determining buff animation.");
				}
				break;
			case BuffType.FarmFoodProductionPerWorker:
			case BuffType.VariousFoodWoodStoneOreProductionPerWorker:
			case BuffType.ShrineSpiritProductionPerWorker:
				symbolAnimationAssetType = BuffGainOrRemoveAnimationAssetType.DoublePlusIcon;
				// TODO: This is obviously hacked together. We want to determine which resources each buff impacted for the building.
				// Scenario to consider: A shrine that gives buildings, that don't innately produce spirit, spirit production - per worker.
				// Further consider, does a temple boost these buildings' spirit productions? If not, then the code below as written fails, since there will be a +spirit buff popped over them.
				// NOTE: The below does successfully have town hall only show a +food buff, even if the farm is producing food and wood innately.
				ResourceAnimationAssetTypeFromResourceProduction(
					buildingEntityReceivingBuff.buildingResourceSetGainsOverTimeComponent + buildingEntityReceivingBuff.buildingStatsScriptableObject.baseResourceProductionCoefficients, 
					buffType,
					ref resourceAnimationAssetTypes
					);
				break;
			case BuffType.FarmExtraWorkerPerFive:
			case BuffType.VariousFoodWoodStoneOreWorkerPerFive:
			case BuffType.ShrineExtraWorkerPerFive:
				symbolAnimationAssetType = BuffGainOrRemoveAnimationAssetType.DoublePlusIcon;

				switch (buildingEntityReceivingBuff.buildingStatsScriptableObject.buildingCategoryType)
				{
					case BuildingStatsScriptableObject.BuildingCategoryType.Basic:
						resourceAnimationAssetTypes.Add(BuffGainOrRemoveAnimationAssetType.BasicExtraPersonIcon);
						break;
					case BuildingStatsScriptableObject.BuildingCategoryType.Advanced:
						resourceAnimationAssetTypes.Add(BuffGainOrRemoveAnimationAssetType.AdvancedExtraPersonIcon);
						break;
					default:
						throw new System.Exception("Unexpected building category type when determining buff animation.");
				}
				break;
			case BuffType.FarmFoodProductionPercentagePerWorker:
			case BuffType.VariousFoodWoodStoneOreProductionPercentagePerWorker:
			case BuffType.ShrineSpiritPercentageProductionPerWorker:
				symbolAnimationAssetType = BuffGainOrRemoveAnimationAssetType.PercentIcon;
				// TODO: This is obviously hacked together. We want to determine which resources each buff impacted for the building.
				// Scenario to consider: A shrine that gives buildings, that don't innately produce spirit, spirit production - per worker.
				// Further consider, does a temple boost these buildings' spirit productions? If not, then the code below as written fails, since there will be a +spirit buff popped over them.
				// NOTE: The below does successfully have town hall only show a +food buff, even if the farm is producing food and wood innately.
				ResourceAnimationAssetTypeFromResourceProduction(
					buildingEntityReceivingBuff.buildingResourceSetGainsOverTimeComponent + buildingEntityReceivingBuff.buildingStatsScriptableObject.baseResourceProductionCoefficients,
					buffType,
					ref resourceAnimationAssetTypes
					);
				break;
			case BuffType.AdditionalExraWorkerLimitForBasicBuildings:
			case BuffType.AdditionalExraWorkerLimitForAdvancedBuildings:
				symbolAnimationAssetType = BuffGainOrRemoveAnimationAssetType.DoublePlusIcon;

				switch (buildingEntityReceivingBuff.buildingStatsScriptableObject.buildingCategoryType)
				{
					case BuildingStatsScriptableObject.BuildingCategoryType.Basic:
						resourceAnimationAssetTypes.Add(BuffGainOrRemoveAnimationAssetType.BasicExtraHouseIcon);
						break;
					case BuildingStatsScriptableObject.BuildingCategoryType.Advanced:
						resourceAnimationAssetTypes.Add(BuffGainOrRemoveAnimationAssetType.AdvancedExtraHouseIcon);
						break;
					default:
						throw new System.Exception("Unexpected building category type when determining buff animation.");
				}
				break;
			default:
				throw new System.Exception("Unexpected buff type when determining buff gain or remove animation type.");
		}

		switch (buffType)
		{
			case BuffType.TrainingCheapnessMultiplierFlat:
			case BuffType.TrainingSpeedMultiplierFlat:
			case BuffType.FarmFoodProductionPerWorker:
			case BuffType.VariousFoodWoodStoneOreProductionPerWorker:
			case BuffType.ShrineSpiritProductionPerWorker:
			case BuffType.FarmExtraWorkerPerFive:
			case BuffType.VariousFoodWoodStoneOreWorkerPerFive:
			case BuffType.ShrineExtraWorkerPerFive:
			case BuffType.FarmFoodProductionPercentagePerWorker:
			case BuffType.VariousFoodWoodStoneOreProductionPercentagePerWorker:
			case BuffType.ShrineSpiritPercentageProductionPerWorker:
			case BuffType.AdditionalExraWorkerLimitForBasicBuildings:
			case BuffType.AdditionalExraWorkerLimitForAdvancedBuildings:
				if (resourceAnimationAssetTypes.Count == 0)
					throw new System.Exception("Unexpected 0 resource animation assets when determining BuffGainOrRemoveAnimationData.");
				else if (resourceAnimationAssetTypes.Count == 1)
					buffGainOrRemoveAnimationType = BuffGainOrRemoveAnimationType.SymbolAndResource;
				else if (resourceAnimationAssetTypes.Count == 2)
					buffGainOrRemoveAnimationType = BuffGainOrRemoveAnimationType.SymbolAndTwoResources;
				else
					buffGainOrRemoveAnimationType = BuffGainOrRemoveAnimationType.SymbolAndThreeOreMoreResources;
				break;
			default:
				throw new System.Exception("Unexpected buff type when determining buff gain or remove animation type.");
		}
	}

	static void AddBuffGainAnimationEntity(
		bool isBuffRemoval,
		BuffType buffType, 
		Dictionary<BuffGainOrRemoveAnimationType, BuffGainOrRemoveAnimationEntity> buffGainOrRemoveAnimationEntities,
		Dictionary<BuffGainOrRemoveAnimationAssetType, RuntimeAnimatorController> buffGainOrRemoveAnimationControllers,
		BuildingEntity buildingEntity, 
		GameStateEntity gameStateEntity, 
		GameObject buffGainOrRemoveAnimationEntitiesObject,
		ref List<BuffGainOrRemoveAnimationAssetType> resourceAnimationAssetTypes
		)
	{
		DetermineBuffGainOrRemoveAnimationData(
			isBuffRemoval,
			buffType, 
			buildingEntity,
			out BuffGainOrRemoveAnimationType buffGainOrRemoveAnimationType,
			out BuffGainOrRemoveAnimationAssetType symbolAnimationType,
			ref resourceAnimationAssetTypes
			);

		BuffGainOrRemoveAnimationEntity buffGainOrRemoveAnimationEntity = GameObject.Instantiate(buffGainOrRemoveAnimationEntities[buffGainOrRemoveAnimationType], buildingEntity.transform.position, Quaternion.identity, buffGainOrRemoveAnimationEntitiesObject.transform);

		if (symbolAnimationType != BuffGainOrRemoveAnimationAssetType.None)
			buffGainOrRemoveAnimationEntity.symbolAnimationController.runtimeAnimatorController = buffGainOrRemoveAnimationControllers[symbolAnimationType];

		// NOTE: The below purposely does not check "null" for the animation controllers - they must be defined for the appropriate buffGainOrRemoveAnimationType
		if (resourceAnimationAssetTypes.Count > buffGainOrRemoveAnimationEntity.resourceAnimationControllers.Length)
			throw new System.Exception("NYI - the ... asset for 5+ resource buff gain animation."); // TODO: Implement the 5 resource buff animation case by having it put "..." in the bottom right quadrant perhaps?
		else
		{
			for (int i = 0; i < resourceAnimationAssetTypes.Count; i++)
			{
				if (resourceAnimationAssetTypes.Count > i)
					buffGainOrRemoveAnimationEntity.resourceAnimationControllers[i].runtimeAnimatorController = buffGainOrRemoveAnimationControllers[resourceAnimationAssetTypes[i]];
			}
		}

		// TODO: Perhaps a state transition should just be suggested here, and have another system handle it? Like for HoverPickupInWorldSystem? Nah...
		buffGainOrRemoveAnimationEntity.floatUpInWorldComponent.dissipationStartTime = gameStateEntity.gameTime;

		buildingEntity.buffGainOrRemoveAnimationEntities.Add(buffGainOrRemoveAnimationEntity);
	}

	static void AddBuffGainAnimationEntity(
		BuffType buffType,
		Dictionary<BuffGainOrRemoveAnimationType, BuffGainOrRemoveAnimationEntity> buffGainOrRemoveAnimationEntities,
		Dictionary<BuffGainOrRemoveAnimationAssetType, RuntimeAnimatorController> buffGainOrRemoveAnimationControllers,
		BuildingEntity buildingEntity,
		GameStateEntity gameStateEntity,
		GameObject buffGainOrRemoveAnimationEntitiesObject,
		ref List<BuffGainOrRemoveAnimationAssetType> resourceAnimationAssetTypes
		)
	{
		AddBuffGainAnimationEntity(
			isBuffRemoval: false,
			buffType, // TODO: This is a hack, but adding a "None" enum value just for this seems unnecessary. It should be an optional function parameter based on isBuffRemoval...
			buffGainOrRemoveAnimationEntities,
			buffGainOrRemoveAnimationControllers,
			buildingEntity,
			gameStateEntity,
			buffGainOrRemoveAnimationEntitiesObject,
			ref resourceAnimationAssetTypes
			);
	}

	static void AddGeneralBuffRemoveAnimationEntity(
		Dictionary<BuffGainOrRemoveAnimationType, BuffGainOrRemoveAnimationEntity> buffGainOrRemoveAnimationEntities,
		Dictionary<BuffGainOrRemoveAnimationAssetType, RuntimeAnimatorController> buffGainOrRemoveAnimationControllers, 
		BuildingEntity buildingEntity, 
		GameStateEntity gameStateEntity, 
		GameObject buffGainOrRemoveAnimationEntitiesObject,
		ref List<BuffGainOrRemoveAnimationAssetType> resourceAnimationAssetTypes
		)
	{
		AddBuffGainAnimationEntity(
			isBuffRemoval: true, 
			BuffType.FarmFoodProductionPerWorker, // TODO: This is a hack, but adding a "None" enum value just for this seems unnecessary. It should be an optional function parameter based on isBuffRemoval...
			buffGainOrRemoveAnimationEntities, 
			buffGainOrRemoveAnimationControllers, 
			buildingEntity, 
			gameStateEntity, 
			buffGainOrRemoveAnimationEntitiesObject,
			ref resourceAnimationAssetTypes
			);
	}

	public override void SystemUpdate()
	{
		// TODO: This needs a loop control variable - do any buildings have anything left in their queue
		if (levelEntity.buildingEntitiesAreDirtiedClearOnUpdate || buildingsHaveAddOrRemoveQueuesLeftFromPreviousUpdate)
		{
			buildingsHaveAddOrRemoveQueuesLeftFromPreviousUpdate = false;

			foreach (var buildingEntity in levelEntity.activeBuildingEntities)
			{
				// TODO: There needs to be some sort of time based guarantee that this queue is emptied promptly... Nice to have...

				if (buildingEntity.isInBuffAnimationCreationDelayFromLastDequeue && gameStateEntity.gameTime > buildingEntity.timeOfLastDequeueFromBuffAddRemoveQueue + ValueHolder.DELAY_BETWEEN_ADD_REMOVE_BUFF_ANIMATIONS)
					buildingEntity.isInBuffAnimationCreationDelayFromLastDequeue = false;

				if (!buildingEntity.isInBuffAnimationCreationDelayFromLastDequeue)
				{
					if (buildingEntity.addedApplicableBuffEntitiesQueue.Count > 0)
					{
						AddBuffGainAnimationEntity(buildingEntity.addedApplicableBuffEntitiesQueue[0].buffType, buffGainOrRemoveAnimationEntities, buffGainOrRemoveAnimationControllers, buildingEntity, gameStateEntity, buffGainOrRemoveAnimationEntitiesObject, ref resourceAnimationAssetTypes);

						buildingEntity.addedApplicableBuffEntitiesQueue.RemoveAt(0); // TODO: Using a list and always dequeueing at the front is... a slight problem. Make an actual queue? Nice to have...
						buildingEntity.isInBuffAnimationCreationDelayFromLastDequeue = true;
						buildingEntity.timeOfLastDequeueFromBuffAddRemoveQueue = gameStateEntity.gameTime;
					}
					// NOTE: This else controls just one add or remove animation being displayed at a time. And all adds being displayed first
					// TODO: Could be improved, to use a joint queue... but would then need data on whether it's an add or remove... Nice to have...
					else if (buildingEntity.removedApplicableBuffEntitiesQueue.Count > 0)
					{
						AddGeneralBuffRemoveAnimationEntity(buffGainOrRemoveAnimationEntities, buffGainOrRemoveAnimationControllers, buildingEntity, gameStateEntity, buffGainOrRemoveAnimationEntitiesObject, ref resourceAnimationAssetTypes);

						buildingEntity.removedApplicableBuffEntitiesQueue.RemoveAt(0); // TODO: Using a list and always dequeueing at the front is... a slight problem. Make an actual queue? Nice to have...
						buildingEntity.isInBuffAnimationCreationDelayFromLastDequeue = true;
						buildingEntity.timeOfLastDequeueFromBuffAddRemoveQueue = gameStateEntity.gameTime;
					}
				}

				if (buildingEntity.addedApplicableBuffEntitiesQueue.Count > 0 || buildingEntity.removedApplicableBuffEntitiesQueue.Count > 0)
					buildingsHaveAddOrRemoveQueuesLeftFromPreviousUpdate = true;
			}
		}
	}
}

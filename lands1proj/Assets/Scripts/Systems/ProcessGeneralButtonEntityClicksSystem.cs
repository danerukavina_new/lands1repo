﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: This system is badly named - it doesn't process all GeneralButtonEntity clicks, just those not accounted for by other systems
// TODO: This system now also processes menu clicks
public class ProcessGeneralButtonEntityClicksSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public InputStateEntity inputStateEntity;
	public LevelEntity levelEntity;
	public RitualStateEntity ritualStateEntity;
	public ButtonManagerEntity buttonManagerEntity;
	public ScreenManagerEntity screenManagerEntity;

	public override void SystemUpdate()
	{
		if (buttonManagerEntity.buyTileButtonEntity.generalButton.buttonClicked)
		{
			buttonManagerEntity.buyTileButtonEntity.generalButton.buttonClicked = false;

			if (gameStateEntity.EnoughGoldToBuyTile())
			{
				gameStateEntity.buyTileState = true;
				gameStateEntity.changedBuyTileStateClearOnUpdate = true;
			}
			// else // TODO: Add better control of the buy tile button?
			// 	Debug.Log("Unexpected click on Ascend when it isn't available.");
		}

		if (buttonManagerEntity.ascendButtonEntity.generalButton.buttonClicked)
		{
			buttonManagerEntity.ascendButtonEntity.generalButton.buttonClicked = false;

			if (levelEntity.CanAscend())
			{
				// TODO: Active confirmation menu for ascend, then process that click instead

				gameStateEntity.ascensionTriggered = true;
				gameStateEntity.triggeredAscensionType = GameStateEntity.AscensionType.Ascend;
			}
			else
				Debug.Log("Unexpected click on Ascend when it isn't available.");
		}

		// TODO: Mastery and Ensrine clicks

		if (buttonManagerEntity.mythsButtonEntity.generalButton.buttonClicked)
		{
			buttonManagerEntity.mythsButtonEntity.generalButton.buttonClicked = false;

			if (gameStateEntity.SpiritualityAndRitualsAndMythsAreAvailable())
			{
				UIHelpers.ShowHideGameObject(screenManagerEntity.mythsScreen.gameObject, true);
			}
			else
				Debug.Log("Unexpected click on Myhts when spirituality isn't available."); // TODO: This is where I decided to not log these interactions as exceptions, since they are minor things. Look to review other exceptions!
		}

		if (buttonManagerEntity.defaultWorldInputTypeButton.generalButton.buttonClicked)
		{
			buttonManagerEntity.defaultWorldInputTypeButton.generalButton.buttonClicked = false;

			// TODO: Implement some UI guards preventing the user from selecting the current input type again
			if (inputStateEntity.worldInputType == InputStateEntity.WorldInputType.DefaultWorldInput)
				throw new System.Exception("NYI - Don't let the player select the same input type as already selected.");

			inputStateEntity.worldInputType = InputStateEntity.WorldInputType.DefaultWorldInput;
			inputStateEntity.worldInputTypeHasChangedClearOnUpdate = true;
		}

		if (buttonManagerEntity.ritualWorldInputTypeButton.generalButton.buttonClicked)
		{
			buttonManagerEntity.ritualWorldInputTypeButton.generalButton.buttonClicked = false;

			// TODO: Implement some UI guards preventing the user from selecting the current input type again
			if (inputStateEntity.worldInputType == InputStateEntity.WorldInputType.RitualWorldInput)
				throw new System.Exception("NYI - Don't let the player select the same input type as already selected.");

			inputStateEntity.worldInputType = InputStateEntity.WorldInputType.RitualWorldInput;
			inputStateEntity.worldInputTypeHasChangedClearOnUpdate = true;
		}


		if (buttonManagerEntity.showRitualDetailsButtonEntity.generalButton.buttonClicked)
		{
			buttonManagerEntity.showRitualDetailsButtonEntity.generalButton.buttonClicked = false;

			UIHelpers.ShowHideGameObject(screenManagerEntity.ritualDetailsScreen.gameObject, true);
		}

		if (screenManagerEntity.mythsScreen.closeScreenClicked)
		{
			screenManagerEntity.mythsScreen.closeScreenClicked = false;

			UIHelpers.ShowHideGameObject(screenManagerEntity.mythsScreen.gameObject, false);
		}

		if (screenManagerEntity.buildingTierUpgradeScreen.closeScreenClicked)
		{
			screenManagerEntity.buildingTierUpgradeScreen.closeScreenClicked = false;

			screenManagerEntity.buildingTierUpgradeScreenShouldBeVisible = false;

			screenManagerEntity.buildingTierUpgradeScreenIsDirtyClearOnUpdate = true;

			// UIHelpers.ShowHideGameObject(screenManagerEntity.buildingTierUpgradeScreen.gameObject, false);
		}

		if (buttonManagerEntity.undoRitualStepButton.generalButton.buttonClicked)
		{
			buttonManagerEntity.undoRitualStepButton.generalButton.buttonClicked = false;

			ritualStateEntity.undoRitualStepsTriggered = 1;
		}

		if (buttonManagerEntity.redoRitualStepButton.generalButton.buttonClicked)
		{
			buttonManagerEntity.redoRitualStepButton.generalButton.buttonClicked = false;

			ritualStateEntity.redoRitualStepsTriggered = 1;
		}

		if (buttonManagerEntity.performRitualButton.generalButton.buttonClicked)
		{
			buttonManagerEntity.performRitualButton.generalButton.buttonClicked = false;

			ritualStateEntity.performRitual = true;
		}
		
		if (buttonManagerEntity.performRitualFromRitualDetailsButton.generalButton.buttonClicked)
		{
			buttonManagerEntity.performRitualFromRitualDetailsButton.generalButton.buttonClicked = false;

			ritualStateEntity.performRitual = true;
		}


		if (screenManagerEntity.ritualDetailsScreen.closeScreenClicked)
		{
			screenManagerEntity.ritualDetailsScreen.closeScreenClicked = false;

			UIHelpers.ShowHideGameObject(screenManagerEntity.ritualDetailsScreen.gameObject, false);
		}
	}
}

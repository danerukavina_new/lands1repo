﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Updates the menus of all the buildings, specifically whether certain building actions can be taken (or are even shown as an option), based on game state.
/// </summary>
public class UpdateResourceGainsOverTimeBasedOnLevelEntitySystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;

	ResourceSetComponent resourceSetGainsOverTimeAccumulatorComponent;

	public override void SystemUpdate()
	{
		if (levelEntity.buildingEntitiesAreDirtiedClearOnUpdate)
		{
			resourceSetGainsOverTimeAccumulatorComponent = new ResourceSetComponent()
			{
				gold = 0d,
				food = 0d,
				wood = 0d,
				stone = 0d,
				ore = 0d,
				spirit = 0d
			};

			foreach (var buildingEntity in levelEntity.activeBuildingEntities)
			{
				resourceSetGainsOverTimeAccumulatorComponent += buildingEntity.buildingResourceSetGainsOverTimeComponent;
			}

			gameStateEntity.playerResourceSetGainsOverTimeComponent = resourceSetGainsOverTimeAccumulatorComponent;
		}
	}
}

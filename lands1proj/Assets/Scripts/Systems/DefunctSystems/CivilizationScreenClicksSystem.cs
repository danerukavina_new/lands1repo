﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes the clicks on the Civilization screen - the click to activate civilizations, to reset them, and presumably the tooltips
/// There is no navigation on the civilization screen
/// Does not control morhping the menu, updating resources, or the costs - that's done by UpdateGameMenusBasedOnGameStateSystem
/// This system may need to come before the UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class CivilizationScreenClicksSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

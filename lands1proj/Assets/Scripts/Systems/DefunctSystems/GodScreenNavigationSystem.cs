﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes clicks that navigate the God screen - flicking between gods and where exactly you can flick to based on game state
/// Does not control morhping the menu, updating the levels of the god abilities, the available resources, or the costs - that's done by UpdateGameMenusBasedOnGameStateSystem
/// This system may need to come before the UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class GodScreenNavigationSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes activation of the ritual, myth, god, and idol screens. Can also activate repeat ritual
/// Does not control morhping the menu, that's done by UpdateGameMenusBasedOnGameStateSystem
/// The state of Spirituality should just be a GameStateEntity component... or PlayerStateEntity?
/// </summary>
public class SpiritualityMenuClickSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

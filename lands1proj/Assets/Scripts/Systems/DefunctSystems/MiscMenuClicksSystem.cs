﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The menu with buttons for achievements, settings, and perhaps info/help. Processes clicks, activates the modals
/// Will be renamed once an appropriate name is devised
/// Does not control morhping the menu, such as highlighting and enabling the buttons - that's done by UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class MiscMenuClicksSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

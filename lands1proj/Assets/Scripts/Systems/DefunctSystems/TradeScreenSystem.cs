﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes the clicks on the Trade Screen - the click to buy resources and the click to buy foreign advisors
/// DOES control the purchase/increment of the resources and foreign advisors, and subtracting gold
/// Does not control morhping the menu, updating the available gold, or incrementing the cost and level of the foreign advisors - that's done by UpdateGameMenusBasedOnGameStateSystem
/// This system may need to come before the UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class TradeScreenSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

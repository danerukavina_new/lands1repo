﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes the clicks on the Test of Faith subscreen - the click to activate starts a test of faith, and presumably the tooltips
/// Does not control navigation on the screen, that's a separate system
/// Does not control morhping the menu, for example which idols are available - that's done by UpdateGameMenusBasedOnGameStateSystem
/// This system may need to come before the UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class TestOfFaithSubscreenClicksSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

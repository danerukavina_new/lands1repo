﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes clicks on the Artifacts screen - the clicks to buy permanent artifact upgrades, and presumably tooltips
/// DOES control the activation of the artifact and artifacts resource subtraction
/// Does not control navigation on the screen, that's a separate system
/// Does not control morhping the menu, updating the levels and costs of the artifact upgrades - that's done by UpdateGameMenusBasedOnGameStateSystem
/// This system may need to come before the UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class ArtifactsScreenClicksSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

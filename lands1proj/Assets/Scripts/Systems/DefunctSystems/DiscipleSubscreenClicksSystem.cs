﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes the clicks on the Disciple subscreen - the click to purchase a disciple, their abilities, and presumably the tooltips
/// Does not control navigation on the screen, that's a separate system
/// Does not control morhping the menu, updating resources, or the costs - that's done by UpdateGameMenusBasedOnGameStateSystem
/// This system may need to come before the UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class DiscipleSubscreenClicksSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

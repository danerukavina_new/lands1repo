﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes clicks that navigate the Test of Faith screen - flicking between the variants of a specific test of faith and where exactly you can flick to based on game state
/// Does not control morhping the menu, updating the indication around completed tests of faith and idols - that's done by UpdateGameMenusBasedOnGameStateSystem
/// This system may need to come before the UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class TestOfFaithNavigationSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

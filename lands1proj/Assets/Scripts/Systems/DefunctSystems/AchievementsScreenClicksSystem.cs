﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes clicks on the Achievements screen - the clicks to claim achievements, and presumably tooltips
/// DOES control the gain of artifact resources from achievements and increasing the level of the achievement claimed
/// Does not control navigation on the screen, that's a separate system
/// Does not control morhping the menu, updating the levels and costs of the achievements - that's done by UpdateGameMenusBasedOnGameStateSystem
/// This system may need to come before the UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class AchievementsScreenClicksSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

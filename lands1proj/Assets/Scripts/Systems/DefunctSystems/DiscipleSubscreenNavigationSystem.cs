﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes clicks that navigate the Disciple screen - flicking between which specific disciple is being displayed
/// Does not control morhping the menu, updating the data about the disciples - that's done by UpdateGameMenusBasedOnGameStateSystem
/// This system may need to come before the UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class DiscipleSubscreenNavigationSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes clicks that navigate the Achievements screen - flicking between the different pages on which you can claim the artifact resource reward from completing achievements
/// Does not control morhping the menu, updating the already claimed achievements, etc. - that's done by UpdateGameMenusBasedOnGameStateSystem
/// This system may need to come before the UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class AchievementsNavigationSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes clicks that navigate the Artifacts screen - flicking between the different pages on which you can spend the artifacts resource
/// Does not control morhping the menu, updating the already bought artifacts upgrades, their costs, etc. - that's done by UpdateGameMenusBasedOnGameStateSystem
/// This system may need to come before the UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class ArtifactsScreenNavigationSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes the clicks on the Myth Screen - the click to activate a myth
/// DOES control the activation of the myth and related Spirit subtraction
/// Does not control morhping the menu, updating available Spirit, or indications of what myths are available - that's done by UpdateGameMenusBasedOnGameStateSystem
/// This system may need to come before the UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class MythScreenClicksSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes clicks on the Ritual Screen, including toggling on and off of ritual options
/// Does not control the execution of the ritual itself, that has a dedicated RitualExecutionSystem
/// Does not control morhping the menu, updating how far along the ritual is, or the costs - that's done by UpdateGameMenusBasedOnGameStateSystem
/// This system may need to come before the UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class RitualScreenClicksSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

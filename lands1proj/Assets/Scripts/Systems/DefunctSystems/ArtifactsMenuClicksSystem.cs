﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes the click that opens the Artifacts Screen
/// Does not control morhping the menu, that's done by UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class ArtifactsMenuClicksSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

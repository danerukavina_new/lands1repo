﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes clicks on the God screen - the click to activate a god, and presumably tooltips
/// Also processes clicks that open the TestOfFaithSubscreen and DiscipleSubscreen
/// DOES control the activating the god itself
/// Does not control navigation on the screen, that's a separate system
/// Does not control morhping the menu, updating the levels of the god abilities, the available resources, or the costs - that's done by UpdateGameMenusBasedOnGameStateSystem
/// This system may need to come before the UpdateGameMenusBasedOnGameStateSystem
/// </summary>
public class GodScreenClicksSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputeGameTimeSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	float deltaTimeAccumulator;

	public override void SystemUpdate()
	{
		deltaTimeAccumulator = 0f;

		if (gameStateEntity.gameTimeAdded)
		{
			gameStateEntity.gameTimeAdded = false;
			deltaTimeAccumulator += gameStateEntity.addedGameTime;

		}

		deltaTimeAccumulator += Time.deltaTime;

		gameStateEntity.updateGameDeltaTime = deltaTimeAccumulator;
		gameStateEntity.gameTime += deltaTimeAccumulator;
	}
}

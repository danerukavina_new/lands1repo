﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateGameStateGainsAndCostsSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	double accumulator;

	public override void SystemUpdate()
	{
		// TODO: Is it legit to use System.Math sometimes? Probably fine

		gameStateEntity.buyTileCostAmount = ValueHolder.BASE_TILE_COST * System.Math.Pow(ValueHolder.EXPONENTIAL_BASE_TILE_COST, gameStateEntity.totalBoughtTiles);

		gameStateEntity.ascendGoldGainAmount = System.Math.Floor(ValueHolder.ASECEND_COEFFICIENT * (gameStateEntity.playerResourceSetGainsOverTimeComponent.food
			+ gameStateEntity.playerResourceSetGainsOverTimeComponent.wood
			+ gameStateEntity.playerResourceSetGainsOverTimeComponent.stone
			+ gameStateEntity.playerResourceSetGainsOverTimeComponent.ore));
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateBuildingTrainingAndOtherTimedThingsProgressSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public RitualStateEntity ritualStateEntity;
	public ButtonManagerEntity buttonManagerEntity;

	float progress;
	Image progressBarImage;

	static void UpdateProgressBarImage(bool conditionForShowingProgressBar, float gameTime, float progressStartTime, float totalProgressTime, Image progressBarImage, bool reverseProgress)
	{
		float progress;

		if (conditionForShowingProgressBar)
		{
			if (totalProgressTime > 0f)
			{
				progress = Mathf.Clamp01((gameTime - progressStartTime) / totalProgressTime);

				if (reverseProgress)
					progress = 1f - progress;

				if (!progressBarImage.gameObject.activeSelf)
					progressBarImage.gameObject.SetActive(true);

				progressBarImage.rectTransform.localScale = new Vector3(progress, progressBarImage.rectTransform.localScale.y, progressBarImage.rectTransform.localScale.z);
			}
		}
	}

	public override void SystemUpdate()
	{
		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			UpdateProgressBarImage(
				buildingEntity.isTraining,
				gameStateEntity.gameTime,
				buildingEntity.trainingStartTime,
				buildingEntity.finalTrainingTime,
				buildingEntity.buildingAssetsObjectConnector.trainingButtonSpriteAndBoarder.progressBarImage,
				reverseProgress: false
				);
		}

		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			UpdateProgressBarImage(
				buildingEntity.isBuildingOrTierUpgrading,
				gameStateEntity.gameTime,
				buildingEntity.buildingOrUpradingStartTime,
				buildingEntity.finalBuildOrTierUpgradeTimes[buildingEntity.finalTierUpgradePathIndex],
				buildingEntity.buildingAssetsObjectConnector.openTierUpgradeScreenButtonSpriteAndBoarder.progressBarImage,
				reverseProgress: false
				);
		}

		foreach (var mythButtonEntity in buttonManagerEntity.mythBuyingButtonEntities)
		{
			UpdateProgressBarImage(
				mythButtonEntity.researchableStateComponent.isBeingResearched,
				gameStateEntity.gameTime,
				mythButtonEntity.researchableStateComponent.researchStartTime,
				mythButtonEntity.researchableStateComponent.baseResearchTime,
				mythButtonEntity.researchableUIComponent.background.progressBarImage,
				reverseProgress: false
				);

			if (mythButtonEntity.researchableStateComponent.isBeingResearched)
			{
				if (!mythButtonEntity.researchableUIComponent.cancelResearchButton.button.gameObject.activeSelf)
					mythButtonEntity.researchableUIComponent.cancelResearchButton.button.gameObject.SetActive(true);

				if (mythButtonEntity.researchableUIComponent.costProgressText.gameObject.activeSelf)
					mythButtonEntity.researchableUIComponent.costProgressText.gameObject.SetActive(false);
			}
		}

		UpdateProgressBarImage(
			ritualStateEntity.ritualIsOnCooldownFromBeingPerformed,
			gameStateEntity.gameTime,
			ritualStateEntity.previouslyPerformedRitualTime,
			ritualStateEntity.basePerformRitualCooldown,
			buttonManagerEntity.performRitualButtonProgressBarImage,
			reverseProgress: true
			);

		UpdateProgressBarImage(
			ritualStateEntity.ritualIsOnCooldownFromBeingPerformed,
			gameStateEntity.gameTime,
			ritualStateEntity.previouslyPerformedRitualTime,
			ritualStateEntity.basePerformRitualCooldown,
			buttonManagerEntity.performRitualFromRitualDetailsButtonProgressBarImage,
			reverseProgress: true
			);
	}
}

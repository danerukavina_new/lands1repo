﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionUIWhenWorldInputTypeChangesSystem : BaseSystem
{
	public GameStateEntity gameStateEntity; // TODO: Probably will be needed in future for timer injection, so just leaving it here
	public InputStateEntity inputStateEntity;
	public GameObject ritualPickupAnimationEntitiesObject;
	public GameObject ritualStepAnimationEntitiesObject;
	public GameObject inputIndicatorFogEntitiesObject;

	void ShowHideRitualObjects(bool shouldShow)
	{
		UIHelpers.ShowHideGameObject(ritualPickupAnimationEntitiesObject, shouldShow);
		UIHelpers.ShowHideGameObject(ritualStepAnimationEntitiesObject, shouldShow);
		UIHelpers.ShowHideGameObject(inputIndicatorFogEntitiesObject, shouldShow);
	}

	public override void SystemUpdate()
	{
		if (inputStateEntity.initialWorldInputTypeTransition)
		{
			inputStateEntity.initialWorldInputTypeTransition = false;

			ShowHideRitualObjects(false);
		}

		if (inputStateEntity.worldInputTypeHasChangedClearOnUpdate)
		{
			if (inputStateEntity.prevWorldInputTypeForControllingTransitions == inputStateEntity.worldInputType)
				throw new System.Exception("Transition UI When World Input Type Changes System asked to transition between the same previous and current world input type.");

			switch (inputStateEntity.prevWorldInputTypeForControllingTransitions)
			{
				case InputStateEntity.WorldInputType.DefaultWorldInput:
					break;
				case InputStateEntity.WorldInputType.RitualWorldInput:
					ShowHideRitualObjects(false);
					break;
				default:
					throw new System.Exception("Unrecognized previous world input type in Transition UI When World Input Type Changes System.");
			}

			inputStateEntity.prevWorldInputTypeForControllingTransitions = inputStateEntity.worldInputType;

			switch (inputStateEntity.worldInputType)
			{
				case InputStateEntity.WorldInputType.DefaultWorldInput:
					break;
				case InputStateEntity.WorldInputType.RitualWorldInput:
					ShowHideRitualObjects(true);
					break;
				default:
					throw new System.Exception("Unrecognized world input type in Transition UI When World Input Type Changes System.");
			}
		}
	}
}

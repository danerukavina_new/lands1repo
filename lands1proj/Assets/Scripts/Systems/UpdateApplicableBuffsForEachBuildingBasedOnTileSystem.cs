﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// NOTE: This system also creates the buff gain or remove animation entities.
public class UpdateApplicableBuffsForEachBuildingBasedOnTileSystem : BaseSystem
{
	public LevelEntity levelEntity;

	TileEntity tileEntity;
	List<BuffEntity> removedBuffEntities;

	public UpdateApplicableBuffsForEachBuildingBasedOnTileSystem()
	{
		removedBuffEntities = new List<BuffEntity>(ValueHolder.MAX_BUFF_ENTITIES);
	}

	static bool BuffIsApplicableToBuilding(BuffType buffType, BuildingEntity buildingEntity)
	{
		// TODO: Consider how to encode this as a characteristic of a BuffEntity... or BuildingEntity. To remove case statements.
		// TODO: Perhaps BuffEntities should have a scriptable object with "matchesIsFarm, matchesIsShrine" etc. and it can just be a long if... but not urgent to fix.
		// So a buff entity could have a checklist of categories of buildings it applies to, such as "isFarm" "isVarious" and the building can have a checklist of categories it falls in, and if there's any match return true
		switch (buffType)
		{
			case BuffType.TrainingCheapnessMultiplierFlat:
				return true;
			case BuffType.TrainingSpeedMultiplierFlat:
				return true;
			case BuffType.FarmFoodProductionPerWorker:
				return buildingEntity.buildingStatsScriptableObject.isFarm;
			case BuffType.VariousFoodWoodStoneOreProductionPerWorker:
				return buildingEntity.buildingStatsScriptableObject.isVariousFoodWoodStoneOreProduction;
			case BuffType.ShrineSpiritProductionPerWorker:
				return buildingEntity.buildingStatsScriptableObject.isShrine;
			case BuffType.FarmExtraWorkerPerFive:
				return buildingEntity.buildingStatsScriptableObject.isFarm;
			case BuffType.VariousFoodWoodStoneOreWorkerPerFive:
				return buildingEntity.buildingStatsScriptableObject.isVariousFoodWoodStoneOreProduction;
			case BuffType.ShrineExtraWorkerPerFive:
				return buildingEntity.buildingStatsScriptableObject.isShrine;
			case BuffType.FarmFoodProductionPercentagePerWorker:
				return buildingEntity.buildingStatsScriptableObject.isFarm;
			case BuffType.VariousFoodWoodStoneOreProductionPercentagePerWorker:
				return buildingEntity.buildingStatsScriptableObject.isVariousFoodWoodStoneOreProduction;
			case BuffType.ShrineSpiritPercentageProductionPerWorker:
				return buildingEntity.buildingStatsScriptableObject.isShrine;
			case BuffType.AdditionalExraWorkerLimitForBasicBuildings:
				return buildingEntity.buildingStatsScriptableObject.buildingCategoryType == BuildingStatsScriptableObject.BuildingCategoryType.Basic;
			case BuffType.AdditionalExraWorkerLimitForAdvancedBuildings:
				return buildingEntity.buildingStatsScriptableObject.buildingCategoryType == BuildingStatsScriptableObject.BuildingCategoryType.Advanced;
			default:
				throw new System.Exception("Unexpected BuffType when trying to set building buff benefit properties.");
		}
	}

	public override void SystemUpdate()
	{
		bool buildingHasChanged = false;

		if (levelEntity.buildingEntitiesAreDirtiedClearOnUpdate)
		{
			foreach (var buildingEntity in levelEntity.activeBuildingEntities)
			{
				// TODO: buffGiverComponentsAddedClearOnUpdate should be used here... to somehow determine whether to add or udpate buffs... or unify/simplify adding and updating
				buildingHasChanged = buildingEntity.workersOrTierIsDirtyClearOnUpdate; // TODO: This seems incomplete at a later glance

				tileEntity = buildingEntity.tileEntity;

				if (buildingEntity.tileEntity.buffEntitiesDirtyClearOnUpdate || buildingHasChanged)
				{
					// Remove applicableBuffEntities that disappeared
					removedBuffEntities.Clear();

					foreach (var incumbentBuffEntity in buildingEntity.applicableBuffEntities)
					{
						if (!tileEntity.buffEntities.Contains(incumbentBuffEntity))
						{
							removedBuffEntities.Add(incumbentBuffEntity);

							buildingEntity.removedApplicableBuffEntitiesQueue.Add(incumbentBuffEntity);
						}
					}

					foreach (var applicableBuffEntityToRemove in removedBuffEntities)
					{
						buildingEntity.applicableBuffEntities.Remove(applicableBuffEntityToRemove);
					}

					// Add applicableBuffEntities that are new
					foreach (var buffEntity in tileEntity.buffEntities)
					{
						if (BuffIsApplicableToBuilding(buffEntity.buffType, buildingEntity))
						{
							if (!buildingEntity.applicableBuffEntities.Contains(buffEntity))
							{
								buildingEntity.addedApplicableBuffEntitiesQueue.Add(buffEntity);

								buildingEntity.applicableBuffEntities.Add(buffEntity);
							}
						}
					}
				}
			}
		}
	}
}

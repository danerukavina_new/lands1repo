﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateBuyButtonEntityDisplaySystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public ButtonManagerEntity buttonManagerEntity;

	public override void SystemUpdate()
	{
		foreach (var buyResourceButtonEntity in buttonManagerEntity.buyResourceButtonEntities)
		{
			// NOTE: This system currently does nothing because at this time there's nothing to update on buttons. When the Trade buttons come in, those do update

			// TODO: give this some dirtying logic instead

			if (buyResourceButtonEntity.buyWithCostAndGainButton.costText1 != null)
			{
				buyResourceButtonEntity.buyWithCostAndGainButton.costText1.text = buyResourceButtonEntity.finalResourceCost.amount.ToNiceString();
			}

			if (buyResourceButtonEntity.buyWithCostAndGainButton.gainText1 != null)
			{
				buyResourceButtonEntity.buyWithCostAndGainButton.gainText1.text = buyResourceButtonEntity.finalResourceGain.amount.ToNiceString();
			}

			UIHelpers.EnableDisableButton(buyResourceButtonEntity.buyWithCostAndGainButton.button, 
				gameStateEntity.playerResourceSetAmountComponent.GetCorrespondingResourceAmount(buyResourceButtonEntity.finalResourceCost) >= buyResourceButtonEntity.finalResourceCost.amount
				);
		}
	}
}

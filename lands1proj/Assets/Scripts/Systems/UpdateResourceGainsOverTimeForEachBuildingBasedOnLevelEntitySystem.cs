﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Updates the menus of all the buildings, specifically whether certain building actions can be taken (or are even shown as an option), based on game state.
/// </summary>
public class UpdateResourceGainsOverTimeForEachBuildingBasedOnLevelEntitySystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public GlobalBuffDataEntity globalBuffDataEntity;
	public LevelEntity levelEntity;

	// TODO: Make this function return something. Or should it return two parameters? Ideally these would just be building properties it sets. And that's all the system does.
	void SetBuildingBuffBenefitProperties(BuildingEntity buildingEntity)
	{
		buildingEntity.flatProductionIncrease = 0d;
		buildingEntity.percentageProductionIncrease = 0d;
		buildingEntity.extraWorkersOverTime = 0d;
		buildingEntity.additionalExtraWorkerLimit = 0d;
		buildingEntity.trainingSpeedMultiplier = 0f;
		buildingEntity.trainingCheapnessMultiplier = 0d;

		foreach (var buffEntityKeyValue in buildingEntity.applicableBuffEntities)
		{
			// TODO: Consider how to encode this matching matrix as data. A dictionary of a hash set? Dictionary<BuffType, HashSet<BaseBuildingType>>
			switch (buffEntityKeyValue.buffType)
			{
				case BuffType.TrainingCheapnessMultiplierFlat:
					buildingEntity.trainingCheapnessMultiplier += buffEntityKeyValue.buffStrength;
					break;
				case BuffType.TrainingSpeedMultiplierFlat:
					buildingEntity.trainingSpeedMultiplier += (float)buffEntityKeyValue.buffStrength; // TODO: Is it ok to do this float conversion? Maybe just track training speed as a double?
					break;
				case BuffType.FarmFoodProductionPerWorker:
					buildingEntity.flatProductionIncrease += buffEntityKeyValue.buffStrength;
					break;
				case BuffType.VariousFoodWoodStoneOreProductionPerWorker:
					buildingEntity.flatProductionIncrease += buffEntityKeyValue.buffStrength;
					break;
				case BuffType.ShrineSpiritProductionPerWorker:
					buildingEntity.flatProductionIncrease += buffEntityKeyValue.buffStrength;
					break;
				case BuffType.FarmExtraWorkerPerFive:
					buildingEntity.extraWorkersOverTime += buffEntityKeyValue.buffStrength;
					break;
				case BuffType.VariousFoodWoodStoneOreWorkerPerFive:
					buildingEntity.extraWorkersOverTime += buffEntityKeyValue.buffStrength;
					break;
				case BuffType.ShrineExtraWorkerPerFive:
					buildingEntity.extraWorkersOverTime += buffEntityKeyValue.buffStrength;
					break;
				case BuffType.FarmFoodProductionPercentagePerWorker:
					buildingEntity.percentageProductionIncrease += buffEntityKeyValue.buffStrength;
					break;
				case BuffType.VariousFoodWoodStoneOreProductionPercentagePerWorker:
					buildingEntity.percentageProductionIncrease += buffEntityKeyValue.buffStrength;
					break;
				case BuffType.ShrineSpiritPercentageProductionPerWorker:
					buildingEntity.percentageProductionIncrease += buffEntityKeyValue.buffStrength;
					break;
				case BuffType.AdditionalExraWorkerLimitForBasicBuildings:
					buildingEntity.additionalExtraWorkerLimit += buffEntityKeyValue.buffStrength;
					break;
				case BuffType.AdditionalExraWorkerLimitForAdvancedBuildings:
					buildingEntity.additionalExtraWorkerLimit += buffEntityKeyValue.buffStrength;
					break;
				default:
					throw new System.Exception("Unexpected BuffType when trying to set building buff benefit properties.");
			}
		}
	}

	// TODO: Maybe not gameStateEntity but the more specific level resource bonuses component
	static void UpdateResourceGainsOverTimeForBuilding(BuildingEntity buildingEntity, GlobalBuffDataEntity globalBuffDataEntity)
	{
		// TODO: Perhaps this should be a member of buildingEntity. Also, this is an allocation, right?
		ResourceSetComponent previousBuildingResourceSetGainsOverTimeComponent = new ResourceSetComponent(buildingEntity.buildingResourceSetGainsOverTimeComponent);

		buildingEntity.buildingResourceSetGainsOverTimeComponent = new ResourceSetComponent();

		ResourceBuffSetComponent resourceBuffSetComponent = globalBuffDataEntity.playerGlobalBuffSetComponent;

		// TODO: This awful code is a result of the "global buff set" approach being outdated.
		// The buffs already knew which building to apply to... and that pattern should be used.
		// It should just be rolled into the "buildingEntity.flatProductionIncrease" calculation and this code should be eliminated
		// Pay attention to spiritGainMultiplier and extraGoldFromTwentyMineWorkers
		double extraFlatResourcesFromSpecificWorkers = 0d;

		if (resourceBuffSetComponent.extraFoodFromFarmWorkers > 0d && buildingEntity.buildingStatsScriptableObject.isFarm)
			extraFlatResourcesFromSpecificWorkers += resourceBuffSetComponent.extraFoodFromFarmWorkers;

		if (resourceBuffSetComponent.extraWoodFromLumberMillWorkers > 0d && buildingEntity.buildingStatsScriptableObject.isLumberMill)
			extraFlatResourcesFromSpecificWorkers += resourceBuffSetComponent.extraWoodFromLumberMillWorkers;

		if (resourceBuffSetComponent.extraStoneFromQuarryWorkers > 0d && buildingEntity.buildingStatsScriptableObject.isQuarry)
			extraFlatResourcesFromSpecificWorkers += resourceBuffSetComponent.extraStoneFromQuarryWorkers;

		if (resourceBuffSetComponent.extraOreFromMineWorkers > 0d && buildingEntity.buildingStatsScriptableObject.isMine)
			extraFlatResourcesFromSpecificWorkers += resourceBuffSetComponent.extraOreFromMineWorkers;

		if (resourceBuffSetComponent.extraSpiritFromShrineWorkers > 0d && buildingEntity.buildingStatsScriptableObject.isShrine)
			extraFlatResourcesFromSpecificWorkers += resourceBuffSetComponent.extraSpiritFromShrineWorkers;

		double extraResourceGainMultiplier = 0d;

		if (resourceBuffSetComponent.spiritGainMultiplier > 0d && buildingEntity.buildingStatsScriptableObject.isShrine)
			extraResourceGainMultiplier += resourceBuffSetComponent.spiritGainMultiplier;

		buildingEntity.buildingResourceSetGainsOverTimeComponent = 
			buildingEntity.buildingStatsScriptableObject.baseResourceProductionCoefficients
			* buildingEntity.finalWorkers
			* (1d + buildingEntity.flatProductionIncrease + extraFlatResourcesFromSpecificWorkers)
			* (1d + buildingEntity.percentageProductionIncrease)
			* (1d + extraResourceGainMultiplier);

		if (resourceBuffSetComponent.extraGoldFromTwentyMineWorkers > 0d && buildingEntity.buildingStatsScriptableObject.isMine)
		{
			buildingEntity.buildingResourceSetGainsOverTimeComponent.gold +=
				(buildingEntity.finalWorkers / ValueHolder.TWENTY_WORKERS)
				* (0d + resourceBuffSetComponent.extraGoldFromTwentyMineWorkers);
		}

		// NOTE: This system may not be the best named for it, but this seems to make the most sense here - where all other building "final" stats are updated when an aspect of a building changes.
		buildingEntity.finalFogSightRadius = buildingEntity.buildingStatsScriptableObject.baseFogSightRadius;

		buildingEntity.showOrRefreshProductionDisplayPanel |= buildingEntity.buildingResourceSetGainsOverTimeComponent != previousBuildingResourceSetGainsOverTimeComponent
			&& buildingEntity.finalWorkers > 0;

		// Debug.Log("cur = " + buildingEntity.buildingResourceSetGainsOverTimeComponent.food + " ... prev = " + previousBuildingResourceSetGainsOverTimeComponent.food + " ... workers = " + buildingEntity.trainedWorkers + " ... bool = " + (buildingEntity.buildingResourceSetGainsOverTimeComponent != previousBuildingResourceSetGainsOverTimeComponent).ToString());
	}

	public override void SystemUpdate()
	{
		// TODO: It's getting annoying and time consuming to write in conditions here rather than just update the buildings all the time
		if (levelEntity.buildingEntitiesAreDirtiedClearOnUpdate
			|| globalBuffDataEntity.activeMythsChangedClearOnUpdate
			)
		{
			foreach (var buildingEntity in levelEntity.activeBuildingEntities)
			{
				if (buildingEntity.workersOrTierIsDirtyClearOnUpdate
					|| buildingEntity.tileEntity.buffEntitiesDirtyClearOnUpdate
					|| globalBuffDataEntity.activeMythsChangedClearOnUpdate
					)
				{
					SetBuildingBuffBenefitProperties(buildingEntity);

					UpdateResourceGainsOverTimeForBuilding(buildingEntity, globalBuffDataEntity);
				}
			}
		}
	}
}

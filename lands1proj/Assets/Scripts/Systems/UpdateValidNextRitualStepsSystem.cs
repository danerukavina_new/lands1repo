﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateValidNextRitualStepsSystem : BaseSystem
{
	public InputStateEntity inputStateEntity;
	public LevelEntity levelEntity;
	public RitualStateEntity ritualStateEntity;

	TileEntity candidateTileForNextPlannedRitualStepEntity;
	Vector2Int candidateTileForNextPlannedRitualStepEntityCoord;
	UndoableList<RitualStepEntity> ritualStepEntities;
	List<Vector2Int> coordinatesInDiamondWithRadius;
	Vector2Int offsetCoord;
	Vector2Int lastRitualTileCoord;

	static bool IsTileAlreadyRitualStep(Vector2Int candidateTileForNextPlannedRitualStepEntityCoord, UndoableList<RitualStepEntity> ritualStepEntities, out int backtrackCount, bool skipCurrentStep)
	{
		RitualStepEntity ritualStepEntitiyBeingTappedOn;
		int initialIndex = ritualStepEntities.Count - 1 + (skipCurrentStep ? -1 : 0);
		backtrackCount = 0;

		// NOTE: -2 since you're never going to undo the current step you're mousing over
		for (int i = initialIndex; i >= 0; i--)
		{
			ritualStepEntitiyBeingTappedOn = ritualStepEntities[i];

			if (ritualStepEntitiyBeingTappedOn.tileEntity.coord == candidateTileForNextPlannedRitualStepEntityCoord)
			{
				backtrackCount = ritualStepEntities.Count - i - 1;
				return true;
			}
		}

		return false;
	}

	static bool NextRitualStepCoordIsValid(Vector2Int candidateTileForNextPlannedRitualStepEntityCoord, UndoableList<RitualStepEntity> ritualStepEntities, RitualType ritualType)
	{
		switch (ritualType)
		{
			case RitualType.BasicRitual:

				// TODO: Design wise, do we want the ritual to be a Hamilton path-like, not letting you revisit a vertex? Or is it about just painting a connected graph? If a connected graph, then we need to check if it's adjacent to all tile coords, not just this one.
				// TODO: Maybe implement a rectilinear distance function instead.
				if (
					ritualStepEntities.Count > 0
					&& !LogicHelpers.CoordIsAdjacent(candidateTileForNextPlannedRitualStepEntityCoord, ritualStepEntities.GetCurrent().tileEntity.coord)
					)
					return false;

				return !IsTileAlreadyRitualStep(candidateTileForNextPlannedRitualStepEntityCoord, ritualStepEntities, out int backtrackCount, skipCurrentStep: false);
			case RitualType.WaterRitual:
			case RitualType.AirRitual:
			case RitualType.MetalRitual:
				throw new System.Exception("Ritual type not yet implemented for checking validity of next step.");
			default:
				throw new System.Exception("Unexpected ritual type when checking validity of next step.");
		}
	}

	public override void SystemUpdate()
	{
		// TODO: This should be set just once... need a pattern for initializing systems (can't be set in constructor without passing it as a variable... the syntactic sugar for initalization doesn't occur before the constructor)
		ritualStepEntities = ritualStateEntity.ritualEntity.ritualStepEntities;
		coordinatesInDiamondWithRadius = ValueHolder.buffAreaShapes[BuildingStatsScriptableObject.BuffAreaShapeType.DiamondWithRadius][1]; // NOTE: Radius 1 means adjacent coordinates

		if (
			levelEntity.tileEntitiesAreDirtiedClearOnUpdate
			|| levelEntity.buildingEntitiesAreDirtiedClearOnUpdate
			|| ritualStateEntity.isDirtyClearOnUpdate
			)
		{
			switch (inputStateEntity.ritualInputType)
			{
				case InputStateEntity.RitualInputType.TapNextNode:
					break;
				case InputStateEntity.RitualInputType.SwipeToSelectWithRecenteringWhenLettingGo:
					break;
				case InputStateEntity.RitualInputType.SwipeToSelectWithRubberBandingAtEdges:

					ritualStateEntity.validNextRitualStepCoords.Clear();
					ritualStateEntity.backtrackCountsForAllowedRitualStepCoords.Clear();

					if (ritualStepEntities.Count > 0)
					{
						for (int i = ritualStepEntities.Count - 1 - ValueHolder.FIRST_RITUAL_STEP_CANT_BE_BACKTRACK_UNDONE; i >= 0; i--)
						{
							ritualStateEntity.backtrackCountsForAllowedRitualStepCoords.Add(ritualStepEntities[i].tileEntity.coord, ritualStepEntities.Count - i - 1);
						}

						lastRitualTileCoord = ritualStepEntities.GetCurrent().tileEntity.coord;

						foreach (var offsetCoord in coordinatesInDiamondWithRadius)
						{
							candidateTileForNextPlannedRitualStepEntityCoord = lastRitualTileCoord + offsetCoord;
							candidateTileForNextPlannedRitualStepEntity = levelEntity.GetTileEntityOrNull(candidateTileForNextPlannedRitualStepEntityCoord);

							if (candidateTileForNextPlannedRitualStepEntity != null)
								if (NextRitualStepCoordIsValid(candidateTileForNextPlannedRitualStepEntityCoord, ritualStepEntities, RitualType.BasicRitual))
									ritualStateEntity.validNextRitualStepCoords.Add(candidateTileForNextPlannedRitualStepEntityCoord);
						}
					}
					else
					{
						foreach (var buildingEntitiy in levelEntity.activeBuildingEntities)
							if (buildingEntitiy.buildingStatsScriptableObject.isShrine)
								ritualStateEntity.validNextRitualStepCoords.Add(buildingEntitiy.tileEntity.coord);
					}

					ritualStateEntity.validNextRitualStepTilesIsDirtyClearOnUpdate = true;

					break;
				default:
					break;
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadLevelSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public Camera mainCamera;
	public TileEntity tileEntityPrefab;
	public GameObject fullFogEntityPrefab;
	public GameObject partialFogEntityPrefab;
	public GameObject inputIndicatorFogEntityPrefab;
	public GameObject tileEntitiesObject;
	public GameObject fullFogEntitiesObject;
	public GameObject partialFogEntitiesObject;
	public GameObject inputIndicatorFogEntitiesObject;

	public void LoadLevelEntityFromLevelDataEntity(LevelDataEntity levelDataEntity)
	{
		levelEntity.Reinitialize(false);

		foreach (var coord in levelDataEntity.activeTiles)
		{
			levelEntity.AddTileEntity(coord, tileEntityPrefab, tileEntitiesObject, mainCamera);
		}

		levelEntity.AddAllFogEntities(fullFogEntityPrefab, partialFogEntityPrefab, inputIndicatorFogEntityPrefab, fullFogEntitiesObject, partialFogEntitiesObject, inputIndicatorFogEntitiesObject);

		levelEntity.tileEntitiesAreDirtiedClearOnUpdate = true; // TODO: Probably unneccesary

		if (levelDataEntity.activeTiles.Count > 0)
			mainCamera.transform.position = new Vector3(levelDataEntity.activeTiles[0].x, levelDataEntity.activeTiles[0].y, mainCamera.transform.position.z);
	}

	public override void SystemUpdate()
	{
		if (gameStateEntity.levelShouldBeLoaded)
		{
			gameStateEntity.levelShouldBeLoaded = false;

			string levelToLoadKey = ValueHolder.TEST_LEVEL_2;

			LoadLevelEntityFromLevelDataEntity(ValueHolder.levelData[levelToLoadKey]);
		}
	}
}

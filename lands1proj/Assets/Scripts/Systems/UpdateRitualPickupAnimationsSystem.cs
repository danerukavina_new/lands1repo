﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateRitualPickupAnimationsSystem : BaseSystem
{
	public RitualStateEntity ritualStateEntity;
	public Dictionary<RitualPickupType, RuntimeAnimatorController> ritualPickupAnimationPanelControllers;
	public RitualPickupAnimationPanelEntity ritualPickupAnimationPanelEntityPrefab;
	public MenuManagerEntity menuManagerEntity;

	static void AddRitualPickupAnimationPanelEntity(
		BuildingEntity buildingEntity,
		RitualPickupEntity ritualPickupEntity,
		Dictionary<RitualPickupType, RuntimeAnimatorController> ritualPickupAnimationPanelControllers,  // TODO: Do we want to use these controllers, or some other controllers? Will they even work?
		RitualPickupAnimationPanelEntity ritualPickupAnimationPanelEntityPrefab,
		GameObject ritualPickupAnimationPanelsMenuGridView
	)
	{
		// TODO: Important - eventually, there should be a design that controls either the sort order of these, or places them in buckets based on type (so effectively a radix sort)
		RitualPickupAnimationPanelEntity ritualPickupAnimationPanelEntity = GameObject.Instantiate(ritualPickupAnimationPanelEntityPrefab, ritualPickupAnimationPanelsMenuGridView.transform);

		// tileEntity.canvas.worldCamera = mainCamera; // TODO: Do we need to set this

		UpdateRitualPickupAnimationPanelEntity(ritualPickupAnimationPanelEntity, ritualPickupEntity, ritualPickupAnimationPanelControllers);

		buildingEntity.ritualPickupAnimationPanelEntity = ritualPickupAnimationPanelEntity;
	}

	static void UpdateRitualPickupAnimationPanelEntity(
		RitualPickupAnimationPanelEntity ritualPickupAnimationPanelEntity,
		RitualPickupEntity ritualPickupEntity,
		Dictionary<RitualPickupType, RuntimeAnimatorController> ritualPickupAnimationPanelControllers  // TODO: Do we want to use these controllers, or some other controllers? Will they even work?
	)
	{
		RuntimeAnimatorController ritualPickupAnimationPanelControllerFromRitualPickupType = ritualPickupAnimationPanelControllers[ritualPickupEntity.ritualPickupType];

		if (ritualPickupAnimationPanelEntity.ritualPickupPanelAnimationController.runtimeAnimatorController != ritualPickupAnimationPanelControllerFromRitualPickupType)
			ritualPickupAnimationPanelEntity.ritualPickupPanelAnimationController.runtimeAnimatorController = ritualPickupAnimationPanelControllerFromRitualPickupType;

		ritualPickupAnimationPanelEntity.valueText.text = StringHelpers.RitualPickupValueText(ritualPickupEntity.ritualPickupType, ritualPickupEntity.ritualPickupStrength);
	}

	public override void SystemUpdate()
	{
		// NOTE: No need to check ritualStateEntity.isDirtyClearOnUpdate, since the array sizes are good conditions

		foreach (var ritualStepEntity in ritualStateEntity.ritualEntity.ritualStepEntitiesAddingPickupsClearOnUpdate)
		{
			// NOTE: ritualStepEntity.tileEntity.buildingEntity.(...) are all guaranteed to be not null if they were added to this list

			// TODO: This is a little dirty, consider which system really owns the logic for managing these states. Probably HoverPickupInWorldSystem... it should know to set these two maybe... but how? You would still need to send something. Maybe make a constructor that sets the initial state to ".Initial" and then the system knows what to do with it
			// But I also don't like using constructors, maybe it's better to be more explicit here, even though it's taking away responsibility
			ritualStepEntity.tileEntity.buildingEntity.ritualPickupAnimationEntity.hoverPickupInWorldComponent.nextHoverPickupInWorldComponentState = HoverPickupInWorldComponentState.PickupDissipating;
			ritualStepEntity.tileEntity.buildingEntity.ritualPickupAnimationEntity.hoverPickupInWorldComponent.transitionToNextState = true;

			if (menuManagerEntity.ritualPickupMenuPickupRows.ContainsKey(ritualStepEntity.tileEntity.buildingEntity.ritualPickupEntity.ritualPickupType))
				AddRitualPickupAnimationPanelEntity(
					ritualStepEntity.tileEntity.buildingEntity,
					ritualStepEntity.tileEntity.buildingEntity.ritualPickupEntity,
					ritualPickupAnimationPanelControllers,
					ritualPickupAnimationPanelEntityPrefab,
					menuManagerEntity.ritualPickupMenuPickupRows[ritualStepEntity.tileEntity.buildingEntity.ritualPickupEntity.ritualPickupType].ritualPickupAnimationPanelsGridView // TODO: This extremely long line of reasoning from the ritual step to its pickup type seems wrong
					);
			// NOTE: Extend Ritual is the only ritual pickup not represented by a menu row
			else if (ritualStepEntity.tileEntity.buildingEntity.ritualPickupEntity.ritualPickupType != RitualPickupType.ExtendRitual)
				throw new System.Exception("Ritual pickup picked up that doesn't have a corresponding row in the display menu for pickups. Add one to RitualPickupRowsGridView.");
		}

		foreach (var ritualStepEntity in ritualStateEntity.ritualEntity.ritualStepEntitiesUpdatingPickupsClearOnUpdate)
		{
			// NOTE: ritualStepEntity.tileEntity.buildingEntity.(...) are all guaranteed to be not null if they were added to this list

			UpdateRitualPickupAnimationPanelEntity(
				ritualStepEntity.tileEntity.buildingEntity.ritualPickupAnimationPanelEntity,
				ritualStepEntity.tileEntity.buildingEntity.ritualPickupEntity,
					ritualPickupAnimationPanelControllers
					);
		}

		foreach (var ritualStepEntity in ritualStateEntity.ritualEntity.ritualStepEntitiesRemovingPickupsClearOnUpdate)
		{
			// NOTE: ritualStepEntity.tileEntity.buildingEntity.(...) are all guaranteed to be not null if they were added to this list

			ritualStepEntity.tileEntity.buildingEntity.ritualPickupAnimationEntity.hoverPickupInWorldComponent.nextHoverPickupInWorldComponentState = HoverPickupInWorldComponentState.UndoPickupMaterializing;
			ritualStepEntity.tileEntity.buildingEntity.ritualPickupAnimationEntity.hoverPickupInWorldComponent.transitionToNextState = true;

			if (ritualStepEntity.tileEntity.buildingEntity.ritualPickupEntity.ritualPickupType != RitualPickupType.ExtendRitual)
				Object.Destroy(ritualStepEntity.tileEntity.buildingEntity.ritualPickupAnimationPanelEntity.gameObject); // TODO: Do we need to set it to null here?
		}
	}
}

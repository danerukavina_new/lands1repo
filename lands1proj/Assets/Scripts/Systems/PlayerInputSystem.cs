﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputSystem : BaseSystem
{
	public Camera mainCamera;
	public GameStateEntity gameStateEntity;
	public InputStateEntity inputStateEntity;
	public LevelEntity levelEntity;

	Touch touch;
	bool inputTapDownFrame;
	bool inputTapActive;
	bool inputTapUpFrame;
	bool tapDownFrame;
	bool tapActive;
	bool tapUpFrame;
	Vector3 tapPosition;
	BuildingEntity tapPositionBuildingEntityOrNull;

	public override void SystemUpdate()
	{
		if (Application.isMobilePlatform)
		{
			touch = Input.GetTouch(0);
			switch (touch.phase)
			{
				case TouchPhase.Began:
					inputTapDownFrame = true;
					inputTapActive = true;
					inputTapUpFrame = false;
					break;
				case TouchPhase.Moved:
					inputTapDownFrame = false;
					inputTapActive = true;
					inputTapUpFrame = false;
					break;
				case TouchPhase.Stationary:
					inputTapDownFrame = false;
					inputTapActive = true;
					inputTapUpFrame = false;
					break;
				case TouchPhase.Ended:
					inputTapDownFrame = false;
					inputTapActive = false;
					inputTapUpFrame = true;
					break;
				case TouchPhase.Canceled:
					inputTapDownFrame = false;
					inputTapActive = false;
					inputTapUpFrame = true;
					break;
				default:
					throw new System.Exception("New kind of touch phase detected, that's not implemented");
			}

			tapPosition = new Vector3(touch.position.x, touch.position.y, 0f);
		}
		else
		{
			inputTapDownFrame= Input.GetMouseButtonDown(0);
			inputTapActive = Input.GetMouseButton(0);
			inputTapUpFrame = Input.GetMouseButtonUp(0);

			tapPosition = Input.mousePosition;
		}

		bool menuBlockedInput = false; // TODO: Placeholder

		// TODO: This probably doesn't work, or in general the camera moves through while you're in menus.
		tapDownFrame = inputTapDownFrame && GUIUtility.hotControl == 0 && !menuBlockedInput && inputStateEntity.anyInputEnabled;
		tapActive = inputTapActive && GUIUtility.hotControl == 0 && !menuBlockedInput && inputStateEntity.anyInputEnabled;
		tapUpFrame = inputTapUpFrame; // TODO: Make more sophisticated logic here

		if (tapDownFrame)
		{
			// TODO: There's no need for this check, right?
			if (inputStateEntity.tapInputState == InputStateEntity.TapInputState.None)
			{
				// TODO: This should be more sophisticated, presumably? At least if the default state is supposed to be "Smart", then we'd be making determinations in quite complicated ways about what functionality tapping activates... perhaps even after a delay
				// TODO: The player input system should perhaps not decide what the input state is. For example, both the PlayerInputSystem and perhaps the ritual system, as well as some other events, could all force movement of the Camera. One system needs to decide and arbitrate between all of these
				switch (inputStateEntity.worldInputType)
				{
					case InputStateEntity.WorldInputType.DefaultWorldInput:
						inputStateEntity.tapInputState = InputStateEntity.TapInputState.FreeSpaceClickAndDragToMoveCamera;
						break;
					case InputStateEntity.WorldInputType.RitualWorldInput:
						inputStateEntity.tapInputState = InputStateEntity.TapInputState.RitualInputBasedOnRitualInputType;
						break;
					default:
						throw new System.Exception("Unrecognized input type in Player Input System.");
				}

				inputStateEntity.inputStartTime = gameStateEntity.gameTime;
				inputStateEntity.initialTapThisUpdateClearOnUpdate = true;

				inputStateEntity.tapStartScreenPosition = tapPosition;
				inputStateEntity.tapStartWorldPosition = mainCamera.ScreenToWorldPoint(tapPosition);
				inputStateEntity.tapStartCameraWorldPosition = mainCamera.transform.position;

				inputStateEntity.inputStateMayCauseStartOfCameraMovement = true;

				// Debug.Log(inputStateEntity.tapStartWorldPosition);
			}
		}

		if (tapActive || tapUpFrame)
		{
			inputStateEntity.tapCurrentScreenPosition = tapPosition;
			inputStateEntity.tapCurrentWorldPosition = mainCamera.ScreenToWorldPoint(tapPosition);
		}

		// TODO: Decide if we need this here. Right now the CameraMovementSystem + Unity Buttons do everything
		if (tapActive)
		{
			//switch (inputStateEntity.tapInputState)
			//{
			//	case InputStateEntity.TapInputState.None:
			//		break;
			//	case InputStateEntity.TapInputState.FreeSpaceClickAndDragToMoveCamera:
			//		// TODO: Add a camera movement system
			//		break;
			//	default:
			//		throw new System.Exception("Unrecognize tap input state in Player Input System.");
			//}
		}
		else // NOTE: This should not be "tapUpFrame" - if something disrupts catching the upFrame, like an exception, we should still restart the input state if we aren't "tapActive"
		{
			inputStateEntity.tapInputState = InputStateEntity.TapInputState.None;
		}
	}
}

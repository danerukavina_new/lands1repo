﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateGameStateAndGlobalBuffsSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public GlobalBuffDataEntity globalBuffDataEntity;
	public ButtonManagerEntity buttonManagerEntity;

	public override void SystemUpdate()
	{
		// TODO: Introduce the civilization increase and also increase based on existing heritages or something
		// gameStateEntity.canHaveSecondMyth = ValueHolder.BASE_CAN_HAVE_SECOND_MYTH;

		// TODO: Looks like this system maybe wasn't needed. But it will be needed eventualyl to recalculate maxMyths and who knows what
	}
}

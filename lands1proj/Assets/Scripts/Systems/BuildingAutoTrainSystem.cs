﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingAutoTrainSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public Dictionary<BuildingAdvancedType, BuildingStatsScriptableObject> buildingStatsScriptableObjects;
	public GlobalBuffDataEntity globalBuffDataEntity;

	public override void SystemUpdate()
	{
		if (gameStateEntity.autoTrain)
			foreach (var buildingEntity in levelEntity.activeBuildingEntities)
			{
				if (!buildingEntity.isTraining && buildingEntity.canTrain) // TODO: add condition for locked training
				{
					buildingEntity.startTraining = true;
				}
				else
				{
					LogicHelpers.SetBuildingCanStartTierUpgradeAndMaxTrainedWorkersForTierAndCosts(buildingEntity, gameStateEntity, buildingStatsScriptableObjects, globalBuffDataEntity);

					if (/*!buildingEntity.isBuildingOrTierUpgrading &&*/ buildingEntity.canStartNextBuildingTierUpgrades[buildingEntity.finalTierUpgradePathIndex] && buildingEntity.canAlsoAutoTierUpgrade) // TODO: Consider taking all of this to the "tryStartUpgradingTier" system
					{
						buildingEntity.startUpgradingBuildingTier = true;
						// TODO: Shouldn't there be a flag saying like "yo this is an attempt by the auto system, so there is no path index" - that would be a lot more explicit. May also get rid of this flag below about player interaction
						buildingEntity.tierUpgradeRequestIsFromPlayerInteraction = false;
					}
				}
			}
	}
}

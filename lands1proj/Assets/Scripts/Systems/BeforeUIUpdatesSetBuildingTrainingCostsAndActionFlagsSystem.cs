﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This system is used to enable / disable trainability of buildings unrelated to the tightly coupled training loop, which has similar code
public class BeforeUIUpdatesSetBuildingTrainingCostsAndActionFlagsSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public GlobalBuffDataEntity globalBuffDataEntity;
	public RitualStateEntity ritualStateEntity;
	public ButtonManagerEntity buttonManagerEntity;
	public Dictionary<BuildingAdvancedType, BuildingStatsScriptableObject> buildingStatsScriptableObjects;

	public override void SystemUpdate()
	{
		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			// TODO: Add dirty flag
			LogicHelpers.SetTrainingCosts(buildingEntity, globalBuffDataEntity, levelEntity);

			// NOTE: To keep things simple, always checked, since game state is changing constantly as the user earns resources
			LogicHelpers.SetBuildingCanTrainAndMaxTrainedWorkersForTier(buildingEntity, gameStateEntity);

			LogicHelpers.SetBuildingCanStartTierUpgradeAndMaxTrainedWorkersForTierAndCosts(buildingEntity, gameStateEntity, buildingStatsScriptableObjects, globalBuffDataEntity);
		}

		// TODO: Again, obviously these two are very similar, consider unifying later
		foreach (var mythButtonEntity in buttonManagerEntity.mythBuyingButtonEntities)
		{
			if (!mythButtonEntity.researchableStateComponent.isBeingResearched)
			{
				LogicHelpers.SetResearchCosts(mythButtonEntity, gameStateEntity);

				LogicHelpers.SetMythButtonCanResearch(mythButtonEntity, gameStateEntity, globalBuffDataEntity);
			}
		}

		// TODO: Observe how and where this is updated. It follows the canTrain pattern... is this good?
		LogicHelpers.SetPlayerCanPerformRitual(ritualStateEntity, gameStateEntity);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Updates the menus of all the buildings, specifically whether certain building actions can be taken (or are even shown as an option), based on game state.
/// </summary>
public class UpdateBuildingMenusBasedOnGameStateSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;

	public override void SystemUpdate()
	{
		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			// NOTE: Don't delete this, presumably we want to enable / disable buttons whether something is buildable? Depends on the future of the building menu
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Update to have this only work for Myths?
// TODO: Perhaps have Myths use similar buff prototypes, perhaps acting as a building?
public class DefunctUpdateGlobalBuffsSystem : BaseSystem
{
	public GlobalBuffDataEntity globalBuffDataEntity;
	public LevelEntity levelEntity;

	ResourceBuffSetComponent globalResourceBuffAccumulatorComponent;

	static ResourceBuffSetComponent CreateMythResourceBuffSetComponent(MythButtonEntity mythButtonEntity, LevelEntity levelEntity)
	{
		// NOTE: This sets all the values to 0d, 0f, false, etc. which is fine. Not gonna bother with a long initalization
		ResourceBuffSetComponent ret = new ResourceBuffSetComponent();

		if (mythButtonEntity == null)
			return ret;

		switch (mythButtonEntity.mythAssetsDataScriptableObject.mythType)
		{
			case MythType.None:
				throw new System.Exception("Unexpected None myth type when trying to determine myth's resource buff set.");
			case MythType.FarmMyth0:
				ret.extraFoodFromFarmWorkers = 1d;
				ret.farmTrainingSpeedMultiplier = 5f - 1f;
				break;
			case MythType.LumberMillMyth0:
				ret.extraWoodFromLumberMillWorkers = 2d;
				ret.lumberMillTrainingSpeedMultiplier = 10f - 1f;
				break;
			case MythType.LumberMillAndShrineMyth0:
				ret.extraWoodFromLumberMillWorkers = levelEntity.buildingCountsByBaseBuilding[BuildingBaseType.Shrine] * 3d;
				ret.extraSpiritFromShrineWorkers = levelEntity.buildingCountsByBaseBuilding[BuildingBaseType.LumberMill] * 3d;
				break;
			case MythType.QuarryMyth0:
				ret.bonusQuarryWorkersPerRitual = 3;
				ret.removeQuarryGoldCost = true;
				break;
			case MythType.TownHallMyth0:
				ret.doubleTownHallEffect = true;
				break;
			case MythType.RitualsAndWellsMyth0:
				ret.extraRitualDuration = 30f;
				ret.extraRitualAutorepeat = 3;
				break;
			case MythType.RitualsMyth0:
				ret.enableAutoTrainControl = true;
				ret.specialRitualConditionsExtraRitualTime = true;
				ret.anyTrainingSpeedMultiplier = 2f - 1f;
				break;
			case MythType.MineMyth0:
				ret.extraGoldFromTwentyMineWorkers = 1d;
				ret.addMineWorkersOverFiveMinutes = 1;
				break;
			case MythType.GuildMyth0:
				ret.doubleGuildEffect = true;
				break;
			case MythType.FarmMyth1:
				ret.farmWorkerCheapnessPerFarmMultiplier = 5d - 1d;
				ret.addFarmWorkerOverFiveMinutes = 3;
				break;
			case MythType.HeroMyth0:
				ret.doubleExperienceGain = true;
				break;
			case MythType.FarmAndMineMyth0:
				ret.bonusFarmWorkersPerMineWorkerTrained = 4;
				break;
			case MythType.SpiritMyth0:
				ret.enableIdolPurchases = true;
				ret.extraSpiritFromShrineWorkers = 5d;
				ret.spiritGainMultiplier = 1.25d - 1d;
				ret.extraRitualAutorepeat = 1;
				break;
			case MythType.GodsMyth0:
				ret.discipleCheapnessMultiplier = 2d - 1d;
				ret.extraConditionalGodResource = 2d;
				break;
			case MythType.Myth14:
				break;
			case MythType.Myth15:
				break;
			case MythType.Myth16:
				break;
			case MythType.Myth17:
				break;
			case MythType.Myth18:
				break;
			default:
				throw new System.Exception("Unexpected myth type when trying to determine myth's resource buff set.");
		}

		return ret;
	}

	public override void SystemUpdate()
	{
		// TODO: There should be an "or" here for other kinds of ways the global buff set can be updated
		if (
			// levelEntity.buildingEntitiesGlobalBuffSetsAreDirtiedClearOnUpdate
			// || levelEntity.buildingEntitiesAreDirtiedClearOnUpdate
			globalBuffDataEntity.activeMythsChangedClearOnUpdate
			)
		{
			// NOTE: This sets all the values to 0d, 0f, false, etc. which is fine. Not gonna bother with a long initalization
			globalResourceBuffAccumulatorComponent = new ResourceBuffSetComponent();

			globalResourceBuffAccumulatorComponent += CreateMythResourceBuffSetComponent(globalBuffDataEntity.firstBoughtMythDisplayEntity, levelEntity);
			globalResourceBuffAccumulatorComponent += CreateMythResourceBuffSetComponent(globalBuffDataEntity.secondBoughtMythDisplayEntity, levelEntity);
			globalResourceBuffAccumulatorComponent += CreateMythResourceBuffSetComponent(globalBuffDataEntity.firstEnshrinedBoughtMythDisplayEntity, levelEntity);
			globalResourceBuffAccumulatorComponent += CreateMythResourceBuffSetComponent(globalBuffDataEntity.secondEnshrinedBoughtMythDisplayEntity, levelEntity);

			//foreach (var activeBuildingEntity in levelEntity.activeBuildingEntities)
			//{
			//	globalResourceBuffAccumulatorComponent += activeBuildingEntity.buildingResourceBuffSetComponent;
			//}

			globalBuffDataEntity.playerGlobalBuffSetComponent = globalResourceBuffAccumulatorComponent;

			// TODO: UpdateResourceGainsOverTimeForEachBuildingBasedOnLevelEntitySystem uses the .buildingEntitiesGlobalBuffSetsAreDirtiedClearOnUpdate flag to determine if it should update. This can be done better, knowing which kinds of buildings are affected
		}
	}
}

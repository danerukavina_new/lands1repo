﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Should this system be per buyentity type as it is now? Probably
public class ProcessBuyButtonEntityClicksSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public ButtonManagerEntity buttonManagerEntity;

	double availableResource;

	public override void SystemUpdate()
	{
		foreach (var buyResourceButtonEntity in buttonManagerEntity.buyResourceButtonEntities)
		{
			// NOTE: This system currently does nothing because at this time there's nothing to update on buttons. When the Trade buttons come in, those do update

			if (buyResourceButtonEntity.buyWithCostAndGainButton.buttonClicked)
			{
				buyResourceButtonEntity.buyWithCostAndGainButton.buttonClicked = false;

				availableResource = gameStateEntity.playerResourceSetAmountComponent.GetCorrespondingResourceAmount(buyResourceButtonEntity.finalResourceCost);

				if (availableResource <= 0d)
					throw new System.Exception("Attempted to buy resource when cost made it unavailable. Though maybe this would happen with multitap on screen...");

				gameStateEntity.playerResourceSetAmountComponent.SubtractResourceAmount(buyResourceButtonEntity.finalResourceCost);
				gameStateEntity.playerResourceSetAmountComponent.AddResourceAmount(buyResourceButtonEntity.finalResourceGain);
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCanBuildTileBuildingSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public GlobalBuffDataEntity globalBuffDataEntity;
	public Dictionary<BuildingAdvancedType, BuildingStatsScriptableObject> buildingStatsScriptableObjects;

	// NOTE: This is used for looking up scriptable objects. Consider refactoring this if base building scriptable objects are ever implemented.
	// TODO: Refactor if base building scriptable objects are implemented
	static BuildingAdvancedType InitialBuildingAdvancedTypeFromBaseType(BuildingBaseType buildingBaseType)
	{
		switch (buildingBaseType)
		{
			case BuildingBaseType.Farm:
				return BuildingAdvancedType.Farm;
			case BuildingBaseType.LumberMill:
				return BuildingAdvancedType.LumberMill;
			case BuildingBaseType.TownHall:
				return BuildingAdvancedType.TownHall;
			case BuildingBaseType.Quarry:
				return BuildingAdvancedType.Quarry;
			case BuildingBaseType.Mine:
				return BuildingAdvancedType.Mine;
			case BuildingBaseType.Well:
				return BuildingAdvancedType.Well;
			case BuildingBaseType.House:
				return BuildingAdvancedType.House;
			case BuildingBaseType.Shrine:
				return BuildingAdvancedType.Shrine;
			case BuildingBaseType.Windmill:
				return BuildingAdvancedType.Windmill;
			case BuildingBaseType.Guild:
				return BuildingAdvancedType.Guild;
			case BuildingBaseType.Tavern:
				return BuildingAdvancedType.Tavern;
			case BuildingBaseType.Temple:
				return BuildingAdvancedType.Temple;
			case BuildingBaseType.Laboratory:
				return BuildingAdvancedType.Laboratory;
			case BuildingBaseType.Blacksmith:
				return BuildingAdvancedType.Blacksmith;
			case BuildingBaseType.Inn:
				return BuildingAdvancedType.Inn;
			case BuildingBaseType.TrainingGrounds:
				return BuildingAdvancedType.TrainingGrounds;
			case BuildingBaseType.Dungeon:
				return BuildingAdvancedType.Dungeon;
			case BuildingBaseType.TradeRoute:
				return BuildingAdvancedType.TradeRoute;
			case BuildingBaseType.Shipyard:
				return BuildingAdvancedType.Shipyard;
			case BuildingBaseType.SpecialCaptureInProgress:
				return BuildingAdvancedType.SpecialCaptureInProgress;
			case BuildingBaseType.SpecialSeaBorder:
				return BuildingAdvancedType.SpecialSeaBorder;
			case BuildingBaseType.SpecialMountainPassBorder:
				return BuildingAdvancedType.SpecialMountainPassBorder;
			case BuildingBaseType.SpecialMountainBorder:
				return BuildingAdvancedType.SpecialMountainPassBorder;
			default:
				throw new System.Exception("Unexpected building base type when trying to determine building advanced type.");
		}
	}

	public override void SystemUpdate()
	{
		// TODO: There are two alternative implementations to this. Consider which is the best one
		// Adding building base type scriptable objects, which hold some common properties for that building. If that's added, then it should just know its initial advanced type
		// Alternately, one could loop through ALL the advanced scriptable objects and perform logic on the ones that are marked with "IsInitialBuilding", which may be an otherwise useful flag
		foreach (var baseBuildingTypeKey in LogicHelpers.baseBuildingTypeKeys)
		{
			// TODO: Remove when all the building stats are implemented
			if (!buildingStatsScriptableObjects.ContainsKey(InitialBuildingAdvancedTypeFromBaseType(baseBuildingTypeKey)))
				continue;

			// TODO: IMPORTANT - We use a function here that determines if the building can be built... but doesn't look at tile buffs, which should make that easier. Change this in the future, if it's needed for gameplay.
			// Compare and contrast to how "SetBuildingCanStartTierUpgradeAndMaxTrainedWorkersForTierAndCosts" does it, using a finalCost approach, which is also in line with how training does it. Simply put, perhaps a "tile" should own its own final build costs for all the buildings...
			// Rather than it being part of the global game state
			gameStateEntity.canBuildTileBuilding[baseBuildingTypeKey] = LogicHelpers.InitialBuildCostSatisfiedWithoutTileBuffsBeingConsidered(
				buildingStatsScriptableObjects[InitialBuildingAdvancedTypeFromBaseType(baseBuildingTypeKey)],
				gameStateEntity.playerResourceSetAmountComponent,
				globalBuffDataEntity
			);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateLevelForBuyTileSystem : BaseSystem
{
	public Camera mainCamera;
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public BuyTileOnLevelEntity buyTileOnLevelEntityPrefab;
	public GameObject tileEntitiesObject;

	//bool[,] tilesWithAdjacentBoughtTiles;
	//List<Vector2Int> candidateBuyTiles;
	Vector2Int coord;

	public override void SystemUpdate()
	{
		if (gameStateEntity.changedBuyTileStateClearOnUpdate)
		{
			if (gameStateEntity.buyTileState)
			{
				//tilesWithAdjacentBoughtTiles = levelEntity.buildingLogicHelperArray;
				//candidateBuyTiles = levelEntity.coordLogicHelperList;

				//LogicHelpers.BoolClearer(levelEntity.buildingLogicHelperArray);
				//candidateBuyTiles.Clear();

				foreach (var tileEntity in levelEntity.activeTileEntities)
				{
					foreach (var direction in ValueHolder.fourWayDirections)
					{
						coord = tileEntity.coord + direction;

						if (levelEntity.GetTileEntity(coord) == null && levelEntity.GetBuyTileOnLevelEntity(coord) == null)
							levelEntity.AddBuyTileOnLevelEntity(coord, buyTileOnLevelEntityPrefab, tileEntitiesObject, mainCamera);

					}
				}
			}
			else
				levelEntity.RemoveBuyTileOnLevelEntities();
		}
	}
}

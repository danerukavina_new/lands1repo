﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverPickupInWorldSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public InputStateEntity inputStateEntity;
	public LevelEntity levelEntity;

	BuildingEntity buildingEntity;
	RitualPickupAnimationEntity ritualPickupAnimationEntity;
	HoverPickupInWorldComponent hoverPickupInWorldComponent;
	bool showOrHide;
	Vector3 direction;
	float hoveringPositionYOffset;

	public override void SystemUpdate()
	{
		// TODO: Certainly this will get more complicated when there's more things using the hover component. For now, this system only needs to do work if they're visible. Which is only true if it's RitualWorldInput time.
		if (inputStateEntity.worldInputType == InputStateEntity.WorldInputType.RitualWorldInput)
		{
			foreach (var tileEntity in levelEntity.activeTileEntities)
			{
				buildingEntity = tileEntity.buildingEntity;

				if (buildingEntity != null)
				{
					ritualPickupAnimationEntity = buildingEntity.ritualPickupAnimationEntity;

					hoverPickupInWorldComponent = ritualPickupAnimationEntity.hoverPickupInWorldComponent;

					if (!hoverPickupInWorldComponent.transitionToNextState)
					{
						switch (hoverPickupInWorldComponent.hoverPickupInWorldComponentState)
						{
							case HoverPickupInWorldComponentState.Hovering:
								hoveringPositionYOffset = hoverPickupInWorldComponent.verticalOffsetFromSource
									+ hoverPickupInWorldComponent.oscillationWorldAmplitude
									* Mathf.Sin(
										(2 * Mathf.PI / hoverPickupInWorldComponent.oscillationPeriod) * gameStateEntity.gameTime
										+ (ritualPickupAnimationEntity.transform.position.x + ritualPickupAnimationEntity.transform.position.y) * hoverPickupInWorldComponent.oscillationPhaseOffsetWorldScale
										);


								hoverPickupInWorldComponent.objectForAnimatingTransform.position = new Vector3(
									ritualPickupAnimationEntity.transform.position.x,
									ritualPickupAnimationEntity.transform.position.y + hoveringPositionYOffset
									);
								break;
							case HoverPickupInWorldComponentState.PickupDissipating:
								if (hoverPickupInWorldComponent.dissipationOrMaterializationStartTime + hoverPickupInWorldComponent.dissipationOrMaterializationDuration <= gameStateEntity.gameTime)
								{
									hoverPickupInWorldComponent.nextHoverPickupInWorldComponentState = HoverPickupInWorldComponentState.FullyDissipated;
									hoverPickupInWorldComponent.transitionToNextState = true;
								}
								else if (hoverPickupInWorldComponent.dissipationOrMaterializationStartTime + hoverPickupInWorldComponent.dissipationOrMaterializationLingerInPlaceDelay >= gameStateEntity.gameTime)
								{
									// TODO: It makes sense to use the real delta time here, perhaps? But it should still be dependency injected via gameStateEntity I think...
									// But then it should be... clamped? Or it might not matter because of the if above checking dissipation?
									hoverPickupInWorldComponent.objectForAnimatingTransform.position += Vector3.up * hoverPickupInWorldComponent.dissipationOrMaterializationWorldMovementSpeed * Time.deltaTime;
								}
								break;
							case HoverPickupInWorldComponentState.FullyDissipated:
								break;
							case HoverPickupInWorldComponentState.UndoPickupMaterializing:
								if (hoverPickupInWorldComponent.dissipationOrMaterializationStartTime + hoverPickupInWorldComponent.dissipationOrMaterializationDuration <= gameStateEntity.gameTime)
								{
									hoverPickupInWorldComponent.nextHoverPickupInWorldComponentState = HoverPickupInWorldComponentState.Hovering;
									hoverPickupInWorldComponent.transitionToNextState = true;
								}
								else if (hoverPickupInWorldComponent.dissipationOrMaterializationStartTime + (hoverPickupInWorldComponent.dissipationOrMaterializationDuration - hoverPickupInWorldComponent.dissipationOrMaterializationLingerInPlaceDelay) < gameStateEntity.gameTime)
								{
									// TODO: It makes sense to use the real delta time here, perhaps? But it should still be dependency injected via gameStateEntity I think...
									// But then it should be... clamped? Or it might not matter because of the if above checking dissipation?
									hoverPickupInWorldComponent.objectForAnimatingTransform.position += Vector3.down * hoverPickupInWorldComponent.dissipationOrMaterializationWorldMovementSpeed * Time.deltaTime;
								}
								break;
							default:
								break;
						}
					}

					if (hoverPickupInWorldComponent.transitionToNextState)
					{
						hoverPickupInWorldComponent.transitionToNextState = false;

						// TODO: Oh god, refactor these state transitions... or put it in an animator
						// This will presumably be VERY coupled with whoever is trying to call it. There should be an interface then, instead, of what flags you can set for the component externally. And it will then know how to handle it. Like "Go towards hover" "Go towards dissipation"

						switch (hoverPickupInWorldComponent.nextHoverPickupInWorldComponentState)
						{
							case HoverPickupInWorldComponentState.Hovering:
								switch (hoverPickupInWorldComponent.hoverPickupInWorldComponentState)
								{
									case HoverPickupInWorldComponentState.Hovering:
										throw new System.Exception("Hover pickup in world attempting to go to a hovering state when already hovering.");
									case HoverPickupInWorldComponentState.PickupDissipating:
										throw new System.Exception("Hover pickup in world attempting to go to a hovering state when dissipating, without going through an undo + materialize state.");
									case HoverPickupInWorldComponentState.FullyDissipated:
										throw new System.Exception("Hover pickup in world attempting to go to a fully dissipated state without first dissipating.");
									case HoverPickupInWorldComponentState.UndoPickupMaterializing:
										// NOTE: Nothing needs to be done, it will just then keep oscillating... might be a little jagged.
										// TODO: How can we resync the animation to the world offset? One way would be a dampened oscillation
										break;
									default:
										break;
								}
								break;
							case HoverPickupInWorldComponentState.PickupDissipating:
								switch (hoverPickupInWorldComponent.hoverPickupInWorldComponentState)
								{
									case HoverPickupInWorldComponentState.Hovering:
										hoverPickupInWorldComponent.dissipationOrMaterializationStartTime = gameStateEntity.gameTime;
										break;
									case HoverPickupInWorldComponentState.PickupDissipating:
										throw new System.Exception("Hover pickup in world attempting to go to a dissipating state when already dissipating.");
									case HoverPickupInWorldComponentState.FullyDissipated:
										throw new System.Exception("Hover pickup in world attempting to go to a dissipating state when already fully dissipated.");
									case HoverPickupInWorldComponentState.UndoPickupMaterializing:
										float alreadyElapsedTime = gameStateEntity.gameTime - hoverPickupInWorldComponent.dissipationOrMaterializationStartTime;
										hoverPickupInWorldComponent.dissipationOrMaterializationStartTime = gameStateEntity.gameTime + alreadyElapsedTime - hoverPickupInWorldComponent.dissipationOrMaterializationDuration;
										break;
									default:
										break;
								}
								break;
							case HoverPickupInWorldComponentState.FullyDissipated:
								switch (hoverPickupInWorldComponent.hoverPickupInWorldComponentState)
								{
									case HoverPickupInWorldComponentState.Hovering:
										throw new System.Exception("Hover pickup in world attempting to go to a fully dissipated state when just hovering.");
									case HoverPickupInWorldComponentState.PickupDissipating:
										UIHelpers.ShowHideGameObject(ritualPickupAnimationEntity.gameObject, false);
										break;
									case HoverPickupInWorldComponentState.FullyDissipated:
										throw new System.Exception("Hover pickup in world attempting to go to a fully dissipated state when already fully dissipated.");
									case HoverPickupInWorldComponentState.UndoPickupMaterializing:
										throw new System.Exception("Hover pickup in world attempting to go to a fully dissipated state when undoing + materializing.");
									default:
										break;
								}
								break;
							case HoverPickupInWorldComponentState.UndoPickupMaterializing:
								switch (hoverPickupInWorldComponent.hoverPickupInWorldComponentState)
								{
									case HoverPickupInWorldComponentState.Hovering:
										throw new System.Exception("Hover pickup in world attempting to go to an undo + materializing state when already hovering and thus is materialized.");
									case HoverPickupInWorldComponentState.PickupDissipating:
										float alreadyElapsedTime = gameStateEntity.gameTime - hoverPickupInWorldComponent.dissipationOrMaterializationStartTime;
										hoverPickupInWorldComponent.dissipationOrMaterializationStartTime = gameStateEntity.gameTime + alreadyElapsedTime - hoverPickupInWorldComponent.dissipationOrMaterializationDuration;
										break;
									case HoverPickupInWorldComponentState.FullyDissipated:
										UIHelpers.ShowHideGameObject(ritualPickupAnimationEntity.gameObject, true);
										hoverPickupInWorldComponent.dissipationOrMaterializationStartTime = gameStateEntity.gameTime;
										break;
									case HoverPickupInWorldComponentState.UndoPickupMaterializing:
										throw new System.Exception("Hover pickup in world attempting to go to an undo + materializing state when already materializing.");
									default:
										break;
								}
								break;
							default:
								break;
						}

						hoverPickupInWorldComponent.hoverPickupInWorldComponentState = hoverPickupInWorldComponent.nextHoverPickupInWorldComponentState;
					}
				}
			}
		}
	}
}
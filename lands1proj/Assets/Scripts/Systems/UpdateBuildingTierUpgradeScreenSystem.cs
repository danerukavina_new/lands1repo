﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateBuildingTierUpgradeScreenSystem : BaseSystem
{
	public ScreenManagerEntity screenManagerEntity;
	// public Camera mainCamera;
	public BuildingTierUpgradeButtonEntity buildingTierUpgradeButtonEntityPrefab;
	public Dictionary<BuildingAdvancedType, BuildingAssetsDataScriptableObject> buildingAssetsDataScriptableObjects;
	// public Dictionary<BuildingAdvancedType, BuildingStatsScriptableObject> buildingStatsScriptableObjects;
	public ButtonManagerEntity buttonManagerEntity;

	BuildingEntity buildingTierUpgradeBuildingEntity;
	BuildingAdvancedType? buildingTierBuildingAdvancedType;
	int tierUpgradePathIndex;

	public override void SystemUpdate()
	{
		// TODO: Perhaps other systems could benefit from this form
		if (!screenManagerEntity.buildingTierUpgradeScreenIsDirtyClearOnUpdate
			&& (screenManagerEntity.buildingTierUpgradeBuildingEntity != null && !screenManagerEntity.buildingTierUpgradeBuildingEntity.isDirtyFromBeingBuilt)
			) 
			return;

		if (!screenManagerEntity.buildingTierUpgradeScreenShouldBeVisible)
		{
			UIHelpers.ShowHideGameObject(screenManagerEntity.buildingTierUpgradeScreen.gameObject, false);
		}
		else
		{
			UIHelpers.DestroyAllChildren(screenManagerEntity.showBuildingTierUpgradesMenuGridView);

			buildingTierUpgradeBuildingEntity = screenManagerEntity.buildingTierUpgradeBuildingEntity;

			// TODO: Alternately... that upgrade menu should be a contextual thing... and have a view when the doesn't have any upgrades available (not at max workers for tier, or at max tier - this may in fact be two views)...
			if (!LogicHelpers.BuildingIsAtMaxTier(buildingTierUpgradeBuildingEntity)) // TODO: Consider this, should this random system be assessing this building is at max tier issue? or using just the check below?
			{
				if (buildingTierUpgradeBuildingEntity.anyTierUpgradeIsAvailable) // TODO: Perhaps this is the correct check... see above logichelpers isatmaxtier call
				{
					// NOTE: This implements the tier upgrade screen having only one option instead of all the options
					if (buildingTierUpgradeBuildingEntity.tierIndex >= ValueHolder.TIER_WITH_LOCKED_UPGRADE_PATH)
					{

						tierUpgradePathIndex = buildingTierUpgradeBuildingEntity.chosenBuildingTierUpgradePathIndex;

						buildingTierBuildingAdvancedType = buildingTierUpgradeBuildingEntity.nextBuildingTiers[tierUpgradePathIndex];

						if (buildingTierBuildingAdvancedType.HasValue)
							AddBuildingTierUpgradeButtonEntity(
								buildingTierBuildingAdvancedType.Value,
								tierUpgradePathIndex,
								buildingTierUpgradeButtonEntityPrefab,
								buildingAssetsDataScriptableObjects,
								screenManagerEntity.showBuildingTierUpgradesMenuGridView,
								buttonManagerEntity.buildingTierUpgradeButtonEntities
								);
					}
					else
					{
						for (int tierUpgradePathIndex = 0; tierUpgradePathIndex < buildingTierUpgradeBuildingEntity.nextBuildingTiers.Count; tierUpgradePathIndex++)
						{
							buildingTierBuildingAdvancedType = buildingTierBuildingAdvancedType = buildingTierUpgradeBuildingEntity.nextBuildingTiers[tierUpgradePathIndex];

							if (buildingTierBuildingAdvancedType.HasValue)
								AddBuildingTierUpgradeButtonEntity(
									buildingTierBuildingAdvancedType.Value,
									tierUpgradePathIndex,
									buildingTierUpgradeButtonEntityPrefab,
									buildingAssetsDataScriptableObjects,
									screenManagerEntity.showBuildingTierUpgradesMenuGridView,
									buttonManagerEntity.buildingTierUpgradeButtonEntities
									);
						}
					}
				}
			}
			else
			{
				// TODO: Add logic for what to display when the building is at max tier
			}

			screenManagerEntity.currentBuildingNameText.text = screenManagerEntity.buildingTierUpgradeBuildingEntity.buildingAssetsDataScriptableObject.buildingName;
			screenManagerEntity.currentBuildingNameText.color = screenManagerEntity.buildingTierUpgradeBuildingEntity.buildingAssetsDataScriptableObject.borderColor;

			UIHelpers.SetSpriteAndBorderUIValues(
				screenManagerEntity.buildingTierUpgradeScreenMenuSpriteAndBorderPanel,
				screenManagerEntity.buildingTierUpgradeBuildingEntity.buildingAssetsDataScriptableObject.mainColor,
				screenManagerEntity.buildingTierUpgradeBuildingEntity.buildingAssetsDataScriptableObject.borderColor,
				screenManagerEntity.buildingTierUpgradeBuildingEntity.buildingAssetsDataScriptableObject.buildingIconSprite // TODO: We hijacked the sprite and bordere panel implementation in a slightly unusual way. I'm concerned it will be hard to figure out.
				);

			UIHelpers.SetSpriteAndBorderUIValues(
				screenManagerEntity.buildingTierUpgradeScreenCloseScreenButtonSpriteAndBorderPanel,
				screenManagerEntity.buildingTierUpgradeBuildingEntity.buildingAssetsDataScriptableObject.mainColor,
				screenManagerEntity.buildingTierUpgradeBuildingEntity.buildingAssetsDataScriptableObject.borderColor,
				null
				);

			// TODO: Add a line for the close button of the upgrade menu, UIHelpers.SetSpriteAndBorderUIValues(...

			screenManagerEntity.buildingTierUpgradeScreenMenuLabelText.text = ValueHolder.TIER_UPGRADE_MENU_LABEL_TEXT; // NOTE: The intent here is that I expect there to be multiple labels for the menu. See the else for max tier
			screenManagerEntity.buildingTierUpgradeScreenMenuLabelText.color = screenManagerEntity.buildingTierUpgradeBuildingEntity.buildingAssetsDataScriptableObject.borderColor;

			UIHelpers.ShowHideGameObject(screenManagerEntity.buildingTierUpgradeScreen.gameObject, true);
		}
	}

	void AddBuildingTierUpgradeButtonEntity(
		BuildingAdvancedType buildingAdvancedType,
		int tierUpgradePathIndex,
		BuildingTierUpgradeButtonEntity buildingTierUpgradeButtonEntityPrefab,
		Dictionary<BuildingAdvancedType, BuildingAssetsDataScriptableObject> buildingAssetsDataScriptableObjects,
		// Dictionary<BuildingAdvancedType, BuildingStatsScriptableObject> buildingStatsScriptableObjects,
		GameObject buildingTierUpgradeButtonsObject,
		// Camera mainCamera
		List<BuildingTierUpgradeButtonEntity> buildingTierUpgradeButtonEntities
		)
	{
		BuildingTierUpgradeButtonEntity buildingTierUpgradeButtonEntity = GameObject.Instantiate(buildingTierUpgradeButtonEntityPrefab, buildingTierUpgradeButtonsObject.transform);

		if (!buildingAssetsDataScriptableObjects.ContainsKey(buildingAdvancedType))
			throw new System.Exception("Trying to create a building tier upgrade button for an enum which doesn't have assets implemented");

		buildingTierUpgradeButtonEntity.buildingAssetsDataScriptableObject = buildingAssetsDataScriptableObjects[buildingAdvancedType];
		buildingTierUpgradeButtonEntity.correspondingTierUpgradePathIndex = tierUpgradePathIndex;

		UIHelpers.SetBuildingTierUpgradeButtonUIValues(buildingTierUpgradeButtonEntity);

		buildingTierUpgradeButtonEntity.researchableUIComponent.dirtyUI = false; // TODO: Make use of this. Or set it to false after following the Myth pattern.

		buildingTierUpgradeButtonEntities.Add(buildingTierUpgradeButtonEntity); // TODO: This is different than with Myth buttons, where we don't add them
	}
}

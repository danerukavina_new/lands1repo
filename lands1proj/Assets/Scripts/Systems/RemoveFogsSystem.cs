﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveFogsSystem : BaseSystem
{
	public LevelEntity levelEntity;
	public RitualStateEntity ritualStateEntity;

	List<List<Vector2Int>> coordinatesInDiamondWithRadiusList;
	List<Vector2Int> coordinatesInDiamondWithRadius;

	static void DisableFullAndPartialFogAtCoord(Vector2Int coord, LevelEntity levelEntity)
	{
		if (!levelEntity.CoordIsInLevel(coord))
			return;

		UIHelpers.ShowHideGameObject(levelEntity.GetFullFogEntity(coord).gameObject, false);
		UIHelpers.ShowHideGameObject(levelEntity.GetPartialFogEntity(coord).gameObject, false);
		levelEntity.SetFogWasPreviouslyRevealed(coord, true);
	}

	static void EnableInputIndicatorFogAtCoord(Vector2Int coord, LevelEntity levelEntity)
	{
		UIHelpers.ShowHideGameObject(levelEntity.GetInputIndicatorFogEntity(coord).gameObject, true);
	}

	static void DisableInputIndicatorFogAtCoord(Vector2Int coord, LevelEntity levelEntity)
	{
		if (!levelEntity.CoordIsInLevel(coord))
			return;

		UIHelpers.ShowHideGameObject(levelEntity.GetInputIndicatorFogEntity(coord).gameObject, false);
	}

	public override void SystemUpdate()
	{
		// TODO: Initialize once
		coordinatesInDiamondWithRadiusList = ValueHolder.buffAreaShapes[BuildingStatsScriptableObject.BuffAreaShapeType.DiamondWithRadius];

		if (levelEntity.tileEntitiesAreDirtiedClearOnUpdate)
		{
			// TODO: This is shitty and is preventing unification. Just make these damn things have a fog clearing component. Then it's a one liner and also can be applied to whatever you want
			coordinatesInDiamondWithRadius = coordinatesInDiamondWithRadiusList[ValueHolder.BASE_TILE_SIGHT_RADIUS];

			foreach (var tileEntity in levelEntity.activeTileEntities)
			{
				if (tileEntity.isDirtyClearOnUpdate)
				{
					// NOTE: The (0,0) coord itself isn't in the offset list currently
					DisableFullAndPartialFogAtCoord(tileEntity.coord, levelEntity);
					foreach (var offsetCoord in coordinatesInDiamondWithRadius)
					{
						DisableFullAndPartialFogAtCoord(tileEntity.coord + offsetCoord, levelEntity);
					}
				}
			}
		}

		if (levelEntity.buildingEntitiesAreDirtiedClearOnUpdate)
		{
			foreach (var buildingEntity in levelEntity.activeBuildingEntities)
			{
				if (buildingEntity.workersOrTierIsDirtyClearOnUpdate) // TODO: Is this true the moment the building gets built?
				{
					coordinatesInDiamondWithRadius = coordinatesInDiamondWithRadiusList[buildingEntity.finalFogSightRadius];

					// NOTE: The (0,0) coord itself isn't in the offset list currently
					DisableFullAndPartialFogAtCoord(buildingEntity.tileEntity.coord, levelEntity);
					foreach (var offsetCoord in coordinatesInDiamondWithRadius)
					{
						DisableFullAndPartialFogAtCoord(buildingEntity.tileEntity.coord + offsetCoord, levelEntity);
					}
				}
			}
		}

		if (ritualStateEntity.validNextRitualStepTilesIsDirtyClearOnUpdate)
		{
			foreach (var tileEntity in levelEntity.activeTileEntities)
			{
				EnableInputIndicatorFogAtCoord(tileEntity.coord, levelEntity);
			}

			foreach (var coord in ritualStateEntity.validNextRitualStepCoords)
			{
				DisableInputIndicatorFogAtCoord(coord, levelEntity);
			}

			// NOTE: For visual reasons, we indicate the last a.k.a. ritual step is also clickable, even though it's not an allowed backtrack (since it would be pointless to backtrack to the last current step).
			if (ritualStateEntity.ritualEntity.ritualStepEntities.Count > 0)
				DisableInputIndicatorFogAtCoord(ritualStateEntity.ritualEntity.ritualStepEntities.GetCurrent().tileEntity.coord, levelEntity);

			foreach (var coord in ritualStateEntity.backtrackCountsForAllowedRitualStepCoords.Keys)
			{
				DisableInputIndicatorFogAtCoord(coord, levelEntity);
			}
		}
	}
}

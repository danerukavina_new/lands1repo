﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateRitualStatsAndRitualPickupRowsSystem : BaseSystem
{
	class CountAndStrength
	{
		public int count;
		public double strength;
	}

	public GameStateEntity gameStateEntity; // TODO: This is just included to compute whether the ritual can be performed. Seems wrong. Should there be a general resource satisfiability function checking this?
	// What pattern was used for other menu buttons?
	public RitualStateEntity ritualStateEntity;
	public MenuManagerEntity menuManagerEntity;

	UndoableList<RitualStepEntity> ritualStepEntities;
	RitualStepEntity ritualStepEntity;
	RitualPickupEntity ritualPickupEntity;
	RitualPickupType ritualPickupType;
	BuildingBaseType buildingBaseType;
	int ritualSteps;
	int ritualStepDiscuounts;
	int largestSetSize;
	int differentBuildings;
	Dictionary<RitualPickupType, CountAndStrength> ritualPickupCountsAndStrengthsByRitualPickupType;
	Dictionary<BuildingBaseType, int> buildingCountsByBaseBuilding; // TODO: Game design, we define different buildings based on the base type. I think this makes sense. But think on it.
	CountAndStrength countAndStrengthForType;
	RitualPickupMenuPickupRowEntity ritualPickupMenuPickupRowEntity;

	public UpdateRitualStatsAndRitualPickupRowsSystem()
	{
		ritualPickupCountsAndStrengthsByRitualPickupType = new Dictionary<RitualPickupType, CountAndStrength>(System.Enum.GetNames(typeof(RitualPickupType)).Length);
		buildingCountsByBaseBuilding = new Dictionary<BuildingBaseType, int>(System.Enum.GetNames(typeof(BuildingBaseType)).Length);
	}	

	static double RitualCost(int ritualSteps, int ritualStepDiscuounts)
	{
		int x = ritualSteps - ritualStepDiscuounts; // - (ValueHolder.MINIMUM_RITUAL_STEPS - 1); // NOTE: Design - in line with the flat cost, there's no justification to skip the initial "2" steps cost

		if (x <= 0)
			return 0d;

		return ValueHolder.BASE_RITUAL_COST * x; // NOTE: Design - October 2021, decided to go for flat ritual cost + cooldown implementation, simplest playable test.
	}

	static double LargestSetBonus(int largestSetSize)
	{
		if (largestSetSize <= ValueHolder.LARGEST_SET_ACTIVATION_THRESHOLD)
			return 0d;

		return ValueHolder.BASE_RITUAL_BONUS_COEFFICIENT * (largestSetSize - ValueHolder.LARGEST_SET_ACTIVATION_THRESHOLD);
	}

	static double DifferentBuildingsBonus(int differentBuildings)
	{
		if (differentBuildings <= ValueHolder.DIFFERENT_BUILDING_ACTIVATION_THRESHOLD)
			return 0d;

		return ValueHolder.BASE_RITUAL_BONUS_COEFFICIENT * (differentBuildings - ValueHolder.DIFFERENT_BUILDING_ACTIVATION_THRESHOLD);
	}

	void InitializeRitualResourceDisplayPanel(ResourceDisplayPanelEntity ritualResourceDisplayPanelEntity)
	{
		UIHelpers.ShowHideGameObject(ritualResourceDisplayPanelEntity.gameObject, false);
		UIHelpers.ShowHideGameObject(ritualResourceDisplayPanelEntity.resourceGainsOverTime.gameObject, false);
	}

	void UpdateRitualResourceGainsAndRitualResourceDisplayPanels(RitualPickupType ritualPickupType, ref double targetResourceGains, ResourceDisplayPanelEntity ritualResourceDisplayPanelEntity)
	{
		if (ritualPickupCountsAndStrengthsByRitualPickupType.TryGetValue(ritualPickupType, out countAndStrengthForType))
		{
			targetResourceGains = countAndStrengthForType.strength;
			UIHelpers.ShowHideGameObject(ritualResourceDisplayPanelEntity.gameObject, true);
			ritualResourceDisplayPanelEntity.resourceAmount.text = targetResourceGains.ToNiceString();
		}
		else
			UIHelpers.ShowHideGameObject(ritualResourceDisplayPanelEntity.gameObject, false);
	}

	void Initialize()
	{
		// TODO: Think about what it means to set icons via the scene, while we set everything else via code. Would like to move to setting all things via code always...

		UIHelpers.ShowHideGameObject(menuManagerEntity.spiritCostRitualPickupRowEntity.ritualPickupAnimationPanelsGridView.gameObject, false);
		// Keep alternateToGridViewText
		// Keep resourceDisplayPanelEntity
		// Keep resourceAmountDisplayText
		UIHelpers.ShowHideGameObject(menuManagerEntity.spiritCostRitualPickupRowEntity.resourceGainsOverTimeDisplayText.gameObject, false);
		UIHelpers.ShowHideGameObject(menuManagerEntity.spiritCostRitualPickupRowEntity.alternateToResourceText.gameObject, false);
		UIHelpers.ShowHideGameObject(menuManagerEntity.spiritCostRitualPickupRowEntity.additionalTextOnTheRight.gameObject, false);

		UIHelpers.ShowHideGameObject(menuManagerEntity.largestSetBonusRitualPickupRowEntity.ritualPickupAnimationPanelsGridView.gameObject, false);
		// Keep alternateToGridViewText
		UIHelpers.ShowHideGameObject(menuManagerEntity.largestSetBonusRitualPickupRowEntity.resourceDisplayPanelEntity.gameObject, false);
		// Implicitly hidden resourceAmountDisplayText
		// Implicitly hidden resourceGainsOverTimeDisplayText
		// Keep alternateToResourceText
		UIHelpers.ShowHideGameObject(menuManagerEntity.largestSetBonusRitualPickupRowEntity.additionalTextOnTheRight.gameObject, false);

		UIHelpers.ShowHideGameObject(menuManagerEntity.differentBuildingsBonusRitualPickupRowEntity.ritualPickupAnimationPanelsGridView.gameObject, false);
		// Keep alternateToGridViewText
		UIHelpers.ShowHideGameObject(menuManagerEntity.differentBuildingsBonusRitualPickupRowEntity.resourceDisplayPanelEntity.gameObject, false);
		// Implicitly hidden resourceAmountDisplayText
		// Implicitly hidden resourceGainsOverTimeDisplayText
		// Keep alternateToResourceText
		UIHelpers.ShowHideGameObject(menuManagerEntity.differentBuildingsBonusRitualPickupRowEntity.additionalTextOnTheRight.gameObject, false);

		UIHelpers.ShowHideGameObject(menuManagerEntity.ritualResourceDisplayEntity.spiritResourceDisplayPanelEntity.gameObject, true);
		UIHelpers.ShowHideGameObject(menuManagerEntity.ritualResourceDisplayEntity.spiritResourceDisplayPanelEntity.resourceGainsOverTime.gameObject, false);

		InitializeRitualResourceDisplayPanel(menuManagerEntity.ritualResourceDisplayEntity.foodResourceDisplayPanelEntity);
		InitializeRitualResourceDisplayPanel(menuManagerEntity.ritualResourceDisplayEntity.woodResourceDisplayPanelEntity);
		InitializeRitualResourceDisplayPanel(menuManagerEntity.ritualResourceDisplayEntity.stoneResourceDisplayPanelEntity);
		InitializeRitualResourceDisplayPanel(menuManagerEntity.ritualResourceDisplayEntity.oreResourceDisplayPanelEntity);
		InitializeRitualResourceDisplayPanel(menuManagerEntity.ritualResourceDisplayEntity.goldResourceDisplayPanelEntity);
		InitializeRitualResourceDisplayPanel(menuManagerEntity.ritualResourceDisplayEntity.basicExtraPersonResourceDisplayPanelEntity);
		InitializeRitualResourceDisplayPanel(menuManagerEntity.ritualResourceDisplayEntity.advancedExtraPersonResourceDisplayPanelEntity);

		foreach (var ritualPickupTypeFromRow in menuManagerEntity.ritualPickupMenuPickupRows.Keys)
		{
			ritualPickupMenuPickupRowEntity = menuManagerEntity.ritualPickupMenuPickupRows[ritualPickupTypeFromRow];

			// Keep ritualPickupMenuPickupRowEntity
			UIHelpers.ShowHideGameObject(ritualPickupMenuPickupRowEntity.alternateToGridViewText.gameObject, false); // TODO: Only once
			// Keep resourceDisplayPanelEntity
			// Keep resourceAmountDisplayText
			UIHelpers.ShowHideGameObject(ritualPickupMenuPickupRowEntity.resourceGainsOverTimeDisplayText.gameObject, false); // TODO: Only once
			UIHelpers.ShowHideGameObject(ritualPickupMenuPickupRowEntity.alternateToResourceText.gameObject, false); // TODO: Only once
			UIHelpers.ShowHideGameObject(ritualPickupMenuPickupRowEntity.additionalTextOnTheRight.gameObject, false); // TODO: Only once
		}
	}

	public override void SystemUpdate()
	{
		if (ritualStateEntity.initializeRitualPickupRowsGridView)
		{
			ritualStateEntity.initializeRitualPickupRowsGridView = false;

			Initialize();
		}

		if (ritualStateEntity.isDirtyClearOnUpdate)
		{
			// TODO: Should this just be set once?
			ritualStepEntities = ritualStateEntity.ritualEntity.ritualStepEntities;

			ritualSteps = ritualStepEntities.Count;
			ritualStepDiscuounts = 0;
			largestSetSize = 0;
			differentBuildings = 0;
			ritualPickupCountsAndStrengthsByRitualPickupType.Clear();
			buildingCountsByBaseBuilding.Clear();

			for (int i = 0; i < ritualStepEntities.Count; i++)
			{
				ritualStepEntity = ritualStepEntities[i];

				if (ritualStepEntity.tileEntity.buildingEntity != null)
				{

					ritualPickupEntity = ritualStepEntity.tileEntity.buildingEntity.ritualPickupEntity;
					ritualPickupType = ritualPickupEntity.ritualPickupType;

					{
						if (ritualPickupCountsAndStrengthsByRitualPickupType.ContainsKey(ritualPickupType))
						{
							ritualPickupCountsAndStrengthsByRitualPickupType[ritualPickupType].count++;
							ritualPickupCountsAndStrengthsByRitualPickupType[ritualPickupType].strength += ritualPickupEntity.ritualPickupStrength;
						}
						else
						{
							// TODO: Allocation, fix it
							ritualPickupCountsAndStrengthsByRitualPickupType.Add(ritualPickupType, new CountAndStrength()
							{
								count = 1,
								strength = ritualPickupEntity.ritualPickupStrength
							});
						}

						if (ritualPickupCountsAndStrengthsByRitualPickupType[ritualPickupType].count > largestSetSize)
							largestSetSize = ritualPickupCountsAndStrengthsByRitualPickupType[ritualPickupType].count;
					}

					buildingBaseType = ritualStepEntity.tileEntity.buildingEntity.buildingStatsScriptableObject.impliedBuildingBaseType;

					{
						if (buildingCountsByBaseBuilding.ContainsKey(buildingBaseType))
							buildingCountsByBaseBuilding[buildingBaseType]++;
						else
							buildingCountsByBaseBuilding.Add(buildingBaseType, 1);
					}
				}
			}

			differentBuildings = buildingCountsByBaseBuilding.Keys.Count;

			if (ritualPickupCountsAndStrengthsByRitualPickupType.TryGetValue(RitualPickupType.ExtendRitual, out countAndStrengthForType))
			{
				ritualStepDiscuounts += Mathf.RoundToInt((float)countAndStrengthForType.strength);
			}

			ritualStateEntity.ritualEntity.ritualSpiritCost = RitualCost(ritualSteps, ritualStepDiscuounts);
			menuManagerEntity.ritualResourceDisplayEntity.spiritResourceDisplayPanelEntity.resourceAmount.text = (-ritualStateEntity.ritualEntity.ritualSpiritCost).ToNiceString();

			// TODO: It's annoying to do it for each resource hardcoded here... but perhaps it's also simpler than the dictionary approach

			UpdateRitualResourceGainsAndRitualResourceDisplayPanels(RitualPickupType.FoodPickup, ref ritualStateEntity.ritualEntity.ritualResourceGains.food, menuManagerEntity.ritualResourceDisplayEntity.foodResourceDisplayPanelEntity);
			UpdateRitualResourceGainsAndRitualResourceDisplayPanels(RitualPickupType.WoodPickup, ref ritualStateEntity.ritualEntity.ritualResourceGains.wood, menuManagerEntity.ritualResourceDisplayEntity.woodResourceDisplayPanelEntity);
			UpdateRitualResourceGainsAndRitualResourceDisplayPanels(RitualPickupType.StonePickup, ref ritualStateEntity.ritualEntity.ritualResourceGains.stone, menuManagerEntity.ritualResourceDisplayEntity.stoneResourceDisplayPanelEntity);
			UpdateRitualResourceGainsAndRitualResourceDisplayPanels(RitualPickupType.OrePickup, ref ritualStateEntity.ritualEntity.ritualResourceGains.ore, menuManagerEntity.ritualResourceDisplayEntity.oreResourceDisplayPanelEntity);
			UpdateRitualResourceGainsAndRitualResourceDisplayPanels(RitualPickupType.GoldPickup, ref ritualStateEntity.ritualEntity.ritualResourceGains.gold, menuManagerEntity.ritualResourceDisplayEntity.goldResourceDisplayPanelEntity);
			UpdateRitualResourceGainsAndRitualResourceDisplayPanels(RitualPickupType.BasicExtraPersonPickup, ref ritualStateEntity.ritualEntity.ritualBasicExtraWorkers, menuManagerEntity.ritualResourceDisplayEntity.basicExtraPersonResourceDisplayPanelEntity);
			UpdateRitualResourceGainsAndRitualResourceDisplayPanels(RitualPickupType.AdvancedExtraPersonPickup, ref ritualStateEntity.ritualEntity.ritualAdvancedExtraWorkers, menuManagerEntity.ritualResourceDisplayEntity.advancedExtraPersonResourceDisplayPanelEntity);

			foreach (var ritualPickupTypeFromRow in menuManagerEntity.ritualPickupMenuPickupRows.Keys)
			{
				ritualPickupMenuPickupRowEntity = menuManagerEntity.ritualPickupMenuPickupRows[ritualPickupTypeFromRow];

				if (ritualPickupCountsAndStrengthsByRitualPickupType.TryGetValue(ritualPickupTypeFromRow, out countAndStrengthForType))
				{
					ritualPickupMenuPickupRowEntity.resourceAmountDisplayText.text = countAndStrengthForType.strength.ToNiceString();

					UIHelpers.ShowHideGameObject(ritualPickupMenuPickupRowEntity.gameObject, true);
				}
				else
					UIHelpers.ShowHideGameObject(ritualPickupMenuPickupRowEntity.gameObject, false);
			}

			menuManagerEntity.spiritCostRitualPickupRowEntity.alternateToGridViewText.text = ritualSteps.ToString() + " steps (" + ritualStepDiscuounts.ToString() + " free)";
			menuManagerEntity.spiritCostRitualPickupRowEntity.resourceAmountDisplayText.text = (-ritualStateEntity.ritualEntity.ritualSpiritCost).ToNiceString(); // TODO: Display negative values in red? Would be nice to set it here, explicitly

			ritualStateEntity.ritualEntity.largestSetBonus = LargestSetBonus(largestSetSize);

			menuManagerEntity.largestSetBonusRitualPickupRowEntity.alternateToGridViewText.text = "Largest set: " + largestSetSize.ToString();
			menuManagerEntity.largestSetBonusRitualPickupRowEntity.alternateToResourceText.text = StringHelpers.StringConstants.PLUS + ritualStateEntity.ritualEntity.largestSetBonus.ToNicePercentageString(); // TODO: Display negative values in red? Would be nice to set it here, explicitly

			ritualStateEntity.ritualEntity.differentBuildingsBonus = DifferentBuildingsBonus(differentBuildings);

			menuManagerEntity.differentBuildingsBonusRitualPickupRowEntity.alternateToGridViewText.text = "Different buildings: " + differentBuildings.ToString();
			menuManagerEntity.differentBuildingsBonusRitualPickupRowEntity.alternateToResourceText.text = StringHelpers.StringConstants.PLUS + ritualStateEntity.ritualEntity.differentBuildingsBonus.ToNicePercentageString(); // TODO: Display negative values in red? Would be nice to set it here, explicitly

			// TODO: Feels a little odd to have this here, but we're basically updating it in a few places where the ritual or spirit availability changes. See the note in BeforeUIUpdateSet...System
			LogicHelpers.SetPlayerCanPerformRitual(ritualStateEntity, gameStateEntity);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddWorkerOverTimeSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public GlobalBuffDataEntity globalBuffDataEntity;

	bool addedWorkersOverTime;
	double candidateFinalExtraWorkers;

	public override void SystemUpdate()
	{
		if (gameStateEntity.gameTime > gameStateEntity.previouslyProcessedAddWorkerOverTimeTime + ValueHolder.DELAY_BETWEEN_AUTO_ADD_WORKER_OVER_TIME)
		{
			gameStateEntity.previouslyProcessedAddWorkerOverTimeTime += ValueHolder.DELAY_BETWEEN_AUTO_ADD_WORKER_OVER_TIME;

			Debug.Log("Add Worker Over Time!");
			// TODO: Does this system need to process longer delta times?
		}
		else
			return;

		// TODO: Consider having a flag whether any building is gaining workers over time...
		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			if (globalBuffDataEntity.playerGlobalBuffSetComponent.addFarmWorkerOverFiveMinutes > 0 || buildingEntity.extraWorkersOverTime > 0d)
			{
				LogicHelpers.SetBuildingMaxExtraWorkersAndIsAtMaxExtraWorkers(buildingEntity, gameStateEntity);

				addedWorkersOverTime = false;

				if (!buildingEntity.isAtMaxExtraWorkers)
				{
					candidateFinalExtraWorkers = buildingEntity.extraWorkers;

					if (buildingEntity.extraWorkersOverTime > 0d)
					{
						candidateFinalExtraWorkers = LogicHelpers.ResultingExtraWorkersWithRounding(candidateFinalExtraWorkers, buildingEntity.extraWorkersOverTime, buildingEntity.maxExtraWorkers);
						addedWorkersOverTime = true;
					}

					if (buildingEntity.buildingStatsScriptableObject.isFarm && globalBuffDataEntity.playerGlobalBuffSetComponent.addFarmWorkerOverFiveMinutes > 0d)
					{
						candidateFinalExtraWorkers = LogicHelpers.ResultingExtraWorkersWithRounding(candidateFinalExtraWorkers, globalBuffDataEntity.playerGlobalBuffSetComponent.addFarmWorkerOverFiveMinutes, buildingEntity.maxExtraWorkers);
						addedWorkersOverTime = true;
					}

					if (buildingEntity.buildingStatsScriptableObject.isMine && globalBuffDataEntity.playerGlobalBuffSetComponent.addMineWorkersOverFiveMinutes > 0d)
					{
						candidateFinalExtraWorkers = LogicHelpers.ResultingExtraWorkersWithRounding(candidateFinalExtraWorkers, globalBuffDataEntity.playerGlobalBuffSetComponent.addMineWorkersOverFiveMinutes, buildingEntity.maxExtraWorkers);
						addedWorkersOverTime = true;
					}
				}

				// TODO: Aren't additional workers going to be a separate thing, which shouldn't matter for building tiers?
				if (addedWorkersOverTime)
				{
					buildingEntity.extraWorkers = candidateFinalExtraWorkers;

					buildingEntity.workersOrTierIsDirtyClearOnUpdate = true;
					levelEntity.buildingEntitiesAreDirtiedClearOnFixedUpdate = true;
					levelEntity.buildingEntitiesAreDirtiedClearOnUpdate = true;

					LogicHelpers.SetBuildingMaxExtraWorkersAndIsAtMaxExtraWorkers(buildingEntity, gameStateEntity); // NOTE: Needed to set isAtMaxExtraWorkers in a timely manner
				}
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcessBuildingButtonClicksThatCauseScreensSystem : BaseSystem
{
	public ScreenManagerEntity screenManagerEntity;
	public LevelEntity levelEntity;

	BuildingEntity buildingTierUpgradeBuildingEntity;

	public override void SystemUpdate()
	{
		// TODO: This system could benefit from a global "dirtyness" variable indicating any such menu item was clicked? Though that would make it more bug prone?

		buildingTierUpgradeBuildingEntity = null;

		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			if (buildingEntity.openBuildingTierUpgradeMenu)
			{
				buildingEntity.openBuildingTierUpgradeMenu = false;

				buildingTierUpgradeBuildingEntity = buildingEntity;
			}
		}

		if (buildingTierUpgradeBuildingEntity != null)
		{
			if (buildingTierUpgradeBuildingEntity.isAtMaxTrainedWorkersForTierAndThusMayTierUpgrade) 
			{
				screenManagerEntity.buildingTierUpgradeBuildingEntity = buildingTierUpgradeBuildingEntity;
				screenManagerEntity.buildingTierUpgradeScreenShouldBeVisible = true;

				screenManagerEntity.buildingTierUpgradeScreenIsDirtyClearOnUpdate = true;
			}
			else
				Debug.Log("Unexpected click on Upgrade Tier when it isn't available.");
		}
	}
}

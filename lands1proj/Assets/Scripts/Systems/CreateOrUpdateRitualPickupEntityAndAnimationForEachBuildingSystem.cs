﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateOrUpdateRitualPickupEntityAndAnimationForEachBuildingSystem : BaseSystem
{
	public LevelEntity levelEntity;
	public RitualStateEntity ritualStateEntity;
	public Dictionary<RitualPickupType, RuntimeAnimatorController> ritualPickupAnimationControllers;
	public RitualPickupAnimationEntity ritualPickupAnimationEntityPrefab;
	public GameObject ritualPickupAnimationEntitiesObject;

	RitualPickupType previousRitualPickupType;
	double previousRitualPickupStrength;
	RitualPickupType nextRitualPickupType;
	double nextRitualPickupStrength;

	static double RitualPickupStrength(RitualPickupType ritualPickupType, BuildingEntity buildingEntity, double ritualPickupStrengthFactor)
	{
		switch (ritualPickupType)
		{
			case RitualPickupType.FoodPickup:
				return ritualPickupStrengthFactor * buildingEntity.buildingResourceSetGainsOverTimeComponent.food * ValueHolder.RITUAL_TO_RESOURCE_GAIN_SECONDS_FACTOR;
			case RitualPickupType.WoodPickup:
				return ritualPickupStrengthFactor * buildingEntity.buildingResourceSetGainsOverTimeComponent.wood * ValueHolder.RITUAL_TO_RESOURCE_GAIN_SECONDS_FACTOR;
			case RitualPickupType.StonePickup:
				return ritualPickupStrengthFactor * buildingEntity.buildingResourceSetGainsOverTimeComponent.stone * ValueHolder.RITUAL_TO_RESOURCE_GAIN_SECONDS_FACTOR;
			case RitualPickupType.OrePickup:
				return ritualPickupStrengthFactor * buildingEntity.buildingResourceSetGainsOverTimeComponent.ore * ValueHolder.RITUAL_TO_RESOURCE_GAIN_SECONDS_FACTOR;
			case RitualPickupType.GoldPickup:
				return ritualPickupStrengthFactor * buildingEntity.buildingResourceSetGainsOverTimeComponent.gold * ValueHolder.RITUAL_TO_RESOURCE_GAIN_SECONDS_FACTOR;
			case RitualPickupType.BasicTrainedPersonSpeedPickup:
				throw new System.Exception("NYI - this enum value is TBD");
			case RitualPickupType.BasicExtraPersonPickup:
				return ritualPickupStrengthFactor;
			case RitualPickupType.AdvancedTrainedPersonSpeedPickup:
				throw new System.Exception("NYI - this enum value is TBD");
			case RitualPickupType.AdvancedExtraPersonPickup:
				return ritualPickupStrengthFactor;
			case RitualPickupType.ExtendRitual:
				return ritualPickupStrengthFactor;
			default:
				throw new System.Exception("Unexpected Ritual Pickup Type when calculating ritual pickup strength.");
		}
	}

	static void AddOrUpdateRitualPickupEntityAndAnimationForBuilding(
		BuildingEntity buildingEntity,
		Dictionary<RitualPickupType, RuntimeAnimatorController> ritualPickupAnimationControllers,
		RitualPickupAnimationEntity ritualPickupAnimationEntityPrefab,
		GameObject ritualPickupAnimationEntitiesObject,
		RitualStateEntity ritualStateEntity,
		bool recreateRitualPickup
		// bool setAnimationControllerDuringUpdate = false // TODO: Consider keeping this after all. We're using "recreateRitualPickup" instead to make the house -> villa scenario clearly show the pickup has changed.
		)
	{
		// TODO: Allocation
		buildingEntity.ritualPickupEntity = new RitualPickupEntity()
		{
			ritualPickupType = buildingEntity.buildingStatsScriptableObject.prototypeRitualPickupComponent.ritualPickupType,
			ritualPickupStrength = RitualPickupStrength(
				buildingEntity.buildingStatsScriptableObject.prototypeRitualPickupComponent.ritualPickupType,
				buildingEntity,
				buildingEntity.buildingStatsScriptableObject.prototypeRitualPickupComponent.ritualPickupStrengthFactor
				),
			sourceBuildingEntity = buildingEntity
		};

		if (recreateRitualPickup)
		{
			if (buildingEntity.ritualPickupAnimationEntity != null)
				GameObject.Destroy(buildingEntity.ritualPickupAnimationEntity.gameObject);

			// TODO: Make sure that instantly picked up RitualPickupEntities are in the right position. May need to include the offset here? Or perhaps in the HoverPickupInWorldSystem
			RitualPickupAnimationEntity ritualPickupAnimationEntity = GameObject.Instantiate(ritualPickupAnimationEntityPrefab, buildingEntity.transform.position, Quaternion.identity, ritualPickupAnimationEntitiesObject.transform);

			ritualPickupAnimationEntity.hoverPickupInWorldComponent.hoverPickupInWorldComponentState = HoverPickupInWorldComponentState.Hovering;
			ritualPickupAnimationEntity.hoverPickupInWorldComponent.transitionToNextState = false;
			ritualPickupAnimationEntity.ritualPickupAnimationController.runtimeAnimatorController = ritualPickupAnimationControllers[buildingEntity.ritualPickupEntity.ritualPickupType];

			buildingEntity.ritualPickupAnimationEntity = ritualPickupAnimationEntity;
		}

		//if (setAnimationControllerDuringUpdate)
		//	buildingEntity.ritualPickupAnimationEntity.ritualPickupAnimationController.runtimeAnimatorController = ritualPickupAnimationControllers[buildingEntity.ritualPickupEntity.ritualPickupType];

		buildingEntity.ritualPickupAnimationEntity.valueText.text = StringHelpers.RitualPickupValueText(buildingEntity.ritualPickupEntity.ritualPickupType, buildingEntity.ritualPickupEntity.ritualPickupStrength);

		// NOTE: The code below determines which state the pickup should be in - if it's already in the path of the ritual, it should immediately get picked up
		RitualStepEntity ritualStepEntity = null;
		bool tileAlreadyIsInPlannedRitual = false;

		// TODO: Refactor this, right? There's a function that generalizes this.
		for (int i = 0; i < ritualStateEntity.ritualEntity.ritualStepEntities.Count && !tileAlreadyIsInPlannedRitual; i++)
		{
			ritualStepEntity = ritualStateEntity.ritualEntity.ritualStepEntities[i];

			if (ritualStepEntity.tileEntity == buildingEntity.tileEntity)
			{
				tileAlreadyIsInPlannedRitual = true;
			}
		}

		if (tileAlreadyIsInPlannedRitual)
		{
			if (recreateRitualPickup)
				ritualStateEntity.ritualEntity.ritualStepEntitiesAddingPickupsClearOnUpdate.Add(ritualStepEntity);
			else
				ritualStateEntity.ritualEntity.ritualStepEntitiesUpdatingPickupsClearOnUpdate.Add(ritualStepEntity);

			// NOTE: This is a little inconsistent with how the other isDirtyClearOnUpdate situations are marked... but it corresponds to an implicit Update of a ritualStepEntity element
			// Without this, various automatic interactions such as buildings adding workers to other buildings do not update the ritual's stats.
			ritualStateEntity.isDirtyClearOnUpdate = true;
		}
	}

	public override void SystemUpdate()
	{
		if (
			levelEntity.buildingEntitiesAreDirtiedClearOnUpdate
			)
		{
			foreach (var buildingEntity in levelEntity.activeBuildingEntities)
			{
				if (
					buildingEntity.workersOrTierIsDirtyClearOnUpdate // TODO: This should be only on tier dirty... and only if the animation changed? How do we track this?
					)
				{
					if (buildingEntity.ritualPickupAnimationEntity != null)
					{
						previousRitualPickupType = buildingEntity.ritualPickupEntity.ritualPickupType;
						previousRitualPickupStrength = buildingEntity.ritualPickupEntity.ritualPickupStrength;

						nextRitualPickupType = buildingEntity.buildingStatsScriptableObject.prototypeRitualPickupComponent.ritualPickupType;
						nextRitualPickupStrength = buildingEntity.buildingStatsScriptableObject.prototypeRitualPickupComponent.ritualPickupStrengthFactor;

						// NOTE: the last parameter, recreateRitualPickup being true in this case works as follow, hopefully. Upgrading a House to a Villa while it's in the ritual causes the pickup animation to play again, to clarify the pickup changed and was then immediately picked up.
						if (previousRitualPickupType != nextRitualPickupType)
							AddOrUpdateRitualPickupEntityAndAnimationForBuilding(buildingEntity, ritualPickupAnimationControllers, ritualPickupAnimationEntityPrefab, ritualPickupAnimationEntitiesObject, ritualStateEntity, recreateRitualPickup: true);
						else if (previousRitualPickupStrength != nextRitualPickupStrength)
							AddOrUpdateRitualPickupEntityAndAnimationForBuilding(buildingEntity, ritualPickupAnimationControllers, ritualPickupAnimationEntityPrefab, ritualPickupAnimationEntitiesObject, ritualStateEntity, recreateRitualPickup: false);
					}
					else
						AddOrUpdateRitualPickupEntityAndAnimationForBuilding(buildingEntity, ritualPickupAnimationControllers, ritualPickupAnimationEntityPrefab, ritualPickupAnimationEntitiesObject, ritualStateEntity, recreateRitualPickup: true);
				}
			}
		}
	}
}

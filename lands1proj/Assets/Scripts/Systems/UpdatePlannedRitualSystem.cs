﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdatePlannedRitualSystem : BaseSystem
{
	public GameStateEntity gameStateEntity; // TODO: Use animationTime instead of gameTime once it's better defined
	public InputStateEntity inputStateEntity;
	public RitualStateEntity ritualStateEntity;
	public Dictionary<RitualStepAnimationType, RuntimeAnimatorController> ritualStepAnimationControllers;
	public RitualStepAnimationEntity ritualStepAnimationEntityPrefab;
	public GameObject ritualStepAnimationEntitiesObject;


	RitualStepEntity ritualStepEntity;
	RitualStepEntity previousRitualStepEntity;
	RitualStepEntity previousPreviousRitualStepEntity;
	UndoableList<RitualStepEntity> ritualStepEntities;

	static void AddRitualStepConsequences(
		RitualStepEntity addedRitualStepEntity, 
		RitualStepEntity previousRitualStepEntity,
		RitualStepEntity previousPreviousRitualStepEntity,
		float animationTime,
		RitualStateEntity ritualStateEntity,
		Dictionary<RitualStepAnimationType, RuntimeAnimatorController> ritualStepAnimationControllers,
		RitualStepAnimationEntity ritualStepAnimationEntityPrefab,
		GameObject ritualStepAnimationEntitiesObject)
	{
		if (
			ritualStateEntity.candidateTileForNextPlannedRitualStepEntity.buildingEntity != null
			&& ritualStateEntity.candidateTileForNextPlannedRitualStepEntity.buildingEntity.ritualPickupEntity != null // TODO: This check seems unnecessary, existance of a ritual pickup should be guaranteed for a building, no?
			)
		{
			ritualStateEntity.ritualEntity.ritualStepEntitiesAddingPickupsClearOnUpdate.Add(addedRitualStepEntity);
		}

		SetRitualStepStuffForCurrentRitualStepEntity(addedRitualStepEntity, previousRitualStepEntity, animationTime, ritualStateEntity, ritualStepAnimationControllers, ritualStepAnimationEntityPrefab, ritualStepAnimationEntitiesObject);

		if (previousRitualStepEntity != null)
		{
			SetRitualStepStuffForPreviousRitualStepEntity(addedRitualStepEntity, previousRitualStepEntity, previousPreviousRitualStepEntity, animationTime, ritualStateEntity, ritualStepAnimationControllers, ritualStepAnimationEntityPrefab, ritualStepAnimationEntitiesObject);
		}
	}

	static void UndoRitualStepConsequences(
		RitualStepEntity undoneRitualStepEntity,
		RitualStepEntity previousRitualStepEntity,
		RitualStepEntity previousPreviousRitualStepEntity,
		float animationTime,
		RitualStateEntity ritualStateEntity,
		Dictionary<RitualStepAnimationType, RuntimeAnimatorController> ritualStepAnimationControllers,
		RitualStepAnimationEntity ritualStepAnimationEntityPrefab,
		GameObject ritualStepAnimationEntitiesObject)
	{
		if (
			undoneRitualStepEntity.tileEntity.buildingEntity != null
			&& undoneRitualStepEntity.tileEntity.buildingEntity.ritualPickupEntity != null // TODO: This check seems unnecessary, existance of a ritual pickup should be guaranteed for a building, no?
			)
		{
			// NOTE: Implicitly, the state of the ritualStepEntities being undoable GUARANTEES that the system that processes this list can set the nextHoverPickupInWorldComponentState to be UndoPickupMaterializing
			ritualStateEntity.ritualEntity.ritualStepEntitiesRemovingPickupsClearOnUpdate.Add(undoneRitualStepEntity);
		}

		RemoveRitualStepAnimation(undoneRitualStepEntity);

		if (previousRitualStepEntity != null)
		{
			RemoveRitualStepAnimation(previousRitualStepEntity);

			SetRitualStepStuffForCurrentRitualStepEntity(previousRitualStepEntity, previousPreviousRitualStepEntity, animationTime, ritualStateEntity, ritualStepAnimationControllers, ritualStepAnimationEntityPrefab, ritualStepAnimationEntitiesObject);
		}
	}

	static void RemoveRitualStepAnimation(RitualStepEntity removedRitualStepEntity)
	{
		Object.Destroy(removedRitualStepEntity.ritualStepAnimationEntity.gameObject);
	}

	static Quaternion FromToRotationInXYPlane(Vector3 directionFrom, Vector3 directionTo)
	{
		// NOTE: This assumes directionFrom and directionTo are in the X, Y plane

		// NOTE: When the two directions are oposite, we can't use FromToRotation because it can pick an arbitrary axisto rotate about... and we want it to make a Z rotation
		// So use this predefined one. Otherwise we may see a -180 rotation about the X or Y axis which would look reflected
		if (Mathf.Approximately((directionFrom + directionTo).sqrMagnitude, 0f))
			return ValueHolder.ROTIATION_OF_180_ABOUT_Z_QUATERNION;

		return Quaternion.FromToRotation(directionFrom, directionTo);
	}

	static Quaternion CalculateRitualStepAnimationQuaternionForIsolatedStep()
	{
		return Quaternion.identity;
	}

	static Quaternion CalculateRitualStepAnimationQuaternionForStarttStep(Vector2Int directionFrom)
	{
		// NOTE: Here we're indeed trying to rotate from the default position of the graphic (facing up) to the direction it's supposed to be pointing at. Which is the directionFrom (effectively directionFromPrevious).
		return FromToRotationInXYPlane(ValueHolder.INITIAL_RITUAL_STEP_ANIMATION_DIRECTION, new Vector3((float)directionFrom.x, (float)directionFrom.y));
	}

	static Quaternion CalculateRitualStepAnimationQuaternionForCurrentOrEndStep(Vector2Int directionTo)
	{
		return FromToRotationInXYPlane(ValueHolder.INITIAL_RITUAL_STEP_ANIMATION_DIRECTION, new Vector3((float)directionTo.x, (float)directionTo.y));
	}

	static void RitualStepAnimationTypeAndQuaternionFromDirections(out RitualStepAnimationType ritualStepAnimationType, out Quaternion quaternion, Vector2Int? directionTo, Vector2Int? directionFrom)
	{
		if (!directionTo.HasValue) 
		{
			if (!directionFrom.HasValue)
			{
				ritualStepAnimationType = RitualStepAnimationType.Isolated;
				quaternion = CalculateRitualStepAnimationQuaternionForIsolatedStep();
			}
			else // Has directionFrom
			{
				ritualStepAnimationType = RitualStepAnimationType.Start;
				quaternion = CalculateRitualStepAnimationQuaternionForStarttStep(directionFrom.Value);
			}
		}
		else // Has directionTo
		{
			quaternion = CalculateRitualStepAnimationQuaternionForCurrentOrEndStep(directionTo.Value);

			if (!directionFrom.HasValue)
			{
				ritualStepAnimationType = RitualStepAnimationType.End;
			}
			else // Has both directionTo and directionFrom
			{
				// NOTE: Although the function here defines a "from" and "to" parameter... in some sense it's "initial" and "final" - and indeed here our "directionTo" (the node) is the initial angle of the path, and "directionFrom" (the node) is the final angle of the path
				// So this function is called correctly, albeit confusingly
				float angle = Vector2.SignedAngle(from:(Vector2)directionTo, to:(Vector2)directionFrom);

				if (Mathf.Approximately(angle, 180f) || Mathf.Approximately(angle, -180f))
					throw new System.Exception("From and to directions for a ritual step animation are pointing in opposite directions, which should be impossible.");
				else if (Mathf.Approximately(angle, 0f))
					ritualStepAnimationType = RitualStepAnimationType.UpToUp;
				else if (Mathf.Approximately(angle, 90f))
					ritualStepAnimationType = RitualStepAnimationType.UpToLeft;
				else if (Mathf.Approximately(angle, -90f))
					ritualStepAnimationType = RitualStepAnimationType.UpToRight;
				else
					throw new System.Exception("Unexpected angle detected when determining ritual step animation type from directions.");
			}
		}
	}

	static void SetRitualStepStuffForCurrentRitualStepEntity(
		RitualStepEntity currentRitualStepEntity, 
		RitualStepEntity previousRitualStepEntity,
		float animationTime,
		RitualStateEntity ritualStateEntity,
		Dictionary<RitualStepAnimationType, RuntimeAnimatorController> ritualStepAnimationControllers,
		RitualStepAnimationEntity ritualStepAnimationEntityPrefab,
		GameObject ritualStepAnimationEntitiesObject
		)
	{
		if (currentRitualStepEntity == null)
			throw new System.Exception("Attempting to set animations for a ritual step entity that doesn't exist. Shouldn't be possible.");

		RitualStepAnimationType ritualStepAnimationType;
		Quaternion quaternion;

		if (previousRitualStepEntity != null)
		{
			if (!LogicHelpers.CoordIsAdjacent(currentRitualStepEntity.tileEntity.coord, previousRitualStepEntity.tileEntity.coord))
				throw new System.Exception("Ritual coordinates are not adjacent, can't set animation properly");

			Vector2Int directionToCurrent = currentRitualStepEntity.tileEntity.coord - previousRitualStepEntity.tileEntity.coord;	

			RitualStepAnimationTypeAndQuaternionFromDirections(out ritualStepAnimationType, out quaternion, directionToCurrent, null); // End
		}
		else
		{
			RitualStepAnimationTypeAndQuaternionFromDirections(out ritualStepAnimationType, out quaternion, null, null); // Isolated
		}

		AddRitualStepAnimation(currentRitualStepEntity, ritualStepAnimationType, quaternion, animationTime, ritualStepAnimationControllers, ritualStepAnimationEntityPrefab, ritualStepAnimationEntitiesObject);
	}

	static void SetRitualStepStuffForPreviousRitualStepEntity(
		RitualStepEntity currentRitualStepEntity, 
		RitualStepEntity previousRitualStepEntity, 
		RitualStepEntity previousPreviousRitualStepEntity,
		float animationTime,
		RitualStateEntity ritualStateEntity,
		Dictionary<RitualStepAnimationType, RuntimeAnimatorController> ritualStepAnimationControllers,
		RitualStepAnimationEntity ritualStepAnimationEntityPrefab,
		GameObject ritualStepAnimationEntitiesObject
		)
	{
		if (currentRitualStepEntity == null)
			throw new System.Exception("Attempting to set animations for a previous ritual step entity while th current one doesn't exist. Shouldn't be possible.");

		if (!LogicHelpers.CoordIsAdjacent(currentRitualStepEntity.tileEntity.coord, previousRitualStepEntity.tileEntity.coord))
			throw new System.Exception("Ritual coordinates are not adjacent between current and previous step, can't set animation properly");

		RitualStepAnimationType ritualStepAnimationType;
		Quaternion quaternion;

		Vector2Int directionFromPrevious = currentRitualStepEntity.tileEntity.coord - previousRitualStepEntity.tileEntity.coord;
		
		if (previousPreviousRitualStepEntity != null)
		{
			if (!LogicHelpers.CoordIsAdjacent(previousRitualStepEntity.tileEntity.coord, previousPreviousRitualStepEntity.tileEntity.coord))
				throw new System.Exception("Ritual coordinates are not adjacent between previous and previous previous step, can't set animation properly");

			Vector2Int directionToPrevious = previousRitualStepEntity.tileEntity.coord - previousPreviousRitualStepEntity.tileEntity.coord;

			RitualStepAnimationTypeAndQuaternionFromDirections(out ritualStepAnimationType, out quaternion, directionToPrevious, directionFromPrevious); // UpToUp, UpToLeft, or UpToRight

		}
		else
		{
			RitualStepAnimationTypeAndQuaternionFromDirections(out ritualStepAnimationType, out quaternion, null, directionFromPrevious); // Start
		}

		RemoveRitualStepAnimation(previousRitualStepEntity);

		AddRitualStepAnimation(previousRitualStepEntity, ritualStepAnimationType, quaternion, animationTime, ritualStepAnimationControllers, ritualStepAnimationEntityPrefab, ritualStepAnimationEntitiesObject);
	}

	// TODO: The usage of this function is really messed up. Sometimes it truly does mean previous... sometimes it doesn't. Perhaps this will cause more confusion than it will help.
	void SetPreviousAndPreviousToPreviousRitualStepEntitiesFromCurrent()
	{
		if (ritualStepEntities.Count > 0)
			previousRitualStepEntity = ritualStepEntities.GetCurrent();
		else
			previousRitualStepEntity = null;

		if (ritualStepEntities.Count > 1)
			previousPreviousRitualStepEntity = ritualStepEntities.GetPreviousToCurrent();
		else
			previousPreviousRitualStepEntity = null;
	}

	static void AddRitualStepAnimation(
		RitualStepEntity ritualStepEntity,
		RitualStepAnimationType calculatedRitualStepAnimationType,
		Quaternion calculatedRotation,
		float animationTime,
		Dictionary<RitualStepAnimationType, RuntimeAnimatorController> ritualStepAnimationControllers,
		RitualStepAnimationEntity ritualStepAnimationEntityPrefab,
		GameObject ritualStepAnimationEntitiesObject
	)
	{
		Vector3 pos = LogicHelpers.CoordToPosition(ritualStepEntity.tileEntity.coord);

		RitualStepAnimationEntity ritualStepAnimationEntity = GameObject.Instantiate(ritualStepAnimationEntityPrefab, pos, calculatedRotation, ritualStepAnimationEntitiesObject.transform);

		ritualStepAnimationEntity.ritualStepAnimationController.runtimeAnimatorController = ritualStepAnimationControllers[calculatedRitualStepAnimationType];

		// NOTE: This could also take the animation's "Speed" into account by multiplying by the said speed. (This conclusion came out of testing and using IsolatedRitualStepAnimationController's Speed as a substitute to the animation time... but not 100% certain)
		float finalAnimationTime = ValueHolder.BASE_RITUAL_STEP_ANIMATION_DURATION;
		float normalizedAnimationTimeUsedToGloballySyncUpRitualStepAnimations = (animationTime % finalAnimationTime) / finalAnimationTime;

		ritualStepAnimationEntity.ritualStepAnimationController.Play(
			ValueHolder.RITUAL_STEP_ANIMATION_STATE_NAME, 
			ValueHolder.RITUAL_STEP_ANIMATION_LAYER,
			normalizedAnimationTimeUsedToGloballySyncUpRitualStepAnimations
			);

		ritualStepEntity.ritualStepAnimationEntity = ritualStepAnimationEntity;
	}

	public override void SystemUpdate()
	{
		// TODO: This should be set just once... need a pattern for initializing systems (can't be set in constructor without passing it as a variable... the syntactic sugar for initalization doesn't occur before the constructor)
		ritualStepEntities = ritualStateEntity.ritualEntity.ritualStepEntities;

		if (inputStateEntity.worldInputType == InputStateEntity.WorldInputType.RitualWorldInput)
		{
			switch (inputStateEntity.ritualInputType)
			{
				case InputStateEntity.RitualInputType.TapNextNode:
					break;
				case InputStateEntity.RitualInputType.SwipeToSelectWithRecenteringWhenLettingGo:
					break;
				case InputStateEntity.RitualInputType.SwipeToSelectWithRubberBandingAtEdges:

					// TODO: As new kinds of rituals are implemented, yet another case is needed here - since inputs are different based on the ritual type. This code is gonna get ugly, it's time to separate it out to functions.

					if (ritualStateEntity.attemptToAddRitualStepEntity)
					{
						ritualStateEntity.attemptToAddRitualStepEntity = false;

						if (ritualStateEntity.redoRitualStepsTriggered > 0 || ritualStateEntity.undoRitualStepsTriggered > 0)
							throw new System.Exception("User input resulted in both undoing/redoing and individually adding a ritual step, which isn't desireable and is somewhat undefined.");

						SetPreviousAndPreviousToPreviousRitualStepEntitiesFromCurrent();

						// TODO: Instead of creating this here, manage creation in a unified system that looks at undoRitualStepsTriggered, redoRitualStepsTriggered, and some creation variables - specifically a tile entity.
						ritualStepEntity = new RitualStepEntity()
						{
							tileEntity = ritualStateEntity.candidateTileForNextPlannedRitualStepEntity
						};

						ritualStepEntities.Add(ritualStepEntity);
						ritualStateEntity.isDirtyClearOnUpdate = true;

						// TODO: Use animationTime instead of gameTime once it's better defined
						AddRitualStepConsequences(addedRitualStepEntity: ritualStepEntity, previousRitualStepEntity, previousPreviousRitualStepEntity, gameStateEntity.gameTime, ritualStateEntity, ritualStepAnimationControllers, ritualStepAnimationEntityPrefab, ritualStepAnimationEntitiesObject);
					}

					while (ritualStateEntity.undoRitualStepsTriggered > 0)
					{
						ritualStateEntity.undoRitualStepsTriggered--;

						if (ritualStepEntities.Count > 0)
						{
							ritualStepEntity = ritualStepEntities.GetCurrent();

							ritualStepEntities.Undo();
							ritualStateEntity.isDirtyClearOnUpdate = true; // TODO: Currently we pair this with every call to Undo, Redo, Clear, and Add. Perhaps it should just be implemented in those functions. How about a "ref" dirtyness tracker bool in the function calls? ewww
																		   // TODO: This is also set when a building that's in the ritual has a stat change, so it's not about just Undo, Redo, Clear, and Add. Also about modifying

							SetPreviousAndPreviousToPreviousRitualStepEntitiesFromCurrent();

							// TODO: Use animationTime instead of gameTime once it's better defined
							UndoRitualStepConsequences(undoneRitualStepEntity: ritualStepEntity, previousRitualStepEntity, previousPreviousRitualStepEntity, gameStateEntity.gameTime, ritualStateEntity, ritualStepAnimationControllers, ritualStepAnimationEntityPrefab, ritualStepAnimationEntitiesObject);
						}
						else
							throw new System.Exception("Attempting to undo a ritual step when there's no ritual steps planned.");
					}

					while (ritualStateEntity.redoRitualStepsTriggered > 0)
					{
						ritualStateEntity.redoRitualStepsTriggered--;

						if (ritualStepEntities.NumberOfRedosPossible > 0)
						{
							SetPreviousAndPreviousToPreviousRitualStepEntitiesFromCurrent();

							ritualStepEntities.Redo();
							ritualStateEntity.isDirtyClearOnUpdate = true;

							ritualStepEntity = ritualStepEntities.GetCurrent();

							// TODO: Use animationTime instead of gameTime once it's better defined
							AddRitualStepConsequences(addedRitualStepEntity: ritualStepEntity, previousRitualStepEntity, previousPreviousRitualStepEntity, gameStateEntity.gameTime, ritualStateEntity, ritualStepAnimationControllers, ritualStepAnimationEntityPrefab, ritualStepAnimationEntitiesObject);
						}
						else
							throw new System.Exception("Attempting to redo a ritual step when there's no ritual steps planned.");
					}

					break;
				default:
					break;
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Updates the menus of all the buildings, specifically whether certain building actions can be taken (or are even shown as an option), based on game state.
/// </summary>
// TODO: Should all systems that update resource displays be one system that updates resource display components from various entities?
public class UpdateResourceDisplayBasedOnGameStateSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public ResourceDisplayEntity resourceDisplayEntity;
	public MenuManagerEntity menuManagerEntity;
	public ButtonManagerEntity buttonManagerEntity;
	public ScreenManagerEntity screenManagerEntity;

	static void UpdateResourceDisplayPanelEntityBasedOnResourceComponent(ResourceDisplayPanelEntity resourceDisplayPanelEntity, double amount, double gainsOverTime)
	{
		if (!Mathf.Approximately((float) amount, 0f))
		{
			resourceDisplayPanelEntity.resourceAmount.text = amount.ToNiceString();

			if (!resourceDisplayPanelEntity.gameObject.activeSelf)
				resourceDisplayPanelEntity.gameObject.SetActive(true);

			if (!Mathf.Approximately((float)gainsOverTime, 0f))
				resourceDisplayPanelEntity.resourceGainsOverTime.text = StringHelpers.StringConstants.PLUS + gainsOverTime.ToNiceString();
			else
				resourceDisplayPanelEntity.resourceGainsOverTime.text = string.Empty;
		}
		else
		{
			if (resourceDisplayPanelEntity.gameObject.activeSelf)
				resourceDisplayPanelEntity.gameObject.SetActive(false);
		}
	}

	static void UpdateSpecificMenusBasedOnGameState(MenuEntity menuEntity, double amount, double gainsOverTime)
	{
		UpdateResourceDisplayPanelEntityBasedOnResourceComponent(menuEntity.resourceDisplayPanelEntity0, amount, gainsOverTime);
	}

	static void UpdateMythMenuSpiritCostProgresses(ButtonManagerEntity buttonManagerEntity, double spiritAmount)
	{
		double minOfSpiritAmountAndCost;

		foreach (var mythBuyingButtonEntity in buttonManagerEntity.mythBuyingButtonEntities)
		{
			minOfSpiritAmountAndCost = System.Math.Min(spiritAmount, mythBuyingButtonEntity.baseResearchCost);

			mythBuyingButtonEntity.researchableUIComponent.costProgressText.text = minOfSpiritAmountAndCost.ToNiceString() + StringHelpers.StringConstants.SPACE + StringHelpers.StringConstants.FORWARD_SLASH + System.Environment.NewLine + mythBuyingButtonEntity.baseResearchCost.ToNiceString();
		}
	}

	public override void SystemUpdate()
	{
		// TODO: Code smell, maybe ResourceSetComponents should be refactored to use a dictionary of resources after all. It may likely require dictionarifying ResourceDisplauEntity, which would be annoying

		UpdateResourceDisplayPanelEntityBasedOnResourceComponent(
			resourceDisplayEntity.foodResourceDisplayPanelEntity,
			gameStateEntity.playerResourceSetAmountComponent.food,
			gameStateEntity.playerResourceSetGainsOverTimeComponent.food
			);

		UpdateResourceDisplayPanelEntityBasedOnResourceComponent(
			resourceDisplayEntity.woodResourceDisplayPanelEntity,
			gameStateEntity.playerResourceSetAmountComponent.wood,
			gameStateEntity.playerResourceSetGainsOverTimeComponent.wood
			);

		UpdateResourceDisplayPanelEntityBasedOnResourceComponent(
			resourceDisplayEntity.stoneResourceDisplayPanelEntity,
			gameStateEntity.playerResourceSetAmountComponent.stone,
			gameStateEntity.playerResourceSetGainsOverTimeComponent.stone
			);

		UpdateResourceDisplayPanelEntityBasedOnResourceComponent(
			resourceDisplayEntity.spiritResourceDisplayPanelEntity,
			gameStateEntity.playerResourceSetAmountComponent.spirit,
			gameStateEntity.playerResourceSetGainsOverTimeComponent.spirit
			);

		UpdateResourceDisplayPanelEntityBasedOnResourceComponent(
			resourceDisplayEntity.oreResourceDisplayPanelEntity,
			gameStateEntity.playerResourceSetAmountComponent.ore,
			gameStateEntity.playerResourceSetGainsOverTimeComponent.ore
			);

		UpdateSpecificMenusBasedOnGameState(menuManagerEntity.goldMenuEntity, gameStateEntity.playerResourceSetAmountComponent.gold, gameStateEntity.playerResourceSetGainsOverTimeComponent.gold);
		UpdateSpecificMenusBasedOnGameState(menuManagerEntity.ascendMenuEntity, gameStateEntity.ascendGoldGainAmount, 0d);

		if (screenManagerEntity.mythsScreen.gameObject.activeSelf)
		{
			UpdateMythMenuSpiritCostProgresses(buttonManagerEntity, gameStateEntity.playerResourceSetAmountComponent.spirit);
		}
	}
}

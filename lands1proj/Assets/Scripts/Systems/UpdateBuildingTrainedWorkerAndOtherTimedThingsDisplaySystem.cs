﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// NOTE: This system exists because we want to update the trained worker display both at building creation and when training happens
// NOTE: Previously, it was planned to have a "UpdateBuildingButtonsBasedOnBuildingStateSystem" but it seems like this system can react to changes to the buildings and only update buttons at that time. Works well enough for now...
public class UpdateBuildingTrainedWorkerAndOtherTimedThingsDisplaySystem : BaseSystem
{
	public LevelEntity levelEntity;
	public ButtonManagerEntity buttonManagerEntity;

	Image progressBarImage;

	static void UpdateWorkerCountForBuildingDisplay(BuildingEntity buildingEntity)
	{
		if (buildingEntity.finalWorkers > 0 || buildingEntity.extraWorkers > 0d)
		{
			if (buildingEntity.extraWorkers > 0d)
				buildingEntity.buildingAssetsObjectConnector.trainingButtonText.text = buildingEntity.trainedWorkers.ToString() + " (+" + buildingEntity.extraWorkers.ToString() + ")";  // buildingEntity.finalWorkers.ToString();
			else
				buildingEntity.buildingAssetsObjectConnector.trainingButtonText.text = buildingEntity.trainedWorkers.ToString(); // buildingEntity.finalWorkers.ToString();
		}
		else
			buildingEntity.buildingAssetsObjectConnector.trainingButtonText.text = ValueHolder.INITIAL_TRAIN_BUTTON_TEXT;
	}

	static void UpdateOpenTierUpgradeScreenButtonForBuildingDisplay(BuildingEntity buildingEntity)
	{
		if (buildingEntity.isAtMaxTrainedWorkersForTierAndThusMayTierUpgrade)
		{
			UIHelpers.ShowHideGameObject(buildingEntity.buildingAssetsObjectConnector.openTierUpgradeScreenButton.gameObject, true);

			// TODO: Use buildingEntity.anyTierUpgradeIsAvailable to change the visual style of the upgrade button, to make it clear nothing good is available. UpdateOpenTierUpgradeScreenButtonForBuildingDisplay
			//if (buildingEntity.anyTierUpgradeIsAvailable)
			//{
				
			//}
			//else
			//{

			//}
		}

		// TODO: Perhaps the upgrade buttons on the screen for tier upgrade should also be controlled here, instead of in ShowHideMenusAnd...System
	}

	public override void SystemUpdate()
	{
		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			if (buildingEntity.workersOrTierIsDirtyClearOnUpdate)
			{
				// TODO: This will get particular for different kinds of buildings... but for most buildings its just trained workers
				UpdateWorkerCountForBuildingDisplay(buildingEntity);
				UpdateOpenTierUpgradeScreenButtonForBuildingDisplay(buildingEntity);

				if (!buildingEntity.isTraining)
				{
					progressBarImage = buildingEntity.buildingAssetsObjectConnector.trainingButtonSpriteAndBoarder.progressBarImage;

					if (progressBarImage.gameObject.activeSelf)
						progressBarImage.gameObject.SetActive(false);
				}
			}
		}

		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			// TODO: This is not a flag that's reliably not cleared at this point... look at how wonkily it is used to control tier upgrading.
			if (buildingEntity.isDirtyFromBeingBuilt)
			{
				UpdateWorkerCountForBuildingDisplay(buildingEntity);
				UpdateOpenTierUpgradeScreenButtonForBuildingDisplay(buildingEntity);

				if (!buildingEntity.isBuildingOrTierUpgrading)
				{
					progressBarImage = buildingEntity.buildingAssetsObjectConnector.openTierUpgradeScreenButtonSpriteAndBoarder.progressBarImage;

					if (progressBarImage.gameObject.activeSelf)
						progressBarImage.gameObject.SetActive(false);
				}
			}
		}

		foreach (var mythButtonEntity in buttonManagerEntity.mythBuyingButtonEntities)
		{
			if (mythButtonEntity.researchableStateComponent.researchIsDirtyClearOnUpdate)
			{
				// TODO: This check is redundant somewhat. The only things that cause researchIsDirtyClearOnUpdate also cease research in some way
				if (!mythButtonEntity.researchableStateComponent.isBeingResearched)
				{
					progressBarImage = mythButtonEntity.researchableUIComponent.background.progressBarImage;

					if (progressBarImage.gameObject.activeSelf)
						progressBarImage.gameObject.SetActive(false);

					if (mythButtonEntity.researchableStateComponent.isResearched)
					{
						if (!mythButtonEntity.mythCheckmark.gameObject.activeSelf)
							mythButtonEntity.mythCheckmark.gameObject.SetActive(true);

						if (mythButtonEntity.researchableUIComponent.costProgressText.gameObject.activeSelf)
							throw new System.Exception("The cost progress text should not be enabled at the time of the end of research.");
					}
					else
					{
						// NOTE: This is always true, does it need to be checked?
						if (!mythButtonEntity.researchableUIComponent.costProgressText.gameObject.activeSelf)
							mythButtonEntity.researchableUIComponent.costProgressText.gameObject.SetActive(true);
					}

					if (mythButtonEntity.researchableUIComponent.cancelResearchButton.button.gameObject.activeSelf)
						mythButtonEntity.researchableUIComponent.cancelResearchButton.button.gameObject.SetActive(false);
				}
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateBuffGiverComponentsForEachBuildingSystem : BaseSystem
{
	public LevelEntity levelEntity;
	public GlobalBuffDataEntity globalBuffDataEntity; // TODO: Should become defunct soon... right? Replaced by the order of buffs.

	BuffEntity buffEntity;
	PrototypeBuffGiverAreaComponent prototypeBuffGiverAreaComponent;
	List<PrototypeBuffGiverAreaComponent> prototypeBuffGiverAreaComponents;

	// TODO: Change, once refactoring away from globalBuffDataEntity
	static double GlobalBuffBasedStrengthFactor(BuildingEntity buildingEntity, GlobalBuffDataEntity globalBuffDataEntity)
	{
		ResourceBuffSetComponent resourceBuffSetComponent = globalBuffDataEntity.playerGlobalBuffSetComponent;

		bool doubleTownHallEffect = buildingEntity.buildingStatsScriptableObject.isTownHall && resourceBuffSetComponent.doubleTownHallEffect;
		bool doubleGuildEffect = buildingEntity.buildingStatsScriptableObject.isGuild && resourceBuffSetComponent.doubleGuildEffect;

		return (1d + (doubleTownHallEffect ? 1d : 0d) + (doubleGuildEffect ? 1d : 0d));
	}

	// TODO: Change, once refactoring away from globalBuffDataEntity
	static double BuffStrength(PrototypeBuffGiverAreaComponent prototypeBuffStrengthFactor, BuildingEntity buildingEntity, GlobalBuffDataEntity globalBuffDataEntity)
	{
		if (prototypeBuffStrengthFactor.buffType.BuffTypeDependsOnWorkers())
		{
			if (prototypeBuffStrengthFactor.buffType.BuffUsesOneTenthScaling())
			{
				if (buildingEntity.finalWorkers > 0)
					return prototypeBuffStrengthFactor.buffStrengthFactor * GlobalBuffBasedStrengthFactor(buildingEntity, globalBuffDataEntity) * (0.1d * buildingEntity.finalWorkers);
				return 0d;
			}
			return prototypeBuffStrengthFactor.buffStrengthFactor * GlobalBuffBasedStrengthFactor(buildingEntity, globalBuffDataEntity) * buildingEntity.finalWorkers;
		}
		return prototypeBuffStrengthFactor.buffStrengthFactor * GlobalBuffBasedStrengthFactor(buildingEntity, globalBuffDataEntity);
	}

	// TODO: This should all be replaced with set logic... it really feels wrong to do this list maintenance
	// SERIOUSLY
	void UpdateBuffGiverComponentsForBuilding(BuildingEntity buildingEntity)
	{
		prototypeBuffGiverAreaComponents = buildingEntity.buildingStatsScriptableObject.prototypeBuffGiverAreaComponents;

		// NOTE: This removes any extra buffs that are not present on the building
		if (prototypeBuffGiverAreaComponents.Count < buildingEntity.buffGiverComponents.Count)
		{
			for (int i = buildingEntity.buffGiverComponents.Count - 1; i >= prototypeBuffGiverAreaComponents.Count - 1; i--)
			{
				buildingEntity.removedBuffGiverComponentsClearOnUpdate.Add(buildingEntity.buffGiverComponents[i]);

				buildingEntity.buffGiverComponents.RemoveAt(i);
			}
		}

		for (int i = prototypeBuffGiverAreaComponents.Count - 1; i >= 0; i--)
		{
			prototypeBuffGiverAreaComponent = prototypeBuffGiverAreaComponents[i];

			// NOTE: Rather than recreate the array of buffs all the time, we attempt to just update the buff entities
			if (i < buildingEntity.buffGiverComponents.Count && buildingEntity.buffGiverComponents[i].buffEntity.buffType == prototypeBuffGiverAreaComponent.buffType)
			{
				// TODO: Most importantly, this calculation can and should depend on buffs that are on the building. In fact, there should be a difference between resource gaining ability and buff giving ability... should resource gains just be a global buff, for example?
				buildingEntity.buffGiverComponents[i].buffEntity.buffStrength = BuffStrength(prototypeBuffGiverAreaComponent, buildingEntity, globalBuffDataEntity);
			}
			else
			{
				// NOTE: Similar to the initial if that cleans up extra buffGiverComponents, replace the buffGiver at the index with the new buff
				if (buildingEntity.buffGiverComponents.Count > i)
				{
					buildingEntity.removedBuffGiverComponentsClearOnUpdate.Add(buildingEntity.buffGiverComponents[i]);

					buildingEntity.buffGiverComponents.RemoveAt(i);
				}

				buffEntity = new BuffEntity()
				{
					buffType = prototypeBuffGiverAreaComponent.buffType,
					// TODO: Most importantly, this calculation can and should depend on buffs that are on the building. In fact, there should be a difference between resource gaining ability and buff giving ability... should resource gains just be a global buff, for example?
					buffStrength = BuffStrength(prototypeBuffGiverAreaComponent, buildingEntity, globalBuffDataEntity),
					sourceBuildingEntity = buildingEntity
				};

				if (BuildingStatsScriptableObject.BuffAreaShapeTypeUsesCoords(prototypeBuffGiverAreaComponent.buffAreaShapeType))
				{
					buildingEntity.buffGiverComponents.Insert(
						i,
						new BuffGiverComponent()
						{
							buffEntity = buffEntity,
							buffAreaShapeType = prototypeBuffGiverAreaComponent.buffAreaShapeType,
							buffOffsetCoords = ValueHolder.buffAreaShapes[prototypeBuffGiverAreaComponent.buffAreaShapeType][prototypeBuffGiverAreaComponent.buffAreaRadius]
						}
					);
				}
				else
				{
					buildingEntity.buffGiverComponents.Insert(
						i,
						new BuffGiverComponent()
						{
							buffEntity = buffEntity,
							buffAreaShapeType = prototypeBuffGiverAreaComponent.buffAreaShapeType,
							buffOffsetCoords = null
						}
					);
				}
			}
		}
	}

	public override void SystemUpdate()
	{
		if (globalBuffDataEntity.activeMythsChangedClearOnUpdate
			|| levelEntity.buildingEntitiesAreDirtiedClearOnUpdate
			)
		{
			foreach (var buildingEntity in levelEntity.activeBuildingEntities)
			{
				if (buildingEntity.tileEntity.buffEntitiesDirtyClearOnUpdate // TODO: This is NYI! The idea is can a building give another building a buff. Presumably this whole calculation should be done in priority waves. Likely even in priority waves per buff type each building gives.
					|| buildingEntity.workersOrTierIsDirtyClearOnUpdate
					)
				{
					UpdateBuffGiverComponentsForBuilding(buildingEntity);
				}
			}
		}
	}
}

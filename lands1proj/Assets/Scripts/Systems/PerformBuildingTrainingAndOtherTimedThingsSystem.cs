﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerformBuildingTrainingAndOtherTimedThingsSystem : BaseSystem
{
	public Camera mainCamera;
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public GlobalBuffDataEntity globalBuffDataEntity;
	public ButtonManagerEntity buttonManagerEntity;
	public ScreenManagerEntity screenManagerEntity;
	public MythButtonEntity mythButtonEntityPrefab;
	public Dictionary<BuildingAdvancedType, BuildingAssetsDataScriptableObject> buildingAssetsDataScriptableObjects;
	public Dictionary<BuildingAdvancedType, BuildingStatsScriptableObject> buildingStatsScriptableObjects;
	public Dictionary<MythType, MythAssetsDataScriptableObject> mythAssetsDataScriptableObjects;


	float remainingDeltaTime;
	int loopControl;
	BuildingAdvancedType buildingTierUpgradeResultingBuildingAdvancedType;


	public static void AddAndSetMythButtonEntityToShowBoughtMythsMenu(
		MythType mythType,
		MythButtonEntity mythButtonEntityPrefab,
		Dictionary<MythType, MythAssetsDataScriptableObject> mythAssetsDataScriptableObjects,
		GameObject showBoughtMythsMenuGridView,
		Camera mainCamera,
		ref MythButtonEntity chosenMythDisplayEntity // TODO: Consider changing GlobalBuffDataEntity to have a different entity type for displaying the chosen myth
	)
	{
		MythButtonEntity mythButtonEntity = GameObject.Instantiate(mythButtonEntityPrefab, Vector3.zero, Quaternion.identity, showBoughtMythsMenuGridView.transform);

		mythButtonEntity.mythAssetsDataScriptableObject = mythAssetsDataScriptableObjects[mythType];

		// TODO: This code is repeated between here and the SetInitialMythAssetsSystem - which is used to initialize the Scene-hard-coded myth buying buttons
		UIHelpers.SetMythButtonUIValues(mythButtonEntity);

		// No need to set baseResearchSpiritCost for these ShowBoughtMythItems
		// No need to set baseResearchTime for these ShowBoughtMyth items

		mythButtonEntity.researchableUIComponent.dirtyUI = false; // NOTE: Setting this to false since the thing that was dirty is rectified above

		chosenMythDisplayEntity = mythButtonEntity;
	}

	public override void SystemUpdate()
	{
		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			if (buildingEntity.startTraining)
			{
				buildingEntity.startTraining = false;

				// NOTE: Needs to be checked again because other actions may also have commenced subtracted resources, such as other buildings also training via auto train
				LogicHelpers.SetBuildingCanTrainAndMaxTrainedWorkersForTier(buildingEntity, gameStateEntity);

				if (buildingEntity.canTrain)
				{
					gameStateEntity.playerResourceSetAmountComponent -= buildingEntity.finalTrainingCost;

					buildingEntity.isTraining = true;
					buildingEntity.trainingStartTime = gameStateEntity.gameTime;
				}
			}

			if (buildingEntity.isTraining)
			{
				remainingDeltaTime = gameStateEntity.gameTime - buildingEntity.trainingStartTime; // TODO: probably delta time needs to be passed via game state, and proceessed in a unified time system

				loopControl = 0;

				while (remainingDeltaTime > 0f && buildingEntity.isTraining && loopControl < ValueHolder.MAX_LOOP_CONTROL)
				{
					loopControl++;

					remainingDeltaTime -= buildingEntity.finalTrainingTime;

					if (remainingDeltaTime >= 0f)
					{
						buildingEntity.trainedWorkers++;
						buildingEntity.trainedWorkersClearOnUpdate++;
						buildingEntity.showOrRefreshProductionDisplayPanel = true;

						// TODO: Obviously these are crap
						buildingEntity.workersOrTierIsDirtyClearOnUpdate = true;
						// TODO: What about resource gaining ability clear on update? I mean it's not the same quite...
						// TODO: Do we need to mark building entities as dirty? Presumably yes. OR! Use a specific flag for training
						levelEntity.buildingEntitiesAreDirtiedClearOnFixedUpdate = true;
						levelEntity.buildingEntitiesAreDirtiedClearOnUpdate = true;

						LogicHelpers.SetTrainingCosts(buildingEntity, globalBuffDataEntity, levelEntity);
						// TODO: This doesn't work well enough since isAtMaxWorkersForTier isn't updated on time for auto training, which triggers the next training cycle. Just add setting isAtMaxWorkersForTier to be on demand, rather than through a system, since it has 3 places its used
						LogicHelpers.SetBuildingCanTrainAndMaxTrainedWorkersForTier(buildingEntity, gameStateEntity);

						if (gameStateEntity.autoTrain)
						{
							if (buildingEntity.canTrain)
							{
								gameStateEntity.playerResourceSetAmountComponent -= buildingEntity.finalTrainingCost;

								buildingEntity.trainingStartTime = gameStateEntity.gameTime - remainingDeltaTime; // NOTE: This variable is writeonly for the purposes of this while loop
							}
							else
							{
								buildingEntity.isTraining = false;
							}
						}
						else
							buildingEntity.isTraining = false;
					}
				}

				// TODO: The above code works, but it doesn't know how to spend resources after enough have accumulated to resume training.
				// Let's say you fast forward 100 seconds, and at the 95th second you would have had enough food to finally resume auto training... you should then witness a training with 5 seconds of progress after a fast forward. Not 0 seconds and just started like now.
				// The solution is that the fast forward functionality needs to have some kind of superior system that loops through all the logic systems in 1 second intervals or so... or something else

				LogicHelpers.LoopControlCheck(loopControl, "Loop Control limit hit in training system.");
			}
		}

		//
		// TODO: How will this loop know to intermingle with auto training... considering the intragrated remainingDeltaTime approach
		// Perhaps first implement the two auto mechanisms separately... then when the gameplay is defined and in place, pursue the remainingDeltaTime approach
		// FURTHERMORE - consider that between each training iteration we should be recomputing global buffs before doing much else... yeah the remainingDeltaTime approach needs serious redesign, ignore it for now
		//

		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			if (buildingEntity.startUpgradingBuildingTier)
			{
				buildingEntity.startUpgradingBuildingTier = false;

				// NOTE: Needs to be checked again because other actions may also have commenced subtracted resources, such as other buildings also training via auto train
				LogicHelpers.SetBuildingCanStartTierUpgradeAndMaxTrainedWorkersForTierAndCosts(buildingEntity, gameStateEntity, buildingStatsScriptableObjects, globalBuffDataEntity);

				if (buildingEntity.canStartNextBuildingTierUpgrades[buildingEntity.finalTierUpgradePathIndex])
				{
					// TODO: Building needs to store its next tier assets once computed... or there needs to be a simple function to retrieve it
					gameStateEntity.playerResourceSetAmountComponent -= buildingEntity.finalBuildOrTierUpgradeCosts[buildingEntity.finalTierUpgradePathIndex]; // TODO: This is incorrect

					buildingEntity.isBuildingOrTierUpgrading = true;
					buildingEntity.buildingOrUpradingStartTime = gameStateEntity.gameTime;

					if (buildingEntity.tierUpgradeRequestIsFromPlayerInteraction) 
					{
						screenManagerEntity.buildingTierUpgradeScreenShouldBeVisible = false;

						screenManagerEntity.buildingTierUpgradeScreenIsDirtyClearOnUpdate = true;

						// UIHelpers.ShowHideGameObject(screenManagerEntity.buildingTierUpgradeScreen.gameObject, false);
					}
					else if (screenManagerEntity.buildingTierUpgradeBuildingEntity == buildingEntity) // TODO: Improve this UX - currently if an auto upgrade triggers the upgrading of a building for which the auto
					{
						screenManagerEntity.buildingTierUpgradeScreenIsDirtyClearOnUpdate = true;
					}
				}
				else
				{
					// TODO: This isn't exception worthy, unless we start disabling the button. Perhaps it should be a modal UX / some kind of audio feedback, or message popping out of the button... or the button shaking and making a platform character hits indestructible block with head denied noise
				}
			}

			if (buildingEntity.isBuildingOrTierUpgrading)
			{
				remainingDeltaTime = gameStateEntity.gameTime - buildingEntity.buildingOrUpradingStartTime; // TODO: probably delta time needs to be passed via game state, and proceessed in a unified time system

				remainingDeltaTime -= buildingEntity.finalBuildOrTierUpgradeTimes[buildingEntity.finalTierUpgradePathIndex];

				if (remainingDeltaTime >= 0)
				{
					buildingEntity.isBuildingOrTierUpgrading = false;

					buildingEntity.tierIndex++;

					if (buildingEntity.tierIndex == ValueHolder.TIER_WITH_LOCKED_UPGRADE_PATH)
						buildingEntity.chosenBuildingTierUpgradePathIndex = buildingEntity.startBuildingTierUpgradePathIndex;

					buildingEntity.showOrRefreshProductionDisplayPanel = true; // TODO: Consider if this makes sense... probably... unless we want to show another screen

					// TODO: Does it make sense to casually set these two right here?
					buildingTierUpgradeResultingBuildingAdvancedType = buildingEntity.nextBuildingTiers[buildingEntity.finalTierUpgradePathIndex].Value; // NOTE: Previous systems guarantee a value exists if the building was tier upgrading... hopefully
					buildingEntity.buildingAssetsDataScriptableObject = buildingAssetsDataScriptableObjects[buildingTierUpgradeResultingBuildingAdvancedType]; 
					buildingEntity.buildingStatsScriptableObject = buildingStatsScriptableObjects[buildingTierUpgradeResultingBuildingAdvancedType];
					buildingEntity.isDirtyFromBeingBuilt = true;
					buildingEntity.workersOrTierIsDirtyClearOnUpdate = true; // TODO: Without this flag, the training button is stuck...

					// TODO: From here down it's the same as the training loop
					levelEntity.buildingEntitiesAreDirtiedClearOnFixedUpdate = true;
					levelEntity.buildingEntitiesAreDirtiedClearOnUpdate = true;

					LogicHelpers.SetTrainingCosts(buildingEntity, globalBuffDataEntity, levelEntity);
					// TODO: This doesn't work well enough since isAtMaxWorkersForTier isn't updated on time for auto training, which triggers the next training cycle. Just add setting isAtMaxWorkersForTier to be on demand, rather than through a system, since it has 3 places its used
					LogicHelpers.SetBuildingCanTrainAndMaxTrainedWorkersForTier(buildingEntity, gameStateEntity);

					if (gameStateEntity.autoTrain)
					{
						if (buildingEntity.canTrain)
						{
							gameStateEntity.playerResourceSetAmountComponent -= buildingEntity.finalTrainingCost;

							buildingEntity.trainingStartTime = gameStateEntity.gameTime - remainingDeltaTime; // NOTE: This variable is writeonly for the purposes of this while loop
						}
						else
						{
							buildingEntity.isTraining = false;
						}
					}
					else
						buildingEntity.isTraining = false;
				}
			}
		}

		// TODO: This loop is a simplified version of the training loop for buildings, do unify somehow
		foreach (var mythButtonEntity in buttonManagerEntity.mythBuyingButtonEntities)
		{
			if (mythButtonEntity.researchableStateComponent.startResearch)
			{
				mythButtonEntity.researchableStateComponent.startResearch = false;

				// TODO: Too many ifs. This represents another state for the button, during which we should not longer perform SetMythButtonCanResearch, because it will then mess up the "First" vs "Second" vs "CantResearch" logic later in this loop
				// Perhaps the button, once it starts researching, should remember which slot it's researching for in yet another variable... but that sounds terrible.
				if (!mythButtonEntity.researchableStateComponent.isBeingResearched)
				{
					LogicHelpers.SetMythButtonCanResearch(mythButtonEntity, gameStateEntity, globalBuffDataEntity);

					// Needs to be checked again because other actions may also have commenced subtracted resources
					if (mythButtonEntity.mythSlotForResearchOrCantResearch != MythSlotForResearchOrCantResearch.CantResearch)
					{
						gameStateEntity.playerResourceSetAmountComponent.spirit -= mythButtonEntity.baseResearchCost;

						mythButtonEntity.researchableStateComponent.isBeingResearched = true;
						mythButtonEntity.researchableStateComponent.researchStartTime = gameStateEntity.gameTime;
						// TODO: Shouldn't this also set researchIsDirtyClearOnUpdate?

						switch (mythButtonEntity.mythSlotForResearchOrCantResearch)
						{
							case MythSlotForResearchOrCantResearch.CantResearch:
								throw new System.Exception("Impossible can research logic - a button allegedly cannot research after a condition saying it can.");
							case MythSlotForResearchOrCantResearch.First:
								globalBuffDataEntity.inProgressFirstMyth = true;
								break;
							case MythSlotForResearchOrCantResearch.Second:
								globalBuffDataEntity.inProgressSecondMyth = true;
								break;
							default:
								throw new System.Exception("Undefined can research outcome.");
						}
					}
				}
			}

			if (mythButtonEntity.researchableStateComponent.cancelResearch)
			{
				mythButtonEntity.researchableStateComponent.cancelResearch = false;

				mythButtonEntity.researchableStateComponent.isBeingResearched = false;
				mythButtonEntity.researchableStateComponent.researchIsDirtyClearOnUpdate = true;

				switch (mythButtonEntity.mythSlotForResearchOrCantResearch)
				{
					case MythSlotForResearchOrCantResearch.CantResearch:
						throw new System.Exception("Impossible can research logic - a buttonbeing cancelled allegedly couldn't research.");
					case MythSlotForResearchOrCantResearch.First:
						globalBuffDataEntity.inProgressFirstMyth = false;
						break;
					case MythSlotForResearchOrCantResearch.Second:
						globalBuffDataEntity.inProgressSecondMyth = false; ;
						break;
					default:
						throw new System.Exception("Undefined can research outcome.");
				}
			}

			// NOTE: No reason to have the looping code as above here, since Myth research can just occur once... orrr can it?
			if (mythButtonEntity.researchableStateComponent.isBeingResearched)
			{
				remainingDeltaTime = gameStateEntity.gameTime - mythButtonEntity.researchableStateComponent.researchStartTime; // TODO: probably delta time needs to be passed via game state, and proceessed in a unified time system

				remainingDeltaTime -= mythButtonEntity.researchableStateComponent.baseResearchTime;

				if (remainingDeltaTime >= 0)
				{
					mythButtonEntity.researchableStateComponent.isBeingResearched = false;

					mythButtonEntity.researchableStateComponent.isResearched = true;
					mythButtonEntity.researchableStateComponent.researchIsDirtyClearOnUpdate = true;
					globalBuffDataEntity.activeMythsChangedClearOnUpdate = true;

					switch (mythButtonEntity.mythSlotForResearchOrCantResearch)
					{
						case MythSlotForResearchOrCantResearch.CantResearch:
							throw new System.Exception("Impossible can research logic - a button allegedly cannot research while it is completing research.");
						case MythSlotForResearchOrCantResearch.First:
							AddAndSetMythButtonEntityToShowBoughtMythsMenu(mythButtonEntity.mythAssetsDataScriptableObject.mythType, mythButtonEntityPrefab, mythAssetsDataScriptableObjects, screenManagerEntity.showBoughtMythsMenuGridView, mainCamera, ref globalBuffDataEntity.firstBoughtMythDisplayEntity);
							break;
						case MythSlotForResearchOrCantResearch.Second:
							AddAndSetMythButtonEntityToShowBoughtMythsMenu(mythButtonEntity.mythAssetsDataScriptableObject.mythType, mythButtonEntityPrefab, mythAssetsDataScriptableObjects, screenManagerEntity.showBoughtMythsMenuGridView, mainCamera, ref globalBuffDataEntity.secondBoughtMythDisplayEntity);
							break;
						default:
							throw new System.Exception("Undefined can research outcome.");
					}
				}
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcessPlayerWorldTapRitualInputSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public InputStateEntity inputStateEntity;
	public LevelEntity levelEntity;
	public RitualStateEntity ritualStateEntity;
	public InputIndicatorAnimationEntity ritualValidNextStepIndicatorAnimationEntityPrefab;
	public InputIndicatorAnimationEntity invalidInputIndicatorAnimationEntityPrefab;
	public GameObject inputIndicatorAnimationEntitiesObject;

	Vector2Int candidateTileForNextRitualInputCoord;
	TileEntity candidateTileForNextRitualInput;
	UndoableList<RitualStepEntity> ritualStepEntities;
	int backtrackCount;
	bool inputWasValid;

	static void AddRitualValidNextStepIndicatorAnimation(
		Vector2Int coord,
		float animationTime,
		InputIndicatorAnimationEntity ritualValidNextStepIndicatorAnimationEntityPrefab,
		GameObject inputIndicatorAnimationEntitiesObject,
		InputStateEntity inputStateEntity
	)
	{
		Vector3 pos = LogicHelpers.CoordToPosition(coord);

		InputIndicatorAnimationEntity ritualValidNextStepIndicatorAnimationEntity = GameObject.Instantiate(ritualValidNextStepIndicatorAnimationEntityPrefab, pos, Quaternion.identity, inputIndicatorAnimationEntitiesObject.transform);

		ritualValidNextStepIndicatorAnimationEntity.dissipateComponent.dissipationStartAnimationTime = animationTime;

		inputStateEntity.inputIndicatorAnimationEntities.Add(ritualValidNextStepIndicatorAnimationEntity);
	}

	static void AddInvalidInputIndicatorAnimation(
		Vector3 worldInputPos,
		float animationTime,
		InputIndicatorAnimationEntity invalidInputIndicatorAnimationEntityPrefab,
		GameObject inputIndicatorAnimationEntitiesObject,
		InputStateEntity inputStateEntity
	)
	{
		Vector3 pos = new Vector3(worldInputPos.x, worldInputPos.y);

		InputIndicatorAnimationEntity invalidIndicatorAnimationEntity = GameObject.Instantiate(invalidInputIndicatorAnimationEntityPrefab, pos, Quaternion.identity, inputIndicatorAnimationEntitiesObject.transform);

		invalidIndicatorAnimationEntity.dissipateComponent.dissipationStartAnimationTime = animationTime;

		inputStateEntity.inputIndicatorAnimationEntities.Add(invalidIndicatorAnimationEntity);
	}

	void DisplayValidNextRitualStepIndicatorsAndInvalidInputIndicator()
	{
		// TODO: Work on InvalidInputIndicator, should appear at inputStateEntity.tapCurrentWorldPosition - if possible use the same "X" animation as for lost buff

		// TODO: gameTime isn't the same as animationTime
		AddInvalidInputIndicatorAnimation(inputStateEntity.tapCurrentWorldPosition, gameStateEntity.gameTime, invalidInputIndicatorAnimationEntityPrefab, inputIndicatorAnimationEntitiesObject, inputStateEntity);

		foreach (var coord in ritualStateEntity.validNextRitualStepCoords)
		{
			// TODO: Instead... this should... possibly refresh or prolong the animations on any existing indicators? Something like that?
			// TODO: gameTime isn't the same as animationTime
			AddRitualValidNextStepIndicatorAnimation(coord, gameStateEntity.gameTime, ritualValidNextStepIndicatorAnimationEntityPrefab, inputIndicatorAnimationEntitiesObject, inputStateEntity);
		}
	}

	public override void SystemUpdate()
	{
		// TODO: This should be set just once... need a pattern for initializing systems (can't be set in constructor without passing it as a variable... the syntactic sugar for initalization doesn't occur before the constructor)
		ritualStepEntities = ritualStateEntity.ritualEntity.ritualStepEntities;

		if (inputStateEntity.tapInputState == InputStateEntity.TapInputState.RitualInputBasedOnRitualInputType)
		{
			switch (inputStateEntity.ritualInputType)
			{
				case InputStateEntity.RitualInputType.TapNextNode:
					break;
				case InputStateEntity.RitualInputType.SwipeToSelectWithRecenteringWhenLettingGo:
					break;
				case InputStateEntity.RitualInputType.SwipeToSelectWithRubberBandingAtEdges:

					// TODO: As new kinds of rituals are implemented, yet another case is needed here - since inputs are different based on the ritual type. This code is gonna get ugly, it's time to separate it out to functions.

					candidateTileForNextRitualInputCoord = new Vector2Int(Mathf.FloorToInt(inputStateEntity.tapCurrentWorldPosition.x), Mathf.FloorToInt(inputStateEntity.tapCurrentWorldPosition.y));
					candidateTileForNextRitualInput = levelEntity.GetTileEntityOrNull(candidateTileForNextRitualInputCoord);
					inputWasValid = false;

					if (candidateTileForNextRitualInput != null)
					{
						if (ritualStateEntity.backtrackCountsForAllowedRitualStepCoords.TryGetValue(candidateTileForNextRitualInputCoord, out backtrackCount))
						{
							ritualStateEntity.undoRitualStepsTriggered = backtrackCount;
							inputWasValid = true;
						}
						else if (ritualStateEntity.validNextRitualStepCoords.Contains(candidateTileForNextRitualInputCoord))
						{
							ritualStateEntity.attemptToAddRitualStepEntity = true;
							ritualStateEntity.candidateTileForNextPlannedRitualStepEntity = candidateTileForNextRitualInput;
							inputWasValid = true;
						}
					}

					if (inputStateEntity.initialTapThisUpdateClearOnUpdate)
					{
						if (!inputWasValid)
						{
							// TODO: This for loop can't distinguish different input indicators so it's definitely making all of them disappear
							foreach (var inputIndicatorAnimationEntity in inputStateEntity.inputIndicatorAnimationEntities)
							{
								inputIndicatorAnimationEntity.dissipateComponent.DissipateImmediately(gameStateEntity.gameTime);
							}

							DisplayValidNextRitualStepIndicatorsAndInvalidInputIndicator();
						}
						else
						{
							// TODO: This for loop can't distinguish different input indicators so it's definitely making all of them disappear
							foreach (var inputIndicatorAnimationEntity in inputStateEntity.inputIndicatorAnimationEntities)
							{
								inputIndicatorAnimationEntity.dissipateComponent.DissipateMoreQuickly(gameStateEntity.gameTime, ValueHolder.QUICK_INPUT_INDICATOR_DISSIPATION_TIME);
							}

						}
					}

					break;
				default:
					break;
			}
		}
	}
}

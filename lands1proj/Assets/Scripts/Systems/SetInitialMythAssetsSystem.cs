﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// DELETE THIS, we'll just do it in the perform training function
public class SetInitialMythAssetsSystem : BaseSystem
{
	public ButtonManagerEntity buttonManagerEntity;
	public GlobalBuffDataEntity globalBuffDataEntity;

	public override void SystemUpdate()
	{
		// NOTE: This system exists to rectify the manually added buttons the the Myths menu in the scene. The bought myths seen on the second screen have their assets set at creation.
		// TODO: Unify this, see the PerformBuildingTrainingAndOtherTimedThingsSystem code that does much of the same
		foreach (var mythButtonEntity in buttonManagerEntity.mythBuyingButtonEntities)
		{
			if (mythButtonEntity.researchableUIComponent.dirtyUI)
			{
				mythButtonEntity.researchableUIComponent.dirtyUI = false;

				UIHelpers.SetMythButtonUIValues(mythButtonEntity);
			}
		}
	}
}


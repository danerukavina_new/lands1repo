﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Updates the menus of all the buildings, specifically whether certain building actions can be taken (or are even shown as an option), based on game state.
/// </summary>
public class UpdateBuildingCountsSystem : BaseSystem
{
	public LevelEntity levelEntity;

	public override void SystemUpdate()
	{
		if (
			levelEntity.buildingEntitiesAreDirtiedClearOnUpdate
			)
		{
			levelEntity.ResetBuildingCounts();

			foreach (var buildingEntity in levelEntity.activeBuildingEntities)
			{
				levelEntity.buildingCountsByBaseBuilding[buildingEntity.buildingStatsScriptableObject.impliedBuildingBaseType]++;
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcessOtherTrainingSideEffectsSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public GlobalBuffDataEntity globalBuffDataEntity;

	bool otherTrainingSideEffectsArePossible;

	public override void SystemUpdate()
	{
		otherTrainingSideEffectsArePossible = globalBuffDataEntity.playerGlobalBuffSetComponent.bonusFarmWorkersPerMineWorkerTrained > 0;

		if (!otherTrainingSideEffectsArePossible)
			return;

		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			switch (buildingEntity.buildingStatsScriptableObject.impliedBuildingBaseType)
			{
				case BuildingBaseType.Farm:
					break;
				case BuildingBaseType.LumberMill:
					break;
				case BuildingBaseType.TownHall:
					break;
				case BuildingBaseType.Quarry:
					break;
				case BuildingBaseType.Mine:
					// TODO: Overall, this system may be worth deleting and standardizing... into one system that adds extra workers (AddWorkerOverTimeSystem)
					if (buildingEntity.trainedWorkersClearOnUpdate > 0)
					{
						foreach (var otherBuildingEntity in levelEntity.activeBuildingEntities)
						{
							if (otherBuildingEntity.buildingStatsScriptableObject.isFarm)
							{
								LogicHelpers.SetBuildingMaxExtraWorkersAndIsAtMaxExtraWorkers(buildingEntity, gameStateEntity);

								if (!buildingEntity.isAtMaxExtraWorkers)
								{
									otherBuildingEntity.extraWorkers = LogicHelpers.ResultingExtraWorkersWithRounding(
										buildingEntity.extraWorkers,
										buildingEntity.trainedWorkersClearOnUpdate * globalBuffDataEntity.playerGlobalBuffSetComponent.bonusFarmWorkersPerMineWorkerTrained,
										buildingEntity.maxExtraWorkers
										);
									otherBuildingEntity.workersOrTierIsDirtyClearOnUpdate = true;
								}
							}
						}
					}
					break;
				case BuildingBaseType.Well:
					break;
				case BuildingBaseType.Shrine:
					break;
				case BuildingBaseType.Windmill:
					break;
				case BuildingBaseType.Guild:
					break;
				case BuildingBaseType.Tavern:
					break;
				case BuildingBaseType.Temple:
					break;
				case BuildingBaseType.Laboratory:
					break;
				case BuildingBaseType.Blacksmith:
					break;
				case BuildingBaseType.Inn:
					break;
				case BuildingBaseType.TrainingGrounds:
					break;
				case BuildingBaseType.Dungeon:
					break;
				case BuildingBaseType.TradeRoute:
					break;
				case BuildingBaseType.Shipyard:
					break;
				case BuildingBaseType.SpecialCaptureInProgress:
					break;
				case BuildingBaseType.SpecialSeaBorder:
					break;
				case BuildingBaseType.SpecialMountainPassBorder:
					break;
				case BuildingBaseType.SpecialMountainBorder:
					break;
				default:
					throw new System.Exception("Unexpected building base type in other training side effects.");
			}
		}
	}
}

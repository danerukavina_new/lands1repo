﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyTileSystem : BaseSystem
{
	public Camera mainCamera;
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public TileEntity tileEntityPrefab;
	public GameObject tileEntitiesObject;

	Vector2Int coord;
	bool performedOneAllowedTileBuyPerUpdate;
	BuyTileOnLevelEntity activeBuyTileOnLevelEntity;

	public override void SystemUpdate()
	{
		if (gameStateEntity.buyTileState)
		{
			performedOneAllowedTileBuyPerUpdate = false;

			//for (int i = 0; i < levelEntity.activeBuyTileOnLevelEntities.Count && !activeBuyTileOnLevelEntity; i++)
			//{
			//	activeBuyTileOnLevelEntity = levelEntity.activeBuyTileOnLevelEntities[i];

			//	if (activeBuyTileOnLevelEntity.generalButtonEntity.generalButton.buttonClicked)
			//	{
			//		activeBuyTileOnLevelEntity.generalButtonEntity.generalButton.buttonClicked = false;

			//		if (gameStateEntity.playerResourceSetAmountComponent.gold >= gameStateEntity.buyTileCost)
			//		{
			//			performedOneAllowedTileBuyPerUpdate = true;

			//			gameStateEntity.playerResourceSetAmountComponent.gold -= gameStateEntity.buyTileCost;

			//			coord = activeBuyTileOnLevelEntity.coord;

			//			levelEntity.AddTileEntity(coord, tileEntityPrefab, tileEntitiesObject, mainCamera);

			//			gameStateEntity.buyTileState = false;
			//			gameStateEntity.changedBuyTileStateClearOnUpdate = false; // NOTE: Because of this, this system has to come before the UpdateLevelForBuyTileSystem
			//		}
			//	}
			//}

			foreach (var activeBuyTileOnLevelEntity in levelEntity.activeBuyTileOnLevelEntities)
			{
				if (activeBuyTileOnLevelEntity.generalButtonEntity.generalButton.buttonClicked)
				{
					// NOTE: We still want to clear all the button clicks, even if we already did the one allowed tile buy
					activeBuyTileOnLevelEntity.generalButtonEntity.generalButton.buttonClicked = false;

					if (!performedOneAllowedTileBuyPerUpdate)
					{
						if (gameStateEntity.EnoughGoldToBuyTile())
						{
							performedOneAllowedTileBuyPerUpdate = true;

							gameStateEntity.playerResourceSetAmountComponent.gold -= gameStateEntity.buyTileCostAmount;
							gameStateEntity.totalBoughtTiles++;

							coord = activeBuyTileOnLevelEntity.coord;

							levelEntity.AddTileEntity(coord, tileEntityPrefab, tileEntitiesObject, mainCamera);

							levelEntity.tileEntitiesAreDirtiedClearOnUpdate = true;

							gameStateEntity.buyTileState = false;
							gameStateEntity.changedBuyTileStateClearOnUpdate = true; // NOTE: Because of this, this system has to come before the UpdateLevelForBuyTileSystem
						}
					}
				}
			}
		}
	}
}

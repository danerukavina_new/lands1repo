﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetFinalWorkersForBuildingsSystem : BaseSystem
{
	public LevelEntity levelEntity;

	bool otherTrainingSideEffectsArePossible;

	public override void SystemUpdate()
	{
		if (levelEntity.buildingEntitiesAreDirtiedClearOnUpdate)
		{
			foreach (var buildingEntity in levelEntity.activeBuildingEntities)
			{
				if (buildingEntity.workersOrTierIsDirtyClearOnUpdate)
				{
					// NOTE: Undelying systems ensure to round extra workers to the nearest .1 - which if it accumulates to a whole number should never allow an xxx.999 scenario
					buildingEntity.finalWorkers = buildingEntity.trainedWorkers + Mathf.FloorToInt((float)buildingEntity.extraWorkers);
				}
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// NOTE: This system also creates the buff gain or remove animation entities.
public class UpdateBuffsForEachTileBasedOnLevelEntitySystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;

	TileEntity tileEntity;

	// TODO: Clean up
	public void RemoveTileBuff(TileEntity tileEntity, BuffGiverComponent buffGiverComponent)
	{
		if (!tileEntity.buffEntities.Contains(buffGiverComponent.buffEntity))
		{
			throw new System.Exception("Attempting to remove a buff from a tile, but the buff is not present.");
		}
		else
		{
			tileEntity.buffEntities.Remove(buffGiverComponent.buffEntity);
		}
	}

	public void AddOrUpdateTileBuff(TileEntity tileEntity, BuffGiverComponent buffGiverComponent)
	{
		tileEntity.buffEntitiesDirtyClearOnUpdate = true;

		if (!tileEntity.buffEntities.Contains(buffGiverComponent.buffEntity))
		{
			tileEntity.buffEntities.Add(buffGiverComponent.buffEntity);
		}
		// TODO: There is barely a concept of updating the buff... except for setting the dirty flag. The BuffEntity was already updated in "UpdateBuffGiverComponentsForEachBuildingSystem"
	}

	public override void SystemUpdate()
	{
		bool processEachBuilding = false;
		bool buildingHasChanged = false;

		if (levelEntity.tileEntitiesAreDirtiedClearOnUpdate)
			processEachBuilding = true;

		// NOTE: What happens if one set of buildings affects another set of buildings buffs? Then there would be a priority order for processing for each buffGiverComponent until all K priorities are performed

		// TODO: There needs to be a removal system here as well... to remove unspoken for buffs for that building

		foreach (var buffSourceBuildingEntity in levelEntity.activeBuildingEntities)
		{
			// TODO: buffGiverComponentsAddedClearOnUpdate should be used here... to somehow determine whether to add or udpate buffs... or unify/simplify adding and updating
			buildingHasChanged = buffSourceBuildingEntity.workersOrTierIsDirtyClearOnUpdate; // TODO: This seems incomplete at a later glance

			if (processEachBuilding || buildingHasChanged)
			{
				// TODO: Add a loop here for newly built buildings to pop up all the buffs from the tile they got built on
				// TODO: What about a building that changes from being considered basic, to advanced? Doesn't it also need to pop buff gain/buff loss indications? How is this even determined?

				foreach (var buffGiverComponent in buffSourceBuildingEntity.removedBuffGiverComponentsClearOnUpdate)
				{
					switch (buffGiverComponent.buffAreaShapeType)
					{
						case BuildingStatsScriptableObject.BuffAreaShapeType.Self:
							RemoveTileBuff(buffSourceBuildingEntity.tileEntity, buffGiverComponent);
							break;
						case BuildingStatsScriptableObject.BuffAreaShapeType.Global:
							foreach (var tileEntity in levelEntity.activeTileEntities)
								RemoveTileBuff(tileEntity, buffGiverComponent);
							break;
						case BuildingStatsScriptableObject.BuffAreaShapeType.DiamondWithRadius:
						case BuildingStatsScriptableObject.BuffAreaShapeType.SquareWithRadius:
						case BuildingStatsScriptableObject.BuffAreaShapeType.PlusWithRadius:
						case BuildingStatsScriptableObject.BuffAreaShapeType.XWithRadius:
						case BuildingStatsScriptableObject.BuffAreaShapeType.CustomSomething:
							foreach (var buffOffsetCoord in buffGiverComponent.buffOffsetCoords)
							{
								tileEntity = levelEntity.GetTileEntity(buffSourceBuildingEntity.tileEntity.coord + buffOffsetCoord);

								if (tileEntity != null)
									RemoveTileBuff(tileEntity, buffGiverComponent);
							}
							break;
						default:
							throw new System.Exception("Unexpected BuffAreaShapeType in update candidate buffs system.");
					}
				}

				foreach (var buffGiverComponent in buffSourceBuildingEntity.buffGiverComponents)
				{
					switch (buffGiverComponent.buffAreaShapeType)
					{
						case BuildingStatsScriptableObject.BuffAreaShapeType.Self:
							AddOrUpdateTileBuff(buffSourceBuildingEntity.tileEntity, buffGiverComponent);
							break;
						case BuildingStatsScriptableObject.BuffAreaShapeType.Global:
							foreach (var tileEntity in levelEntity.activeTileEntities)
								AddOrUpdateTileBuff(tileEntity, buffGiverComponent);
							break;
						case BuildingStatsScriptableObject.BuffAreaShapeType.DiamondWithRadius:
						case BuildingStatsScriptableObject.BuffAreaShapeType.SquareWithRadius:
						case BuildingStatsScriptableObject.BuffAreaShapeType.PlusWithRadius:
						case BuildingStatsScriptableObject.BuffAreaShapeType.XWithRadius:
						case BuildingStatsScriptableObject.BuffAreaShapeType.CustomSomething:
							foreach (var buffOffsetCoord in buffGiverComponent.buffOffsetCoords)
							{
								tileEntity = levelEntity.GetTileEntity(buffSourceBuildingEntity.tileEntity.coord + buffOffsetCoord);

								if (tileEntity != null)
									AddOrUpdateTileBuff(tileEntity, buffGiverComponent);
							}
							break;
						default:
							throw new System.Exception("Unexpected BuffAreaShapeType in update candidate buffs system.");
					}
				}
			}
		}
	}
}

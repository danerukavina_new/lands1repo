﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHideBuildingProductionDisplaySystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;

	bool refreshedThisUpdate;
	bool otherTrainingSideEffectsArePossible;
	float normalizedTimeFromAnimationStart;
	float animatedProductionDisplayHeight;
	float animatedProductionDisplayTop;

	public static void SetBuildingProductionDisplayStrings(BuildingEntity buildingEntity, double resourceGainsOverTime)
	{
		if (Mathf.Approximately((float)resourceGainsOverTime, 0f))
		{
			buildingEntity.buildingAssetsObjectConnector.productionAmountText.text = StringHelpers.StringConstants.ELLIPSIS;
			buildingEntity.buildingAssetsObjectConnector.productionDescriptionText.text = string.Empty;
		}
		else
		{
			buildingEntity.buildingAssetsObjectConnector.productionAmountText.text = StringHelpers.StringConstants.PLUS + resourceGainsOverTime.ToNiceString(); // TODO: Ommit the + if the production display is 5 4 digits, decimal, and a "W" symbol, or something similar... as in the largest size. Should just be a function for STring
			buildingEntity.buildingAssetsObjectConnector.productionDescriptionText.text = buildingEntity.buildingAssetsDataScriptableObject.productionDescription;
		}
	}

	public static void SetBuildingProductionDisplayStringsWithTwoResources(BuildingEntity buildingEntity, double resourceGainsOverTime, double secondaryResourceGainsOverTime)
	{
		if (Mathf.Approximately((float)resourceGainsOverTime, 0f))
		{
			buildingEntity.buildingAssetsObjectConnector.productionAmountText.text = StringHelpers.StringConstants.ELLIPSIS;
			buildingEntity.buildingAssetsObjectConnector.productionDescriptionText.text = string.Empty;
		}
		else
		{
			string secondaryResourceGainsText;

			if (!Mathf.Approximately((float)secondaryResourceGainsOverTime, 0f))
				secondaryResourceGainsText = StringHelpers.StringConstants.COMMA + StringHelpers.StringConstants.SPACE + secondaryResourceGainsOverTime.ToNiceString() + buildingEntity.buildingAssetsDataScriptableObject.additionalProductionDescription0;
			else
				secondaryResourceGainsText = string.Empty;

			buildingEntity.buildingAssetsObjectConnector.productionAmountText.text = StringHelpers.StringConstants.PLUS + resourceGainsOverTime.ToNiceString(); // TODO: Ommit the + if the production display is 5 4 digits, decimal, and a "W" symbol, or something similar... as in the largest size. Should just be a function for STring
			buildingEntity.buildingAssetsObjectConnector.productionDescriptionText.text = buildingEntity.buildingAssetsDataScriptableObject.productionDescription + secondaryResourceGainsText;
		}
	}

	public static void UpdateResourceGainsOverTimeForBuildingProductionDisplay(BuildingEntity buildingEntity)
	{
		// TODO: CASE This is a tough case to refactor... we would need multiple enums to describe every property of the building that makes some sense to display. Would seem similar to this case...
		// Is there a better solution?
		// It can be a string with substitution like "%f" for food... but then again you would need a place where you're binding "%f" to .food in the buildingResourceSetGainsOverTimeComponent.
		// Consider doing this as part of the production refactor... except some buildings don't produce resources and instead buff... so it's inconsistent. A buildings string might be a merge of the resource production and its buff? Let's just keep the case and have it be bespoke.
		switch (buildingEntity.buildingStatsScriptableObject.buildingAdvancedType)
		{
			case BuildingAdvancedType.Farm:
			case BuildingAdvancedType.FarmA2:
			case BuildingAdvancedType.FarmA3:
			case BuildingAdvancedType.FarmA4:
			case BuildingAdvancedType.FarmB2:
			case BuildingAdvancedType.FarmB3:
			case BuildingAdvancedType.FarmB4:
			case BuildingAdvancedType.FarmC2:
			case BuildingAdvancedType.FarmC3:
			case BuildingAdvancedType.FarmC4:
				SetBuildingProductionDisplayStrings(buildingEntity, buildingEntity.buildingResourceSetGainsOverTimeComponent.food);
				break;
			case BuildingAdvancedType.LumberMill:
			case BuildingAdvancedType.LumberMillA2:
			case BuildingAdvancedType.LumberMillA3:
			case BuildingAdvancedType.LumberMillA4:
			case BuildingAdvancedType.LumberMillB2:
			case BuildingAdvancedType.LumberMillB3:
			case BuildingAdvancedType.LumberMillB4:
				SetBuildingProductionDisplayStrings(buildingEntity, buildingEntity.buildingResourceSetGainsOverTimeComponent.wood);
				break;
			case BuildingAdvancedType.House:
			case BuildingAdvancedType.HouseA2:
			case BuildingAdvancedType.HouseA3:
			case BuildingAdvancedType.HouseA4:
			case BuildingAdvancedType.HouseB2:
			case BuildingAdvancedType.HouseB3:
			case BuildingAdvancedType.HouseB4:
				SetBuildingProductionDisplayStrings(buildingEntity, buildingEntity.finalWorkers);
				break;
			case BuildingAdvancedType.TownHall:
				// TODO: At some point consider protecting this with an if. For now, it's better to explore whether buffGiverComponents are always defined at the time this needs to be displayed (they should be, or if they're not, determine why not).
				SetBuildingProductionDisplayStrings(buildingEntity, buildingEntity.buffGiverComponents[0].buffEntity.buffStrength);
				break;
			case BuildingAdvancedType.Quarry:
				SetBuildingProductionDisplayStrings(buildingEntity, buildingEntity.buildingResourceSetGainsOverTimeComponent.stone);
				break;
			case BuildingAdvancedType.Mine:
				SetBuildingProductionDisplayStringsWithTwoResources(buildingEntity, buildingEntity.buildingResourceSetGainsOverTimeComponent.ore, buildingEntity.buildingResourceSetGainsOverTimeComponent.gold);
				break;
			case BuildingAdvancedType.Well:
				break;
			case BuildingAdvancedType.Shrine:
				SetBuildingProductionDisplayStrings(buildingEntity, buildingEntity.buildingResourceSetGainsOverTimeComponent.spirit);
				break;
			case BuildingAdvancedType.Windmill:
				break;
			case BuildingAdvancedType.Guild:
				// TODO: At some point consider protecting this with an if. For now, it's better to explore whether buffGiverComponents are always defined at the time this needs to be displayed (they should be, or if they're not, determine why not).
				SetBuildingProductionDisplayStrings(buildingEntity, buildingEntity.buffGiverComponents[0].buffEntity.buffStrength);
				break;
			case BuildingAdvancedType.Tavern:
				break;
			case BuildingAdvancedType.Temple:
				// TODO: At some point consider protecting this with an if. For now, it's better to explore whether buffGiverComponents are always defined at the time this needs to be displayed (they should be, or if they're not, determine why not).
				SetBuildingProductionDisplayStrings(buildingEntity, buildingEntity.buffGiverComponents[0].buffEntity.buffStrength);
				break;
			case BuildingAdvancedType.Laboratory:
				break;
			case BuildingAdvancedType.Blacksmith:
				break;
			case BuildingAdvancedType.Inn:
				break;
			case BuildingAdvancedType.TrainingGrounds:
				break;
			case BuildingAdvancedType.Dungeon:
				break;
			case BuildingAdvancedType.TradeRoute:
				break;
			case BuildingAdvancedType.Shipyard:
				break;
			case BuildingAdvancedType.SpecialCaptureInProgress:
				break;
			case BuildingAdvancedType.SpecialSeaBorder:
				break;
			case BuildingAdvancedType.SpecialMountainPassBorder:
				break;
			case BuildingAdvancedType.SpecialMountainBorder:
				break;
			default:
				break;
		}
	}

	public override void SystemUpdate()
	{
		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			refreshedThisUpdate = false;

			if (buildingEntity.showOrRefreshProductionDisplayPanel)
			{
				buildingEntity.showOrRefreshProductionDisplayPanel = false;

				refreshedThisUpdate = true;

				buildingEntity.lastRefreshOfProductionDisplayPanelTime = gameStateEntity.gameTime; // TODO: This seems iffy, using game time for UI

				if (!buildingEntity.buildingAssetsObjectConnector.productionDisplayPanel.gameObject.activeSelf)
				{
					buildingEntity.startedShowingProductionDisplayPanelTime = gameStateEntity.gameTime; // TODO: This seems iffy, using game time for UI

					buildingEntity.buildingAssetsObjectConnector.productionDisplayPanel.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, ValueHolder.PRODUCTION_DISPLAY_COLLAPSED_TOP_VALUE);
					buildingEntity.buildingAssetsObjectConnector.productionAmountText.text = string.Empty;
					buildingEntity.buildingAssetsObjectConnector.productionDescriptionText.text = string.Empty;

					buildingEntity.buildingAssetsObjectConnector.productionDisplayPanel.gameObject.SetActive(true);
				}

				
			}

			if (buildingEntity.buildingAssetsObjectConnector.productionDisplayPanel.gameObject.activeSelf)
			{
				if (gameStateEntity.gameTime - buildingEntity.lastRefreshOfProductionDisplayPanelTime <= ValueHolder.DURATION_FOR_SHOWING_PRODUCTION_DISPLAY)
				{
					normalizedTimeFromAnimationStart = (gameStateEntity.gameTime - buildingEntity.startedShowingProductionDisplayPanelTime) / ValueHolder.DURATION_FOR_PRODUCTION_DISPLAY_ANIMATION;

					if (normalizedTimeFromAnimationStart <= 1f)
					{
						animatedProductionDisplayTop = Mathf.Lerp(ValueHolder.PRODUCTION_DISPLAY_COLLAPSED_TOP_VALUE, ValueHolder.PRODUCTION_DISPLAY_FULLY_OPEN_TOP_VALUE, normalizedTimeFromAnimationStart);
						animatedProductionDisplayHeight = 1f - animatedProductionDisplayTop - ValueHolder.PRODUCTION_DISPLAY_FIXED_BOTTOM_VALUE;

						buildingEntity.buildingAssetsObjectConnector.productionDisplayPanel.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, animatedProductionDisplayTop, animatedProductionDisplayHeight);
					}

					if (normalizedTimeFromAnimationStart >= 1f)
					{
						// NOTE: Using empty string as a sentinel value to indicate the display has not been generated yet
						if (refreshedThisUpdate || buildingEntity.buildingAssetsObjectConnector.productionAmountText.text == string.Empty)
						{
							UpdateResourceGainsOverTimeForBuildingProductionDisplay(buildingEntity);
						}
					}
				}
				else
					buildingEntity.buildingAssetsObjectConnector.productionDisplayPanel.gameObject.SetActive(false); // TODO: Should definitely be an outro animation, lerping again back up to fully expanded when a new refresh hits
			}
		}
	}
}

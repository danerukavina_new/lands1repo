﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DissipationSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public InputStateEntity inputStateEntity;

	InputIndicatorAnimationEntity inputIndicatorAnimationEntity;
	DissipateComponent dissipateComponent;
	float leftoverTime;

	public override void SystemUpdate()
	{
		// TODO: IMMEDIATELY FIX - was interrupted, but I believe this is immediately dissapating. The equation is flipped I think
		for (int i = inputStateEntity.inputIndicatorAnimationEntities.Count - 1; i >= 0; i--)
		{
			inputIndicatorAnimationEntity = inputStateEntity.inputIndicatorAnimationEntities[i];
			dissipateComponent = inputIndicatorAnimationEntity.dissipateComponent;

			leftoverTime = (dissipateComponent.dissipationStartAnimationTime + dissipateComponent.dissipationDuration) - gameStateEntity.gameTime;

			if (dissipateComponent.dissipateMoreQuickly)
				leftoverTime = Mathf.Min(leftoverTime, (dissipateComponent.quickerDissipationStartAnimationTime + dissipateComponent.quickerDissipationDuration) - gameStateEntity.gameTime);

			if (leftoverTime <= 0f)
			{
				inputStateEntity.inputIndicatorAnimationEntities.RemoveAt(i);
				GameObject.Destroy(inputIndicatorAnimationEntity.gameObject);
			}
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatUpInWorldSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	BuffGainOrRemoveAnimationEntity buffGainOrRemoveAnimationEntity;

	public override void SystemUpdate()
	{
		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			for (int i = buildingEntity.buffGainOrRemoveAnimationEntities.Count - 1; i >= 0; i--)
			{
				buffGainOrRemoveAnimationEntity = buildingEntity.buffGainOrRemoveAnimationEntities[i];

				if (buffGainOrRemoveAnimationEntity.floatUpInWorldComponent.dissipationStartTime + buffGainOrRemoveAnimationEntity.floatUpInWorldComponent.dissipationDuration <= gameStateEntity.gameTime)
				{
					buildingEntity.buffGainOrRemoveAnimationEntities.RemoveAt(i);
					GameObject.Destroy(buffGainOrRemoveAnimationEntity.gameObject);
				}
				else if (buffGainOrRemoveAnimationEntity.floatUpInWorldComponent.dissipationStartTime + buffGainOrRemoveAnimationEntity.floatUpInWorldComponent.lingerInPlaceDelay >= gameStateEntity.gameTime)
				{
					// TODO: It makes sense to use the real delta time here, perhaps? But it should still be dependency injected via gameStateEntity I think...
					// But then it should be... clamped? Or it might not matter because of the if above checking dissipation?
					buffGainOrRemoveAnimationEntity.floatUpInWorldComponent.objectForAnimatingTransform.position += Vector3.up * buffGainOrRemoveAnimationEntity.floatUpInWorldComponent.worldMovementSpeed * Time.deltaTime;
				}
			}
		}
	}
}
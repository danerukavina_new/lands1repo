﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: DELETE THIS, PROBABLY. PerformBuildingTr...System takes care of this functionality
public class LoadBoughtMythsSystem : BaseSystem
{
	public Camera mainCamera;
	public GameStateEntity gameStateEntity;
	public MythButtonEntity mythButtonEntityPrefab;
	public GameObject showBoughtMythsMenuGridView;
	public GlobalBuffDataEntity globalBuffDataEntity;

	//// TODO: Was copied over to the PerformBuildingTr...System
	//public static void AddMythButtonEntityToShowBoughtMythsMenu(
	//	MythType mythType,
	//	MythButtonEntity mythButtonEntityPrefab,
	//	// Dictionary<BuildingAdvancedType, BuildingAssetsDataScriptableObject> buildingAssetsDataScriptableObjects,
	//	// Dictionary<BuildingAdvancedType, BuildingStatsScriptableObject> buildingStatsScriptableObjects,
	//	GameObject showBoughtMythsMenuGridView,
	//	Camera mainCamera
	//)
	//{
	//	MythButtonEntity mythButtonEntity = GameObject.Instantiate(mythButtonEntityPrefab, Vector3.zero, Quaternion.identity, showBoughtMythsMenuGridView.transform);

	//	mythButtonEntity.canvas.worldCamera = mainCamera;

	//	mythButtonEntity.tempJustShowBoughtMythDisplay = true;

	//	//buildingEntity.buildingAssetsDataScriptableObject = buildingAssetsDataScriptableObjects[buildingAdvancedType];
	//	//buildingEntity.buildingStatsScriptableObject = buildingStatsScriptableObjects[buildingAdvancedType];
	//}

	public override void SystemUpdate()
	{
		if (globalBuffDataEntity.activeMythsChangedClearOnUpdate)
		{

		}
	}
}

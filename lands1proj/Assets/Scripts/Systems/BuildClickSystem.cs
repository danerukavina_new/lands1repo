﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildClickSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public Camera mainCamera;
	public BuildingEntity buildingEntityPrefab;
	public Dictionary<BuildingAdvancedType, BuildingAssetsDataScriptableObject> buildingAssetsDataScriptableObjects;
	public Dictionary<BuildingAdvancedType, BuildingStatsScriptableObject> buildingStatsScriptableObjects;
	public GameObject buildingEntitiesObject;
	public GlobalBuffDataEntity globalBuffDataEntity;

	BuildingStatsScriptableObject buildingStatsScriptableObject;

	public override void SystemUpdate()
	{
		foreach (var tileEntity in levelEntity.activeTileEntities)
		{
			if (tileEntity.buildBuildingButtonClicked)
			{
				// TODO: Add a check if the building can actually be built on this active tile. What's the outcome? A message? What's the UX for checking building price ahead of time?
				tileEntity.buildBuildingButtonClicked = false;

				buildingStatsScriptableObject = buildingStatsScriptableObjects[tileEntity.buildBuildingButtonClickBuildingAdvancedType];

				// TODO: This should indeed be checked here, but also the buttons should be disabled and/or show progress towards being able to be chosen. This requires UX work.
				if (gameStateEntity.canBuildTileBuilding[
					buildingStatsScriptableObject.impliedBuildingBaseType
					])
				{
					// TODO: This shouldn't subtract directly, it should consider discounts
					// TODO: Perhaps the discounted building cost should be stored. Then no need to move around GlobalBuffDataEntity
					gameStateEntity.playerResourceSetAmountComponent -= LogicHelpers.BuildingCostWithGlobalBuffReductions(buildingStatsScriptableObject, globalBuffDataEntity);

					levelEntity.AddBuildingEntity(tileEntity, tileEntity.buildBuildingButtonClickBuildingAdvancedType, buildingEntityPrefab, buildingAssetsDataScriptableObjects, buildingStatsScriptableObjects, buildingEntitiesObject, mainCamera);

					tileEntity.buildingBuiltOnTopOfThisTile = true;
				}
			}
		}
	}
}

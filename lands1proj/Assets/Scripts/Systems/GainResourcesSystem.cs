﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Updates the menus of all the buildings, specifically whether certain building actions can be taken (or are even shown as an option), based on game state.
/// </summary>
public class GainResourcesSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;

	public override void SystemUpdate()
	{
		// TODO: Improve this to never gain non-integer resources, and instead accumulate time until a whole resource can be gained
		// Or should like each building actually give resources? And the calculated total is just for display and ascend? Nah

		gameStateEntity.playerResourceSetAmountComponent += gameStateEntity.playerResourceGainsThisUpdateComponent + gameStateEntity.playerResourceSetGainsOverTimeComponent * (double)gameStateEntity.updateGameDeltaTime;

		gameStateEntity.playerResourceGainsThisUpdateComponent.SetToZero();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: This system is badly named - it doesn't process all GeneralButtonEntity clicks, just those not accounted for by other systems
public class PerformAscensionSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public ButtonManagerEntity buttonManagerEntity;
	public LevelEntity levelEntity;

	public static void ResetTileEntityMenus(LevelEntity levelEntity)
	{
		foreach (var tileEntity in levelEntity.activeTileEntities)
		{
			tileEntity.gameEventDemandsReturnToInitialMenu = true;
		}
	}

	public override void SystemUpdate()
	{
		if (gameStateEntity.ascensionTriggered)
		{
			gameStateEntity.ascensionTriggered = false;

			switch (gameStateEntity.triggeredAscensionType)
			{
				case GameStateEntity.AscensionType.Ascend:
					if (levelEntity.CanAscend())
					{
						gameStateEntity.PartiallyReinitializeResourcesAfterAscend();
						gameStateEntity.playerResourceSetAmountComponent.gold += gameStateEntity.ascendGoldGainAmount;

						levelEntity.RemoveBuildingEntities();
						levelEntity.ReenableSomeFogEntities();
						levelEntity.MakeDirtyTileEntities(); // TODO: In the future, perhaps only the fog clearing components, if implemented, should be "dirtied"
						ResetTileEntityMenus(levelEntity);
						break;
					}
					else
						throw new System.Exception("Asension triggered when it should be impossible.");
				case GameStateEntity.AscensionType.Master:
					throw new System.Exception("Master ascension NYI.");
				case GameStateEntity.AscensionType.Enshrine:
					throw new System.Exception("Enshrine ascension NYI.");
				default:
					throw new System.Exception("Unexpected ascension type in PerformAscensionSystem.");
			}
		}

		// TODO: Mastery and Ensrine clicks
	}
}

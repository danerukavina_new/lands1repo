﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// TODO: The point of all this is to allow for storing the data in something like a json. Otherwise, it would have made more sense to just write the data into the prefab.
/// If it was annoying to set colors for multiple buttons in Prefab, then it should have just been a color value on the prfab that's set via script to all the buttons, not everything
/// Actually! An important advantage of this is if you want to change all your buildings, parceling out building configuration data lets you update only one prefab
/// TODO: Why is this system not just a function performed when a building is built? Are there some other systems that need to rectify data? I don't think so. Put this in the end of the building system.
public class SetInitialBuildingAssetsSystem : BaseSystem
{
	public LevelEntity levelEntity;

	BuildingAssetsObjectConnector buildingAssetsObjectConnector;
	BuildingAssetsDataScriptableObject buildingAssetsDataScriptableObject;
	Color textColor;

	public override void SystemUpdate()
	{
		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			if (buildingEntity.isDirtyFromBeingBuilt)
			{
				buildingEntity.isDirtyFromBeingBuilt = false;

				buildingAssetsObjectConnector = buildingEntity.buildingAssetsObjectConnector;
				buildingAssetsDataScriptableObject = buildingEntity.buildingAssetsDataScriptableObject;
				textColor = buildingAssetsDataScriptableObject.borderColor;

				//// TODO: Unify this with UIHelpers.SetMythButtonUIValues, it has a lof in common
				//buildingAssetsObjectConnector.buildingIcon.sprite = buildingAssetsDataScriptableObject.buildingIconSprite;
				//buildingAssetsObjectConnector.buildingIcon.color = buildingAssetsDataScriptableObject.borderColor;

				UIHelpers.SetSpriteAndBorderUIValues(buildingAssetsObjectConnector.background, buildingAssetsDataScriptableObject.mainColor, buildingAssetsDataScriptableObject.borderColor, buildingAssetsDataScriptableObject.buildingIconSprite);

				UIHelpers.SetSpriteAndBorderUIValues(buildingAssetsObjectConnector.trainingButtonSpriteAndBoarder, buildingAssetsDataScriptableObject.mainColor, buildingAssetsDataScriptableObject.borderColor, null);

				UIHelpers.ShowHideGameObject(buildingAssetsObjectConnector.openTierUpgradeScreenButton.gameObject, false);
				UIHelpers.SetSpriteAndBorderUIValues(buildingAssetsObjectConnector.openTierUpgradeScreenButtonSpriteAndBoarder, buildingAssetsDataScriptableObject.mainColor, buildingAssetsDataScriptableObject.borderColor, null);
				buildingAssetsObjectConnector.openTierUpgradeScreenButtonText.color = textColor;
				buildingAssetsObjectConnector.openTierUpgradeScreenButtonText.color = textColor;

				UIHelpers.SetSpriteAndBorderUIValues(buildingAssetsObjectConnector.buildingDetailsTopRightLensButton, buildingAssetsDataScriptableObject.mainColor, buildingAssetsDataScriptableObject.borderColor, null);
				UIHelpers.SetSpriteAndBorderUIValues(buildingAssetsObjectConnector.buildingLockingTopLeftLockButton, buildingAssetsDataScriptableObject.mainColor, buildingAssetsDataScriptableObject.borderColor, null);

				buildingAssetsObjectConnector.buildingNameText.text = buildingAssetsDataScriptableObject.buildingName;
				buildingAssetsObjectConnector.buildingNameText.color = textColor;

				if (buildingAssetsDataScriptableObject.hasTrainingButton)
				{
					// TODO: This is a quick fix for tier upgrading to not get its train button emptied out. Ideally, the approach should be to correctly handle tier upgrading rather than piggybacking on this building code.
					if (buildingEntity.finalWorkers == 0)
					{
						buildingAssetsObjectConnector.trainingButtonText.gameObject.SetActive(true);

						buildingAssetsObjectConnector.trainingButtonText.text = string.Empty; // TODO: Refreshing the building here with dirtyFromBeingBuilt causes problems

						buildingAssetsObjectConnector.trainingButtonText.color = textColor;
					}
				}
				else
					buildingAssetsObjectConnector.trainingButtonText.gameObject.SetActive(false);

				foreach (var additionalTrainingButton in buildingAssetsObjectConnector.additionalTrainingButtons)
				{
					UIHelpers.SetSpriteAndBorderUIValues(additionalTrainingButton, buildingAssetsDataScriptableObject.mainColor, buildingAssetsDataScriptableObject.borderColor, null);
				}

				if (buildingAssetsObjectConnector.additionalTrainingButtonTexts.Length != buildingAssetsDataScriptableObject.additionalTrainingSentences.Length)
					throw new System.Exception("Not enough additional training sentences for the additional training buttons when initializing a building.");

				for (int i = 0; i < buildingAssetsObjectConnector.additionalTrainingButtonTexts.Length; i++)
				{
					buildingAssetsObjectConnector.additionalTrainingButtonTexts[i].text = buildingAssetsDataScriptableObject.additionalTrainingSentences[i];
					buildingAssetsObjectConnector.additionalTrainingButtonTexts[i].color = textColor;
				}

				buildingAssetsObjectConnector.workerCountDisplayText.color = textColor;

				UIHelpers.ShowHideGameObject(buildingAssetsObjectConnector.productionDisplayPanel.gameObject, false);
				UIHelpers.SetSpriteAndBorderUIValues(buildingAssetsObjectConnector.productionDisplayPanelSpriteAndBoarder, buildingAssetsDataScriptableObject.mainColor, buildingAssetsDataScriptableObject.borderColor, null);
				buildingAssetsObjectConnector.productionAmountText.color = textColor;
				buildingAssetsObjectConnector.productionDescriptionText.color = textColor;

				UIHelpers.SetSpriteAndBorderUIValues(buildingAssetsObjectConnector.lockTrainingButton, buildingAssetsDataScriptableObject.mainColor, buildingAssetsDataScriptableObject.borderColor, null);
				// TODO: It's likely the lock training and demolish buttons will lose their text, so don't forget to clean this up
				buildingAssetsObjectConnector.lockTrainingButtonText.color = textColor;

				UIHelpers.SetSpriteAndBorderUIValues(buildingAssetsObjectConnector.demolishBuildingButton, buildingAssetsDataScriptableObject.mainColor, buildingAssetsDataScriptableObject.borderColor, null);
				buildingAssetsObjectConnector.demolishBuildingButtonText.color = textColor;

				if (buildingAssetsDataScriptableObject.additionalBackgroundArtSprite != null)
				{
					buildingAssetsObjectConnector.additionalBackroungArtImage.gameObject.SetActive(true);
					buildingAssetsObjectConnector.additionalBackroungArtImage.sprite = buildingAssetsDataScriptableObject.additionalBackgroundArtSprite;
				}
				else
					buildingAssetsObjectConnector.additionalBackroungArtImage.gameObject.SetActive(false);
			}
		}
	}
}


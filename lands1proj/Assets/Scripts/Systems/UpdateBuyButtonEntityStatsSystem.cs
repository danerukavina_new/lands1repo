﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateBuyButtonEntityStatsSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public ButtonManagerEntity buttonManagerEntity;

	BuyResourcePrototypeEntity buyResourcePrototypeEntity;

	public override void SystemUpdate()
	{
		foreach (var buyResourceButtonEntity in buttonManagerEntity.buyResourceButtonEntities)
		{
			buyResourcePrototypeEntity = buyResourceButtonEntity.buyResourcePrototypeEntity;

			buyResourceButtonEntity.finalResourceCost.resourceType = buyResourcePrototypeEntity.baseResourceCost.resourceType;
			buyResourceButtonEntity.finalResourceCost.amount = buyResourcePrototypeEntity.baseResourceCost.baseValue; // TODO: Make the gold cost scale with something to make buying easier later in the game. Then use this amount to multiply the gain

			buyResourceButtonEntity.finalResourceGain.resourceType = buyResourcePrototypeEntity.baseResourceGain.resourceType;

			switch (buyResourcePrototypeEntity.buyResourceType)
			{
				case BuyResourceType.BuyFood:
					buyResourceButtonEntity.finalResourceGain.amount = buyResourcePrototypeEntity.baseResourceGain.baseValue + buyResourcePrototypeEntity.baseResourceGain.scalingFactor * gameStateEntity.playerResourceSetGainsOverTimeComponent.food;
					break;
				case BuyResourceType.BuyWood:
					buyResourceButtonEntity.finalResourceGain.amount = buyResourcePrototypeEntity.baseResourceGain.baseValue + buyResourcePrototypeEntity.baseResourceGain.scalingFactor * gameStateEntity.playerResourceSetGainsOverTimeComponent.wood;
					break;
				case BuyResourceType.BuyFoodViaTrade:
					break;
				case BuyResourceType.BuyWoodViaTrade:
					break;
				case BuyResourceType.BuyStoneViaTrade:
					break;
				case BuyResourceType.BuyMetalViaTrade:
					break;
				default:
					throw new System.Exception("Unexpected Buy Resource Type in the UpdateButtonStats system");
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// TODO: Perhaps call this either "Auto" or "BasedOn Game State, Level Entity... etc."
public class ShowHideMenusAndSomeButtonsSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public InputStateEntity inputStateEntity;
	public LevelEntity levelEntity;
	public GlobalBuffDataEntity globalBuffDataEntity;
	public RitualStateEntity ritualStateEntity;
	public ButtonManagerEntity buttonManagerEntity;
	public MenuManagerEntity menuManagerEntity;
	public ScreenManagerEntity screenManagerEntity;


	bool showAscendMenu;
	bool showBuyMythsMenuInsteadOfShowBoughtMythsMenu;
	BuildingEntity buildingTierUpgradeBuildingEntity;
	bool canStartParticularBuildingTierUpgrade;

	public override void SystemUpdate()
	{
		// Gold Menu
		UIHelpers.ShowHideGameObject(menuManagerEntity.goldMenuEntity.gameObject, gameStateEntity.playerResourceSetAmountComponent.gold > 0d && inputStateEntity.worldInputType == InputStateEntity.WorldInputType.DefaultWorldInput);

		// Ascend Menu
		showAscendMenu = levelEntity.CanAscend() && inputStateEntity.worldInputType == InputStateEntity.WorldInputType.DefaultWorldInput; // || other conditions

		UIHelpers.ShowHideGameObject(menuManagerEntity.ascendMenuEntity.gameObject, showAscendMenu);

		UIHelpers.ShowHideGameObject(buttonManagerEntity.ascendButtonEntity.gameObject, levelEntity.CanAscend());

		UIHelpers.ShowHideGameObject(buttonManagerEntity.masterButtonEntity.gameObject, gameStateEntity.CanMaster());

		UIHelpers.ShowHideGameObject(buttonManagerEntity.enshrineButtonEntity.gameObject, gameStateEntity.CanEnshrine());

		// Spirituality Menu
		UIHelpers.ShowHideGameObject(menuManagerEntity.spiritualityMenuEntity.gameObject, gameStateEntity.SpiritualityAndRitualsAndMythsAreAvailable() && inputStateEntity.worldInputType == InputStateEntity.WorldInputType.DefaultWorldInput);

		UIHelpers.EnableDisableButton(buttonManagerEntity.godsButtonEntity.generalButton.button, gameStateEntity.GodsAreAvailable());

		UIHelpers.ShowHideGameObject(buttonManagerEntity.idolsButtonEntity.gameObject, gameStateEntity.canBuyIdols);

		UIHelpers.EnableDisableButton(buttonManagerEntity.repeatRitualButtonEntity.generalButton.button, gameStateEntity.CanRepeatRitual());

		// Super Ascend Menu
		UIHelpers.ShowHideGameObject(menuManagerEntity.superAscendMenuEntity.gameObject, levelEntity.CanSuperAscend() && inputStateEntity.worldInputType == InputStateEntity.WorldInputType.DefaultWorldInput);

		// Civilization Menu
		UIHelpers.ShowHideGameObject(menuManagerEntity.civilizationMenuEntity.gameObject, gameStateEntity.CanManageCvilization() && inputStateEntity.worldInputType == InputStateEntity.WorldInputType.DefaultWorldInput);

		// Choosing among menus on screens
		showBuyMythsMenuInsteadOfShowBoughtMythsMenu = !LogicHelpers.MaxMythsReached(gameStateEntity, globalBuffDataEntity);

		UIHelpers.ShowHideGameObject(screenManagerEntity.mythsScreen.menuInScreenEntity0.gameObject, showBuyMythsMenuInsteadOfShowBoughtMythsMenu);
		UIHelpers.ShowHideGameObject(screenManagerEntity.mythsScreen.menuInScreenEntity1.gameObject, !showBuyMythsMenuInsteadOfShowBoughtMythsMenu);

		// Ritual Input Menu
		UIHelpers.ShowHideGameObject(menuManagerEntity.ritualInputMenuEntity.gameObject, inputStateEntity.worldInputType == InputStateEntity.WorldInputType.RitualWorldInput);

		// TODO: Perhaps group all of these into a panel and use the "!screenManagerEntity.ritualDetailsScreen.gameObject.activeSelf" condition on that
		UIHelpers.ShowHideGameObject(buttonManagerEntity.undoRitualStepButton.gameObject, ritualStateEntity.ritualEntity.ritualStepEntities.Count > 0 && !screenManagerEntity.ritualDetailsScreen.gameObject.activeSelf);
		UIHelpers.ShowHideGameObject(buttonManagerEntity.redoRitualStepButton.gameObject, ritualStateEntity.ritualEntity.ritualStepEntities.NumberOfRedosPossible > 0 && !screenManagerEntity.ritualDetailsScreen.gameObject.activeSelf);
		UIHelpers.ShowHideGameObject(buttonManagerEntity.performRitualButton.gameObject, LogicHelpers.ShouldShowRitualPerformButton(ritualStateEntity) && !screenManagerEntity.ritualDetailsScreen.gameObject.activeSelf);
		UIHelpers.ShowHideGameObject(buttonManagerEntity.performRitualFromRitualDetailsButton.gameObject, LogicHelpers.ShouldShowRitualPerformButton(ritualStateEntity) && screenManagerEntity.ritualDetailsScreen.gameObject.activeSelf);
		UIHelpers.EnableDisableButton(buttonManagerEntity.performRitualButton.generalButton.button, ritualStateEntity.playerCanPerformRitual);
		UIHelpers.EnableDisableButton(buttonManagerEntity.performRitualFromRitualDetailsButton.generalButton.button, ritualStateEntity.playerCanPerformRitual);
		UIHelpers.ShowHideGameObject(buttonManagerEntity.showRitualDetailsButtonEntity.gameObject, !screenManagerEntity.ritualDetailsScreen.gameObject.activeSelf);
		UIHelpers.ShowHideGameObject(menuManagerEntity.ritualResourceDisplayEntity.gameObject, !screenManagerEntity.ritualDetailsScreen.gameObject.activeSelf);

		if (screenManagerEntity.buildingTierUpgradeScreenMenuSpriteAndBorderPanel.gameObject.activeSelf)
		{
			buildingTierUpgradeBuildingEntity = screenManagerEntity.buildingTierUpgradeBuildingEntity;

			foreach (var buildingTierUpgradeButtonEntity in buttonManagerEntity.buildingTierUpgradeButtonEntities)
			{
				if (buildingTierUpgradeButtonEntity != null && buildingTierUpgradeButtonEntity.gameObject.activeSelf)
				{
					canStartParticularBuildingTierUpgrade = buildingTierUpgradeBuildingEntity.canStartNextBuildingTierUpgrades[buildingTierUpgradeButtonEntity.correspondingTierUpgradePathIndex];

					UIHelpers.EnableDisableButton(buildingTierUpgradeButtonEntity.researchableUIComponent.researchButton.button, canStartParticularBuildingTierUpgrade);
				}
			}
		}
	}
}

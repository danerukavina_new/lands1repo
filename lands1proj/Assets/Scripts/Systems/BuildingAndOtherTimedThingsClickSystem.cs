﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingAndOtherTimedThingsClickSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;
	public ButtonManagerEntity buttonManagerEntity;
	public ScreenManagerEntity screenManagerEntity;

	BuildingEntity buildingTierUpgradeBuildingEntity;

	public override void SystemUpdate()
	{
		if (screenManagerEntity.buildingTierUpgradeScreen.gameObject.activeSelf)
		{
			buildingTierUpgradeBuildingEntity = screenManagerEntity.buildingTierUpgradeBuildingEntity;

			foreach (var buildingTierUpgradeButtonEntity in buttonManagerEntity.buildingTierUpgradeButtonEntities)
			{
				if (buildingTierUpgradeButtonEntity.researchableUIComponent.researchButton.buttonClicked)
				{
					buildingTierUpgradeButtonEntity.researchableUIComponent.researchButton.buttonClicked = false;

					// TODO: This code seems backwards. The canStartTierUpgrade is set somewhat arbitrarily, to the specific thing the user clicked. The building should know for each different index if it can start.

					// TODO: Perhaps the "startUpgradeBuildingTier" request should change to "tryStartUpgradeBuildingTier" and only one, perhaps 2 places (the while loops for long time steps) should be checking "canStartTierUpgrade". To centralize. Same for canTrain. Might be a big refactor, hopefully not
					if (buildingTierUpgradeBuildingEntity.canStartNextBuildingTierUpgrades[buildingTierUpgradeBuildingEntity.finalTierUpgradePathIndex])
					{
						// TODO: Separating where this flag is set, and where the upgrade path is set... seems wrong
						buildingTierUpgradeBuildingEntity.startUpgradingBuildingTier = true;
						buildingTierUpgradeBuildingEntity.startBuildingTierUpgradePathIndex = buildingTierUpgradeButtonEntity.correspondingTierUpgradePathIndex;
						buildingTierUpgradeBuildingEntity.tierUpgradeRequestIsFromPlayerInteraction = true;
					}
					else
						throw new System.Exception("Building entity trying to start upgrading while not able to (due to resources, etc.)");
				}
				else
				{
					// NOTE: It doesn't make sense to update the buttons being enabled/disabled here, because that's not the responsibility of this system
				}
			}
		}

		foreach (var buildingEntity in levelEntity.activeBuildingEntities)
		{
			if (buildingEntity.buildingButtonClicked)
			{
				buildingEntity.buildingButtonClicked = false;

				switch (buildingEntity.buildingButtonClickAction)
				{
					case BuildingButtonClickAction.Train:
						if (!buildingEntity.isTraining && buildingEntity.canTrain)
							buildingEntity.startTraining = true;
						break;
					case BuildingButtonClickAction.OpenTierUpgradeScreen:
						if (buildingEntity.isAtMaxTrainedWorkersForTierAndThusMayTierUpgrade)
							buildingEntity.openBuildingTierUpgradeMenu = true;
						else
							throw new System.Exception("It should be impossible to click on the Upgrade button when a building is not at max trained workers for tier.");
						break;
					case BuildingButtonClickAction.BuildingDetailsTopRightLensClick:
						break;
					case BuildingButtonClickAction.TrainSpecial0:
						break;
					case BuildingButtonClickAction.TrainSpecial1:
						break;
					case BuildingButtonClickAction.TrainSpecial2:
						break;
					case BuildingButtonClickAction.LockTraining:
						break;
					case BuildingButtonClickAction.DemolishBuilding:
						break;
					case BuildingButtonClickAction.HeroChooseGear:
						break;
					case BuildingButtonClickAction.HeroCaptureTile:
						break;
					case BuildingButtonClickAction.HeroLevelUp:
						break;
					case BuildingButtonClickAction.HeroAutoCombat:
						break;
					default:
						break;
				}
			}
		}


		foreach (var mythButtonEntity in buttonManagerEntity.mythBuyingButtonEntities)
		{
			if (mythButtonEntity.researchableUIComponent.researchButton.buttonClicked)
			{
				mythButtonEntity.researchableUIComponent.researchButton.buttonClicked = false;

				if (!mythButtonEntity.researchableStateComponent.isBeingResearched && mythButtonEntity.mythSlotForResearchOrCantResearch != MythSlotForResearchOrCantResearch.CantResearch)
					mythButtonEntity.researchableStateComponent.startResearch = true;
				else
					Debug.Log("Can't start resaerch on this myth."); // TODO: The myth button should be disabled and this should be an exception-like error message
			}

			if (mythButtonEntity.researchableUIComponent.cancelResearchButton.buttonClicked)
			{
				mythButtonEntity.researchableUIComponent.cancelResearchButton.buttonClicked = false;

				if (mythButtonEntity.researchableStateComponent.isBeingResearched)
					mythButtonEntity.researchableStateComponent.cancelResearch = true;
				else
					Debug.Log("Cancel research attempted on a myth that isn't in progress.");
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMenuClickSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public LevelEntity levelEntity;

	TileEntity tileEntity;
	bool stateChanged;

	public override void SystemUpdate()
	{
		foreach (var tileEntity in levelEntity.activeTileEntities)
		{
			stateChanged = false;

			// TODO: Replace all the logic below with a candidate state
			// TODO: gameEventDemandsReturnToInitialMenu means this isn't just a tile menu "click" system anymore, consider renaming
			if (tileEntity.generalCancelButtonClicked || tileEntity.gameEventDemandsReturnToInitialMenu)
			{
				tileEntity.generalCancelButtonClicked = false;
				tileEntity.gameEventDemandsReturnToInitialMenu = false;

				tileEntity.tileMenuState = TileMenuState.InitialMenu;
				stateChanged = true;
			}

			if (tileEntity.buildButtonClicked)
			{
				tileEntity.buildButtonClicked = false;

				tileEntity.tileMenuState = TileMenuState.Build;
				stateChanged = true;

				tileEntity.buildMenuPanel.SetActive(true);
			}

			if (tileEntity.buildAdvancedButtonClicked)
			{
				tileEntity.buildAdvancedButtonClicked = false;

				tileEntity.tileMenuState = TileMenuState.BuildAdvanced;
				stateChanged = true;

				tileEntity.buildAdvancedMenuPanel.SetActive(true);
			}

			if (tileEntity.buildHeroicButtonClicked)
			{
				tileEntity.buildHeroicButtonClicked = false;

				tileEntity.tileMenuState = TileMenuState.BuildHeroic;
				stateChanged = true;

				tileEntity.buildHeroicMenuPanel.SetActive(true);
			}

			if (tileEntity.buildBorderButtonClicked)
			{
				tileEntity.buildBorderButtonClicked = false;

				tileEntity.tileMenuState = TileMenuState.BuildBorder;
				stateChanged = true;

				tileEntity.buildBorderMenuPanel.SetActive(true);
			}

			if (tileEntity.buildingBuiltOnTopOfThisTile)
			{
				tileEntity.buildingBuiltOnTopOfThisTile = false;

				tileEntity.tileMenuState = TileMenuState.BuiltOnTopOfNoMenu;
				stateChanged = true;
			}

			switch (tileEntity.tileMenuState)
			{
				case TileMenuState.InitialMenu:
					if (stateChanged)
					{
						if (tileEntity.buildMenuPanel.activeSelf)
							tileEntity.buildMenuPanel.SetActive(false);

						if (tileEntity.buildAdvancedMenuPanel.activeSelf)
							tileEntity.buildAdvancedMenuPanel.SetActive(false);

						if (tileEntity.buildHeroicMenuPanel.activeSelf)
							tileEntity.buildHeroicMenuPanel.SetActive(false);

						if (tileEntity.buildBorderMenuPanel.activeSelf)
							tileEntity.buildBorderMenuPanel.SetActive(false);

						tileEntity.initialMenuPanel.SetActive(true);
					}

					if (!tileEntity.buildAdvancedButton.activeSelf && gameStateEntity.showAdvancedBuildMenus)
						tileEntity.buildAdvancedButton.SetActive(true);

					if (!tileEntity.buildHeroicButton.activeSelf && gameStateEntity.showHeroicBuildMenus)
						tileEntity.buildHeroicButton.SetActive(true);

					if (!tileEntity.buildBorderButton.activeSelf && gameStateEntity.showBorderBuildMenus)
						tileEntity.buildBorderButton.SetActive(true);

					if (tileEntity.generalCancelButton.activeSelf)
						tileEntity.generalCancelButton.SetActive(false);
					break;
				case TileMenuState.Build:
					if (stateChanged)
					{
						tileEntity.initialMenuPanel.SetActive(false);
						tileEntity.buildMenuPanel.SetActive(true);
						tileEntity.generalCancelButton.SetActive(true);
					}
					break;
				case TileMenuState.BuildAdvanced:
					if (stateChanged)
					{
						tileEntity.initialMenuPanel.SetActive(false);
						tileEntity.buildAdvancedMenuPanel.SetActive(true);
						tileEntity.generalCancelButton.SetActive(true);
					}
					break;
				case TileMenuState.BuildHeroic:
					if (stateChanged)
					{
						tileEntity.initialMenuPanel.SetActive(false);
						tileEntity.buildHeroicMenuPanel.SetActive(true);
						tileEntity.generalCancelButton.SetActive(true);
					}
					break;
				case TileMenuState.BuildBorder:
					if (stateChanged)
					{
						tileEntity.initialMenuPanel.SetActive(false);
						tileEntity.buildBorderMenuPanel.SetActive(true);
						tileEntity.generalCancelButton.SetActive(true);
					}
					break;
				default:
					break;
			}
		}
	}
}

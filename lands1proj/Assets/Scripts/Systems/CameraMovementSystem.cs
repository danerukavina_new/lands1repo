﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementSystem : BaseSystem
{
	public Camera mainCamera;
	public GameStateEntity gameStateEntity;
	public InputStateEntity inputStateEntity;
	public RitualStateEntity ritualStateEntity; // TODO: Not used at time of first commit

	Vector3 tapPosition;
	Vector2 localPoint;
	Vector3 worldDelta;

	void InputStartLogic()
	{
		switch (inputStateEntity.cameraControlType)
		{
			case InputStateEntity.CameraControlType.ClickAndDrag:
				// Do nothing
				break;
			default:
				Debug.Log("Unsupported camera control type in input start logic.");
				break;
		}
	}

	void MoveByClickAndDrag()
	{
		worldDelta = mainCamera.ScreenToWorldPoint(inputStateEntity.tapCurrentScreenPosition) - mainCamera.ScreenToWorldPoint(inputStateEntity.tapStartScreenPosition);

		if (worldDelta.sqrMagnitude > float.Epsilon)
			mainCamera.transform.position = inputStateEntity.tapStartCameraWorldPosition - worldDelta;
	}

	static void RubberBandingCoefficient(Vector3 normalizedTapCurrentScreenPosition, out Vector3 direction, out float power)
	{
		direction = Vector3.Normalize(normalizedTapCurrentScreenPosition - ValueHolder.RITUAL_CAMERA_NO_RUBBER_BANDING_BOUNDS.ClosestPoint(normalizedTapCurrentScreenPosition));
		power = ValueHolder.RITUAL_CAMERA_RUBBER_BANDING_SPEED_COEFFICIENT * Mathf.Sqrt(ValueHolder.RITUAL_CAMERA_NO_RUBBER_BANDING_BOUNDS.SqrDistance(normalizedTapCurrentScreenPosition));
	}

	void InputUpdateLogic()
	{
		switch (inputStateEntity.tapInputState)
		{
			case InputStateEntity.TapInputState.None:
				throw new System.Exception("Trying to do input update logic when the input state is none.");
			case InputStateEntity.TapInputState.FreeSpaceClickAndDragToMoveCamera:
				switch (inputStateEntity.cameraControlType)
				{
					case InputStateEntity.CameraControlType.ClickAndDrag:
						MoveByClickAndDrag();
						break;
					default:
						Debug.Log("Unsupported camera control type in input start logic.");
						break;
				}
				break;
			case InputStateEntity.TapInputState.RitualInputBasedOnRitualInputType:
				switch (inputStateEntity.ritualInputType)
				{
					case InputStateEntity.RitualInputType.TapNextNode:
						break;
					case InputStateEntity.RitualInputType.SwipeToSelectWithRecenteringWhenLettingGo:
						break;
					case InputStateEntity.RitualInputType.SwipeToSelectWithRubberBandingAtEdges:
						Vector3 normalizedTapCurrentScreenPosition = new Vector3(Mathf.Clamp01(inputStateEntity.tapCurrentScreenPosition.x / Screen.width), Mathf.Clamp01(inputStateEntity.tapCurrentScreenPosition.y / Screen.height), 0f);

						RubberBandingCoefficient(normalizedTapCurrentScreenPosition, out Vector3 direction, out float power);

						if (power > float.Epsilon)
							mainCamera.transform.position += direction * power * Time.deltaTime; // TODO: It makes sense to use the real delta time here, perhaps? But it should still be dependency injected via gameStateEntity I think...
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}

	void NoInputLogic()
	{
		switch (inputStateEntity.cameraControlType)
		{
			case InputStateEntity.CameraControlType.ClickAndDrag:
				break;
			default:
				Debug.Log("Unsupported camera control type in input start logic.");
				break;
		}
	}

	public override void SystemUpdate()
	{
		if (
			inputStateEntity.tapInputState == InputStateEntity.TapInputState.FreeSpaceClickAndDragToMoveCamera
			|| inputStateEntity.tapInputState == InputStateEntity.TapInputState.RitualInputBasedOnRitualInputType)
		{
			if (inputStateEntity.inputStateMayCauseStartOfCameraMovement)
			{
				InputStartLogic();
			}

			InputUpdateLogic();
		}
		else
			NoInputLogic();
	}
}

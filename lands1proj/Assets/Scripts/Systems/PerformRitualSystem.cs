﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerformRitualSystem : BaseSystem
{
	public GameStateEntity gameStateEntity;
	public RitualStateEntity ritualStateEntity;
	public MenuManagerEntity menuManagerEntity;

	//UndoableList<RitualStepEntity> ritualStepEntities;
	//RitualStepEntity ritualStepEntity;
	//RitualPickupEntity ritualPickupEntity;
	//RitualPickupType ritualPickupType;
	//BuildingBaseType buildingBaseType;
	//int ritualSteps;
	//int ritualStepDiscuounts;
	//int largestSetSize;
	//int differentBuildings;
	//Dictionary<RitualPickupType, CountAndStrength> ritualPickupCountsAndStrengthsByRitualPickupType;
	//Dictionary<BuildingBaseType, int> buildingCountsByBaseBuilding; // TODO: Game design, we define different buildings based on the base type. I think this makes sense. But think on it.
	//CountAndStrength countAndStrengthForType;
	//RitualPickupMenuPickupRowEntity ritualPickupMenuPickupRowEntity;

	//public UpdateRitualStatsAndRitualPickupRowsSystem()
	//{
	//	ritualPickupCountsAndStrengthsByRitualPickupType = new Dictionary<RitualPickupType, CountAndStrength>(System.Enum.GetNames(typeof(RitualPickupType)).Length);
	//	buildingCountsByBaseBuilding = new Dictionary<BuildingBaseType, int>(System.Enum.GetNames(typeof(BuildingBaseType)).Length);
	//}	

	public override void SystemUpdate()
	{
		if (ritualStateEntity.performRitual)
		{
			ritualStateEntity.performRitual = false;

			// TODO: It's always concievable that in the same or next frame when a player tries to perform a ritual, train, upgrade, etc. that they run out of resources in some execution
			// order of systems. Instead of an exception, a Modal should be given to the player, implying they indeed clicked on a button that was shown to them as enabled but that they ran out of resources...
			// or perhaps the button should just disappear (and the player may think it was gone as they clicked). But if it disappears, should this system just silently fail then?
			// And what if it reappears in the next frame... due to some oscillation of losses and gains? The modal is better in this case...
			// ... perhaps even the modal and everything involves a retry mechanism... and if it fails for a few frames the player gets a "toasty" notification that they don't have the resources, to make it less disruptive?
			if (!ritualStateEntity.playerCanPerformRitual)
				throw new System.Exception("Attempting to perform ritual even though it can't be (spirit cost, number of steps), should be impossible.");

			ritualStateEntity.previouslyPerformedRitualTime = gameStateEntity.gameTime;

			gameStateEntity.playerResourceGainsThisUpdateComponent.CopyValues(ritualStateEntity.ritualEntity.ritualResourceGains); // TODO: This should be a copy

			gameStateEntity.playerResourceSetAmountComponent.spirit -= ritualStateEntity.ritualEntity.ritualSpiritCost;

			LogicHelpers.SetPlayerCanPerformRitual(ritualStateEntity, gameStateEntity);

			// TODO: Lots of code to add when the ritual is performed... depends on UX

			// Close the ritual details screen?
		}
	}
}

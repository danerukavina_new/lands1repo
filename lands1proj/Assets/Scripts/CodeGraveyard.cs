﻿// TODO: Replace by making scriptable objects know the base type
//public static class BuildingTypeMethods
//{
//	public static BuildingBaseType BuildingBaseTypeFromAdvancedType(BuildingAdvancedType buildingAdvancedType)
//	{
//		switch (buildingAdvancedType)
//		{
//			case BuildingAdvancedType.Farm:
//				return BuildingBaseType.Farm;
//			case BuildingAdvancedType.LumberMill:
//				return BuildingBaseType.LumberMill;
//			case BuildingAdvancedType.TownHall:
//				return BuildingBaseType.TownHall;
//			case BuildingAdvancedType.Quarry:
//				return BuildingBaseType.Quarry;
//			case BuildingAdvancedType.Mine:
//				return BuildingBaseType.Mine;
//			case BuildingAdvancedType.Well:
//				return BuildingBaseType.Well;
//			case BuildingAdvancedType.Shrine:
//				return BuildingBaseType.Shrine;
//			case BuildingAdvancedType.Windmill:
//				return BuildingBaseType.Windmill;
//			case BuildingAdvancedType.Guild:
//				return BuildingBaseType.Guild;
//			case BuildingAdvancedType.Tavern:
//				return BuildingBaseType.Tavern;
//			case BuildingAdvancedType.Temple:
//				return BuildingBaseType.Temple;
//			case BuildingAdvancedType.Laboratory:
//				return BuildingBaseType.Laboratory;
//			case BuildingAdvancedType.Blacksmith:
//				return BuildingBaseType.Blacksmith;
//			case BuildingAdvancedType.Inn:
//				return BuildingBaseType.Inn;
//			case BuildingAdvancedType.TrainingGrounds:
//				return BuildingBaseType.TrainingGrounds;
//			case BuildingAdvancedType.Dungeon:
//				return BuildingBaseType.Dungeon;
//			case BuildingAdvancedType.TradeRoute:
//				return BuildingBaseType.TradeRoute;
//			case BuildingAdvancedType.Shipyard:
//				return BuildingBaseType.Shipyard;
//			case BuildingAdvancedType.SpecialCaptureInProgress:
//				return BuildingBaseType.SpecialCaptureInProgress;
//			case BuildingAdvancedType.SpecialSeaBorder:
//				return BuildingBaseType.SpecialSeaBorder;
//			case BuildingAdvancedType.SpecialMountainPassBorder:
//				return BuildingBaseType.SpecialMountainBorder;
//			case BuildingAdvancedType.SpecialMountainBorder:
//				return BuildingBaseType.SpecialMountainBorder;
//			case BuildingAdvancedType.Farm2:
//				return BuildingBaseType.Farm;
//			default:
//				throw new System.Exception("Unexpected building advanced type");
//		}
//	}
//}


// NOTE: This was killed when I decided I don't need it to be cyclic - because the max cycle length I needed was going to be >= the max number of elements I would ever store (in other words, I would want the number of ritual undo's to be >= the max number of ritual steps... allowing me to not do the cycling)
//using System.Collections;
//using System.Collections.Generic;

//public class UndoableCyclicList<T> where T : new()
//{
//	T[] underlyingArray;
//	int startIndex;
//	int currentIndexPlusOne; // TODO: Replace this with # of elements... it would be much simpler
//	int cycleCapacity; // TODO: Get rid of this, just use the .Length of the underlyingArray
//	int numberOfRedosPossible;

//	public UndoableCyclicList(int cycleCapacity)
//	{
//		this.cycleCapacity = cycleCapacity;
//		underlyingArray = new T[this.cycleCapacity];

//		for (int i = 0; i < this.underlyingArray.Length; i++)
//		{
//			underlyingArray[i] = new T();
//		}

//		Clear();
//	}

//	// start of array 0
//	// currentIndexPlusOne 4
//	// startIndex 5
//	// cycleCapacity 7
//	// ==> 7 - 5 => 2 items at the end of the list. 4 => 4 items at the start of the list. => 6 items. Correct, only index "4" is missing
//	// start of array 0
//	// currentIndexPlusOne 4
//	// startIndex 0
//	// cycleCapacity 7
//	// ==> 4 - 0 => 4. Correct
//	// start of array 0
//	// currentIndexPlusOne 2
//	// startIndex 2
//	// cycleCapacity 7
//	// ==> 2 - 2 => 0. Correct
//	// start of array 0
//	// currentIndexPlusOne 2
//	// startIndex 2
//	// cycleCapacity 7
//	// ==> 2 - 2 => 0. Correct

//	public int Count
//	{
//		get
//		{
//			if (currentIndexPlusOne >= startIndex)
//				return currentIndexPlusOne - startIndex;
//			return cycleCapacity - startIndex + currentIndexPlusOne;
//		}
//	}

//	void CheckAndIterateStartIndexIfOutOfCapacity()
//	{
//		if (currentIndexPlusOne - 1 >= startIndex)
//			startIndex = currentIndexPlusOne;
//	}

//	bool IndexIsValid(int i)
//	{
//		return (i >= startIndex && (currentIndexPlusOne + numberOfRedosPossible > i || (currentIndexPlusOne + numberOfRedosPossible <= startIndex && i < cycleCapacity)));
//	}

//	// TODO: Look at the two TODOs for get and set. Ideally this class wouldn't have either function. Perhaps get if the UI needs to preview something from the past.
//	public T this[int i]
//	{
//		// TODO: Make this Get private if it's not needed
//		get
//		{
//			// TODO: Not sure about hte condition currentIndexPlusOne + numberOfRedosPossible <= startIndex ... seems like the numberOfRedoesPossible would never affect the truth of the statement if redo logic is being tracked correctly.
//			if (IndexIsValid(i))
//				return underlyingArray[i];
//			throw new System.Exception("Attempting to access an element of the UndoableCyclicList that's not currently defined.");
//		}
//		// TODO: Figure out a way to pre-initialize arrays in this class without opening up access to the array elements. Heck
//		set
//		{
//			// if (IndexIsValid(i))
//			underlyingArray[i] = value; // NOTE: This is and should only be used for pre-initializing the arrays.
//										// throw new System.Exception("Attempting to access an element of the UndoableCyclicList that's not currently defined.");
//		}
//	}

//	//// TODO: This is wrong... the way we want to use the array is not to initialize new items, but to set the existing items
//	//public void Add(T item)
//	//{
//	//	if (Count == 0)
//	//	{
//	//		startIndex = 0;
//	//		currentIndexPlusOne = 1;
//	//	}

//	//	if (currentIndexPlusOne > cycleCapacity)
//	//		throw new System.Exception("UndoableCyclicList has exceeded the cycle capacity somehow, should be impossible.");
//	//	if (currentIndexPlusOne == cycleCapacity)
//	//		currentIndexPlusOne = 1; // TODO: It's possible for startIndex to be at cycle capacity if cycleCapacity = 1... this seems odd. It should still say "0"
//	//	else
//	//		currentIndexPlusOne++;

//	//	CheckAndIterateStartIndexIfOutOfCapacity();

//	//	numberOfRedosPossible = 0;

//	//	underlyingArray[currentIndexPlusOne - 1] = item;
//	//}

//	// TODO: This is so weird... but using "Add" just doesn't work, since it requires more allocation
//	// Presumably this should be an interface thing on the typical user of this... RitualEntity
//	public void PrepareNextIndexSortOfLikeAdd()
//	{
//		if (Count == 0)
//		{
//			startIndex = 0;
//			currentIndexPlusOne = 1;
//		}

//		if (currentIndexPlusOne > cycleCapacity)
//			throw new System.Exception("UndoableCyclicList has exceeded the cycle capacity somehow, should be impossible.");
//		if (currentIndexPlusOne == cycleCapacity)
//			currentIndexPlusOne = 1; // TODO: It's possible for startIndex to be at cycle capacity if cycleCapacity = 1... this seems odd. It should still say "0"
//		else
//			currentIndexPlusOne++;

//		CheckAndIterateStartIndexIfOutOfCapacity();

//		numberOfRedosPossible = 0;

//		// NOTE: Typically here we would "set" the next element with "underlyingArray[currentIndexPlusOne - 1] = whatever;" - but instead the user of this array will do that manually.
//	}

//	public T GetCurrent()
//	{
//		return underlyingArray[currentIndexPlusOne - 1];
//	}

//	public void Clear()
//	{
//		startIndex = 0;
//		currentIndexPlusOne = 1;
//		numberOfRedosPossible = 0;
//	}

//	public void Undo()
//	{
//		if (Count == 0)
//			throw new System.Exception("The UndoableCyclicList cannot perform an Undo, because it's empty. Check Count() first.");

//		currentIndexPlusOne--;

//		if (currentIndexPlusOne < 1)
//			currentIndexPlusOne = cycleCapacity;

//		numberOfRedosPossible++;
//	}

//	public int NumberOfRedosPossible()
//	{
//		return numberOfRedosPossible;
//	}

//	public void Redo()
//	{
//		if (numberOfRedosPossible <= 0)
//			throw new System.Exception("The UndoableCyclicList cannot perform a Redo, because there haven't been enough Undos since the last Add or initialization. Check NumberOfRedosPossible() first.");

//		currentIndexPlusOne++;

//		// NOTE: There's no need to "CheckAndIterateStartIndexIfOutOfCapacity();" since undoing doesn't move the startIndex around, so it should never be run into
//		// So instead we check this
//		if (currentIndexPlusOne - 1 >= startIndex)
//			throw new System.Exception("The UndoableCyclicList somehow performed a Redo that exceeded the startIndex, which should be impossible since Undo(s) cannot.");

//		numberOfRedosPossible--;
//	}

//	public override string ToString()
//	{
//		string ret = "Indexes from start to current: ";

//		if (currentIndexPlusOne - 1 >= startIndex)
//			for (int i = startIndex; i < currentIndexPlusOne; i++)
//				ret += i + ", ";
//		else
//		{
//			for (int i = startIndex; i < cycleCapacity; i++)
//				ret += i + ", ";

//			for (int i = 0; i < currentIndexPlusOne; i++)
//				ret += i + ", ";
//		}

//		ret += " ; redoable elements: " + numberOfRedosPossible;

//		return ret;
//	}
//}

//static bool NextRitualStepIsAnAllowedBacktrack(Vector2Int candidateTileForNextPlannedRitualStepEntityCoord, UndoableList<RitualStepEntity> ritualStepEntities, RitualType ritualType, bool initialTapThisUpdate, out int backtrackCount)
//{
//	backtrackCount = 0;

//	if (ritualStepEntities.Count <= ValueHolder.FIRST_RITUAL_STEP_CANT_BE_BACKTRACK_UNDONE)
//	{
//		return false;
//	}

//	switch (ritualType)
//	{
//		case RitualType.BasicRitual:

//			if (IsTileAlreadyRitualStep(candidateTileForNextPlannedRitualStepEntityCoord, ritualStepEntities, out backtrackCount, skipCurrentStep: true))
//			{
//				if (
//					initialTapThisUpdate
//					&& ValueHolder.ALLOW_FAR_RITUAL_BACKTRACK_UNDO_ON_INITIAL_TAP
//					)
//					return true;

//				if (
//					backtrackCount <= ValueHolder.MAX_RITUAL_BACKTRACK_COUNT
//					&& (ritualStepEntities.GetCurrent().tileEntity.coord - candidateTileForNextPlannedRitualStepEntityCoord).sqrMagnitude <= ValueHolder.MAX_RITUAL_BACKTRACK_SQR_MAGNITUDE
//					)
//					return true;
//			}

//			return false;
//		case RitualType.WaterRitual:
//		case RitualType.AirRitual:
//		case RitualType.MetalRitual:
//			throw new System.Exception("Ritual type not yet implemented for checking validity of next step.");
//		default:
//			throw new System.Exception("Unexpected ritual type when checking validity of next step.");
//	}
//}

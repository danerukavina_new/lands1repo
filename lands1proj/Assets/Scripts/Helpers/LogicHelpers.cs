﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LogicHelpers
{
	// TODO: This counts on the BuildingBaseType having both unique values and unique names. The way this field is used is consistent with this - as unique keys to a dictionary - but the dictionaries might never throw an error because of this, if the values become non-unique.
	public static readonly List<BuildingBaseType> baseBuildingTypeKeys = new List<BuildingBaseType>((BuildingBaseType[])System.Enum.GetValues(typeof(BuildingBaseType)));

	public static Vector3 CoordToPosition(Vector2Int coord)
	{
		return new Vector3((float)coord.x, (float)coord.y, 0f);
	}

	public static bool CoordIsAdjacent(Vector2Int coord1, Vector2Int coord2)
	{
		return (coord1 - coord2).sqrMagnitude <= 1;
	}

	// TODO: Consider how to unify this with the other EntityRemover... sadly where T : MonoBheavior, GameObject is not working
	public static void EntityRemover(GameObject[,] entityArray)
	{
		GameObject gameObject;

		for (int x = entityArray.GetLength(0) - 1; x >= 0; x--)
			for (int y = entityArray.GetLength(1) - 1; y >= 0; y--)
			{
				gameObject = entityArray[x, y];
				if (gameObject != null)
				{
					GameObject.Destroy(gameObject);
					entityArray[x, y] = null;
				}
			}
	}

	public static void EntityRemover<T>(T[,] entityArray) where T : MonoBehaviour
	{
		T monoBehavior;

		for (int x = entityArray.GetLength(0) - 1; x >= 0; x--)
			for (int y = entityArray.GetLength(1) - 1; y >= 0; y--)
			{
				monoBehavior = entityArray[x, y];
				if (monoBehavior != null)
				{
					GameObject.Destroy(monoBehavior.gameObject);
					entityArray[x, y] = null;
				}
			}
	}

	public static void BoolClearer(bool[,] boolArray)
	{
		for (int x = boolArray.GetLength(0) - 1; x >= 0; x--)
			for (int y = boolArray.GetLength(1) - 1; y >= 0; y--)
				boolArray[x, y] = false;
	}

	public static void LoopControlCheck(int loopControl, string context)
	{
		if (loopControl == ValueHolder.MAX_LOOP_CONTROL)
		{
			Debug.Log("Loop Control limit hit in training system.");
			// throw new System.Exception("Loop Control limit hit in training system.");
		}
	}

	public static void EntityRemover<T>(List<T> entities) where T : MonoBehaviour
	{
		foreach (var entity in entities)
		{
			GameObject.Destroy(entity.gameObject);
		}

		entities.Clear();
	}

	static ResourceSetComponent TrainingCost(BuildingEntity buildingEntity, GlobalBuffDataEntity globalBuffDataEntity, LevelEntity levelEntity)
	{
		ResourceSetComponent trainingCost = buildingEntity.buildingStatsScriptableObject.baseTrainingCost
			* (double)Mathf.Pow((float)ValueHolder.TRAINING_COST_INCREASE_FACTOR, buildingEntity.trainedWorkers)
			* (1f / (1f + buildingEntity.trainingCheapnessMultiplier));

		if (buildingEntity.buildingStatsScriptableObject.isFarm)
			trainingCost *= 1d / System.Math.Pow(1d + globalBuffDataEntity.playerGlobalBuffSetComponent.farmWorkerCheapnessPerFarmMultiplier, levelEntity.buildingCountsByBaseBuilding[BuildingBaseType.Farm]);

		return trainingCost;
	}

	static float TrainingTime(BuildingEntity buildingEntity, GlobalBuffDataEntity globalBuffDataEntity)
	{
		float trainingSpeed = buildingEntity.buildingStatsScriptableObject.baseTrainingTime * Mathf.Pow((float)ValueHolder.TRAINING_COST_INCREASE_FACTOR, buildingEntity.trainedWorkers) / (1f + globalBuffDataEntity.playerGlobalBuffSetComponent.anyTrainingSpeedMultiplier);

		if (buildingEntity.buildingStatsScriptableObject.isLumberMill)
			trainingSpeed /= (1f + globalBuffDataEntity.playerGlobalBuffSetComponent.lumberMillTrainingSpeedMultiplier);

		if (buildingEntity.buildingStatsScriptableObject.isTownHall)
			trainingSpeed /= (1f + globalBuffDataEntity.playerGlobalBuffSetComponent.townHallTrainingSpeedMultiplier);

		if (buildingEntity.buildingStatsScriptableObject.isQuarry)
			trainingSpeed /= (1f + globalBuffDataEntity.playerGlobalBuffSetComponent.quarryTrainingSpeedMultiplier);

		// TODO: Shouldn't be taking game state entity, and instead the global stat effects component, perhaps
		return trainingSpeed;
	}

	public static void SetTrainingCosts(BuildingEntity buildingEntity, GlobalBuffDataEntity globalBuffDataEntity, LevelEntity levelEntity)
	{
		// TODO: This is pretty silly, so think about it.
		switch (buildingEntity.buildingAssetsDataScriptableObject.buildingAdvancedType)
		{
			case BuildingAdvancedType.SpecialCaptureInProgress:
			case BuildingAdvancedType.SpecialSeaBorder:
			case BuildingAdvancedType.SpecialMountainPassBorder:
			case BuildingAdvancedType.SpecialMountainBorder:
				throw new System.Exception("Special advanced building trying to assess training cost.");
			default:
				break;
		}

		buildingEntity.finalTrainingTime = TrainingTime(buildingEntity, globalBuffDataEntity) / (1f + buildingEntity.trainingSpeedMultiplier);

		buildingEntity.finalTrainingCost = TrainingCost(buildingEntity, globalBuffDataEntity, levelEntity);
	}

	public static void SetResearchCosts(MythButtonEntity mythButtonEntity, GameStateEntity gameStateEntity)
	{
		mythButtonEntity.researchableStateComponent.baseResearchTime = ValueHolder.MYTH_RESEARCH_TIME;
		mythButtonEntity.baseResearchCost = mythButtonEntity.mythAssetsDataScriptableObject.mythCost.amount;
	}

	static bool BuildingCanTrain(BuildingEntity buildingEntity, GameStateEntity gameStateEntity)
	{
		// NOTE: On 10/27/2021 this was not triggering
		if (buildingEntity.isBuildingOrTierUpgrading)
			throw new System.Exception(@"BuildingCanTrain check is called even during tier upgrading, uncomment the ""&& !buildingEntity.isBuildingOrTierUpgrading"" line.");

		return gameStateEntity.playerResourceSetAmountComponent.EntirelyGreaterThanOrEqualTo(buildingEntity.finalTrainingCost)
			&& !buildingEntity.isTraining // NOTE: Otherwise, you might get buildings entering into canTrain as soon as there's enough resources and the player/auto train can reset their training progress... although there are other checks in place
										  // && !buildingEntity.isBuildingOrTierUpgrading // NOTE: Determine if this check is needed based on the exception above
			;
	}

	static bool BuildingCanTierUpgrade(BuildingEntity buildingEntity, GameStateEntity gameStateEntity, int upgradePathIndex)
	{
		// NOTE: On 10/27/2021 this was not triggering
		if (buildingEntity.isTraining)
			throw new System.Exception(@"BuildingCanTierUpgrade check is called even during training, uncomment the ""&& !buildingEntity.isTraining"" line.");
		return gameStateEntity.playerResourceSetAmountComponent.EntirelyGreaterThanOrEqualTo(buildingEntity.finalBuildOrTierUpgradeCosts[upgradePathIndex])
			&& !buildingEntity.isBuildingOrTierUpgrading // NOTE: Otherwise, you might get buildings entering into canStartTierUpgrade as soon as there's enough resources and the player/auto train can reset their upgrading progress... although there are other checks in place
														 // && !buildingEntity.isTraining // NOTE: Determine if this check is needed based on the exception above
			;
	}

	public static bool BuildingIsAtMaxTier(BuildingEntity buildingEntity)
	{
		if (buildingEntity.tierIndex > ValueHolder.buildingTierLevels.Count)
			throw new System.Exception("Building somehow higher than max tier, when checking if it's at max tier.");

		return buildingEntity.tierIndex == ValueHolder.buildingTierLevels.Count;
	}

	static int MaxTrainedWorkersForTierFromCurrentWorkers(BuildingEntity buildingEntity)
	{
		if (BuildingIsAtMaxTier(buildingEntity))
			return int.MaxValue;

		return ValueHolder.buildingTierLevels[buildingEntity.tierIndex];
	}

	public static void SetBuildingIsAtMaxTrainedWorkersForTierAndThusMayTierUpgrade(BuildingEntity buildingEntity, GameStateEntity gameStateEntity)
	{
		int maxTrainedWorkersForTier = MaxTrainedWorkersForTierFromCurrentWorkers(buildingEntity);

		if (buildingEntity.trainedWorkers > maxTrainedWorkersForTier)
			throw new System.Exception("Building has more workers than the max for its tier."); // TODO: Consider simply... setting it to the max...
		else if (buildingEntity.trainedWorkers == maxTrainedWorkersForTier)
		{
			buildingEntity.isAtMaxTrainedWorkersForTierAndThusMayTierUpgrade = true;
			// Debug.Log("At max workers: " + maxWorkersForTier);
		}
		else
		{
			buildingEntity.isAtMaxTrainedWorkersForTierAndThusMayTierUpgrade = false;
			// Debug.Log("Not max workers: " + buildingEntity.trainedWorkers + " vs " + maxWorkersForTier);
		}
	}

	static int MaxExtraWorkersFromTierAndBuffs(BuildingEntity buildingEntity)
	{
		int tierBasedMaxValue;

		if (BuildingIsAtMaxTier(buildingEntity))
			tierBasedMaxValue = ValueHolder.BASE_MAX_EXTRA_WORKERS_AT_MAX_TIER; // NOTE: Design - see the note on ValueHolder.BASE_MAX_EXTRA_WORKERS_AT_MAX_TIER, for now it's ok to used a fixed value
		else
			tierBasedMaxValue = ValueHolder.buildingTierLevels[buildingEntity.tierIndex];

		return tierBasedMaxValue + Mathf.FloorToInt((float)buildingEntity.additionalExtraWorkerLimit);
	}

	public static void SetBuildingMaxExtraWorkersAndIsAtMaxExtraWorkers(BuildingEntity buildingEntity, GameStateEntity gameStateEntity)
	{
		buildingEntity.maxExtraWorkers = MaxExtraWorkersFromTierAndBuffs(buildingEntity);

		// NOTE: By design, it's ok for buildings to exceed max extra workers. Specifically this happens when a building loses a additional extra worker limit buff.
		if (buildingEntity.extraWorkers >= buildingEntity.maxExtraWorkers)
		{
			buildingEntity.isAtMaxExtraWorkers = true;
			// Debug.Log("At max extra workers: " + buildingEntity.maxExtraWorkers);
		}
		else
		{
			buildingEntity.isAtMaxExtraWorkers = false;
			// Debug.Log("Not max extra workers: " + buildingEntity.extraWorkers + " vs " + buildingEntity.maxExtraWorkers);
		}
	}

	public static void SetBuildingCanTrainAndMaxTrainedWorkersForTier(BuildingEntity buildingEntity, GameStateEntity gameStateEntity)
	{
		// TODO: This is pretty silly, so think about it.
		switch (buildingEntity.buildingAssetsDataScriptableObject.buildingAdvancedType)
		{
			case BuildingAdvancedType.SpecialCaptureInProgress:
			case BuildingAdvancedType.SpecialSeaBorder:
			case BuildingAdvancedType.SpecialMountainPassBorder:
			case BuildingAdvancedType.SpecialMountainBorder:
				throw new System.Exception("Special advanced building trying to assess training cost.");
			default:
				break;
		}

		SetBuildingIsAtMaxTrainedWorkersForTierAndThusMayTierUpgrade(buildingEntity, gameStateEntity);

		if (buildingEntity.isAtMaxTrainedWorkersForTierAndThusMayTierUpgrade)
		{
			buildingEntity.canTrain = false;
			return;
		}

		buildingEntity.canTrain = BuildingCanTrain(buildingEntity, gameStateEntity);
	}

	public static void SetBuildingCanStartTierUpgradeAndMaxTrainedWorkersForTierAndCosts(
		BuildingEntity buildingEntity,
		GameStateEntity gameStateEntity,
		Dictionary<BuildingAdvancedType, BuildingStatsScriptableObject> buildingStatsScriptableObjects,
		GlobalBuffDataEntity globalBuffDataEntity
		)
	{
		SetBuildingIsAtMaxTrainedWorkersForTierAndThusMayTierUpgrade(buildingEntity, gameStateEntity);

		if (BuildingIsAtMaxTier(buildingEntity)
			|| !ValueHolder.buildingTierMap.ContainsKey(buildingEntity.buildingStatsScriptableObject.impliedBuildingBaseType))
		{
			buildingEntity.anyTierUpgradeIsAvailable = false;

			for (int tierUpgradePathIndex = 0; tierUpgradePathIndex < ValueHolder.TIER_WITH_LOCKED_UPGRADE_PATH; tierUpgradePathIndex++)
			{
				buildingEntity.canStartNextBuildingTierUpgrades[tierUpgradePathIndex] = false;
			}

			return;
		}

		buildingEntity.nextBuildingTiers = ValueHolder.buildingTierMap[buildingEntity.buildingStatsScriptableObject.impliedBuildingBaseType][buildingEntity.tierIndex];

		if (!buildingEntity.isAtMaxTrainedWorkersForTierAndThusMayTierUpgrade)
		{
			buildingEntity.anyTierUpgradeIsAvailable = false;

			for (int tierUpgradePathIndex = 0; tierUpgradePathIndex < buildingEntity.nextBuildingTiers.Count; tierUpgradePathIndex++)
			{
				buildingEntity.canStartNextBuildingTierUpgrades[tierUpgradePathIndex] = false;
			}

			return;
		}

		buildingEntity.anyTierUpgradeIsAvailable = (buildingEntity.nextBuildingTiers.Count > 0);

		// Loop through the buildingEntity.nextBuildingTierArray and set the flags for upgradeable or not
		for (int tierUpgradePathIndex = 0; tierUpgradePathIndex < buildingEntity.nextBuildingTiers.Count; tierUpgradePathIndex++)
		{
			if (buildingEntity.nextBuildingTiers[tierUpgradePathIndex].HasValue)
			{
				BuildingAdvancedType temp = buildingEntity.nextBuildingTiers[tierUpgradePathIndex].Value;

				if (!buildingStatsScriptableObjects.TryGetValue(temp, out BuildingStatsScriptableObject buildingStatsScriptableObject))
					throw new System.Exception("The building tier list has enums that are not yet implemented");

				// BuildingStatsScriptableObject buildingStatsScriptableObject = buildingStatsScriptableObjects[temp];

				buildingEntity.finalBuildOrTierUpgradeTimes[tierUpgradePathIndex] = buildingStatsScriptableObject.baseBuildOrTierUpgradeTime;
				buildingEntity.finalBuildOrTierUpgradeCosts[tierUpgradePathIndex] = BuildingCostWithGlobalBuffReductions(buildingStatsScriptableObject, globalBuffDataEntity);
				buildingEntity.canStartNextBuildingTierUpgrades[tierUpgradePathIndex] = BuildingCanTierUpgrade(buildingEntity, gameStateEntity, tierUpgradePathIndex);
			}
			else
			{
				buildingEntity.finalBuildOrTierUpgradeTimes[tierUpgradePathIndex] = -1f;
				buildingEntity.finalBuildOrTierUpgradeCosts[tierUpgradePathIndex] = null;
				buildingEntity.canStartNextBuildingTierUpgrades[tierUpgradePathIndex] = false;
			}
		}

		if (buildingEntity.tierIndex >= ValueHolder.TIER_WITH_LOCKED_UPGRADE_PATH)
		{
			buildingEntity.canAlsoAutoTierUpgrade = true;
			buildingEntity.finalTierUpgradePathIndex = buildingEntity.chosenBuildingTierUpgradePathIndex;
		}
		else
		{
			buildingEntity.canAlsoAutoTierUpgrade = false;
			buildingEntity.finalTierUpgradePathIndex = buildingEntity.startBuildingTierUpgradePathIndex; // TODO: Will this always be set? What if it isn't, is it creating junk data?
		}
	}

	public static double ResultingExtraWorkersWithRounding(double initialExtraWorkers, double additionalExtraWorkers, int maxExtraWorkers)
	{
		// NOTE: By design, buildings that go over the extra worker limit, perhaps by losing a buff, keep their extra workers. Without this if, the "Min" below removes the extra workers when the buff goes away
		if (initialExtraWorkers > maxExtraWorkers)
			return initialExtraWorkers;

		return System.Math.Min(
			System.Math.Round(initialExtraWorkers + additionalExtraWorkers, 1), 
			maxExtraWorkers
			);
	}

	public static bool MaxMythsReached(GameStateEntity gameStateEntity, GlobalBuffDataEntity globalBuffDataEntity)
	{
		if (globalBuffDataEntity.firstBoughtMythDisplayEntity == null)
			return false;
		else if (globalBuffDataEntity.secondBoughtMythDisplayEntity == null && gameStateEntity.canHaveSecondMyth)
			return false;

		return true;
	}

	public static void SetMythButtonCanResearch(MythButtonEntity mythButtonEntity, GameStateEntity gameStateEntity, GlobalBuffDataEntity globalBuffDataEntity)
	{
		if (mythButtonEntity.researchableStateComponent.isResearched
			|| !(gameStateEntity.playerResourceSetAmountComponent.spirit >= mythButtonEntity.baseResearchCost))
		{
			mythButtonEntity.mythSlotForResearchOrCantResearch = MythSlotForResearchOrCantResearch.CantResearch;
			return;
		}

		if (!globalBuffDataEntity.FirstMythFilled())
			mythButtonEntity.mythSlotForResearchOrCantResearch = MythSlotForResearchOrCantResearch.First;
		else if (!globalBuffDataEntity.SecondMythFilled() && gameStateEntity.canHaveSecondMyth)
			mythButtonEntity.mythSlotForResearchOrCantResearch = MythSlotForResearchOrCantResearch.Second;
		else
			mythButtonEntity.mythSlotForResearchOrCantResearch = MythSlotForResearchOrCantResearch.CantResearch;
	}

	public static ResourceSetComponent BuildingCostWithGlobalBuffReductions(BuildingStatsScriptableObject buildingStatsScriptableObject, GlobalBuffDataEntity globalBuffDataEntity)
	{
		ResourceSetComponent requirementResourceSetComponent = buildingStatsScriptableObject.baseBuildOrTierUpgradeCost;

		bool ignoreWoodCost = buildingStatsScriptableObject.isLumberMill && globalBuffDataEntity.playerGlobalBuffSetComponent.removeLumberMillWoodCost;
		bool ignoreGoldCost = buildingStatsScriptableObject.isQuarry && globalBuffDataEntity.playerGlobalBuffSetComponent.removeQuarryGoldCost;

		return new ResourceSetComponent()
		{
			gold = ignoreGoldCost ? 0d : requirementResourceSetComponent.gold,
			food = requirementResourceSetComponent.food,
			wood = ignoreWoodCost ? 0d : requirementResourceSetComponent.wood,
			stone = requirementResourceSetComponent.stone,
			ore = requirementResourceSetComponent.ore,
			spirit = requirementResourceSetComponent.spirit,
		};
	}

	public static bool InitialBuildCostSatisfiedWithoutTileBuffsBeingConsidered(BuildingStatsScriptableObject buildingStatsScriptableObject, ResourceSetComponent resourceSetComponent, GlobalBuffDataEntity globalBuffDataEntity)
	{
		ResourceSetComponent requirementResourceSetComponent = BuildingCostWithGlobalBuffReductions(buildingStatsScriptableObject, globalBuffDataEntity);
		
		return resourceSetComponent.EntirelyGreaterThanOrEqualTo(requirementResourceSetComponent);
	}

	// NOTE: This can neither be an extension method, nor an extension constructor - neither is supported by C# for this scenario (Which includes initialization)
	public static void InitializeHashSetWithCapacity<T>(ref HashSet<T> hashSet, int capacity) where T : new()
	{
		hashSet = new HashSet<T>();

		for (int i = 0; i < capacity; i++)
		{
			hashSet.Add(new T());
		}

		hashSet.Clear();
	}

	static bool RitualIsOnCooldownFromBeingPerformed(float gameTime, float previouslyPerformedRitualTime, float performRitualCooldown)
	{
		return gameTime < previouslyPerformedRitualTime + performRitualCooldown;
	}

	static bool RitualIsPerformable(int ritualSteps, double ritualSpiritCost, double spiritAvailable, bool ritualIsOnCooldownFromBeingPerformed)
	{
		return ritualSpiritCost <= spiritAvailable
			&& ritualSteps >= ValueHolder.MINIMUM_RITUAL_STEPS
			&& !ritualIsOnCooldownFromBeingPerformed;
	}

	static bool ShouldShowRitualPerformButton(int ritualSteps)
	{
		return ritualSteps >= ValueHolder.MINIMUM_RITUAL_STEPS;
	}

	public static bool ShouldShowRitualPerformButton(RitualStateEntity ritualStateEntity)
	{
		return ShouldShowRitualPerformButton(ritualStateEntity.ritualEntity.ritualStepEntities.Count);
	}

	public static void SetPlayerCanPerformRitual(RitualStateEntity ritualStateEntity, GameStateEntity gameStateEntity)
	{
		ritualStateEntity.ritualIsOnCooldownFromBeingPerformed = RitualIsOnCooldownFromBeingPerformed(
			gameStateEntity.gameTime,
				ritualStateEntity.previouslyPerformedRitualTime,
				ritualStateEntity.basePerformRitualCooldown
				);

		ritualStateEntity.playerCanPerformRitual = RitualIsPerformable(
				ritualStateEntity.ritualEntity.ritualStepEntities.Count,
				ritualStateEntity.ritualEntity.ritualSpiritCost,
				gameStateEntity.playerResourceSetAmountComponent.spirit,
				ritualStateEntity.ritualIsOnCooldownFromBeingPerformed
				);
	}
}

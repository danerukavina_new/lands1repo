﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StringHelpers
{
	// TODO: Keep improving these suffixes, these are ridiculous
	static readonly List<string> thousandsSuffixes = new List<string>() { "", "k", "M", "B", "T", "Qa", "Qi", "Sx", "Sp", "Oc", "No", "De", "UnD", "DuD", "TrD", "QaD", "QiD", "SeD", "SpD", "OcD", "NoD", "Vi", "UnV" };
	static readonly double LOG10 = System.Math.Log(10);

	static int SpecialFloor(double d)
	{
		return (System.Math.Abs(d) - System.Math.Abs(System.Math.Floor(d)) >= 0.9999999999999991d) ? (int)System.Math.Ceiling(d) : (int)System.Math.Floor(d);
	}

	// TODO: Keep improving this function, aiming for the 999.9W format
	// NOTE: This function and related constants / functions inspired by this thread: https://www.reddit.com/r/incremental_games/comments/3tki2w/incremental_game_math_functions/
	// Also see these for reference:
	// https://www.reddit.com/r/incremental_games/comments/2cu61a/beautifying_numbers_rounding_and_separators/
	// http://almostidle.com/tutorial/beautifying-numbers
	// https://bl.ocks.org/MrHen/451232024824041701de
	public static string ToNiceString(this double d, int decimals = 1)
	{

		int decimalExponent = System.Math.Max(0, SpecialFloor(System.Math.Log(System.Math.Abs(d)) / LOG10));
		int thousandsOffset = (decimalExponent % 3 == 0) ? 2 : (((decimalExponent - 1) % 3 == 0) ? 1 : 0); // TODO: this is tricky to understand, make it more explicit.
		int thousandsExponent = decimalExponent / 3;

		if (System.Math.Abs(d) < 1000d)
		{
			return System.Math.Round(d, decimals).ToString();
		}
		else
		{
			int decimalExponentFlooredToThousands = thousandsExponent * 3 - thousandsOffset;
			double val = SpecialFloor(d / System.Math.Pow(10, decimalExponentFlooredToThousands)) / System.Math.Pow(10, thousandsOffset);
			return val.ToString() + thousandsSuffixes[thousandsExponent];
		}
	}

	public static string ToNicePercentageString(this double d, int decimals = 0)
	{
		return ToNiceString(100d * d, decimals) + StringConstants.PERCENT;
	}

	public static string RitualPickupValueText(RitualPickupType ritualPickupType, double ritualPickupStrength)
	{
		// TODO: Implement a case based on the ritual pickup type, to display different texts
		return StringHelpers.StringConstants.PLUS + ritualPickupStrength.ToNiceString();
		// return StringHelpers.StringConstants.PLUS + ritualPickupStrength.ToNicePercentageString();
	}

	public static class StringConstants
	{
		public const string SPACE = " ";
		public const string PLUS = "+";
		public const string COMMA = ",";
		public const string ELLIPSIS = "...";
		public const string FORWARD_SLASH = "/";
		public const string PERCENT = "%";
	}
}

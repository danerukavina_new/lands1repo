﻿using System.Collections;
using System.Collections.Generic;

public class UndoableList<T>
{
	// NOTE: Right now the owner of this undoable list will just 
	List<T> underlyingList; // TODO: Make this an object pooler friendly list, which is passed the object pooler at initialization and its operations like Clear, RemoveRange, etc. talk to the pooler.
	int count;
	int numberOfRedosPossible;

	public UndoableList(int capacity)
	{
		underlyingList = new List<T>(capacity);

		count = 0;
		numberOfRedosPossible = 0;
	}

	public int Count
	{
		get { return count; }
	}

	bool IndexIsValid(int i)
	{
		return i < (count + numberOfRedosPossible);
	}

	public List<T> GetUnderlyingListForSomeAllocationAvoidanceOperations()
	{
		return underlyingList;
	}

	public T this[int i]
	{
		get
		{
			if (IndexIsValid(i))
				return underlyingList[i];
			throw new System.Exception("Attempting to retrieve an item from an undoable list whose index is too high, and thus isn't valid.");
		}
	}

	// TODO: To call this in an object pooler friednly way, the caller should get the "item" from an Object pool
	// Then, the underlyingList, if implemented to work with the pooler, should handle releasing back to the object pool
	// AddAndGivePoolingControlFor(T item) ?
	public void Add(T item)
	{
		// NOTE: Often this is called with a 0, doing nothing presumably
		// TODO: The underlying list needs to be implemented to release to the pool
		// TODO: This is a problem... where do we do destructor cleanup on the ritual step entities? Here?
		underlyingList.RemoveRange(underlyingList.Count - numberOfRedosPossible, numberOfRedosPossible);

		numberOfRedosPossible = 0;

		underlyingList.Add(item);

		count++;
	}

	// TODO: Is this redundant with the GetEnumerator implementation? Probably it's fine
	public T GetCurrent()
	{
		if (Count == 0)
			throw new System.Exception("The UndoableCyclicList is empty and cannot GetCurrent.");

		// TODO: Why is this check here?
		if (count > underlyingList.Count)
			throw new System.Exception("The pre-allocated capacity of the undoable list has been exceeded, allocate more next time."); // TODO: Just make an "underlyingList.Add(new T());" statement here when you're done testing

		return underlyingList[count - 1];
	}

	public T GetPreviousToCurrent()
	{
		if (Count <= 1)
			throw new System.Exception("The UndoableCyclicList doesn't have 2 or more items and cannot GetPreviousToCurrent.");

		// TODO: Why is this check here?
		if (count > underlyingList.Count)
			throw new System.Exception("The pre-allocated capacity of the undoable list has been exceeded, allocate more next time."); // TODO: Just make an "underlyingList.Add(new T());" statement here when you're done testing

		return underlyingList[count - 2];
	}
	
	public void Clear()
	{
		// TODO: The underlying list needs to be implemented to release to the pool
		underlyingList.Clear();
		count = 0;
		numberOfRedosPossible = 0;
	}

	public void Undo()
	{
		if (Count == 0)
			throw new System.Exception("The UndoableCyclicList cannot perform an Undo, because it's empty. Check Count() first.");

		count--;
		numberOfRedosPossible++;
	}

	public int NumberOfRedosPossible
	{
		get { return numberOfRedosPossible; }
	}

	public void Redo()
	{
		if (numberOfRedosPossible <= 0)
			throw new System.Exception("The UndoableCyclicList cannot perform a Redo, because there haven't been enough Undos since the last Add or initialization. Check NumberOfRedosPossible() first.");

		count++;
		numberOfRedosPossible--;
	}

	public override string ToString()
	{
		string ret = "Indexes from 0 to current: ";

		for (int i = 0; i < count; i++)
			ret += underlyingList[i].ToString() + ";; ";

		ret += " ; # redoable elements: " + numberOfRedosPossible + " ; sanity check, must be equal: " + (count + numberOfRedosPossible) + " =?= " + underlyingList.Count;

		return ret;
	}
}

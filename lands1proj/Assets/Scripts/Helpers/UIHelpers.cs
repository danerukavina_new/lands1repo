﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class UIHelpers
{
	// TODO: always called with Null currently for additionalArtSprite, especially notice comment in class SpriteAndBoarderUIDataEntity that that last field is not used
	public static void SetSpriteAndBorderUIValues(SpriteAndBoarderUIDataEntity spriteAndBoarderUIDataEntity, Color mainColor, Color borderColor, Sprite additionalArtSprite)
	{
		spriteAndBoarderUIDataEntity.backgroundImage.color = mainColor;
		spriteAndBoarderUIDataEntity.borderSpriteImage.color = borderColor;

		if (additionalArtSprite != null)
		{
			spriteAndBoarderUIDataEntity.additionalArtImage.sprite = additionalArtSprite;
			spriteAndBoarderUIDataEntity.additionalArtImage.color = borderColor; // NOTE: This is for the new art style that uses the border color for the sprites

			spriteAndBoarderUIDataEntity.additionalArtImage.gameObject.SetActive(true);
		}
		else if (spriteAndBoarderUIDataEntity.additionalArtImage != null) // TODO: This comes from the "SpriteAndBackgroundPanel" prefab also using SpriteAndBorder... the former doesn't define an additional art image. Either split them up, define an additional art for it, or get rid of additional art across the board. Probably split up, since the progress bar usecase is a good reason to split up.
			spriteAndBoarderUIDataEntity.additionalArtImage.gameObject.SetActive(false);

		if (spriteAndBoarderUIDataEntity.progressBarImage != null) // TODO: This also comes from the "SpriteAndBackgroundPanel" prefab also using SpriteAndBorder... the former doesn't define a progress bar. Either split them up, define an additional art for it, or get rid of additional art across the board. Probably split up, since the progress bar usecase is a good reason to split up.
			spriteAndBoarderUIDataEntity.progressBarImage.color = borderColor;
	}

	// TODO: Unify this with the SetInitialBuildingAssetsSystem code section for setting some building assets
	public static void SetMythButtonUIValues(MythButtonEntity mythButtonEntity)
	{
		MythAssetsDataScriptableObject mythAssetsDataScriptableObject = mythButtonEntity.mythAssetsDataScriptableObject;
		Color textColor = mythAssetsDataScriptableObject.borderColor;

		// TODO: Change this, set it through the mythButtonEntity.researchableUIComponent.background... by wiring it up in the prefab and not sending null. See hwo the building upgrade button does it
		mythButtonEntity.researchableUIComponent.entityIcon.sprite = mythAssetsDataScriptableObject.mythIconSprite;
		UIHelpers.SetSpriteAndBorderUIValues(mythButtonEntity.researchableUIComponent.background, mythAssetsDataScriptableObject.mainColor, mythAssetsDataScriptableObject.borderColor, null);

		UIHelpers.SetSpriteAndBorderUIValues(mythButtonEntity.researchableUIComponent.researchButtonSpriteAndBoarderEntity, mythAssetsDataScriptableObject.mainColor, mythAssetsDataScriptableObject.borderColor, null);

		mythButtonEntity.researchableUIComponent.descriptionText.text = mythAssetsDataScriptableObject.mythDescription;
		mythButtonEntity.researchableUIComponent.descriptionText.color = textColor;

		mythButtonEntity.researchableUIComponent.costProgressText.text = string.Empty;
		mythButtonEntity.researchableUIComponent.costProgressText.color = textColor;

		mythButtonEntity.researchableUIComponent.researchButtonText.text = mythAssetsDataScriptableObject.mythName;
		mythButtonEntity.researchableUIComponent.researchButtonText.color = textColor;

		// NOTE: This code is here in anticipation of the enshrine system
		// TODO: Enshrined myths should have a different marking than just a mysterious pre-existing checkmark
		if (mythButtonEntity.researchableStateComponent.isResearched)
		{
			// TODO: Clearly there needs to be a state enum here. Idle, Researching, IsResearched... that strictly controls the state of the checkmark, the cancel button, and the cost display text
			UIHelpers.ShowHideGameObject(mythButtonEntity.mythCheckmark.gameObject, true);
			UIHelpers.ShowHideGameObject(mythButtonEntity.researchableUIComponent.costProgressText.gameObject, false);
		}
		else
		{
			UIHelpers.ShowHideGameObject(mythButtonEntity.mythCheckmark.gameObject, false);
			UIHelpers.ShowHideGameObject(mythButtonEntity.researchableUIComponent.costProgressText.gameObject, true);
		}
	}

	// TODO: Make a Set...ButtonUIValues for the building tier upgrade thing. Consdier then settings its ditryUI to false as well.
	public static void SetBuildingTierUpgradeButtonUIValues(BuildingTierUpgradeButtonEntity buildingTierUpgradeButtonEntity)
	{
		BuildingAssetsDataScriptableObject buildingAssetsDataScriptableObject = buildingTierUpgradeButtonEntity.buildingAssetsDataScriptableObject;
		Color textColor = buildingAssetsDataScriptableObject.borderColor;

		// NOTE: This is different than the myth entity
		// buildingTierUpgradeButtonEntity.researchableUIComponent.entityIcon.sprite = buildingAssetsDataScriptableObject.buildingIconSprite;
		UIHelpers.SetSpriteAndBorderUIValues(buildingTierUpgradeButtonEntity.researchableUIComponent.background, buildingAssetsDataScriptableObject.mainColor, buildingAssetsDataScriptableObject.borderColor, buildingAssetsDataScriptableObject.buildingIconSprite);

		UIHelpers.SetSpriteAndBorderUIValues(buildingTierUpgradeButtonEntity.researchableUIComponent.researchButtonSpriteAndBoarderEntity, buildingAssetsDataScriptableObject.mainColor, buildingAssetsDataScriptableObject.borderColor, null);

		buildingTierUpgradeButtonEntity.researchableUIComponent.descriptionText.text = buildingAssetsDataScriptableObject.buildingDescription; // TODO: Improve this
		buildingTierUpgradeButtonEntity.researchableUIComponent.descriptionText.color = textColor;

		buildingTierUpgradeButtonEntity.researchableUIComponent.costProgressText.text = string.Empty;
		buildingTierUpgradeButtonEntity.researchableUIComponent.costProgressText.color = textColor;

		buildingTierUpgradeButtonEntity.researchableUIComponent.researchButtonText.text = ValueHolder.TIER_UPGRADE_BUTTON_TEXT; // NOTE: Different than myth, which has name on the button. // TODO: Consolidate
		buildingTierUpgradeButtonEntity.researchableUIComponent.researchButtonText.color = textColor;

		buildingTierUpgradeButtonEntity.buildingNameText.text = buildingAssetsDataScriptableObject.buildingName; // NOTE: Different than myth, which has name on the button. // TODO: Consolidate
		buildingTierUpgradeButtonEntity.buildingNameText.color = textColor;

		// TODO: Confused about this cost progress text seen here... was this just copy paste? Does a building have cost progress text?
		if (buildingTierUpgradeButtonEntity.researchableStateComponent.isResearched)
		{
			UIHelpers.ShowHideGameObject(buildingTierUpgradeButtonEntity.researchableUIComponent.costProgressText.gameObject, false);
		}
		else
		{
			UIHelpers.ShowHideGameObject(buildingTierUpgradeButtonEntity.researchableUIComponent.costProgressText.gameObject, true);
		}
	}

	public static void ShowHideGameObject(GameObject gameObjectToShowHide, bool shouldShow)
	{
		if (shouldShow && !gameObjectToShowHide.activeSelf)
			gameObjectToShowHide.SetActive(true);
		else if (!shouldShow && gameObjectToShowHide.activeSelf)
			gameObjectToShowHide.SetActive(false);
	}

	public static void EnableDisableButton(Button buttonToEnableDisable, bool shouldEnable)
	{
		if (shouldEnable && !buttonToEnableDisable.interactable)
			buttonToEnableDisable.interactable = true;
		else if (!shouldEnable && buttonToEnableDisable.interactable)
			buttonToEnableDisable.interactable = false;
	}

	public static void DestroyAllChildren(GameObject gameObject)
	{
		foreach (Transform item in gameObject.transform)
		{
			GameObject.Destroy(item.gameObject);
		}
	}
}
